# Multi-Agent Transport Simulation (MATSim)

Multi-Agent Transport Simulation (MATSim) simulates a fleet of vehicles commuting during peak hours of traffic with similar start and end points in the roadway. With a limited number of cars that can occupy an intersection or road at one time, congestion on the roadway is bound to occur causing cars to be idle during their route. 

This simulation has cars do a morning route where they travel to a destination near the center of the city grid, and an evening route that has them travel back to their start point on the outskirts.

The program outputs the amount of arbitrary time units taken for cars to complete their morning and evening routes.


## Configuring the Program Files in Your Directory

Running the benchmark programs relies on having the compile.sh and run.sh files point to the mass_cpp_core folder and requires symbolic links in your directory to three library files in mass_cpp_core/ubuntu.

1.	Create symbolic links to the following three files in the mass_cpp_core directory:
    *	/mass_cpp_core/ubuntu/libmass.so
    *	/mass_cpp_core/ubuntu/mprocess
    *	/mass_cpp_core/ubuntu/killMProcess.sh 
2.	Find compile.sh in the benchmark program’s directory. Edit line 2 of this file to point to the mass_cpp_core directory in relation to compile.sh’s location. For example, if the mass_cpp_core folder is located one directory back from compile.sh, the line will read “export MASS_DIR=../mass_cpp_core”.
3.	Find run.sh in the benchmark program’s directory. Edit line 1 of this file to point to the mass_cpp_core/ubuntu folder in relation to run.sh’s location. For example, if the mass_cpp_core folder is located one directory back from run.sh, the line will read “LD_LIBRARY_PATH=../mass_cpp_core/ubuntu”.
4.	Make sure machinefile.txt lists all the machines you want to establish a connection with during execution. The file provided on Bitbucket lists hermes02-hermes12 and cssmpi1h-cssmpi12h and assumes that hermes01 is the master node. The master node should always be excluded from this file.
5.	Compile the benchmark’s source files with  “./compile.sh” to ensure all executable files are up to date.


## Providing Inputs

This benchmark requires two input files to run the program: Car_Agents.txt and Node_Links.txt. 

Car_Agents.txt lists the cars’ start and endpoints in the roadway graph and provides the route the car will take to get from the start to endpoint by listing the road IDs. 

Node_Links.txt is the input file to generate the roadway graph, assigning IDs to intersections and roadways and indicating which intersections are connected.

To ensure these files are accurate, please use Nathan’s program to automatically generate these text files located on Bitbucket at: https://bitbucket.org/mass_application_developers/mass_cpp_appl/src/Nathan_Repast_HPC/Repast_MATSIM_Final/InputFileMATSIM/.


## Running the Program

Once the text files are generated, use the steps below to execute the program:

1.	Execute run.sh with “./run.sh”. 
2.	Enter the following arguments when prompted:
    *	Number of Nodes – the number of computing nodes (machines) you want to use to run your program.
    *	Number of Threads – the number of threads to use.
    *	XSIZE – the number of intersections along the x-axis in the roadway graph. This value must be consistent with the Node_Links.txt file.
    *	YSIZE – the number of intersections along the y-axis in the roadway graph. This value must be consistent with the Node_Links.txt file.
    *	NumAgents – The number of car agents to instantiate on the places. This value must be consistent with the Car_Agents.txt file.
    *	Port Number - the port number you want to use for communicating between remote computing nodes.
    *	Password - your UW password for the hermes machines. It is recommended that you establish an RSA key so that you can authenticate remote computing nodes without having to pass your password between remote computing nodes. 
3.	If the program terminates for any reason before MASS::finish is called, execute ./killMProcess.sh to clean up the processes on the other computing nodes before attempting to execute again.
4. If the program terminates before the shared memory segments are deleted, they will persist. Therefore, you must execute ./deleteShm.sh to delete all the shared memory segments on all computing nodes used in the last run.


# MATSim File Descriptions

The files in this repository (in alphabetical order) are...

* Car_Agents.txt - the text file listing each car agent's start and endpoint and the shortest path between them.

* Car.h, Car.cpp - Source files for the Car Agent in the simulation.

* compile.sh - Compiles your program into executables to run. Simply use "./compile.sh" to run. Note: Line 2 of this file must have the correct path to the mass_cpp_core library.

* deleteShm.sh - deletes any persistent ipc shared memory segments if the program terminated before File2ShmPlace::free was called. This file relies on machinefile.txt accurately listing all the computing nodes to be connected to.

* File2ShmPlace - Source files for the class that sets up shared memory segments on each computing node for agents and places to read in date from a file.

* Intersection.h, Intersection.cpp - Source files for the Intersection Place in the simulation.

* killMProcess.sh - kills any running processes if the program terminates before MASS::finish() was called to delete running processes on other computing nodes. This file must be a symbolic link to "mass_cpp_core/ubuntu/killMProcess.sh".

* libmass.so - A symbolic link to "mass_cpp_core/ubuntu/libmass.so"

* machinefile.txt - Lists the names of all computers we want to establish a connection with to use as a remote computing node. Note that the master node should be excluded from this file.

* main.cpp - The main driver of the simulation.

* mprocess - A symbolic link to "mass_cpp_core/ubuntu/mprocess".

* Node_Links.txt - a text file providing all information for the roadway. Lists all intersections IDs and coordinates in the roadway as well as the IDs of the Roads linking them together.

* Road.h, Road.cpp - Source files for the Road class.

* run.sh - Runs the benchmark program once all the source files have been compiled. Note that line 1 of this file must have the correct path to the mass_cpp_core library files.

* testrun.sh - runs the program according to the default input files in the directory.

* Timer.h, Timer.cpp - Keeps track of the program's runtime. Useful for evaluation.