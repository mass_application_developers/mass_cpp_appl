#include "Car.h"
#include <fstream>

extern "C" Agent* instantiate(void* argument) {
  return new Car(argument);
}

extern "C" void destroy(Agent* carObj) {
  delete carObj;
}


//----------------------------------- init -------------------------------------
/** Initializes the Car agent's member data and reads in the car's route from
  *  a shared memory segment given by "shmAddr".
  *  @param shmAddr - a pointer to the shared memory containing the route file
  */
void* Car::init(void* shmAddr) {
  carID = agentId; 
  distToGo = 0;
  onLane = finishedRoute = false;
  movingDirection = '?';
  timeOfDay = 'M';
  void* myshm = (void*)( ((void**)shmAddr)[MASS_base::myPid] );
  readRoutes((char*)myshm);
  move(start);  // Go to start
}


//-------------------------------- startRoute --------------------------------
/** Prepares the Car to begin either the Moring (M) or evening (V) route.
  *  @param routeType - Either 'M' to indicate morning, or 'V' to indicate
  *         evening.
  */
void Car::readRoutes(char* routes) {
  int fsize = (int)strlen(routes);
  for (int fpos = 0; fpos < fsize; fpos++) {
    string line = "";
    while (fpos < fsize && routes[fpos] != '\n') {
      line += routes[fpos++];
    }
    // Parse line
    if (line.length() > 0 && line != " ") {
      if (line[line.length() - 1] == '\r') {
        line.erase(line.length() - 1);
      }
      if (line != "Agents") {
        int delimPos = 0;
        string argVal = "";
        vector<int> routeArgs;
        while((delimPos = line.find(": ")) != string::npos) {
          int pos = delimPos + 2;
          string argName = line.substr(0,delimPos);
          while (pos < line.length()) {
            while (pos < line.length() && line[pos] != ' ') {
              argVal += line[pos++];
            }
            routeArgs.push_back(stoi(argVal));
            argVal = "";
            if (pos < line.length()) pos++;
            if (argName != "Route") {
              break;
            }
          }
          line.erase(0,pos);
        }
        if (carID == routeArgs[0]) {
          start = routeArgs[1];
          end = routeArgs[2];
          for (int i = 3; i < routeArgs.size(); i++) {
            route.push_back(routeArgs[i]);
          }
          routePos = route.size() - 1;
          fpos = fsize; // Stop reading file
        }
      }
    }
  }
}


//---------------------------------- move -------------------------------------
/** Performs all tasks necessary for the car to migrate to a new intersection.
  *  @param destId - the ID of the intersection to move to.
  */
void Car::move(int destId) {
  prepForMigration();
  int x = 0, y = 0;
  placeIdToCoords(destId, x, y);
  vector<int> dest;
  dest.push_back(x); 
  dest.push_back(y); 
  migrate(dest);
}


//------------------------------ prepForMigration ----------------------------
/** Prepares for a migration by backing up the car's member data.
  */
void Car::prepForMigration() {
  int arrSize = 5 + route.size();
  int* memberData = new int[arrSize];
  memberData[0] = carID;
  memberData[1] = routePos;
  memberData[2] = start;
  memberData[3] = end;
  memberData[4] = timeOfDay;
  for (int memIndex = 5, routeIndex = 0; routeIndex < route.size(); memIndex++, routeIndex++) {
    memberData[memIndex] = route[routeIndex];
  }
  migratableData = (void*)memberData;
  migratableDataSize = sizeof(int) * arrSize;
  route.clear();
}


//------------------------------- placeIdToCoords ----------------------------
/** Determines the coordinates of the given Intersection. Does not return 
  *  anything, but changes the values of x and y to the coordinates.
  */
void Car::placeIdToCoords(int placeId, int& x, int& y) {
  x = placeId % place->size[0];
  y = (placeId - x) / place->size[0];
}


//------------------------------- coordsToPlaceId ----------------------------
/** Converts the given coordinates to an Intersection ID.
  */
int Car::coordsToPlaceId(int x, int y) {
  return ( (y * place->size[0]) + x);
}


//-------------------------------- startRoute --------------------------------
/** Prepares the Car to begin either the Moring (M) or evening (V) route.
  *  @param routeType - Either 'M' to indicate morning, or 'V' to indicate
  *         evening.
  */
void* Car::startRoute(void* routeType) {
  timeOfDay = *(char*)routeType;
  finishedRoute = false;
  if (timeOfDay == 'M') {
    restoreMemberData(); // Just migrated to start
    routePos = route.size() - 1;
  }
  else if (timeOfDay == 'V') {
    routePos = 0;
  }
  place->callMethod(Intersection::addCar_, nullptr); //Add car to simulation
}


//----------------------------- settleOnNewPlace -----------------------------
/** Restores the car's member data after migration and checks if the car has
  *  completed its route. If not, it advances routePos to the next position.
  */
void* Car::settleOnNewPlace() {
  if (migratableData != nullptr) {
    restoreMemberData();
    /*Morning Route Settling*/
    if (timeOfDay == 'M')  {
      if ( end == coordsToPlaceId(index[0], index[1]) ) {
        finishedRoute = true;
        place->callMethod(Intersection::removeCar_, nullptr); //Remove car from simulation (finished)
      } 
      else {
        routePos--; // Continue morning route
      }
    }
    /*Evening Route Settling*/ 
    else if (timeOfDay == 'V') {
      if ( start == coordsToPlaceId(index[0], index[1]) ) {
        finishedRoute = true;
        place->callMethod(Intersection::removeCar_, nullptr); //Remove car from simulation (finished)
      } 
      else {
        routePos++; // Continue evening route
      }
    }
  }
  return (void*)&finishedRoute;
}


//----------------------------- restoreMemberData ----------------------------
/** Restores the car's member data after a migration.
  */
void Car::restoreMemberData() {
  carID = ((int*)migratableData)[0];
  routePos = ((int*)migratableData)[1];
  start = ((int*)migratableData)[2];
  end = ((int*)migratableData)[3];
  timeOfDay = ((int*)migratableData)[4];
  route.resize( (migratableDataSize / sizeof(int)) - 5 ); 
  for (int memIndex = 5, routeIndex = 0; memIndex < migratableDataSize / sizeof(int); memIndex++, routeIndex++) {
    route[routeIndex] = ((int*)migratableData)[memIndex];
  }
  distToGo = 0;
  onLane = finishedRoute = false;
  movingDirection = '?';
  migratableData = nullptr; // All data resotred
}


//-------------------------------- moveOnLane --------------------------------
/** Advances the car forward on their lane or moves them onto a new lane if
  *  there is space.
  */
void* Car::moveOnLane() {
  if (!finishedRoute) {
    if (onLane) {
      if (distToGo > 0) {
        distToGo--; // Move forward on lane
      }
    }
    else {
      int* roadDist = (int*)place->callMethod( Intersection::putCarOnLane_, (void*)(&route[routePos]) );
      if (*roadDist != -1) {
        distToGo = *roadDist;
        onLane = true;
        movingDirection = *(char*)place->callMethod(Intersection::getRoadDirection_, &route[routePos]);
      }
      delete roadDist;
    }
  }
}


//---------------------------- moveOnIntersection ----------------------------
/** Moves a car from a lane onto another intersection if the car is at the end
  *  of its lane and there is room for it in the next intersection.
  */
void* Car::moveToIntersection(void* direction) {
  if (!finishedRoute) {
    if (onLane && distToGo == 0 && movingDirection == *(char*)direction) {
      int* destId = (int*)place->callMethod(Intersection::getDestId_, &route[routePos]);
      if (destId != nullptr) {
        bool* canMove = (bool*)place->callMethod(Intersection::depart_, &route[routePos]);
        if (*canMove) {
          move(*destId);
        }
        delete canMove; // Created "new" in intersection::depart
      }
    }
  }
}