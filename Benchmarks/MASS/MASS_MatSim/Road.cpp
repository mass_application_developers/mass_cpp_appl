#include "Road.h"
#include <algorithm>

Road::Road(int roadId, int length, int numLanes, int source, int destination) {
  this->roadID = roadId;
  this->length = length;
  this->numLanes = numLanes;
  this->source = source;
  this->destination = destination;
  capacity = std::max((int)(numLanes * length) / 15, 1);
  numCars = 0;
  if (destination == source + 1) direction = 'E';
  else if (destination == source - 1) direction = 'W';
  else if (destination > source + 1) direction = 'S';
  else if (destination < source - 1) direction = 'N';
}

//================================== Getters ==================================
int Road::getID() const { return roadID; }

int Road::getLength() const { return length; }

int Road::getNumLanes() const { return numLanes; }

int Road::getSource() const { return source; }

int Road::getDest() const { return destination; }

int Road::getCapacity() const { return capacity; }

int Road::getVacancy() const { return capacity - numCars; }

char Road::getDirection() const { return direction; }

//================================= Helpers =================================
bool Road::addCar() {
  bool addedCar = (getVacancy() > 0);
  if (addedCar) {
    numCars++;
  }
  return addedCar;
}

void Road::removeCar() {
  if (numCars > 0) {
    numCars--;
  }
}