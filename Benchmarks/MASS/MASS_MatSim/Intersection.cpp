#include "Intersection.h"

extern "C" Place* instantiate(void* argument) {
  return new Intersection(argument);
}

extern "C" void destroy(Place* intersectionObj) {
  delete intersectionObj;
}

//---------------------------------- init ------------------------------------
/** Initializes the Intersection's member data and reads in the roadway from 
  *  a shared memory segment given by "shmAddr".
  *  @param shmAddr - a pointer to the shared memory containing the road file
  */
void* Intersection::init(void* shmAddr) {
  intersectionID = index[1] * size[0] + index[0]; 
  numCars = numOutgoingCars = 0;
  void* myshm = (void*)( ((void**)shmAddr)[MASS_base::myPid] );
  readRoadway((char*)myshm);
  capacity = outgoingRoads.size() * (outgoingRoads.begin())->second->getNumLanes(); 
}


//------------------------------- readRoadway --------------------------------
/** Reads the roadway in from the file stored in the shared memory.
  *  @param roadfile - A pointer to the shared memory segment containing the
  *         road file data.
  */
void Intersection::readRoadway(char* roadFile) {
  int fsize = (int)strlen(roadFile);
  bool onLinks;
  for (int fpos = 0; fpos < fsize; fpos++) /*Loop File*/ {
    string line = "";
    while (fpos < fsize && roadFile[fpos] != '\n') /*Get Line*/ {
      line += roadFile[fpos++];
    }
    // Parse Line
    if (line.length() > 0 && line != " ") {
      if (line[line.length() - 1] == '\r') {
        line.erase(line.length() - 1);
      }
      if (line == "Links") {
        onLinks = true;
      }
      else if (onLinks) {
        int delimPos = 0;
        string argVal = "";
        int linkArgs[3];
        int curArg = 0;
        while ((delimPos = line.find(": ")) != string::npos) {
          int pos = delimPos + 2;
          while (pos < line.length() && line[pos] != ' ') {
            argVal += line[pos++];
          }
          linkArgs[curArg++] = stoi(argVal);
          argVal = "";
          if (pos < line.length()) pos++;
          line.erase(0,pos);
        }
        // If intersection ID appears in either source or dest, add a road.
        Road* rd = nullptr;
        if (intersectionID == linkArgs[1]) {
          rd = new Road(linkArgs[0], /*length*/10, /*numlanes*/1, linkArgs[1], linkArgs[2]);
        }
        else if (intersectionID == linkArgs[2]) {
          rd = new Road(linkArgs[0], /*length*/10, /*numlanes*/1, linkArgs[2], linkArgs[1]);
        }
        if (rd != nullptr) {
          outgoingRoads[rd->getID()] = rd;
        }
      }
    }
  }
}


//--------------------------------- addCar -----------------------------------
/** Adds a car onto the intersection, increasing the number of cars here.
  */
void* Intersection::addCar() {
  mtx.lock();
  numCars++;
  mtx.unlock();
}


//-------------------------------- removeCar ---------------------------------
/** Removes a car from the intersection, decreasing the number of cars here.
  */
void* Intersection::removeCar() {
  mtx.lock();
  if (numCars > 0) {
    numCars--;
  }
  mtx.unlock();
}


//------------------------------- putCarOnlane -------------------------------
/** Moves a car from the intersection onto one of the outgoing roads to a
  *  different intersection. 
  *  @param roadId - the ID of the road to be moved to.
  *  @return - returns the length of the lane.
  */
void* Intersection::putCarOnLane(void* roadId) {
  int* roadLength = new int(-1);
  mtx.lock();
  auto rd = outgoingRoads.find(*(int*)roadId);
  if (rd != outgoingRoads.end() && rd->second->addCar()) {
    *roadLength = rd->second->getLength();
    numCars--;
  } 
  mtx.unlock();
  return (void*)roadLength;
}


//----------------------------- getRoadDirection -----------------------------
/** Gets the cardinal direction of a road.
  *  @param roadId - the ID of the road to be moved to.
  *  @return - returns the cardinal direction of the road (N,S,E,W) or nullptr
  *          if not found.
  */
void* Intersection::getRoadDirection(void* roadId) {
  char* dir;
  auto rd = outgoingRoads.find(*(int*)roadId);
  dir = (rd != outgoingRoads.end()) ? new char(rd->second->getDirection()) : nullptr;
  return (void*)dir;
}



//--------------------------------- getDestId --------------------------------
/** Gets the ID of the intersection at the end of the outgoing road.
  *  @param roadId - the ID of the road to be moved to.
  *  @return - returns the ID of the intersection or nullptr if not found.
  */
void* Intersection::getDestId(void* roadId) {
  int* destId;
  auto rd = outgoingRoads.find(*(int*)roadId);
  destId = (rd != outgoingRoads.end()) ? new int(rd->second->getDest()) : nullptr;
  return (void*)destId;
}



//---------------------------- prepVacancyExchange ---------------------------
/** Prepares for an ExchangeAll call to report the number of cars that can 
  *  migrate here.
  *  @param direction - the cardinal direction of motion.
  */
void* Intersection::prepVacancyExchange(void* direction) {
  numOutgoingCars = 0; // Reset number of outgoing cars
  cleanNeighbors();   // Remove old neighbors
  cleanInMessages();
  for (auto it = outgoingRoads.begin(); it != outgoingRoads.end(); it++) {
    if (it->second->getDirection() == *(char*)direction) {
      int destId = it->second->getDest();
      int x = destId % size[0];
      int y = (destId - x) / size[0];
      int dest[2] = {x - index[0], y - index[1]};
      addNeighbor(dest, 2); // Add neighbor 
      outMessage = (void*)&intersectionID;
      outMessage_size = sizeof(int);
      inMessage_size = sizeof(int);
      break;
    }
  }
}


//----------------------------- exchangeVacancies ----------------------------
/** Returns the number of cars that can migrate here in an exchangeAll.
  *  @return - the number of cars that can migrate here.
  */
void* Intersection::exchangeVacancies() {
  int* vacancy = new int( (capacity - numCars) + numOutgoingCars);
  return vacancy;
}



//--------------------------------- depart -----------------------------------
/** Called by a car agent wishing to migrate to the intersection at the end of
  *  the specified road. 
  *  @param roadId - the roadID with the desired intersection at the end.
  *  @return - returns true if the car can move and false if moving would 
  *          exceed the capacity of the intersection.
  */
void* Intersection::depart(void* roadId) {
  mtx.lock();
  bool* canMove = new bool(false);
  if (*(int*)inMessages[0] /*numCarsAllowed*/ > 0) {
    numOutgoingCars++;
    auto rd = outgoingRoads.find(*(int*)roadId);
    if (rd != outgoingRoads.end()) {
      rd->second->removeCar(); // Remove from road
    }
    (*(int*)inMessages[0]) -= 1;
    *canMove = true;
  }
  mtx.unlock();
  return canMove;
}


//------------------------------ prepCarExchange -----------------------------
/** Prepares for an ExchangeAll call to report the number of cars moving to 
  *  the other intersection.
  */
void* Intersection::prepCarExchange() {
  cleanInMessages();
  outMessage = (void*)&numOutgoingCars;
  outMessage_size = sizeof(int);
  inMessage_size = 0;
}


//--------------------------- exchangeNumMovingCars --------------------------
/** Gives the number of cars that will be moving to the local intersection so
  *  the local intersection can update its numCars value.
  *  @param numIncomingCars - the number of cars migrating onto the intersection.
  */
void* Intersection::exchangeNumMovingCars(void* numIncomingCars) {
  mtx.lock();
  numCars += *(int*)numIncomingCars;
  mtx.unlock();
  return nullptr;
}