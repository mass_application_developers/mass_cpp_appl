/*==============================================================================
  MATSIM - MASS C++ Benchmark Program
  Author: Ian Dudder
================================================================================
  Multi-Agent Transport Simulation (MATSim) simulates a fleet of vehicles 
  commuting during peak hours of traffic with similar start and end points in 
  the roadway. With a limited number of cars that can occupy an intersection or 
  road at one time, congestion on the roadway is bound to occur causing cars to 
  be idle during their route. 

  This simulation has cars do a morning route where they travel to a destination
  near the center of the city grid, and an evening route that has them travel 
  back to their start point on the outskirts.

  Output: The program outputs the number of arbitrary time units it took for 
  all cars to finish their morning and evening route.
==============================================================================*/

#include <iostream>
#include "MASS.h"
#include "Intersection.h"
#include "File2ShmPlace.h"
#include "Car.h"
#include "Timer.h"
#include <stdlib.h>

Timer timer;            // Records runtime 
ostringstream logText;  // Used for MASS logs

//------------------------------ moveCars ------------------------------------
/** Performs all the necessary operations to move cars across intersections
 *  in the specified direction.
 *  @param intersections - a pointer to the intersection Places
 *  @param cars - a pointer to the Car agents
 *  @param direction - the cardinal direction of movement (N,S,E,W)
 *  @param handle - the handle for the intersections
 */
void moveCars(Places* intersections, Agents* cars, char direction, int handle);


//---------------------------- finishedRoute ---------------------------------
/** Sorts through the return values from cars to see if the cars have finished
 *  their routes.
 *  @param routeStatus - an array of bools stating whether a car has finished
 *         the route or not.
 *  @param numCars - the number of cars in the simulation.
 *  @return - true if all cars have finished and false otherwise.
 */
bool finishedRoute(bool* routeStatus, const int numCars);



int main (int argc, char* args[]) {
  if (argc != 12) {
    cerr << "usage: ./main $USER $PASSWORD .tempmachinefile.txt $PORT $NUMNODES" <<
     " $NUMTHREADS $NUMAGENTS $XSIZE $YSIZE Node_Links.txt Car_Agents.txt" << endl;
    return -1;
  }

  //---------------------- Get the arguments passed in -------------------------
	char *arguments[4];
	arguments[0] = args[1];               // Username
	arguments[1] = args[2];               // Password
	arguments[2] = args[3];               // machinefile
	arguments[3] = args[4];               // Port Number
	int nProc = atoi( args[5] );          // number of processes
	int nThr = atoi( args[6] );           // number of threads
	const int sizeX = atoi( args[7] );    // Number of columns in Place
	const int sizeY = atoi( args[8] );    // Number of rows in Place
  const int numCars = atoi( args[9] ); // Number of car Agents
  char* roadFile = args[10];            // Name of Edge text file
  char* routeFile = args[11];           // Name of Car text file
  char* dummyArg = " ";
  const int intersectionHandle = 1, carHandle = 2, initPlHandle = 3, initAgHandle = 4;

	MASS::init( arguments, nProc, nThr ); 
  timer.start(); // Evaluation begins now

  //-------------------- Initialize Place Shared memory ------------------------
  char fileArgs[MASS_base::systemSize][100];  // Input argument for file names
  bzero(fileArgs, MASS_base::systemSize * 100);
  for (int i = 0; i < MASS_base::systemSize; i++)
    strncpy(fileArgs[i], roadFile, (int)strlen(roadFile));

  Places* init_places = new Places(initPlHandle, "File2ShmPlace", 1, dummyArg, 1, 1, MASS_base::systemSize);
  void** shmAddresses = (void**)init_places->callAll(File2ShmPlace::read2Shm_,
    (void**)fileArgs, sizeof(char) * 100, sizeof(void*));
  
  //----------------------- Initialize Intersections ---------------------------
  Places* intersections = new Places(intersectionHandle, "Intersection", 1, 
    dummyArg, 1, 2, sizeX, sizeY);
  intersections->callAll(Intersection::init_, shmAddresses, sizeof(void*) * MASS_base::systemSize);
  init_places->callAll(File2ShmPlace::freeShm_, dummyArg, 1); // Free shm

  //-------------------- Initialize Agent Shared memory ------------------------
  bzero(fileArgs, MASS_base::systemSize * 100);
  for (int i = 0; i < MASS_base::systemSize; i++)
    strncpy(fileArgs[i], routeFile, (int)strlen(routeFile));

  Places* init_agents = new Places(initAgHandle, "File2ShmPlace", 1, dummyArg, 1, 1, MASS_base::systemSize);
  void** agShmAddr = (void**)init_agents->callAll(File2ShmPlace::read2Shm_, (void**)fileArgs,
    sizeof(char) * 100, sizeof(void*));

  //--------------------------- Initialize Cars --------------------------------
  Agents* cars = new Agents(carHandle, "Car", dummyArg, 1, intersections, numCars);
  cars->callAll(Car::init_, agShmAddr, sizeof(void*) * MASS_base::systemSize);
  init_agents->callAll(File2ShmPlace::freeShm_, dummyArg, 1); // Free Shm

  // Move Cars to Start
  cars->manageAll();
  char* timeOfDay = new char('M');
  cars->callAll(Car::startRoute_, timeOfDay, 1);

  //--------------------------- Simulation Loop --------------------------------
  int morningTime = 0;
  bool finished = false;
  for (; !finished; morningTime++) {
    cars->callAll(Car::moveOnLane_, dummyArg, 1);
    moveCars(intersections, cars, 'N', intersectionHandle);   /*North*/
    moveCars(intersections, cars, 'E', intersectionHandle);   /*East*/
    moveCars(intersections, cars, 'S', intersectionHandle);   /*South*/
    moveCars(intersections, cars, 'W', intersectionHandle);   /*West*/
    cars->manageAll();
    bool* routeStatus = (bool*)cars->callAll(Car::settleOnNewPlace_, nullptr, 0, sizeof(bool));
    finished = finishedRoute(routeStatus, numCars);
    delete [] routeStatus;
  }
  
  int eveningTime = 0;
  finished = false;
  *timeOfDay = 'V';
  cars->callAll(Car::startRoute_, timeOfDay, 1);
  for (; !finished; eveningTime++) {
    cars->callAll(Car::moveOnLane_, dummyArg, 1);
    moveCars(intersections, cars, 'N', intersectionHandle);   /*North*/
    moveCars(intersections, cars, 'E', intersectionHandle);   /*East*/
    moveCars(intersections, cars, 'S', intersectionHandle);   /*South*/
    moveCars(intersections, cars, 'W', intersectionHandle);   /*West*/
    cars->manageAll();
    bool* routeStatus = (bool*)cars->callAll(Car::settleOnNewPlace_, nullptr, 0, sizeof(bool));
    finished = finishedRoute(routeStatus, numCars);
    delete [] routeStatus;
  }
  
  logText.str("");
  logText << endl << endl << "All cars finished the morning route after " << morningTime << " units of time" << endl;
  logText << "All cars finished the evening route after " << eveningTime << " units of time" << endl << endl;

  long elaspedTime_END =  timer.lap();
  logText << endl << "End of simulation. Elapsed time using MASS framework with " 
    << nProc << " processes and " << nThr << " threads is: " << elaspedTime_END << endl << endl;
  MASS_base::log(logText.str());

	MASS::finish( );
  return 0;
}


void moveCars(Places* intersections, Agents* cars, char direction, int handle) {
  char* arg = &direction;
  intersections->callAll(Intersection::prepVacancyExchange_, arg, 1); 
  intersections->exchangeAll(handle, Intersection::exchangeVacancies_);
  cars->callAll(Car::moveToIntersection_, arg, 1);
  intersections->callAll(Intersection::prepCarExchange_, arg, 1);
  intersections->exchangeAll(handle, Intersection::exchangeNumMovingCars_);
}


bool finishedRoute(bool* routeStatus, const int numCars) {
  for (int car = 0; car < numCars; car++) {
    if ( !routeStatus[car] ) {
      return false; // If any car is not finished, return false
    }
  }
  return true; // If all cars are finished, return true
}