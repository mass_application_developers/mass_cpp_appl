# MASS C++ Software License
# © 2014-2015 University of Washington
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# The following acknowledgment shall be used where appropriate in publications, presentations, etc.:
# © 2014-2015 University of Washington. MASS was developed by Computing and Software Systems at University of Washington Bothell.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

#/bin/bash
######################################################################
#
#       Delete Unix SHM for User
#       ---------------------------
#       A small script to delete the persisting Unix shared memory
# segments on each of the machines listed in the machine.txt file.
#
#       By:     Ian Dudder
#       Date:   12/18/2021
#
######################################################################
filename="./machinefile.txt"

# Delete local node's shm
echo -e "Local Node"
echo -e   "============="
ipcs -m | grep $USER | awk ' { print $2 } ' | xargs -I {} ipcrm -m {}
echo -e " -- DONE! \n"

for host in `cat ${filename}`
do
	echo -e "\n${host}"
	echo -e   "============="
  mems=`ssh ${USER}@${host} ipcs -m | grep $USER`
  echo -e ">>> $mems"

  for shm in `echo $mems | awk ' { print $2 } '`
  do 
    echo -e "> Freeing ${shm}"
    ssh ${USER}@${host} "ipcrm -m ${shm}"
    echo -e " -- DONE! \n"
  done
done

echo -e "\n"
