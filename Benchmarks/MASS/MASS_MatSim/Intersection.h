/*==============================================================================
  Intersection Class
  Author: Ian Dudder
================================================================================
  The Intersection Class is the Place class in the MATSim simulation. It manages
  all outgoing roads to other intersections and keeps track of how many cars 
  reside on it at one time, ensuring its capacity is not exceeded.
==============================================================================*/

#ifndef INTERSECTION_H
#define INTERSECTION_H

#include <iostream>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "MASS_base.h"
#include "Place.h"
#include "Road.h"
#include <unordered_map>
#include <mutex>
#include <fstream>

class Intersection : public Place {
public:
	//=============================== Function Ids ===============================
  static const int init_ = 0;
  static const int addCar_ = 1;
  static const int removeCar_ = 2;
  static const int putCarOnLane_ = 3;
  static const int getRoadDirection_ = 4;
  static const int getDestId_ = 5;
  static const int prepVacancyExchange_ = 6;
  static const int exchangeVacancies_ = 7;
  static const int depart_ = 8;
  static const int prepCarExchange_ = 9;
  static const int exchangeNumMovingCars_ = 10;
  
	//============================== Public Methods ==============================
  Intersection(void* argument) : Place(argument) {} 
  
	//------------------------------- Call Method --------------------------------
	virtual void* callMethod(int functionId, void* argument) {
    switch(functionId) {
      case init_: 
        return init(argument);

      case addCar_ :
        return addCar();

      case removeCar_ :
        return removeCar();

      case putCarOnLane_ :
        return putCarOnLane(argument);

      case getRoadDirection_ :
        return getRoadDirection(argument);

      case getDestId_ :
        return getDestId(argument);

      case prepVacancyExchange_ :
        return prepVacancyExchange(argument);

      case exchangeVacancies_ :
        return exchangeVacancies();

      case depart_ :
        return depart(argument);

      case prepCarExchange_ :
        return prepCarExchange();

      case exchangeNumMovingCars_ :
        return exchangeNumMovingCars(argument);
    }
    return nullptr;
  }

  //---------------------------------- init ------------------------------------
  /** Initializes the Intersection's member data and reads in the roadway from 
   *  a shared memory segment given by "shmAddr".
   *  @param shmAddr - a pointer to the shared memory containing the road file
   */
  void* init(void* shmAddr); 

  
  //--------------------------------- addCar -----------------------------------
  /** Adds a car onto the intersection, increasing the number of cars here.
   */
  void* addCar();


  //-------------------------------- removeCar ---------------------------------
  /** Removes a car from the intersection, decreasing the number of cars here.
   */
  void* removeCar();


  //------------------------------- putCarOnlane -------------------------------
  /** Moves a car from the intersection onto one of the outgoing roads to a
   *  different intersection. 
   *  @param roadId - the ID of the road to be moved to.
   *  @return - returns the length of the lane.
   */
  void* putCarOnLane(void* roadId);

  
  //----------------------------- getRoadDirection -----------------------------
  /** Gets the cardinal direction of a road.
   *  @param roadId - the ID of the road to be moved to.
   *  @return - returns the cardinal direction of the road (N,S,E,W) or nullptr
   *          if not found.
   */
  void* getRoadDirection(void* roadId);


  //--------------------------------- getDestId --------------------------------
  /** Gets the ID of the intersection at the end of the outgoing road.
   *  @param roadId - the ID of the road to be moved to.
   *  @return - returns the ID of the intersection or nullptr if not found.
   */
  void* getDestId(void* roadId);


  //---------------------------- prepVacancyExchange ---------------------------
  /** Prepares for an ExchangeAll call to report the number of cars that can 
   *  migrate here.
   *  @param direction - the cardinal direction of motion.
   */
  void* prepVacancyExchange(void* direction);


  //----------------------------- exchangeVacancies ----------------------------
  /** Returns the number of cars that can migrate here in an exchangeAll.
   *  @return - the number of cars that can migrate here.
   */
  void* exchangeVacancies();


  //--------------------------------- depart -----------------------------------
  /** Called by a car agent wishing to migrate to the intersection at the end of
   *  the specified road. 
   *  @param roadId - the roadID with the desired intersection at the end.
   *  @return - returns true if the car can move and false if moving would 
   *          exceed the capacity of the intersection.
   */
  void* depart(void* roadId);


  //------------------------------ prepCarExchange -----------------------------
  /** Prepares for an ExchangeAll call to report the number of cars moving to 
   *  the other intersection.
   */
  void* prepCarExchange();


  //--------------------------- exchangeNumMovingCars --------------------------
  /** Gives the number of cars that will be moving to the local intersection so
   *  the local intersection can update its numCars value.
   *  @param numIncomingCars - the number of cars migrating onto the intersection.
   */
  void* exchangeNumMovingCars(void* numIncomingCars);

private:
  //============================= Private Methods ==============================

  //------------------------------- readRoadway --------------------------------
  /** Reads the roadway in from the file stored in the shared memory.
   *  @param roadfile - A pointer to the shared memory segment containing the
   *         road file data.
   */
  void readRoadway(char* roadFile);

	//================================ Member Data ===============================
	int intersectionID;						  // Vertex number in graph
	int numCars;								    // Number of cars in the intersection
	int capacity;									  // Max number of cars that can reside here   
  int numOutgoingCars;            // Number of cars moving out of this intersection
  unordered_map<int, Road*> outgoingRoads; // Roads to other intersections
  ostringstream logText;          // Used to make MASS logs
  mutex mtx;
};

#endif //INTERSECTION_H