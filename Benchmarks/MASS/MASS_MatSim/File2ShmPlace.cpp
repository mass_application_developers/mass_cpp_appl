#include "File2ShmPlace.h"

extern "C" Place* instantiate(void* argument) {
  return new File2ShmPlace(argument);
}

extern "C" void destroy(Place* f2shmObj) {
  delete f2shmObj;
}


//---------------------------------- read2Shm ---------------------------------
/** Creates a shared memory (shm) space, reads a given input file, stores the
*  file data into this shm space, and returns the address of shm back to the 
*  main program.
*  @param filename - a char* containing the name of the file.
*/
void* File2ShmPlace::read2Shm( void* filename ) {
	int fd = ::open( (char*)filename, O_RDONLY );
	size = lseek( fd, 0, SEEK_END );
	shmid = shmget( key, size, IPC_CREAT | IPC_EXCL | 0666 );
	if (shmid < 0) {
		if ( (shmctl(shmid, IPC_RMID, NULL)) == -1) {
			return nullptr;
		}
		else {
			shmid = shmget( key, size, IPC_CREAT | IPC_EXCL | 0666 );
			if (shmid < 0) {
				return nullptr;
			}
		}
	}
	shm = shmat( shmid, NULL, 0);
	lseek( fd, 0, SEEK_SET );
	::read( fd, shm, size );
	return &shm;
}


//--------------------------------- freeShm ---------------------------------
/** Detaches the shared memory from the current memory space and deletes it.          
 */
void* File2ShmPlace::freeShm() {
  ostringstream logText;
  logText.str("");
	if ( (shmdt(shm)) == -1 ) {
		logText << "Failed to detach shared memory with ID " << shmid << " and key " << key;   
	} 
	else {
		logText << "Successfully detached shared memory with ID " << shmid << " and key " << key;  
	}
  MASS_base::log(logText.str());
  logText.str("");
	if ( (shmctl(shmid, IPC_RMID, NULL)) == -1 ) {
		logText << "Failed to delete shared memory with ID " << shmid << " and key " << key;  
	}
	else {
		logText << "Successfully deleted shared memory with ID " << shmid << " and key " << key; 
	}
  MASS_base::log(logText.str());
}