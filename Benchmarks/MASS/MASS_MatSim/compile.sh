#!/bin/sh
export MASS_DIR=/home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_core/

rm Intersection
rm Car
rm main
rm File2ShmPlace

g++ -Wall File2ShmPlace.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o File2ShmPlace
g++ -Wall Intersection.cpp Road.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o Intersection
g++ -Wall Car.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o Car
g++ -std=c++11 -Wall main.cpp Timer.cpp -I$MASS_DIR/source -L$MASS_DIR/ubuntu -lmass -I$MASS_DIR/ubuntu/ssh2/include -L$MASS_DIR/ubuntu/ssh2/lib -lssh2 -o main

echo "Compiled Files: "
ls Intersection Car main File2ShmPlace