/*==============================================================================
  Car Class
  Author: Ian Dudder
================================================================================
  The Car class is the Agent in the MATSim simulation, simulating a vehicle 
  commuting during rush-hour traffic. Cars traverse the roadway to complete a
  morning and evening route.
==============================================================================*/

#ifndef CAR_H
#define CAR_H

#include <iostream>
#include "Agent.h"
#include "MASS_base.h"
#include "Intersection.h"
#include <vector>

class Car : public Agent {
public:
  //=============================== Function Ids ===============================
  static const int init_ = 0;       // Initializes Car objects with their routes
  static const int startRoute_ = 1; // Starts the morning or evening route
  static const int moveOnLane_ = 2;
  static const int moveToIntersection_ = 3;
  static const int settleOnNewPlace_ = 4; 

  //============================== Public Methods ==============================
  Car(void* argument) : Agent(argument) {}

  //------------------------------- Call Method --------------------------------
  virtual void* callMethod(int functionId, void* argument) {
    switch(functionId) {
      case init_ : 
        return init(argument);

      case startRoute_ :
        return startRoute(argument);

      case settleOnNewPlace_ :
        return settleOnNewPlace();

      case moveOnLane_ :
        return moveOnLane();

      case moveToIntersection_ :
        return moveToIntersection(argument);
    }
    return nullptr;
  }

private:
  //=========================== CallMethod Functions ===========================

  //---------------------------------- init ------------------------------------
  /** Initializes the Car agent's member data and reads in the car's route from
   *  a shared memory segment given by "shmAddr".
   *  @param shmAddr - a pointer to the shared memory containing the route file
   */
  void* init(void* shmAddr);

  //-------------------------------- startRoute --------------------------------
  /** Prepares the Car to begin either the Moring (M) or evening (V) route.
   *  @param routeType - Either 'M' to indicate morning, or 'V' to indicate
   *         evening.
   */
  void* startRoute(void* routeType);

  //-------------------------------- moveOnLane --------------------------------
  /** Advances the car forward on their lane or moves them onto a new lane if
   *  there is space.
   */
  void* moveOnLane();

  //----------------------------- settleOnNewPlace -----------------------------
  /** Restores the car's member data after migration and checks if the car has
   *  completed its route. If not, it advances routePos to the next position.
   */
  void* settleOnNewPlace();

  //---------------------------- moveOnIntersection ----------------------------
  /** Moves a car from a lane onto another intersection if the car is at the end
   *  of its lane and there is room for it in the next intersection.
   */
  void* moveToIntersection(void* direction);

  
  //========================= Private Helper Methods ===========================

  //------------------------------- readRoutes ---------------------------------
  /** Reads the car's route from the file stored in the shared memory.
   *  @param routefile - A pointer to the shared memory segment containing the
   *         route file data.
   */
  void readRoutes(char* routefile);


  //---------------------------------- move -------------------------------------
  /** Performs all tasks necessary for the car to migrate to a new intersection.
   *  @param destId - the ID of the intersection to move to.
   */
  void move(int destId);


  //------------------------------ prepForMigration ----------------------------
  /** Prepares for a migration by backing up the car's member data.
   */
  void prepForMigration();


  //------------------------------- placeIdToCoords ----------------------------
  /** Determines the coordinates of the given Intersection. Does not return 
   *  anything, but changes the values of x and y to the coordinates.
   */
  void placeIdToCoords(int placeId, int& x, int& y);


  //------------------------------- coordsToPlaceId ----------------------------
  /** Converts the given coordinates to an Intersection ID.
   */
  int coordsToPlaceId(int x, int y);


  //----------------------------- restoreMemberData ----------------------------
  /** Restores the car's member data after a migration.
   */
  void restoreMemberData();

  //================================ Member Data ===============================
  int carID; 											// Unique Identifier for a car
  int routePos;                   // Position in route vector
  int start;                      // Starting point for morning route
  int end;                        // Ending point for morning route
  int distToGo;										// Distance left to be travelled on road
  char movingDirection;           // Stores the direction the car is moving
  char timeOfDay;                 // 'M' for morning, 'V' for evening
  bool onLane;										// True when on a lane
  bool finishedRoute;             // Indicates if the car has finished its route
  vector<int> route;              // The route of roads to be traversed
  ostringstream logText;          // Used to make MASS logs
};

#endif //CAR_H