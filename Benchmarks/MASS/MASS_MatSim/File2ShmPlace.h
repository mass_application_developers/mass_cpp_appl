/*==============================================================================
  File2ShmPlace Class
================================================================================
  This class aids the initialization of MASS-derived classes on remote computing
  Nodes. For example, this class can be created with one instance per computing
  node to set up a shared memory segment to be accessed by different processes.
==============================================================================*/

#ifndef FILE2SHMPLACE_H
#define FILE2SHMPLACE_H

#include <sys/types.h>    // key_t, off_t, etc. 
#include <sys/ipc.h>      // Interprocess comm, semaphores, shared mem
#include <sys/shm.h>      // Shared memory
#include <sys/stat.h>     // For open() 
#include <fcntl.h>        // Flags for file open (O_Read/Write)
#include <stdio.h>
#include <unistd.h>       // For read()
#include <stdlib.h>
#include <iostream>
#include "MASS_base.h"
#include "Place.h"

class File2ShmPlace : public Place {
public:
	//=============================== Function Ids ===============================
  static const int read2Shm_ = 0;
  static const int freeShm_ = 1;

	//============================== Public Methods ==============================
  File2ShmPlace(void* argument) : Place(argument) {}

  //------------------------------- Call Method --------------------------------
	virtual void* callMethod(int functionId, void* argument) {
    switch(functionId) {
      case read2Shm_ :
        return read2Shm(argument);

      case freeShm_ :
        return freeShm();
    }
    return nullptr;
  }

private:
  //--------------------------------- read2Shm ---------------------------------
  /** Creates a shared memory (shm) space, reads a given input file, stores the
   *  file data into this shm space, and returns the address of shm back to the 
   *  main program.
   *  @param filename - a char* containing the name of the file.              
   */
  void* read2Shm(void* filename);


  //--------------------------------- freeShm ---------------------------------
  /** Detaches the shared memory from the current memory space and deletes it.          
   */
  void* freeShm();
  
	//================================ Member Data ===============================
  key_t key = 0x00001234;  // Key for shared memory space 
  int shmid;               // ID of shared memory segment 
  off_t size;              // Size (in bytes) of shared memory
  void* shm;               // Pointer to shared memory address space
};

#endif // FILE2SHMPLACE_H