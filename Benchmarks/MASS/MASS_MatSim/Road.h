#ifndef ROAD_H
#define ROAD_H

#include <iostream>

class Road {
public:
  //============================== Public Methods ==============================
  Road(int roadId, int length, int numLanes, int source, int destination);

  //================================= Getters =================================
  int getID() const;
  int getLength() const;
  int getNumLanes() const;
  int getSource() const;
  int getDest() const;
  int getCapacity() const;
  int getVacancy() const;
  char getDirection() const;

  //================================= Helpers =================================
  bool addCar();
  void removeCar();

private:
	//================================ Member Data ===============================
	int roadID;											// Unique identifier for the road
	int length;											// Length of the road in feet
	int numLanes;										// Number of lanes on the road
  int source;											// Vertex ID of source
	int destination;								// Vertex ID of destination
  int capacity;										// Maximum number of cars allowed on road
	int numCars; 	      						// Number of cars in the intersection
  char direction;                 // Stores cardinal direction of road (NSEW)
};

#endif // ROAD_H