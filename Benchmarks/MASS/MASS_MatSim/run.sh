LD_LIBRARY_PATH=/home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_core/ubuntu
export LD_LIBRARY_PATH

echo "Number of nodes? {1, 2, 4, 8, 12, 16, 24}: "
read NUMNODES
echo "Number of threads? {1, 2, 3, 4}: "
read NUMTHREADS
echo "X size? "
read XSIZE
echo "Y size? "
read YSIZE
echo "Number of Agents? "
read NUMAGENTS
echo "Port? "
read PORT
echo "Password? "
read -s PASSWORD

head -$(($NUMNODES-1)) machinefile.txt > .tempmachinefile.txt

./main $USER $PASSWORD .tempmachinefile.txt $PORT $NUMNODES $NUMTHREADS $XSIZE $YSIZE $NUMAGENTS Node_Links.txt Car_Agents.txt &> output.txt

rm .tempmachinefile.txt
