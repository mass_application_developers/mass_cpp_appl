/*==============================================================================
  Bank Class
  Author: Ian Dudder
================================================================================
  The Bank Class is an agent in the Bail-in/Bail-out simulation that interacts 
  with other classes in the following ways:
    - Provides loans to struggling Firm objects
    - Receives deposits from Workers
    - Fulfill loans from other Banks that are struggling

  Bank objects each keep track of their interest rates they offer on loans, 
  their liquidity (i.e., assets; cash), and the debt accumulated from 
  outstanding loans. Each Bank has a unique bank ID to identify them.
==============================================================================*/

#ifndef BANK_H
#define BANK_H

#include <iostream>
#include "Agent.h"
#include "MASS_base.h"
#include "FinancialMarket.h"

class Bank : public Agent {
public:
	//=============================== Function IDs ===============================
	static const int init_ = 0;                   // Initialize Member Data
	static const int act_ = 1;                    // Act in round
	static const int readRoundTransactions_ = 2;  // Read transactions 
	static const int isBankrupt_ = 3;             // Check for negative liquidity
	static const int randomizeRate_ = 4;          // Randomize interest rates
	static const int reportRoundValues_ = 5;      // Write bank info to place

	//=============================== Public Methods =============================
	Bank(void* argument) : Agent(argument) {}

	virtual void *callMethod(int functionId, void* argument) {
		switch(functionId) {
			case init_ :
				return init(argument);

			case act_ :
				return act();

			case readRoundTransactions_ :
				return readRoundTransactions();

			case isBankrupt_ :
				return isBankrupt();

			case randomizeRate_ :
				return randomizeRate();

			case reportRoundValues_ :
				return reportRoundValues();
		}
		return nullptr;
	}

private:
	//============================ Call Method Helpers ===========================

  //---------------------------------- init ------------------------------------
  /** Initializes the member data for the Bank object writes it to the place
   *  element.
   *  @param initVals - A pair<double,double> where the first item is the 
   *        initial interest rate, and the second item is the initial liquidity. 
   *--------------------------------------------------------------------------*/
  void* init(void* initVals);
  
  
  //----------------------------------- act ------------------------------------
  /** Runs the Bank's sequence of actions in the simulation loop in each round.                                                            
   *--------------------------------------------------------------------------*/
	void* act();
  

  //--------------------------- readRoundTransactions --------------------------
  /** Reads all payments made and loans given during this round from the 
   *  Financial Market Place.                                           
   *--------------------------------------------------------------------------*/
	void* readRoundTransactions(); 
  

  //-------------------------------- isBankrupt --------------------------------
  /** Checks if this Bank has gone bankrupt based on its liquidity. 
   *  @return - returns true if bankruptcy is detected
   *--------------------------------------------------------------------------*/     
	void* isBankrupt(); 

  
  //------------------------------- randomizeRate ------------------------------
  /** Randomizes the bank's interest rate for a new round.                 
   *--------------------------------------------------------------------------*/  
	void* randomizeRate();


  //----------------------------- reportRoundValues ----------------------------
  /** Writes the Bank's Id, interest rate, liquidity, and location in the place
   *  matrix to the Financial Market.                                 
   *--------------------------------------------------------------------------*/ 
	void* reportRoundValues();


  //---------------------------------- payLoans --------------------------------
  /** Iterates through the vector of loans and attempts to pay back half of the
   *  amount of each outstanding loan. Returns false if the loans could not be
   *  repaid.
   *--------------------------------------------------------------------------*/
	void payLoans();


  //------------------------------ accumulateInterest --------------------------
  /** Iterates through the vector of loans and compounds interest on each loan 
   *  based on the rate at which the loan was given at.
   *--------------------------------------------------------------------------*/
	void accumulateInterest();


  //----------------------------------- getLoan --------------------------------
  /** Attempts to get a loan from another bank. Returns true if the loan was 
   *  secured, and false if the loan was rejected. 
   *--------------------------------------------------------------------------*/
	bool getLoan(int k);
  

	//================================ Member Data ===============================
	double interestRate;            // Interest rate offered for loans.
	double liquidity;               // The Bank's assets/cash
	double totalDebt;               // Total debt from outstanding loans
	int bankId;                     // Unique bank identifier
	vector<Loan> debt;              // Outstanding loans
	ostringstream logText;          // Used to make MASS logs
};

#endif // BANK_H