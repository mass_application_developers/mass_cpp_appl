/*==============================================================================
  Financial Market Class
	Author: Ian Dudder
================================================================================
	The FinancialMarket Class fosters all communication between agents in the 
  Bail-in/Bail-out simulation. This class allows residing agents to read and 
  write to it as a shared memory to communicate with one another. This class 
  will also use message-passing to send messages to other place elements to 
  allow distant communication.
==============================================================================*/

#ifndef FINANCIALMARKET_H
#define FINANCIALMARKET_H

#include <iostream>
#include "Place.h"
#include "MASS_base.h"
#include <vector>
#include <map>
#include <unordered_map>
#include <set>

struct BankInfo {
	double interestRate;
	double liquidity;
	int bankId;
	int linearIndex;
};

struct BankSelectionArgs {
	double minAcceptableInterest;
	double amountNeeded;
	int k;
};

struct Loan {
	double amount;  
	double interestRate;
	int loaningBankId;
};

class FinancialMarket : public Place {
public:
	//============================= Function IDs =================================
	static const int init_ = 0;
  static const int addBank_ = 1;                  // Sets residingBank 
  static const int addWorker_ = 2;                // Add Workers wages
  static const int calculateWorkforceCost_ = 3;   // Help firms
  
  // Get most current information on bank's liquidity and interest
	static const int prepBankInfoExchange_ = 4;     
	static const int exchangeBankInfo_ = 5;         
	static const int updateBankRegistry_ = 6;       // Updates from inMessages
  static const int updateBankInfo_ = 7;           // Updates from 1 BankInfo

  // Get deposits and loans to the right banks
	static const int prepForIncomingTransactions_ = 8;
	static const int exchangeOutgoingTransactions_ = 9;
	static const int manageIncomingTransactions_ = 10;
	static const int getIncomingTransactionAmt_ = 11; 

  static const int resetRoundValues_ = 12;       // Preps for next round  
	static const int selectFromKBanks_ = 13;       // Helps take out loans
	static const int addBankTransaction_ = 14;     // Helps banks

	//============================== Public Methods ==============================
	FinancialMarket(void* arg) : Place(arg) { /**/}
  
	virtual void *callMethod(int functionId, void* argument) {
		switch(functionId) {
			case init_ :
				return init(argument);

			case addWorker_ :
				return addWorker(argument);

			case calculateWorkforceCost_ :
				return calculateWorkforceCost();
			
			case addBank_ :
				return addBank(argument);

			case updateBankInfo_ :
				return updateBankInfo(argument);

			case prepBankInfoExchange_ :
				return prepBankInfoExchange();

			case exchangeBankInfo_ :
				return exchangeBankInfo();

			case updateBankRegistry_ :
				return updateBankRegistry();

			case selectFromKBanks_ :
				return selectFromKBanks(argument);

			case addBankTransaction_ :
				return addBankTransaction(argument);

			case prepForIncomingTransactions_ :
				return prepForIncomingTransactions();

			case exchangeOutgoingTransactions_ :
				return exchangeOutgoingTransactions(argument);

			case manageIncomingTransactions_ :
				return manageIncomingTransactions();

			case getIncomingTransactionAmt_ :
				return getIncomingTransactionAmt();

      case resetRoundValues_ :
        return resetRoundValues();
		}
		return nullptr;
	}

private:
	//=========================== Call Method Helpers ============================

  //---------------------------------- init --------------------------------------
  /** Initializes default values.
  *  @param nAgents - a pair<int,int> where the first item is the number of Bank
  *         agents, and the second item is the number of Firm agents.
  *----------------------------------------------------------------------------*/
	void* init(void* nAgents);


  //---------------------------------- addBank -----------------------------------
  /** Stores information on the residing Bank agent.
  *  @param bankInfo - a BankInfo* storing the information of the residing bank.
  *----------------------------------------------------------------------------*/
	void* addBank(void* bankInfo); 


  //-------------------------------- addWorker -----------------------------------
  /** Inserts the wages of a residing worker into the localWorkerWages map for 
  *  firms to read and calculate their workforce cost.
  *  @param workerInfo - a pair<double,int> with the first item as the wages and
  *         the second item as the workerId.
  *----------------------------------------------------------------------------*/
	void* addWorker(void* workerInfo);


  //-------------------------- calculateWorkforceCost ----------------------------
  /** Calculates the sum of the wages of all residing workers.
  *  @return - a double* reporting the sum of all worker wages on this place.
  *----------------------------------------------------------------------------*/
	void* calculateWorkforceCost();


  //------------------------------ updateBankInfo --------------------------------
  /** Updates the residing Bank's information to reflect any changes to its values
  *  during the last round.
  *  @param updatedInfo - a BankInfo* storing the info of the residing bank.
  *----------------------------------------------------------------------------*/
	void* updateBankInfo(void* updatedInfo);


  //---------------------------- prepBankInfoExchange ----------------------------
  /** Prepares each Place to receive information on the Banks in the simulation.
  *  Must be called before exchangeBankInfo().
  *----------------------------------------------------------------------------*/
	void* prepBankInfoExchange();


  //------------------------------ exchangeBankInfo ------------------------------
  /** Returns a BankInfo object containing all information for the residing bank.
  *  prepBankInfoExchange() must be called before this function.
  *  @return - a BankInfo* containing the information of the residing bank.
  *----------------------------------------------------------------------------*/
	void* exchangeBankInfo();


  //--------------------------- updateBankRegistry -------------------------------
  /** Updates the local bank registry that stores information on all the Banks in
  *  the simulation. exchangeBankInfo() must be called before this function.
  *----------------------------------------------------------------------------*/
	void* updateBankRegistry();


  //------------------------------ selectFromKBanks ------------------------------
  /** Chooses from k banks the bank with the lowest interest rate that can afford
  *  to provide the loan/
  *  @param arg - a BankSelectionArgs* where k is the number of banks to choose
  *         from, minAcceptableInterest is the lowest interest rate that is 
  *         acceptable, and amountNeeded is the amount needed from the bank.
  *  @return - Returns a BankInfo* storing the information on the bank selected.
  *         if no bank fit the criteria, nullptr is returned.
  *----------------------------------------------------------------------------*/
	void* selectFromKBanks(void* arg);


  //-------------------------- addBankTransaction --------------------------------
  /** Adds the monetary value of a transaction for a specified bank. A loan will 
  *  be a negative value, decreasing the running total, and a deposit will be
  *  a positive value, increasing the running total.
  *  @param transactionInfo - a pair<int,double>* where the first item is the
  *         bankId and the second item is the transaction amount.
  *----------------------------------------------------------------------------*/
	void* addBankTransaction(void* transaction);


  //------------------------ prepForIncomingTransactions -------------------------
  /** Prepares all Financial Market elements with a residing Bank agent to send
  *  the id of the bank as an integer, and receive the total value of 
  *  transactions accumulated from all other Financial Market places during the
  *  round. This function must precede exchangeOutgoingTransactions.
  *----------------------------------------------------------------------------*/
	void* prepForIncomingTransactions();


  //------------------------ exchangeOutgoingTransactions ------------------------
  /** Receives a bankId and returns the total sum of all transactions accumulated
  *  for that bank during the round. Must be preceded by the function
  *  prepForIncomingTransactions().
  *  @param givenBankId - an int representing the bank's id.
  *  @return - a double representing the value of the transactions.
  *----------------------------------------------------------------------------*/
	void* exchangeOutgoingTransactions(void* givenBankId);


  //-------------------------- manageIncomingTransactions ------------------------
  /** Has any Financial Market with a residing Bank sort through its inMessages 
  *  and total the transactions received from other place elements. Must be
  *  preceded by exchangeOutgoingTransactions().
  *----------------------------------------------------------------------------*/
	void* manageIncomingTransactions();


  //------------------------- getIncomingTransactionAmt --------------------------
  /** Returns the sum of all incoming transactions totalled in the
  *  manageIncomingTransactions() function. 
  *  @return - a double storing the value of all incoming transactions
  *----------------------------------------------------------------------------*/
	void* getIncomingTransactionAmt();


  //---------------------------- resetRoundValues --------------------------------
  // Resets the round values to prepare for next round
  void* resetRoundValues();


	//=============================== Member Data ================================
	double incomingTransactions;    /* Sum of payments/loans for local bank */
	int numBanks;                   /* Number of banks in the simulation */
  int numFirms;                   /* Number of firms in the simulation */
	int residingBank;               /* ID of Bank Agent that resides here */
	unordered_map<int, double> outgoingTransactions;
	unordered_map<int, double> localWorkerWages;
	unordered_map<int, BankInfo*> bankRegistry;  /* Stores info on Banks */
	ostringstream logText;          /* Used to send messages to MASS log */
  mutex mtx;                      // Used to handle critical sections
};

#endif //FINANCIALMARKET_H