#include "Owner.h"

using namespace std;

Owner::Owner(int firmId_, double capital_) : firmId{firmId_}, capital{capital_}  { }

double Owner::getCapital() const { return capital; }

void Owner::setCapital(double newCapital) { capital = newCapital; }

// End Owner.cpp