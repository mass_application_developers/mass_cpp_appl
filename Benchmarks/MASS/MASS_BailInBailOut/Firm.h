/*==============================================================================
	Firm Class
	Author: Ian Dudder
================================================================================
	The Firm Class is an Agent in the Bail-in/Bail-out simulation. Each
  round, it calculates its costs and profits, pays its workers and owner, 
  pays off loans, and determines if it needs a loan if it goes into debt. 
  The loan may come from its owner, or a bank. When the Firm takes out a
  loan, it sends a negative value to the bank. If it makes a payment, it
  sends a positive value to the bank.
==============================================================================*/

#ifndef FIRM_H
#define FIRM_H

#include <iostream>
#include "Agent.h"
#include "Owner.h"
#include "MASS_base.h"
#include <map>
#include "FinancialMarket.h"


class Firm : public Agent {
public:
  //============================== Function IDs ================================
  static const int init_ = 0;                  
  static const int calculateWorkforceCost_ = 1;
  static const int act_ = 2;   
  
  //============================= Public Methods ===============================
  Firm(void* argument) : Agent(argument) {}

  virtual void *callMethod(int functionId, void* argument) {
    switch(functionId) {
      case init_ :
        return init(argument);

      case calculateWorkforceCost_ :
        return calculateWorkforceCost();

      case act_ :
        return act();
    }
    return nullptr;
  }

private:
  //============================ Call Method Helpers ===========================
  
  //---------------------------------- init --------------------------------------
  /** Initializes default values.
  *  @param initArgs - a pair<double,double>* where the first item is the 
  *         simulation's production costs, and the second value is the liquidity.
  *----------------------------------------------------------------------------*/
  void* init(void* initArgs);


  //---------------------------------- act ---------------------------------------
  /** Performs all round actions for the Firm.
  *----------------------------------------------------------------------------*/
  void* act();


  //---------------------------- calculateWorkforceCost---------------------------
  /** Computes the total sum of all worker wages to be paid out each round.
  *----------------------------------------------------------------------------*/
  void* calculateWorkforceCost();


  //------------------------------- accumulateInterest ---------------------------
  /** Accumulates interest for every outstanding loan.
  *----------------------------------------------------------------------------*/
  void accumulateInterest();


  //--------------------------- calculateProductionCosts -------------------------
  /** Calculates the round's production costs
   *----------------------------------------------------------------------------*/
  void calculateProductionCosts();


  //------------------------------ calculateProfits ------------------------------
  /** Calculates the round's traffic.
  *----------------------------------------------------------------------------*/
  void calculateProfits();


  //------------------------------ payWorkforce ----------------------------------
  /** Pays the workers their wages and pays the owner if profits are positive.
  *----------------------------------------------------------------------------*/
  void payWorkforce();


  //----------------------------- calculateLiquidity -----------------------------
  /** Computes the liquidity for the round.
  *----------------------------------------------------------------------------*/
  void calculateLiquidity();


  //-------------------------------- getOwnerLoan --------------------------------
  /** Asks the Firm's owner for a loan
  *  @return - true if the owner could provide one and false otherwise.
  *----------------------------------------------------------------------------*/
  bool getOwnerLoan();


  //------------------------------- requestBankLoan ------------------------------
  /** Approaches k random banks to ask for a loan at a low interest rate.
  *  @return - returns true if a loan was granted and false otherwise.
  *----------------------------------------------------------------------------*/
  bool requestBankLoan(int k);


  //---------------------------------- payBank -----------------------------------
  /** Attempts to pay back half the amount for all outstanding loans.
  *----------------------------------------------------------------------------*/
  void payBank();


  //----------------------------------- resetFirm --------------------------------
  /** Resets a bankrupt firm to default starting values to re-enter simulation.
  *----------------------------------------------------------------------------*/
  void resetFirm();
  
  
  //=============================== Member Data ================================
  pair<double,double> resetVals; // Stores default {cost, liq} for reset
  double productionCost;      // Current round's production costs
  double liquidity;           // Ongoing liquidity
  double workforceCost;       // Cost to pay workers
  double profit;              // Current round's profits
  int firmId;                 // Unique identified for firm
  Owner* owner;               // Pointer to owner object 
  vector<Loan> debt;          // Outstanding Loans
  ostringstream logText;      // Used to write MASS logs
};

#endif // FIRM_H