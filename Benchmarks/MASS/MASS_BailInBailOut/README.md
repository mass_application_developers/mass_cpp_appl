# Bail-In/Bail-Out

The Bail-In/Bail-Out Benchmark program models a financial market and simulates interactions between three types of financial entities: banks, firms, and households (owners and workers). The purpose of this program is to monitor how these entities interact with one another leading up to a bankruptcy of one of the banks.


## Configuring the Program Files in Your Directory

Running the benchmark programs relies on having the compile.sh and run.sh files point to the mass_cpp_core folder and requires symbolic links in your directory to three library files in mass_cpp_core/ubuntu.

1.	Create symbolic links to the following three files in the mass_cpp_core directory:
    *	/mass_cpp_core/ubuntu/libmass.so
    *	/mass_cpp_core/ubuntu/mprocess
    *	/mass_cpp_core/ubuntu/killMProcess.sh 
2.	Find compile.sh in the benchmark program’s directory. Edit line 2 of this file to point to the mass_cpp_core directory in relation to compile.sh’s location. For example, if the mass_cpp_core folder is located one directory back from compile.sh, the line will read “export MASS_DIR=../mass_cpp_core”.
3.	Find run.sh in the benchmark program’s directory. Edit line 1 of this file to point to the mass_cpp_core/ubuntu folder in relation to run.sh’s location. For example, if the mass_cpp_core folder is located one directory back from run.sh, the line will read “LD_LIBRARY_PATH=../mass_cpp_core/ubuntu”.
4.	Make sure machinefile.txt lists all the machines you want to establish a connection with during execution. The file provided on Bitbucket lists hermes02-hermes12 and cssmpi1h-cssmpi12h and assumes that hermes01 is the master node. The master node should always be excluded from this file.
5.	Compile the benchmark’s source files with  “./compile.sh” to ensure all executable files are up to date.


## Providing Inputs

This benchmark program requires only one additional text file to run and contains information on the number of agents to instantiate as well as initial values. This text file is named “simArgs.txt” by default. An example is given below to demonstrate how this file should be formatted:
```
Workers 20000
Firms 2000
Banks 5
Production 35000
Interest 0.8
Liquidity 20000
```

## Running the Program

When you have the simulation arguments you want, use the following instructions to execute the program:

1.	Execute run.sh with “./run.sh”. 
2.	Enter the following arguments when prompted:
    *	Number of Nodes – the number of computing nodes (machines) you want to use to run your program.
    *	Number of Threads – the number of threads to use.
    *	Number of Turns – provides the number of rounds the program will execute the simulation loop for. It is guaranteed to run this number of rounds regardless of when a bankruptcy happens.
    *	Port Number - the port number you want to use for communicating between remote computing nodes.
    *	Password - your UW password for the hermes machines. It is recommended that you establish an RSA key so that you can authenticate remote computing nodes without having to pass your password between remote computing nodes. 
3.	If the program terminates for any reason before MASS::finish is called, execute ./killMProcess.sh to clean up the processes on the other computing nodes before attempting to execute again.

# Bail-In/Bail-Out File Descriptions

The files in this directory (in alphabetical order) are...

* Bank.h, Bank.cpp - Source files for the Bank Agent in the simulation.

* compile.sh - Compiles your program into executables to run. Simply use "./compile.sh" to run. Note: Line 2 of this file must have the correct path to the mass_cpp_core library.

* FinancialMarket.h, FinancialMarket.cpp - Source files for the Financial Market Place in the simulation.

* Firm.h, Firm.cpp - Source files for the Firm Agent in the simulation.

* killMProcess.sh - kills any running processes if the program terminates before MASS::finish() was called to delete running processes on other computing nodes. This file must be a symbolic link to "mass_cpp_core/ubuntu/killMProcess.sh".

* libmass.so - A symbolic link to "mass_cpp_core/ubuntu/libmass.so"

* machinefile.txt - Lists the names of all computers we want to establish a connection with to use as a remote computing node. Note that the master node should be excluded from this file.

* main.cpp - The main driver of the simulation.

* mprocess - A symbolic link to "mass_cpp_core/ubuntu/mprocess".

* Owner.h, Owner.cpp - Source files for the Owner Class.

* run.sh - Runs the benchmark program once all the source files have been compiled. Note that line 1 of this file must have the correct path to the mass_cpp_core library files.

* simArgs.txt - Provides values for the simulation arguments.

* Timer.h, Timer.cpp - Keeps track of the program's runtime. Useful for evaluation.

* Worker.h, Worker.cpp - Source files for the Worker Agent in the simulation.