#include "Worker.h"

using namespace std;

extern "C" Agent* instantiate(void* argument) {
  return new Worker(argument);
}

extern "C" void destroy(Agent* obj) {
  delete obj;
}

//----------------------------------- init -------------------------------------
/** Initializes Worker agent's member data.
 *  @param initArgs - an InitWorker* object storing values for the member data.
 *----------------------------------------------------------------------------*/
void* Worker::init(void* initArgs) {
  wages = ((InitWorker*)initArgs)->defaultWage / ( ((InitWorker*)initArgs)->numWorkers * ((float)rand()/RAND_MAX + 0.5) );
  capital = 0;
  consumptionBudget = 0.3;
  workerId = agentId;
  pair<double, int>* workerCost = new pair<double, int>(wages, workerId);
  place->callMethod(FinancialMarket::addWorker_, (void*)workerCost); //Send to place
  delete workerCost; // Delete new value
  myBank = (int)(rand()) % ((InitWorker*)initArgs)->numBanks; // select a bank
}

//------------------------------------- act ------------------------------------
/** Performs all round actions for the Firm.
 *----------------------------------------------------------------------------*/
void* Worker::act() {
  int fixedWage = wages;
  receiveWages();
  consume();
  depositFunds();
  wages = fixedWage;
}

//-------------------------------- receiveWages --------------------------------
/** Receive wages for this round.
 *----------------------------------------------------------------------------*/
void Worker::receiveWages() {
  capital += wages;
}

//---------------------------------- consume -----------------------------------
/** Spend wages consuming products. Note: this function reduces wages for next 
 *  round as well.
 *----------------------------------------------------------------------------*/
void Worker::consume() {
  wages *= consumptionBudget;    // Decrease wages (matches RepastHPC benchamrk)
}

//-------------------------------- depositFunds --------------------------------
/** Sends a deposit to the bank
 *----------------------------------------------------------------------------*/
void Worker::depositFunds() {
  pair<int,double> deposit = {myBank, wages};
  place->callMethod(FinancialMarket::addBankTransaction_, (void*)&deposit);
}

// End Worker.cpp