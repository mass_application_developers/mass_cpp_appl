/*==============================================================================
	Owner Class
	Author: Ian Dudder
================================================================================
	The Owner Class owns a single Firm in the Bail-in/Bail-out simulation.
  If the Firm's profits are positive, the Owner gets paid. When the Firm
  has a negative liquidity, the Owner will attempt to provide the Firm 
  with a loan to bring them back into the positive.
==============================================================================*/

#ifndef OWNER_H
#define OWNER_H

#include <iostream>

class Owner {
public:
  Owner(int firmId_, double capital_);
  double getCapital() const;
  void setCapital(double newCapital);
private:
  double capital;         // Amount of savings
  int firmId;             // ID of the Firm this Owner runs
};

#endif // OWNER_H