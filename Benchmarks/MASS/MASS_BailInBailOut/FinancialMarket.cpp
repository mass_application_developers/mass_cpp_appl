#include "FinancialMarket.h"
#include <mutex>

using namespace std;

extern "C" Place* instantiate(void* argument) {
	return new FinancialMarket(argument);
}

extern "C" void destroy(Place* obj) {
	delete obj;
}


//---------------------------------- init --------------------------------------
/** Initializes default values.
 *  @param nAgents - a pair<int,int> where the first item is the number of Bank
 *         agents, and the second item is the number of Firm agents.
 *----------------------------------------------------------------------------*/
void* FinancialMarket::init(void* nAgents) {
	residingBank = -1; // Placeholder value
  numBanks = ((pair<int,int>*)nAgents)->first; // simArgs.first;
  numFirms = ((pair<int,int>*)nAgents)->second; // simArgs.second;
  srand(0); //Seed to be the same across runs
}


//---------------------------------- addBank -----------------------------------
/** Stores information on the residing Bank agent.
 *  @param bankInfo - a BankInfo* storing the information of the residing bank.
 *----------------------------------------------------------------------------*/
void* FinancialMarket::addBank(void* bankInfo) {
	residingBank = ((BankInfo*)bankInfo)->bankId;    
	bankRegistry[residingBank] = (BankInfo*)bankInfo;  
}


//-------------------------------- addWorker -----------------------------------
/** Inserts the wages of a residing worker into the localWorkerWages map for 
 *  firms to read and calculate their workforce cost.
 *  @param workerInfo - a pair<double,int> with the first item as the wages and
 *         the second item as the workerId.
 *----------------------------------------------------------------------------*/
void* FinancialMarket::addWorker(void* workerInfo) {
  localWorkerWages[ ((pair<double,int>*)workerInfo)->second ] = ((pair<double,int>*)workerInfo)->first;
}


//-------------------------- calculateWorkforceCost ----------------------------
/** Calculates the sum of the wages of all residing workers.
 *  @return - a double* reporting the sum of all worker wages on this place.
 *----------------------------------------------------------------------------*/
void* FinancialMarket::calculateWorkforceCost() {
	double* totalWages = new double(0.0);
	for (auto worker : localWorkerWages) {
    *totalWages += worker.second;
  }
	localWorkerWages.clear(); // Clear the container to free storage.
	return (void*)totalWages;
}


//---------------------------- prepBankInfoExchange ----------------------------
/** Prepares each Place to receive information on the Banks in the simulation.
 *  Must be called before exchangeBankInfo().
 *----------------------------------------------------------------------------*/
void* FinancialMarket::prepBankInfoExchange() {
	inMessage_size = sizeof(BankInfo); // Prepare to receive a BankInfo message
  outMessage_size = 0;  // Send nothing
  outMessage = nullptr;
	cleanNeighbors();     // Clear old neighbors
  for (int n = 0; n < numBanks; n++) /*Add places with banks as neighbors*/ {
    if (n != index[0]) {
      int neighbor[1] = {n - index[0]};
      addNeighbor(neighbor, 1);
    }
  }
	cleanInMessages( );   // Clear out old messages
}


//------------------------------ exchangeBankInfo ------------------------------
/** Returns a BankInfo object containing all information for the residing bank.
 *  prepBankInfoExchange() must be called before this function.
 *  @return - a BankInfo* containing the information of the residing bank.
 *----------------------------------------------------------------------------*/
void* FinancialMarket::exchangeBankInfo() {
	BankInfo* msg = new BankInfo;
	if (residingBank != -1) 
		*msg = *(bankRegistry[residingBank]);
	else 
		msg->bankId == -1;
	return msg;
}

//--------------------------- updateBankRegistry -------------------------------
/** Updates the local bank registry that stores information on all the Banks in
 *  the simulation. exchangeBankInfo() must be called before this function.
 *----------------------------------------------------------------------------*/
void* FinancialMarket::updateBankRegistry() {
	for (int msgNum = 0; msgNum < inMessages.size(); msgNum++) {
		updateBankInfo(inMessages[msgNum]);
	}
	cleanInMessages( );  // Clear old messages after using them
}


//------------------------------ updateBankInfo --------------------------------
/** Updates the residing Bank's information to reflect any changes to its values
 *  during the last round.
 *  @param updatedInfo - a BankInfo* storing the info of the residing bank.
 *----------------------------------------------------------------------------*/
void* FinancialMarket::updateBankInfo(void* updatedInfo) {
  if (updatedInfo != nullptr) {
    BankInfo* b = new BankInfo( *(BankInfo*)updatedInfo );
    unordered_map<int, BankInfo*>::iterator it = bankRegistry.find( b->bankId );
    if ( it != bankRegistry.end() ) {
      delete it->second;  // Delete old BankInfo* if it already exists
      it->second = b;     // Set new BankInfo*
    }
    else {
      bankRegistry[b->bankId] = b;
    }
  }
}


//------------------------------ selectFromKBanks ------------------------------
/** Chooses from k banks the bank with the lowest interest rate that can afford
 *  to provide the loan/
 *  @param arg - a BankSelectionArgs* where k is the number of banks to choose
 *         from, minAcceptableInterest is the lowest interest rate that is 
 *         acceptable, and amountNeeded is the amount needed from the bank.
 *  @return - Returns a BankInfo* storing the information on the bank selected.
 *         if no bank fit the criteria, nullptr is returned.
 *----------------------------------------------------------------------------*/
void* FinancialMarket::selectFromKBanks(void* arg) {
	BankSelectionArgs args = *(BankSelectionArgs*)arg;
	if (args.k > numBanks) args.k = numBanks;
	BankInfo* selectedBank = nullptr;
  set<int> consideredBanks;
	for (int i = 0; i < args.k; i++) /* Select k random, unique banks*/ {
    int randomBankId;
    do {
      randomBankId = (int)(rand()) % numBanks;
    } while ( consideredBanks.find(randomBankId) != consideredBanks.end() );
    consideredBanks.insert(randomBankId);
		unordered_map<int, BankInfo*>::iterator it = bankRegistry.find(randomBankId);
		if (it != bankRegistry.end()) {
			// Find bank with lowest interest rate that can afford to offer the loan
			if (it->second->interestRate < args.minAcceptableInterest && it->second->liquidity > args.amountNeeded) {
				args.minAcceptableInterest = it->second->interestRate;
				selectedBank = it->second;
			}  
		}
	}
	return selectedBank;
}


//-------------------------- addBankTransaction --------------------------------
/** Adds the monetary value of a transaction for a specified bank. A loan will 
 *  be a negative value, decreasing the running total, and a deposit will be
 *  a positive value, increasing the running total.
 *  @param transactionInfo - a pair<int,double>* where the first item is the
 *         bankId and the second item is the transaction amount.
 *----------------------------------------------------------------------------*/
void* FinancialMarket::addBankTransaction(void* transaction) {
  mtx.lock();   // Begin Critical Section
	outgoingTransactions[ ((pair<int,double>*)transaction)->first ] += ((pair<int,double>*)transaction)->second;
  mtx.unlock(); // End Critical Section
}


//------------------------ prepForIncomingTransactions -------------------------
/** Prepares all Financial Market elements with a residing Bank agent to send
 *  the id of the bank as an integer, and receive the total value of 
 *  transactions accumulated from all other Financial Market places during the
 *  round. This function must precede exchangeOutgoingTransactions.
 *----------------------------------------------------------------------------*/
void* FinancialMarket::prepForIncomingTransactions() {
  cleanNeighbors();       // Remove old enighbors
  if (residingBank != -1) /* Have Banks add all other places */ {
    for (int n = 0; n < numFirms; n++) /*Add places with banks as neighbors*/ {
      if (n != index[0]) {
        int neighbor[1] = {n - index[0]};
        addNeighbor(neighbor, 1);
      }
    }
    outMessage_size = sizeof(int);  
    outMessage = (void*)&residingBank;  // Prepare to send bankId
    inMessage_size = sizeof(double);    // Prepare to receive amount
    cleanInMessages( );                 // Clear in messages
  }
}



//------------------------ exchangeOutgoingTransactions ------------------------
/** Receives a bankId and returns the total sum of all transactions accumulated
 *  for that bank during the round. Must be preceded by the function
 *  prepForIncomingTransactions().
 *  @param givenBankId - an int representing the bank's id.
 *  @return - a double representing the value of the transactions.
 *----------------------------------------------------------------------------*/
void* FinancialMarket::exchangeOutgoingTransactions(void* givenBankId) {
  double* amount = new double;    // value to be returned
  *amount = 0.0;
  if ((int*)givenBankId != nullptr) {
    unordered_map<int,double>::iterator transaction = outgoingTransactions.find(*(int*)givenBankId);  
    if (transaction != outgoingTransactions.end()) {
      *amount = outgoingTransactions[*(int*)givenBankId];
    }
  }
  return (void*)amount;
}


//-------------------------- manageIncomingTransactions ------------------------
/** Has any Financial Market with a residing Bank sort through its inMessages 
 *  and total the transactions received from other place elements. Must be
 *  preceded by exchangeOutgoingTransactions().
 *----------------------------------------------------------------------------*/
void* FinancialMarket::manageIncomingTransactions() {
  if (residingBank != -1) /* Have places w/ banks read transactions*/{
    for (int msgNum = 0; msgNum < inMessages.size(); msgNum++) {
      if ((double*)inMessages[msgNum] != nullptr) {
        incomingTransactions += *((double*)inMessages[msgNum]);
      }
    }
    // Add transactions from self
    unordered_map<int,double>::iterator transactionLookup = outgoingTransactions.find(residingBank);
    if (transactionLookup != outgoingTransactions.end()) {
      incomingTransactions += transactionLookup->second;
    }
  }
}


//------------------------- getIncomingTransactionAmt --------------------------
/** Returns the sum of all incoming transactions totalled in the
 *  manageIncomingTransactions() function. 
 *  @return - a double storing the value of all incoming transactions
 *----------------------------------------------------------------------------*/
void* FinancialMarket::getIncomingTransactionAmt() {
  return (void*)&incomingTransactions;
}


//---------------------------- resetRoundValues --------------------------------
// Resets the round values to prepare for next round
void* FinancialMarket::resetRoundValues() {
  incomingTransactions = 0.0;
  outgoingTransactions.clear();
  cleanNeighbors();
  cleanInMessages();
  return nullptr;
}
