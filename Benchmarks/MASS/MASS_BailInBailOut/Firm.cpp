#include "Firm.h"

using namespace std;

extern "C" Agent* instantiate(void* argument) {
  return new Firm(argument);
}

extern "C" void destroy(Agent* obj) {
  delete obj;
}


//---------------------------------- init --------------------------------------
/** Initializes default values.
 *  @param initArgs - a pair<double,double>* where the first item is the 
 *         simulation's production costs, and the second value is the liquidity.
 *----------------------------------------------------------------------------*/
void* Firm::init(void* initArgs) {
  productionCost = resetVals.first = ((pair<double,double>*)initArgs)->first; 
  liquidity = resetVals.second = ((pair<double,double>*)initArgs)->second; 
  profit = 0;
  firmId = agentId;
  owner = new Owner(firmId, 0);
}


//---------------------------- calculateWorkforceCost---------------------------
/** Computes the total sum of all worker wages to be paid out each round.
 *----------------------------------------------------------------------------*/
void* Firm::calculateWorkforceCost() {
  double* temp = (double*)place->callMethod(FinancialMarket::calculateWorkforceCost_, nullptr);
  workforceCost = *temp;
  delete temp;
}


//---------------------------------- act ---------------------------------------
/** Performs all round actions for the Firm.
 *----------------------------------------------------------------------------*/
void* Firm::act() {
  accumulateInterest();   // 1) Calculate Interest on outstanding loans
  calculateProductionCosts(); // 2) Calculate production costs
  calculateProfits();     // 3) Calculate Profits
  payWorkforce();         // 4) Pay Worker Wages
  calculateLiquidity();   // 5) Calculate liquidity
  payBank();              // 6) Pay Banks for outstanding loans
  bool costsCovered = (liquidity >= 0);
  if (!costsCovered) {
    costsCovered = getOwnerLoan(); // 7a) Ask Owner for loan
    if (!costsCovered && debt.size() <= 0) {
      costsCovered = requestBankLoan(3);
    }
  }
  if (liquidity < 0)
    resetFirm();
}

//------------------------------- accumulateInterest ---------------------------
/** Accumulates interest for every outstanding loan.
 *----------------------------------------------------------------------------*/
void Firm::accumulateInterest() {
  for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
    debt[curLoan].amount = debt[curLoan].amount * (1.0 + debt[curLoan].interestRate);
  }
}

//--------------------------- calculateProductionCosts -------------------------
/** Calculates the round's production costs
 *----------------------------------------------------------------------------*/
void Firm::calculateProductionCosts() {
  double modifier = (float) ( (float)rand() / RAND_MAX + 0.5);
  productionCost *= modifier;
}

//------------------------------ calculateProfits ------------------------------
/** Calculates the round's traffic.
 *----------------------------------------------------------------------------*/
void Firm::calculateProfits() {
  double x = (float) ( (float)rand() / RAND_MAX + 0.5);
  profit = (productionCost * x) - productionCost;
}

//------------------------------ payWorkforce ----------------------------------
/** Pays the workers their wages and pays the owner if profits are positive.
 *----------------------------------------------------------------------------*/
void Firm::payWorkforce() {
  profit -= workforceCost;    // Pay Workers
  double dividends;           // Owner's cut        
  if (profit > 0) {
    dividends = profit * 0.2;
    profit -= dividends;
  }
  else {
    dividends = 0;
  }
  owner->setCapital(owner->getCapital() + dividends);
}

//----------------------------- calculateLiquidity -----------------------------
/** Computes the liquidity for the round.
 *----------------------------------------------------------------------------*/
void Firm::calculateLiquidity() {
  liquidity += profit;
}

//---------------------------------- payBank -----------------------------------
/** Attempts to pay back half the amount for all outstanding loans.
 *----------------------------------------------------------------------------*/
void Firm::payBank() {
  for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
    double amtToPay = debt[curLoan].amount / 2;
    debt[curLoan].amount -= amtToPay;
    liquidity -= amtToPay;
    pair<int,double> payment;
    payment.first = debt[curLoan].loaningBankId;
    payment.second = amtToPay; // Positive value
    place->callMethod(FinancialMarket::addBankTransaction_, (void*)&payment);
  }
}

//-------------------------------- getOwnerLoan --------------------------------
/** Asks the Firm's owner for a loan
 *  @return - true if the owner could provide one and false otherwise.
 *----------------------------------------------------------------------------*/
bool Firm::getOwnerLoan() {
  if (owner->getCapital() > liquidity * -1) {
    double amount = liquidity * -1;
    owner->setCapital(owner->getCapital() - amount);
    liquidity += amount;
    return true;
  }
  return false;
}

//------------------------------- requestBankLoan ------------------------------
/** Approaches k random banks to ask for a loan at a low interest rate.
 *  @return - returns true if a loan was granted and false otherwise.
 *----------------------------------------------------------------------------*/
bool Firm::requestBankLoan(int k) {
  BankSelectionArgs selArgs;
  selArgs.minAcceptableInterest = 2134.0; // Value matches RepastHPC benchmark
  selArgs.amountNeeded = liquidity * -1;
  selArgs.k = k;
  BankInfo* selectedBank = (BankInfo*)place->callMethod(FinancialMarket::selectFromKBanks_, &selArgs);
  if (selectedBank != nullptr) {
    Loan newLoan;
    newLoan.amount = liquidity * -1; // Positive value
    newLoan.interestRate = selectedBank->interestRate;
    newLoan.loaningBankId = selectedBank->bankId;
    debt.push_back(newLoan);
    liquidity += newLoan.amount;
    /*Add loan information to Financial Market's Bank Transactions*/
    pair<int,double> loanReceipt = {selectedBank->bankId, newLoan.amount * -1};
    place->callMethod(FinancialMarket::addBankTransaction_, (void*)&loanReceipt);
    return true;
  }
  else {
    return false;
  }
}

//----------------------------------- resetFirm --------------------------------
/** Resets a bankrupt firm to default starting values to re-enter simulation.
 *----------------------------------------------------------------------------*/
void Firm::resetFirm() {
  productionCost = resetVals.first;
  liquidity = resetVals.second;
  profit = 0;
  owner->setCapital(liquidity * ((float) rand() / RAND_MAX + 0.5));
  debt.clear();
}

// End Firm.cpp