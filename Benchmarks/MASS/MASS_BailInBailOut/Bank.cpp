#include "Bank.h"

using namespace std;

extern "C" Agent* instantiate(void* argument) {
  return new Bank(argument);
}

extern "C" void destroy(Agent* obj) {
  delete obj;
}


//----------------------------------- init -------------------------------------
/** Initializes the member data for the Bank object writes it to the place
 *  element.
 *  @param initVals - A pair<double,double> where the first item is the 
 *        initial interest rate, and the second item is the initial liquidity. 
 *----------------------------------------------------------------------------*/
void* Bank::init(void* initVals) {
  BankInfo* myInfo = new BankInfo; 
  interestRate = myInfo->interestRate = ((pair<double,double>*)initVals)->first;  
  liquidity = myInfo->liquidity = ((pair<double,double>*)initVals)->second;     
  bankId = myInfo->bankId = agentId;
  myInfo->linearIndex = index[0];
  totalDebt = 0;
	place->callMethod(FinancialMarket::addBank_, myInfo);  //Pass Bank Info
}


//------------------------------------ act -------------------------------------
/** Runs the Bank's sequence of actions in the simulation loop in each round.
  *----------------------------------------------------------------------------*/
void* Bank::act() {
	payLoans();           // 1) Pay outstanding loans to other banks
	accumulateInterest(); // 2) Accumulate interest on outstanding loans
	if (liquidity < 0)    // 3) Get a loan if needed
    getLoan(3);
}


//----------------------------------- payLoans ---------------------------------
/** Iterates through the vector of loans and attempts to pay back half of the
  *  amount of each outstanding loan. If a loan cannot be repaid, the liquidity
  *  is set to -1.
  *---------------------------------------------------------------------------*/
void Bank::payLoans() {
	for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
		double amtToPay = debt[curLoan].amount / 2;
		if (liquidity > amtToPay) {
			debt[curLoan].amount -= amtToPay;
			liquidity -= amtToPay;
			totalDebt -= amtToPay;
      // Pass payment to the Financial Market to forward to the bank
			pair<int,double> payment = {debt[curLoan].loaningBankId, amtToPay};
			place->callMethod(FinancialMarket::addBankTransaction_, (void*)&payment);
		}
		else {
			liquidity = -1;
		}
	}
}


//------------------------------- accumulateInterest ---------------------------
/** Iterates through the vector of loans and compounds interest on each loan 
 *  based on the rate at which the loan was given at.
 *-----------------------------------------------------------------------------*/
void Bank::accumulateInterest() {
	for (int curLoan = 0; curLoan < debt.size(); curLoan++) {
		double increase = debt[curLoan].amount * debt[curLoan].interestRate;
		debt[curLoan].amount += increase;
		totalDebt += increase;
	}
}


//------------------------------------ getLoan ---------------------------------
/** Attempts to get a loan from another bank. Returns true if the loan was 
 *  secured, and false if the loan was rejected. 
 *----------------------------------------------------------------------------*/
bool Bank::getLoan(int k) {
	BankSelectionArgs selArgs;
	selArgs.minAcceptableInterest = 2134.0;
	selArgs.amountNeeded = liquidity * -1;
	selArgs.k = k;
	BankInfo* selectedBank = (BankInfo*)place->callMethod(FinancialMarket::selectFromKBanks_, &selArgs);
	if (selectedBank != nullptr) {
		Loan newLoan; // Add loan to debt
		newLoan.amount = liquidity * -1; // Save loan as a positive value (liq is negative)
		newLoan.interestRate = selectedBank->interestRate;
		newLoan.loaningBankId = selectedBank->bankId;
		debt.push_back(newLoan);
		totalDebt += newLoan.amount;
		liquidity += newLoan.amount;
    // Send Loan to the Financial Market
		pair<int,double> loanReceipt = {selectedBank->bankId, (newLoan.amount * -1)};
		place->callMethod(FinancialMarket::addBankTransaction_, (void*)&loanReceipt);
		return true;
	}
	else {
		return false;
	}
}


//--------------------------------- randomizeRate ------------------------------
/** Randomizes the bank's interest rate for a new round.                 
  *---------------------------------------------------------------------------*/
void* Bank::randomizeRate() {
	interestRate = (float) rand() / RAND_MAX;
}


//------------------------------ reportRoundValues -----------------------------
/** Writes the Bank's Id, interest rate, liquidity, and location in the place
 *  matrix to the Financial Market.                                 
 *----------------------------------------------------------------------------*/ 
void* Bank::reportRoundValues() {
  BankInfo myInfo;
	myInfo.interestRate = interestRate;
	myInfo.liquidity = liquidity;
	myInfo.bankId = bankId;
  myInfo.linearIndex = index[0];
	place->callMethod(FinancialMarket::updateBankInfo_, &myInfo); 
}


 //---------------------------- readRoundTransactions ---------------------------
/** Reads all payments made and loans given during this round from the Financial
 *  Market Place.                                           
 *-----------------------------------------------------------------------------*/
void* Bank::readRoundTransactions() {
	double transactionAmt = *(double*)place->callMethod(FinancialMarket::getIncomingTransactionAmt_, nullptr);
	liquidity += transactionAmt; //TransactionAmt may be positive (from deposits) or negative (from loans)
}


//--------------------------------- isBankrupt ---------------------------------
/** Checks if this Bank has gone bankrupt based on its liquidity. 
 *  @return - returns true if bankruptcy is detected
 *----------------------------------------------------------------------------*/
void* Bank::isBankrupt() {
  bool* ret = new bool( liquidity + totalDebt < 0 );
  return ret;
}