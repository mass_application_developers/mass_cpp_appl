/*==============================================================================
	Worker Class
	Author: Ian Dudder
================================================================================
	The Worker Class is an Agent in the Bail-in/Bail-out simulation. In
  each round of the simulation, the Worker will receive wages from its
  Firm, spend wages, and deposit leftover wages in the Bank.
==============================================================================*/

#ifndef WORKER_H
#define WORKER_H

#include <iostream>
#include "Agent.h"
#include "MASS_base.h"
#include "FinancialMarket.h"

struct InitWorker {
  double defaultWage;
  int numWorkers;
  int numBanks;
};

class Worker : public Agent {
public:
  //=============================== Function IDs ===============================
  static const int init_ = 0;   // Initializes worker agents
  static const int act_ = 1;    // Performs all round actions
  
  //============================== Public Methods ==============================
  Worker(void* argument) : Agent(argument) {  }
      
  //-------------------------------- callMethod --------------------------------
  virtual void *callMethod(int functionId, void* argument) {
    switch(functionId) {
      case init_ :
        return init(argument);

      case act_ :
        return (void*)act();
    }
    return nullptr;
  }

private:
  //============================ Call Method Helpers ===========================

  //----------------------------------- init -------------------------------------
  /** Initializes Worker agent's member data.
  *  @param initArgs - an InitWorker* object storing values for the member data.
  *----------------------------------------------------------------------------*/
  void* init(void* initArgs);


  //------------------------------------- act ------------------------------------
  /** Performs all round actions for the Firm.
  *----------------------------------------------------------------------------*/
  void* act();


  //-------------------------------- receiveWages --------------------------------
  /** Receive wages for this round.
  *----------------------------------------------------------------------------*/
  void receiveWages();


  //---------------------------------- consume -----------------------------------
  /** Spend wages consuming products. Note: this function reduces wages for next 
  *  round as well.
  *----------------------------------------------------------------------------*/
  void consume();


  //-------------------------------- depositFunds --------------------------------
  /** Sends a deposit to the bank
  *----------------------------------------------------------------------------*/
  void depositFunds();
  
  
  //=============================== Member Data ================================
  double wages;               // Income
  double capital;             // Saved money
  double consumptionBudget;   // Amount spend on products each step
  int workerId;               // Unique identifier for worker
  int myBank;                 // Identifies bank holding personal account
  ostringstream logText;      // Used to make MASS logs
};

#endif // WORKER_H