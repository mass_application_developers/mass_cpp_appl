/*==============================================================================
	Bail-in/Bail-out Simulation
	Author: Ian Dudder
================================================================================
	The Bail-in/Bail-out Simulation models a financial market comprised of 
  Households (i.e. Workers, Owners), Firms, and Banks, and observes as these 
  three types of entities interact with one another until a Bank goes bankrupt 
  and can't continue. This simulation models the scenario posed by the paper 
  titled "To Bail-out or to Bail-in? Answers from an Agent-Based Model" by Peter
  Klimek, et al.

  This implementation leverages the MASS C++ library by Professor Munehiro 
  Fukuda at the University of Washington, Bothell.
==============================================================================*/

#include "MASS.h"
#include "Firm.h"
#include "Bank.h"
#include "Worker.h"
#include "FinancialMarket.h"
#include "Timer.h"	//Timer
#include <stdlib.h> // atoi
#include <unistd.h>
#include <vector>
#include <fstream>

using namespace std;

Timer timer;  // Records Elapsed time of the run
ostringstream logText; // Used to make mass logs

bool readSimulationArguments(string filename, int &numWorkers, int &numFirms, 
  int &numBanks, double &initialProductionCost, double &initialInterest, 
  double &initialLiquidity);


bool foundBankruptcy(Agents* banks, const int numBanks);


int main( int argc, char *args[] ) {
	if ( argc != 9 ) /* Ensure all Simulation Args are Present.*/ {
		cerr << "usage: ./main username password machinefile.txt port nProc" 
      << "nThreads numTurns simArgs.txt" << endl;
		return -1;
	}
  
	//---------------------------Receive Simulation Arguments-----------------------
	char *arguments[4];
	arguments[0] = args[1]; 	      // username
	arguments[1] = args[2]; 	      // password
	arguments[2] = args[3]; 	      // machinefile
	arguments[3] = args[4]; 	      // port
	int nProc = atoi( args[5] );    // number of processes
	int nThr = atoi( args[6] );     // number of threads
  int numTurns = atoi ( args[7] );// number of simulation rounds 
	string simArgFile = args[8];    // Text file of simulation values
	
	//----------------Initialize MASS with the machine information------------------
	MASS::init( arguments, nProc, nThr ); 
  timer.start(); // Simulation Start
  
	//--------------------------Set up Simulation Arguments-------------------------
  const int marketHandle = 1, bankHandle = 2, firmHandle = 3, workerHandle = 4;
	int numWorkers, numFirms, numBanks;
	double initialProductionCost, initialInterest, initialLiquidity;

  //------------------Read in simulation arguments from the file------------------
	bool successfulRead = readSimulationArguments(simArgFile, numWorkers, numFirms,
    numBanks, initialProductionCost, initialInterest, initialLiquidity);
	if (!successfulRead) {
    cerr << "Error: Unable to open argument file" << endl;
		return -1;
  }
	double defaultWage = initialProductionCost * numFirms * 0.1 * 0.8;
	char* dummyArg = "A";

	//---Create the Financial Market large enough to house one firm per element---
	Places *market = new Places(marketHandle, "FinancialMarket", 1, dummyArg, sizeof(char), 
    1, numFirms);
  pair<int,int> numAgents = {numBanks, numFirms};
	market->callAll(FinancialMarket::init_, (void*)&numAgents, sizeof(pair<int,int>));
  
	//--------------------------------Create Banks----------------------------------
  pair<double,double> bankArgs = {initialInterest, initialLiquidity};
  Agents* banks = new Agents(bankHandle, "Bank", dummyArg, 1, market, numBanks);
	banks->callAll(Bank::init_, (void*)&bankArgs, sizeof(pair<double,double>));
  
	//---------------------Have Banks broadcast their information-------------------
	market->callAll(FinancialMarket::prepBankInfoExchange_, dummyArg, 1);
	market->exchangeAll(marketHandle, FinancialMarket::exchangeBankInfo_);
	market->callAll(FinancialMarket::updateBankRegistry_, dummyArg, 1);
  
	//--------------------------------Create Firms----------------------------------
	Agents* firms = new Agents(firmHandle, "Firm", dummyArg, 1, market, numFirms);
  pair<double,double> firmInitVals = {initialProductionCost, initialLiquidity};
	firms->callAll(Firm::init_, &firmInitVals, sizeof(pair<double,double>));
  
	//--------------------------------Create Workers--------------------------------
	Agents* workers = new Agents(workerHandle, "Worker", dummyArg, 1, market, numWorkers);
  InitWorker workerInitVals;
  workerInitVals.defaultWage = defaultWage;
  workerInitVals.numWorkers = numWorkers;
  workerInitVals.numBanks = numBanks;
	workers->callAll(Worker::init_, &workerInitVals, sizeof(pair<double,int>));
  
	//-----------------Calculate workforce cost based on workers wages--------------
	firms->callAll(Firm::calculateWorkforceCost_, dummyArg, 1);

	//----------------------------- Simulation Loop --------------------------------
  int round = 1;
  for (; !foundBankruptcy(banks, numBanks) && round <= numTurns; round++) {
    // Agents Act in Round
		firms->callAll(Firm::act_, dummyArg, 1);   
		workers->callAll(Worker::act_, dummyArg, 1);  
		banks->callAll(Bank::act_, dummyArg, 1);
    // Send Transactions to Banks
		market->callAll(FinancialMarket::prepForIncomingTransactions_, dummyArg, 1);
		market->exchangeAll(marketHandle, FinancialMarket::exchangeOutgoingTransactions_);
		market->callAll(FinancialMarket::manageIncomingTransactions_, dummyArg, 1); // Sometimes crashes here instead
		banks->callAll(Bank::readRoundTransactions_, dummyArg, 1);
		//banks->callAll(Bank::randomizeRate_, dummyArg, 1); //Line removed to match RepastHPC version
		banks->callAll(Bank::reportRoundValues_, dummyArg, 1);
		// Prepare for Next Round
		market->callAll(FinancialMarket::prepBankInfoExchange_, dummyArg, 1);
		market->exchangeAll(marketHandle, FinancialMarket::exchangeBankInfo_);
		market->callAll(FinancialMarket::updateBankRegistry_, dummyArg, 1);
    market->callAll(FinancialMarket::resetRoundValues_, dummyArg, 1);
	} 
	
	long elaspedTime_END =  timer.lap(); // Simulation End
  logText.str("");
  logText << "\nEnd of Simulation. Elapsed time using MASS framework with " << nProc
          << " processes, " << nThr << " threads, and " << round - 1 << " turns: "
          << elaspedTime_END << endl; 
  MASS_base::log(logText.str());

	MASS::finish( );
}

/* Reads in the arguments from a file for this simulation */
bool readSimulationArguments(string filename, int &numWorkers, int &numFirms, int &numBanks, 
double &initialProductionCost, double &initialInterest, double &initialLiquidity) {
	bool successfulRead = false;
	string varName;
	ifstream simArgs;
  simArgs.open(filename);

  if (!simArgs) {
    successfulRead = false;
  }
	else {
		simArgs >> varName >> numWorkers;
		simArgs >> varName >> numFirms;
		simArgs >> varName >> numBanks;
		simArgs >> varName >> initialProductionCost;
		simArgs >> varName >> initialInterest;
		simArgs >> varName >> initialLiquidity;
		successfulRead = true;
	}
  simArgs.close();
	return successfulRead;
}

bool foundBankruptcy(Agents* banks, const int numBanks) {
  bool bankruptcyFound = false;
  bool* bankrupt = (bool*)banks->callAll(Bank::isBankrupt_, nullptr, 0, sizeof(bool));
  for (int i = 0; i < numBanks; i++) {
    if (bankrupt[i]) {
      bankruptcyFound = true; // Detected a bankruptcy
      logText.str("");
      logText << "Bank " << i << " bankrupt!";
      MASS_base::log(logText.str());
      /*eval only*/ bankruptcyFound = false; // Keeps running until maxRounds hit
      break;
    }
  }
  delete [] bankrupt;
  return bankruptcyFound;
}