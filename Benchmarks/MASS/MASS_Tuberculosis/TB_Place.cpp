//
// Created by sarah
//

#include "TB_Place.h"
#include <algorithm>
#include <fstream>
#include <cstring>

extern "C" Place *instantiate(void *argument) {
    return new TB_Place(argument);
}

extern "C" void destroy(Place *object) {
    delete object;
}

/**
 * Only used before first iteration
 *
 * - calculate pId using x/y indices
 * - set bloodVessel, macrophage, bacteria
 * @return null
 */
void *TB_Place::init(void *placesToSpawnMacro) {

    // pId is the one dimensional index of this place in the simulation space grid
    pId = toId(index[0], index[1], size[0]);

    int *spawnMacro = reinterpret_cast<int *>(placesToSpawnMacro);

    //debug
    cerr << "TB_Place " << pId << " init(), macroSpawn size = " << sizeof(spawnMacro) / sizeof(spawnMacro[0]);
    cerr << " , contains [";
    for (int i = 0; i < initMacroNum; i++) {
        cerr << " {" << spawnMacro[i] << "} ";
    }
    cerr << "] "<< endl;

    // if this place ID is in list of places to spawn macrophages
    macrophage = find(spawnMacro, spawnMacro + initMacroNum, pId) != spawnMacro + initMacroNum;

    tCell = false;
    macrophageDeath = false;

    //initialize current state variables
    vector<int> entryPts = bloodVessels(size[0]);
    bloodVessel = find(entryPts.begin(), entryPts.end(), pId) != entryPts.end();

    bacteria = isInitBacteriaSpawnPt();
    chemokine = 0;

    // initialize next state variables
    nextBacteria = false;
    createNewMacro = false;
    createNewTCell = false;
    macroMoveReq = -1;
    tCellMoveReq = -1;
    approvingMacroFromPid = -1;
    approvingTCellFromPid = -1;
    shouldMaxChemokine = false;

    convert.str("");
    convert << "\nTBPlace.init() -> :D end of function for Place " << pId;
    MASS_base::log(convert.str());

    return NULL;
}

/**
 * Decides if this will be one of the four initial bacteria spawn points in the simulation space
 * - bacteria is spawned in the center four squares of the grid
 * - assumes simulation grid is a square
 * @return true if initial bacteria spawn point
 *         false if not
 */
bool TB_Place::isInitBacteriaSpawnPt() {
    int highI = size[0] / 2;
    int lowI = highI - 1;

    return pId == toId(lowI, lowI, size[0]) ||
           pId == toId(lowI, highI, size[0]) ||
           pId == toId(highI, highI, size[0]) ||
           pId == toId(highI, lowI, size[0]);
}

// set outmessage to bacteria
void *TB_Place::broadcastBacteria() {
    outMessage_size = sizeof(bool);
    outMessage = &bacteria;
    return NULL;
}

// need to sync all places before calling this (places.exchangeBoundary())
void *TB_Place::checkNeighborBacteria(void *day) {
    int today = *(int *) day;

    // if this place doesn't already have a bacteria and today is a bacterial growth day, check myNeighbors to see if
    // bacteria will spread here
    if (!bacteria && ((today > 0) && today % bacterialGrowth) == 0) {
        // check TB_Place myNeighbors to see if they are infected with bacteria
        for (int i = 0; i < mooreNeighborNum; i++) {
            bool *ptr = (bool *) getOutMessage(handleTB_Place, myNeighbors[i]);
            if (ptr != NULL) {
                bool neighborHasBacteria = *ptr;
                if (neighborHasBacteria) {
                    nextBacteria = true;
                    break;
                }
            }
        }
    }
    return NULL;
}

void *TB_Place::decayChemokineAndGrowBacteria() {
    // decay chemokine level
    if (chemokine > 0) {
        --chemokine;
    }
    // grow bacteria if necessary
    if (!bacteria) {
        bacteria = nextBacteria;
        nextBacteria = false;
    }

    return NULL;
}

void *TB_Place::cellRecruitment(void *day) {
    // if not a blood vessel, do nothing
    if (!bloodVessel) {
        return NULL;
    }

    int today = *(int *) day;
    Spawn spawnType = spawnImmuneCell(today);

    switch (spawnType) {
        case macroSpawn:
            // spawn macrophage
            macrophage = true;
            // set next state
            createNewMacro = true;
            createNewTCell = false;
            break;

        case tCellSpawn:
            // spawn tCell
            tCell = true;
            // set next state
            createNewTCell = true;
            createNewMacro = false;
            break;

        case noSpawn:
            // set next state
            createNewMacro = false;
            createNewTCell = false;

        default:
            break;
    }
    return NULL;
}

/**
 * Helps cell_recruitment() function determine whether to request a T-cell or macrophage from the Agent Factory or if
 * no immune cell should be spawned
 *
 * @param today
 * @return 0 - all Places remain in the simulation
 */
TB_Place::Spawn TB_Place::spawnImmuneCell(int today) {

    // if there are max number of immune cells on this Place, don't recruit any new cells
    if (macrophage && tCell) {
        return noSpawn;
    } else if (!tCell && macrophage) {
        if (today >= tCellEntrance) {
            return tCellSpawn;
        } else {
            return noSpawn;
        }
    } else if (!macrophage && tCell) {
        return macroSpawn;
    }
        // no immune cells at this current Place
    else {
        return ((today >= tCellEntrance) && (rand() & 1) == 1) ? tCellSpawn : macroSpawn;
    }
}

void *TB_Place::broadcastChemokineLevel() {
    outMessage_size = sizeof(chemokine);
    outMessage = &chemokine;
    return NULL;
}

void *TB_Place::getRemotePlaceOutMessage(void *placeRelativeIndex) {
    int *neighbor = (int *) placeRelativeIndex;
    return getOutMessage(1, neighbor);
}

// finds a random place to move to based on chemokine level
// - assumes TB_Places have set out message to their current chemokine level and exchangeBoundary() has been called
void *TB_Place::findNewPlace() {
    // Note: deterministic random movement for debugging - can be altered later
    vector<int> bestNeighbors; // myNeighbors with max chemokine levels (= 2)
    vector<int> mediocreNeighbors; // myNeighbors with chemokine = 1
    vector<int> worstNeighbors; // chemokine = 0

    // debug
    cerr << pId;

    for (int i = 0; i < mooreNeighborNum; i++) {
        int neighborPid = toId(index[0] + myNeighbors[i][0], index[1] + myNeighbors[i][1], size[0]);
        cerr << ": for neighbor place " << neighborPid << " -> ";
        int *chemoPtr = (int *) getOutMessage(handleTB_Place, myNeighbors[i]);
        int neighborChemokine = (chemoPtr != NULL) ? *chemoPtr : -1;

        switch (neighborChemokine) {
            case (maxChemokine) :
                bestNeighbors.push_back(neighborPid);
                // debug
                cerr << " [added to best neighbors] " << neighborPid;
            case (1) :
                mediocreNeighbors.push_back(neighborPid);
                // debug
                cerr << " [added to mediocre neighbors] " << neighborPid;
            case (0) :
                worstNeighbors.push_back(neighborPid);
                // debug
                cerr << " [added to worst neighbors] " << neighborPid;
            default  :
                continue;
        }
    }
    // debug
    cerr << endl;

    int newPlace = 0;
    // find random neighbor to move to that has the highest chemokine level
    if (!bestNeighbors.empty()) {
        newPlace = bestNeighbors[rand() % bestNeighbors.size()];

    } else if (!mediocreNeighbors.empty()) {
        newPlace = mediocreNeighbors[rand() % mediocreNeighbors.size()];

    } else if (!worstNeighbors.empty()){
        newPlace = worstNeighbors[rand() % worstNeighbors.size()];
    }
    else {
        return NULL;
    }

    // debug
    cerr << pId << " will request place " << newPlace << endl;

    void *newPlacePtr = &newPlace;
    return newPlacePtr;
}

// argument is the pId of the place the macrophage is requesting to move to; this should be a neighbor in the current
// Place's moore's area
void *TB_Place::macroRequestMove(void *neighbor) {
    macroMoveReq = *(int *) neighbor;
    return NULL;
}

void *TB_Place::sendMacroMoveReqs() {
    outMessage_size = sizeof(macroMoveReq);
    outMessage = &macroMoveReq;
    return NULL;
}

// examine myNeighbors and approve requests if you don't already have a macrophage
void *TB_Place::approveMacroMove() {
    if (!macrophage) {
        approvingMacroFromPid = pickOneNeighborMoveReq();
    }
    return NULL;
}

void *TB_Place::broadcastMacroApproval() {
    outMessage_size = sizeof(approvingMacroFromPid);
    outMessage = &approvingMacroFromPid;
    return NULL;
}

int TB_Place::pickOneNeighborMoveReq() {
    //check myNeighbors to see if their macrophages are requesting to migrate to you
    // if multiple, pick one to send an approval msg to later
    vector<int> requestsByRequestingPid;
    for (int i = 0; i < mooreNeighborNum; i++) {
        int *requestedPlacePtr = (int *) getOutMessage(handleTB_Place, myNeighbors[i]);
        int requestedPlace = (requestedPlacePtr != NULL) ? *requestedPlacePtr : -1;

        if (requestedPlace == pId) {
            int applicantPid = toId(index[0] + myNeighbors[i][0], index[1] + myNeighbors[i][1], size[0]);
            requestsByRequestingPid.push_back(applicantPid);
        }
    }
    if (!requestsByRequestingPid.empty()) {
        return requestsByRequestingPid[rand() % requestsByRequestingPid.size()];
    } else {
        return -1;
    }
}

void *TB_Place::macrophageMovement() {
    // reset macrophage move request and approvingMacroFromPid, regardless if move happened or not
    macroMoveReq = -1;
    approvingMacroFromPid = -1;
    return NULL;
}


// argument is the pId of the place the t-cell is requesting to move to; this should be a neighbor in the current
// Place's moore's area
void *TB_Place::tCellRequestMove(void *neighbor) {
    tCellMoveReq = *(int *) neighbor;
    return NULL;
}

void *TB_Place::sendTCellMoveReqs() {
    outMessage_size = sizeof(tCellMoveReq);
    outMessage = &tCellMoveReq;
    return NULL;
}

void *TB_Place::approveTCellMove() {
    if (!tCell) {
        approvingTCellFromPid = pickOneNeighborMoveReq();
    }
    return NULL;
}

void *TB_Place::broadcastTCellApproval() {
    outMessage_size = sizeof(approvingTCellFromPid);
    outMessage = &approvingTCellFromPid;
    return NULL;
}

void *TB_Place::tCellMovement() {
    // reset t-cell move request and approvingTCellFromPid, regardless if move happened or not
    tCellMoveReq = -1;
    approvingTCellFromPid = -1;
    return NULL;
}

// check myNeighbors and propagate chemokines here if needed
void *TB_Place::receiveMaxedChemokineMsgs() {
    if (chemokine < maxChemokine) {
        for (int i = 0; i < mooreNeighborNum; i++) {
            int *chemokinePtr = (int *) getOutMessage(handleTB_Place, myNeighbors[i]);
            int neighborChemokine = (chemokinePtr != NULL) ? *chemokinePtr : -1;

            if (neighborChemokine == maxChemokine) {
                shouldMaxChemokine = true;
                break;
            }
        }
    }
    return NULL;
}

// update chemokine current state and clear shouldMaxChemokine variable
void *TB_Place::updateChemokineAfterMsg() {
    if (shouldMaxChemokine) {
        chemokine = maxChemokine;
    }
    shouldMaxChemokine = false;
    return NULL;
}

void *TB_Place::spreadBacteriaFromMacroDeaths() {
    outMessage_size = sizeof(macrophageDeath);
    outMessage = &macrophageDeath;
    // if this macrophage has died due to a bacterial death, and spread bacteria
    // - assumes that macrophage agent has already removed itself by changing the macrophage flag on this place
    if (macrophageDeath) {
        bacteria = true;
    }
    return NULL;
}

void *TB_Place::receiveBacteriaFromMacroDeaths() {
    // if this place doesn't already have bacteria, check the myNeighbors to see if it will spread here from a
    // macrophage explosion
    if (!bacteria) {
        for (int i = 0; i < mooreNeighborNum; i++) {
            // get macrophageDeath messages from myNeighbors
            bool *macroDeathPtr = (bool *) getOutMessage(handleTB_Place, myNeighbors[i]);
            bool macroDeath = (macroDeathPtr != NULL) ? (*macroDeathPtr) : (false);
            if (macroDeath) {
                bacteria = true;
                break;
            }
        }
    }
    // reset state after processing macrophage deaths
    if (macrophageDeath) {
        macrophage = false;
        macrophageDeath = false;
    }

    return NULL;
}

void *TB_Place::writeStateToXMLFile() {
    convert.str("");
    convert << "\n<xagent>";
    convert << "\n<name>Place</name>";
    convert << "\n<p_id>" << pId << "</p_id>";
    convert << "\n<x>" << index[0] << "</x>";
    convert << "\n<y>" << index[1] << "</y>";

    int bactWrite = (bacteria) ? 1 : 0;
    convert << "\n<bacteria>" << bactWrite << "</bacteria>";
    convert << "\n<chemokine>" << chemokine << "</chemokine>";
    int bloodVWrite = (bloodVessel) ? 1 : 0;
    convert << "\n<blood_vessel>" << bloodVWrite << "</blood_vessel>";
    int macrophageWrite = (macrophage) ? 1 : 0;
    convert << "\n<macrophage>" << macrophageWrite << "</macrophage>";
    int tCellWrite = (tCell) ? 1 : 0;
    convert << "\n<t_cell>" << tCellWrite << "</t_cell>";
    convert << "\n</xagent>";
    MASS_base::log(convert.str());
    return NULL;
}

















