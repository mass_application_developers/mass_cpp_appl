/*==============================================================================
  Person Class
  Author: Ian Dudder
================================================================================
  The Person Class derives from the MASS Place class. Conceptually, each person
  is a vertex and each friend is an edge. Each Person can calculate their xth
  degree friends by repeatedtly asking their first degree friends what their
  latest degree friends are.
==============================================================================*/

#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include "Place.h"
#include "MASS_base.h"
#include <vector>
#include <unordered_set>

class Person : public Place {
  public:
  //=============================== Function Ids ===============================
  static const int init_ = 0;                     // Initializes member data
  static const int setFirstDegreeFriends_ = 1;    // Sets up 1st degree friends
  static const int prepXthDegreeFriends_ = 2;     // Preps for exchangeAll
  static const int exchangeXthDegreeFriends_ = 3; // ExchangeAll call
  static const int readXthDegreeFriends_ = 4;     // Read inMessages
  static const int printFriends_ = 5;             // Prints all degrees

  //============================== Public Methods ==============================
  Person(void* argument) : Place(argument) {}

  //------------------------------- Call Method --------------------------------
  virtual void* callMethod(int functionId, void* argument) {
    switch(functionId) {
      case init_ :
        return init(argument);

      case setFirstDegreeFriends_ :
        return setFirstDegreeFriends(argument);

      case prepXthDegreeFriends_ :
        return prepXthDegreeFriends(argument);

      case exchangeXthDegreeFriends_ :
        return exchangeXthDegreeFriends();

      case readXthDegreeFriends_ :
        return readXthDegreeFriends();

      case printFriends_ :
        return printFriends();
    }
    return nullptr;
  }

  private:
  //============================= Private Methods ==============================

  //---------------------------------- init ------------------------------------
  /** Initializes the person's member data.
   *  @param networkValues - a pair<int,int>* where the first item is the number
   *         of first degree friends and the second item is the degree of 
   *         friendship to find.*/
  void* init(void* networkValues);


  //--------------------------- setFirstDegreeFriends --------------------------
  /** Sets up the first degree friends based on the k-regular graph passed in.
   *  @param friendsList - an array of integers stating which Person objects
   *         should be added as first degree friends.*/
  void* setFirstDegreeFriends(void* friendsList);

  
  //---------------------------- prepXthDegreeFriends --------------------------
  /** Prepares each person for passing its xth degree friends to a caller.
   *  @param degree - the current degree of friendship being exchanged.*/
  void* prepXthDegreeFriends(void* degree);


  //-------------------------- exchangeXthDegreeFriends ------------------------
  /** Sends xth degree friends to caller */
  void* exchangeXthDegreeFriends();


  //---------------------------- readXthDegreeFriends --------------------------
  /** Reads xth degree friends received as inMessages and adds them to the 
   *  xth degree friend list.*/
  void* readXthDegreeFriends();

  
  //------------------------------ alreadyFriended -----------------------------
  /** Checks to see if a friend has already been added in a previous degree.
   *  @param friendNum - the friend number we wish to check  */
  bool alreadyFriended(int friendNum) const;

  
  //-------------------------------- printFriends ------------------------------
  /** Prints a list of the person's friends from the first to the xth degree. */
  void* printFriends();
  

  //================================ Member Data ===============================
  int personId;                         // Unique Identifier for each person
  int numFirstDegreeFriends;            // Number of first degree friends
  int currentDegree;                    // Stores degree of current list pass
  vector<unordered_set<int>> friends;   // Sets of friends of each degree
  ostringstream logText;                // Output Text for MASS logs
};

#endif //PERSON_H