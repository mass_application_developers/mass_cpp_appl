#include "Person.h"

extern "C" Place* instantiate(void* argument) {
	return new Person(argument);
}

extern "C" void destroy(Place* obj) {
	delete obj;
}


//----------------------------------- init -------------------------------------
/** Initializes the person's member data.
 *  @param networkValues - a pair<int,int>* where the first item is the number
 *         of first degree friends and the second item is the degree of 
 *         friendship to find.*/
void* Person::init(void* networkValues) {
  personId = index[0]; // Let personId be linear index
  numFirstDegreeFriends = (*(pair<int,int>*)networkValues).first;
  int maxDegree = (*(pair<int,int>*)networkValues).second;
  friends.resize(maxDegree + 1);
}


//---------------------------- setFirstDegreeFriends ---------------------------
/** Sets up the first degree friends based on the k-regular graph passed in.
 *  @param friendsList - an array of integers stating which Person objects
 *         should be added as first degree friends.*/
void* Person::setFirstDegreeFriends(void* friendsList) {
  int* myFriends = (int*)friendsList;
  int friendIndex = personId * numFirstDegreeFriends;
  for (int friendNum = 0; friendNum < numFirstDegreeFriends; friendNum++, friendIndex++) {
    friends[1].insert(myFriends[friendIndex]);
    int* neighbor;
    *neighbor = myFriends[friendIndex] - index[0];
    addNeighbor(neighbor, 1);
  }
}


//----------------------------- prepXthDegreeFriends ---------------------------
/** Prepares each person for passing its xth degree friends to a caller.
 *  @param degree - the current degree of friendship being exchanged.*/
void* Person::prepXthDegreeFriends(void* degree) {
  cleanInMessages();
  currentDegree = *(int*)degree;
  int* temp = new int(*(int*)degree);
  outMessage = (void*)temp;
  outMessage_size = sizeof(int);
  inMessage_size = sizeof(int) * (friends[currentDegree - 1].size() + 1);
}


//--------------------------- exchangeXthDegreeFriends -------------------------
/** Sends xth degree friends to caller */
void* Person::exchangeXthDegreeFriends() {
  int arrSize = friends[currentDegree-1].size() + 1;
  int* xthDegreeFriends = new int[arrSize];
  xthDegreeFriends[0] = arrSize;
  unordered_set<int>::iterator it = friends[currentDegree - 1].begin();
  for (int curFriend = 1; curFriend < arrSize; curFriend++, it++) {
    xthDegreeFriends[curFriend] = *it;
  }
  return (void*)xthDegreeFriends;
}


//----------------------------- readXthDegreeFriends ---------------------------
/** Reads xth degree friends received as inMessages and adds them to the 
 *  xth degree friend list.*/
void* Person::readXthDegreeFriends() {
  for (int msg = 0; msg < inMessages.size(); msg++) {
    int* friendList = (int*)inMessages[msg];
    int listSize = friendList[0];
    for (int f = 1; f < listSize; f++) {
      if (friendList[f] != personId && !alreadyFriended(friendList[f])) {
        friends[currentDegree].insert(friendList[f]);
      }
    }
  }
}


//------------------------------- alreadyFriended ------------------------------
/** Checks to see if a friend has already been added in a previous degree.
 *  @param friendNum - the friend number we wish to check  */
bool Person::alreadyFriended(int friendNum) const {
  bool alreadyFound = false;
  for (int degree = 1; degree < friends.size(); degree++) {
    if (friends[degree].find(friendNum) != friends[degree].end()) {
      alreadyFound = true;
      break;
    }
  }
  return alreadyFound;
}


//--------------------------------- printFriends -------------------------------
/** Prints a list of the person's friends from the first to the xth degree. */
void* Person::printFriends() {
  logText.str("");
  logText << "Person " << personId << "'s Friends:" << endl;
  for (int degree = 1; degree < friends.size(); degree++) {
    logText << "    Degree " << degree << ": ";
    for (unordered_set<int>::iterator it = friends[degree].begin(); it != friends[degree].end(); it++) {
      logText << *it << ", ";
    }
    logText << endl;
  }
  MASS_base::log(logText.str());
}