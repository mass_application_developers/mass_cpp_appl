LD_LIBRARY_PATH=/home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_core/ubuntu
export LD_LIBRARY_PATH

echo "Number of nodes? {1, 2, 4, 8, 16}: "
read NUMNODES 

echo "Number of threads? {1, 2, 4}: "
read NUMTHREADS

echo "Port number? "
read PORT

echo "Password? "
read -s PASSWORD

rm output.txt

head -$(($NUMNODES-1)) machinefile.txt > .tempmachinefile.txt

./main $USER $PASSWORD .tempmachinefile.txt $PORT $NUMNODES $NUMTHREADS 6000 30 -1 60

rm .tempmachinefile.txt