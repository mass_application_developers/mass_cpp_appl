LD_LIBRARY_PATH=/home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_core/ubuntu
export LD_LIBRARY_PATH

echo "Number of nodes? {1, 2, 4, 8, 16}: "
read NUMNODES 

echo "Number of threads? {1, 2, 4}: "
read NUMTHREADS

echo "Number of people in the social network? " # n
read NUMPEOPLE

echo "Number of first-degree friends each person has? " # k
read NUMFRIENDS

echo "Fraction of the total group each person is friends with? " # m
read FRACTGROUP

echo "Degree of friendship we wish to find? " # x
read DEGREE

echo "Port number? "
read PORT

echo "Password? "
read -s PASSWORD

rm output.txt

head -$(($NUMNODES-1)) machinefile.txt > .tempmachinefile.txt

./main $USER $PASSWORD .tempmachinefile.txt $PORT $NUMNODES $NUMTHREADS $NUMPEOPLE $NUMFRIENDS $FRACTGROUP $DEGREE &> output.txt

rm .tempmachinefile.txt