/*==============================================================================
  Social Network - MASS C++ Benchmark Program
  Author: Ian Dudder
================================================================================
  The Social Network simulation models degrees of friendship between people in
  a social network. For example, a friend of a friend is a second degree 
  friendship. 

  In addition to the usual MASS arguments, the inputs are the number of people 
  in the network, the number of first-degree friends each person has, and the 
  degree of friendship we wish to find. If you would like to specify a fraction
  of the group that each person is friends with as opposed to a specific value,
  then provide -1 for the number of first-degree friends each person has and
  provide a value > 0 for the fraction of the total group to be friends with.

  The output of this program is a list of each person's friends from the 1st to
  the xth degree.
==============================================================================*/

#include "MASS.h"
#include "Timer.h"	  
#include "Person.h"
#include <stdlib.h>   

using namespace std;

Timer timer;            // Keeps track of program runtime for evaluation
ostringstream logText;  // Used to print output logs


//-------------------------- createKRegularGraph -------------------------------
/** Creates a k-regular graph given a number of vertices and edges. Directly
 *  manipulates the graph passed in.*/
void createKRegularGraph(int** graph, int numVertices, int numEdges);

//--------------------------- addEdgesXStepsAway -------------------------------
/** Conceptually arranges the vertices in a circle and forms an edge between two 
 *  vertices X steps apart. */
void addEdgesXStepsAway(int numStepsAway, int** graph, int numVertices, 
  int numEdges);

//------------------------------- addEdge --------------------------------------
/** Helper method to form edges between two vertices.*/
bool addEdge(int source, int dest, int** graph, int numEdges);


int main( int argc, char *args[] ) {
	if ( argc != 11 ) /*Make sure the correct arguments were given*/ {
		cerr << "usage: ./main $USER $PASSWORD machinefile.txt $PORT $NUMNODES "
      << "$NUMTHREADS $NUMPEOPLE $NUMFRIENDS $FRACTGROUP &> output.txt" << endl;
		return -1;
	}
  
	//---------------------- Get the arguments passed in -------------------------
	char *arguments[4];
	arguments[0] = args[1];             // Username
	arguments[1] = args[2];             // Password
	arguments[2] = args[3];             // machinefile
	arguments[3] = args[4];             // Port Number
	int nProc = atoi( args[5] );        // number of processes
	int nThr = atoi( args[6] );         // number of threads
	int numPeople = atoi( args[7] );    // Number of people in the network
  int numFriends = atoi( args[8] );   // Number of friends each person has
  int fracGroup = atoi( args[9] );    // Fraction of group to be friends with
  int maxDegree = atoi(args[10]);     // Degree of friendship we want to find

  //---------------------------- Validate Input --------------------------------
  // If numFriends was not specified, look for the fractional argument instead
  if (numFriends == -1) 
    numFriends = numPeople / fracGroup; // k = n / m

  // Make sure numFriends or numPeople (or both) are even integers
  if (numFriends % 2 != 0 && numPeople % 2 != 0) {
    cerr << "Error: Invalid input provided. Either the number of people or the"
      << " number of friends (or both) must be an even integer." << endl;
    return -1;
  }

	/* Initialize MASS */
	MASS::init( arguments, nProc, nThr ); 
  timer.start(); // Evaluation timer starts now

  //------------------------- Create K-Regular Graph ---------------------------
  // Create the graph as a contiguous array of space (to be passed to places)
  int* friendsList = new int[numPeople * numFriends]; 
  for (int f = 0; f < numPeople * numFriends; f++) {
    friendsList[f] = -1;
  }
  createKRegularGraph(&friendsList, numPeople, numFriends);

  //------------------------ Initialize Social Network -------------------------
  char* dummyArg = new char('D');
  Places* socialNetwork = new Places(1, "Person", 1, dummyArg, 1, 1, numPeople);
  pair<int,int>* initArgs = new pair<int,int>(numFriends, maxDegree);
  socialNetwork->callAll(Person::init_, initArgs, sizeof(pair<int,int>));
  socialNetwork->callAll(Person::setFirstDegreeFriends_, (void*)friendsList, 
    sizeof(int) * numPeople * numFriends);

  //---------------------------- Simulation Loop -------------------------------
  int* degree = new int;
  for (*degree = 2; *degree <= maxDegree; (*degree)++) {
    socialNetwork->callAll(Person::prepXthDegreeFriends_, (void*)degree, sizeof(int));
    socialNetwork->exchangeAll(1, Person::exchangeXthDegreeFriends_);
    socialNetwork->callAll(Person::readXthDegreeFriends_, (void*)degree, sizeof(int));
  }
  //------------------------------- Final Steps --------------------------------
  socialNetwork->callAll(Person::printFriends_, dummyArg, 1); // Output Friends List	
	long elaspedTime_END = timer.lap();
  logText.str("");
  logText << endl << "End of simulation. Elapsed time using MASS framework with " 
    << nProc << " processes and " << nThr << " threads to degree " << maxDegree 
    << " of friendship is: " << elaspedTime_END << endl << endl;
  MASS_base::log(logText.str());

	MASS::finish( );
  return 0;
}

//-------------------------- createKRegularGraph -------------------------------
void createKRegularGraph(int** graph, int numVertices, int numEdges) {
  if (numVertices % 2 == 0) /*Case 1: numVertices (n) is even*/ {
    if (numEdges % 2 == 0) /*Case 1a: numEdges (k) is also even*/ {
      int numEvens = numEdges / 2; // Number of even integers in the interval [1,numEdges]
      for (int i = 0; i < numEvens; i++) {
        addEdgesXStepsAway(i + 1, graph, numVertices, numEdges);
      }
    }
    else /*Case 1b: k is odd*/ {
      int numOdds = ((numEdges - 1) / 2) + 1; // Number of odd integers in the interval [1,numEdges]
      for (int i = 0; i < numOdds; i++) {
        addEdgesXStepsAway((numVertices / 2) - i, graph, numVertices, numEdges);
      }
    }
  }
  else /*Case 2: n is odd*/ {
    for (int i = 1; i < numEdges; i++) {
      if (i % 2 == 1) {
        addEdgesXStepsAway(i, graph, numVertices, numEdges);
      }
    }
  }
}


//--------------------------- addEdgesXStepsAway -------------------------------
void addEdgesXStepsAway(int numStepsAway, int** graph, int numVertices, int numEdges) {
  for (int sourceVtx = 0; sourceVtx < numVertices; sourceVtx++) {
    int destVtx = (sourceVtx + numStepsAway) % numVertices;
    if (addEdge(sourceVtx, destVtx, graph, numEdges)) /*Source adds Dest*/ {
      addEdge(destVtx, sourceVtx, graph, numEdges); // Dest adds source back
    }
  }
}

//------------------------------- addEdge --------------------------------------
bool addEdge(int source, int dest, int** graph, int numEdges) {
  int edgeIndex = -1;
  int srcStart = source * numEdges;
  for (int curEdge = srcStart; curEdge < srcStart + numEdges; curEdge++) {
    if ((*graph)[curEdge] == dest) /*Already Added*/ {
      break;
    }
    else if ((*graph)[curEdge] == -1) /*Found open slot*/ {
      edgeIndex = curEdge;
      break;
    }
  }
  bool addingEdge = (edgeIndex != -1);
  if (addingEdge) {
    (*graph)[edgeIndex] = dest;
  }
  return addingEdge;
}