# Social Network

Social Network is a simulation that models degrees of friendship between people in a social group. The social network is represented conceptually by a graph, where people are vertices and edges are friendships. The social network will always be a k-regular graph, so each person is guaranteed to have the same number of first-degree friends.


## Configuring the Program Files in Your Directory

Running the benchmark programs relies on having the compile.sh and run.sh files point to the mass_cpp_core folder and requires symbolic links in your directory to three library files in mass_cpp_core/ubuntu.

1.	Create symbolic links to the following three files in the mass_cpp_core directory:
    *	/mass_cpp_core/ubuntu/libmass.so
    *	/mass_cpp_core/ubuntu/mprocess
    *	/mass_cpp_core/ubuntu/killMProcess.sh 
2.	Find compile.sh in the benchmark program’s directory. Edit line 2 of this file to point to the mass_cpp_core directory in relation to compile.sh’s location. For example, if the mass_cpp_core folder is located one directory back from compile.sh, the line will read “export MASS_DIR=../mass_cpp_core”.
3.	Find run.sh in the benchmark program’s directory. Edit line 1 of this file to point to the mass_cpp_core/ubuntu folder in relation to run.sh’s location. For example, if the mass_cpp_core folder is located one directory back from run.sh, the line will read “LD_LIBRARY_PATH=../mass_cpp_core/ubuntu”.
4.	Make sure machinefile.txt lists all the machines you want to establish a connection with during execution. The file provided on Bitbucket lists hermes02-hermes12 and cssmpi1h-cssmpi12h and assumes that hermes01 is the master node. The master node should always be excluded from this file.
5.	Compile the benchmark’s source files with  “./compile.sh” to ensure all executable files are up to date.



## Providing Inputs 
The user will specify how many people will be in the social group and how many friendships each person will have. Alternatively, the user may specify the fraction of the group that each person will be friends with. Either the number of people in the group or the number of friendships must be an even integer (or both must be even integers) for a regular k-graph to be constructed. The user must also provide the degree of friendship they wish to find in the simulation. 


## Output Example 
The output of this simulation will list each person's friends from the 1st to the xth degree.

Example:
```
Person 0's Friends:

Degree 1    : 1, 5, 8, 10

Degree 2     : 2, 3, 4, .... 21

Degree 3     : 7, 11, 20, 30, 31, ...... 52
```

## Running the Program

Social Network does not require any text files as input. Therefore, you can immediately run the following instructions after compiling your program:

1.	Execute run.sh with “./run.sh”. 
2.	Enter the following arguments when prompted:
    *	Number of Nodes – the number of computing nodes (machines) you want to use to run your program.
    *	Number of Threads – the number of threads to use.
    *	Number of People – the number of people you want in the social network (number of vertices in the k-regular graph). Note that either the number of people or the number of first-degree friends (or both) must be an even number for a k-regular graph to be generated.
    *	Number of first-degree friends – the number of first-degree friends you want each person to have (the number of edges in the k-regular graph). Note that either the number of people or the number of first-degree friends (or both) must be an even number for a k-regular graph to be generated.
    *	Fraction of the group – this value will be used to calculate the number of first-degree friends in the social network by dividing the number of people in the group by this number. If you already entered a value for the number of first-degree friends, you can enter -1 here to have this argument ignored.
    *	Port Number - the port number you want to use for communicating between remote computing nodes.
    *	Password - your UW password for the hermes machines. It is recommended that you establish an RSA key so that you can authenticate remote computing nodes without having to pass your password between remote computing nodes. 
3.	If the program terminates for any reason before MASS::finish is called, execute ./killMProcess.sh to clean up the processes on the other computing nodes before attempting to execute again.


## File Descriptions 

The files in this repository (in alphabetical order) are...

* 1thr, 2thr, 3thr, 4thr - the output files used to collect evaluative data for my presentation. Provide examples of good runs on each thread.

* compile.sh - Compiles your program into executables to run. Simply use "./compile.sh" to run. Note: Line 2 of this file must have the correct path to the mass_cpp_core library.

* evalrun.sh - Run file used to collect evaluative data for my presentation. Hard codes simulation inputs.

* killMProcess.sh - kills any running processes if the program terminates before MASS::finish() was called to delete running processes on other computing nodes. This file must be a symbolic link to "mass_cpp_core/ubuntu/killMProcess.sh".

* libmass.so - A symbolic link to "mass_cpp_core/ubuntu/libmass.so"

* machinefile.txt - Lists the names of all computers we want to establish a connection with to use as a remote computing node. Note that the master node should be excluded from this file.

* main.cpp - The main driver of the simulation.

* mprocess - A symbolic link to "mass_cpp_core/ubuntu/mprocess".

* Person.h, Person.cpp - Source files for the Place-derived Person class.

* run.sh - Runs the benchmark program once all the source files have been compiled. Note that line 1 of this file must have the correct path to the mass_cpp_core library files.

* Timer.h, Timer.cpp - Keeps track of the program's runtime. Useful for evaluation.
