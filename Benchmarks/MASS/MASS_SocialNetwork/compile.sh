#!/bin/sh
export MASS_DIR=/home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_core/

rm Person
rm main

g++ -Wall Person.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o Person
g++ -std=c++11 -Wall main.cpp Timer.cpp -I$MASS_DIR/source -L$MASS_DIR/ubuntu -lmass -I$MASS_DIR/ubuntu/ssh2/include -L$MASS_DIR/ubuntu/ssh2/lib -lssh2 -o main

echo "Compiled files: "
ls Person main