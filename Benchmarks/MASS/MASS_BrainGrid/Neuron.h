/* Craig Shih
* 12/11/16
* Program 4
* Life.h
*
* This is the Life.h file that operates the game of life program
* which utilizes the MASS library
*/

#ifndef Neuron_H
#define Neuron_H

#include <string.h>
#include "Place.h"

using namespace std;

class Neuron : public Place {
public:

	static const int NUM_NEIGHBORS = 8;									//A life has 8 neighbors
  
  // define functionId's that will 'point' to the functions they represent.
  static const int init_ = 0;
  static const int sendSynapses_ = 1;
  static const int computeSynapses_ = 2;
  static const int displayStageStatus_ = 3;
  static const int displayTypeStatus_ = 4;
  static const int displayIncomingDirectionStatus_ = 5;
  static const int getBoundaryStageStatus_ = 6;
  static const int getBoundaryTypeStatus_ = 7;
  static const int getBoundaryIncomingDirectionStatus_ = 8;
  
  /**
   * Initialize a Life object by allocating memory for it.
   */
  Neuron( void *argument ) : Place( argument ) {
    bzero( arg, sizeof( arg ) );
    strcpy( arg, (char *)argument );
  };
  
  /**
   * the callMethod uses the function ID to determine which method to execute.
   * It is assumed the arguments passed in contain everything those 
   * methods need to run.
   */
  virtual void *callMethod( int functionId, void *argument ) {
    switch( functionId ) {
      case init_: return init( argument );
      case sendSynapses_: return sendSynapses();
      case computeSynapses_: return computeSynapses();
      case displayStageStatus_: return displayStageStatus();
      case displayTypeStatus_: return displayTypeStatus();
      case displayIncomingDirectionStatus_: return displayIncomingDirectionStatus();
      case getBoundaryStageStatus_: return getBoundaryStageStatus();
      case getBoundaryTypeStatus_: return getBoundaryTypeStatus();
      case getBoundaryIncomingDirectionStatus_: return getBoundaryIncomingDirectionStatus();
    }
    return NULL;
  };

private:
  char arg[100];
  void *init( void *argument );
  void *sendSynapses();
  void *computeSynapses();
  void *displayStageStatus();										//Set the outmessage as population of this life
  void *displayTypeStatus();										//Set the outmessage as population of this life								
  void *displayIncomingDirectionStatus();
  void *getBoundaryStageStatus();
  void *getBoundaryTypeStatus();
  void *getBoundaryIncomingDirectionStatus();
  
  //PartitionMove
  void *getBoundaryHealthStatus();									//Based on neighbor population, calculate nextMove and save it 		#partition-space
  
  vector<int*> cardinals;										//Vector form of cardinals
  static const int neighbor[8][2];								//Array form of cardinals
  enum Type { ACTIVE = 0, INACTIVE, NEUTRAL };
  Type type;
  enum Stage { VISIT1 = 0, VISIT2, ENACTED, CONNECTED, STOPPED, INDEF };
  Stage stage;
  int neighborStageStatus[8];											//An array to store int health status of neighbors
  int neighborTypeStatus[8];											//An array to store int health status of neighbors
  int neighborSynapseStatus[8];
  int neighborIncomingDirection[8];
  bool currStage;
  int incomingDirection;
  int neuronSynapseNum;
};

#endif
