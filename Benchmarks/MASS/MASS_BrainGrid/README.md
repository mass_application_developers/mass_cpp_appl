# BrainGrid

## Configuring the Program Files in Your Directory

Running the benchmark programs relies on having the compile.sh and run.sh files point to the mass_cpp_core folder and requires symbolic links in your directory to three library files in mass_cpp_core/ubuntu.

1.	Create symbolic links to the following three files in the mass_cpp_core directory:
    *	/mass_cpp_core/ubuntu/libmass.so
    *	/mass_cpp_core/ubuntu/mprocess
    *	/mass_cpp_core/ubuntu/killMProcess.sh 
2.	Find compile.sh in the benchmark program’s directory. Edit line 2 of this file to point to the mass_cpp_core directory in relation to compile.sh’s location. For example, if the mass_cpp_core folder is located one directory back from compile.sh, the line will read “export MASS_DIR=../mass_cpp_core”.
3.	Find run.sh in the benchmark program’s directory. Edit line 1 of this file to point to the mass_cpp_core/ubuntu folder in relation to run.sh’s location. For example, if the mass_cpp_core folder is located one directory back from run.sh, the line will read “LD_LIBRARY_PATH=../mass_cpp_core/ubuntu”.
4.	Make sure machinefile.txt lists all the machines you want to establish a connection with during execution. The file provided on Bitbucket lists hermes02-hermes12 and cssmpi1h-cssmpi12h and assumes that hermes01 is the master node. The master node should always be excluded from this file.
5.	Compile the benchmark’s source files with  “./compile.sh” to ensure all executable files are up to date.

## Providing Inputs 
The user will specify the length of the game in turns and the size of the game along the x- and y-axes.

## Running the Program

BrainGrid does not require any text files as input. Therefore, you can immediately run the following instructions after compiling your program:

1.	Execute run.sh with “./run.sh”. 
2.	Enter the following arguments when prompted:
    *	Number of Nodes – the number of computing nodes (machines) you want to use to run your program. Can choose between 1, 2, 4, 8, 16.
    *	Number of Threads – the number of threads to use. Can choose between 1, 2, 4.
    *	Number of Turns – the number of turns you want the BrainGrid simulation to go on for
    *	X size - the size of the simulation along the x-axis
    *   Y size - the size of the simulation along the y-axis
    *	Port Number - the port number you want to use for communicating between remote computing nodes.
    *	Password - your UW password for the hermes machines. It is recommended that you establish an RSA key so that you can authenticate remote computing nodes without having to pass your password between remote computing nodes. 
3.	If the program terminates for any reason before MASS::finish is called, execute ./killMProcess.sh to clean up the processes on the other computing nodes before attempting to execute again.