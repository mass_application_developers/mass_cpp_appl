#include "Task.h"

using namespace std;

//--------------------------- Default Constructor ----------------------------
// Initializes the task with default values.
Task::Task() {
  type = priority = totalHours = taskId = collabStartDay = -1;
}

//------------------------------- Constructor --------------------------------
// Initializes the member data with the specified values.
Task::Task(int type_, int priority_, int totalHours_, int taskId_) {
  type = type_;
  priority = priority_;
  totalHours = totalHours_;
  taskId = taskId_;
  collabStartDay = -1;
}

//---------------------------- Copy Constructor ------------------------------
/** Initializes the task with default values.
 *  @param toCopy - a Task object that we wish to create a deep copy of. */
Task::Task(Task* toCopy) {
  type = toCopy->type;
  priority = toCopy->priority;
  totalHours = toCopy->totalHours;
  taskId = toCopy->taskId;
  collabStartDay = toCopy->collabStartDay;
  for (int hr = 0; hr < 5; hr++)
    hours[hr] = toCopy->hours[hr];
}

//------------------------------ makeProgress ------------------------------------
/** Allows an engineer to make progress on a task by reducing the number of 
  *  hours required from that type of engineer by one hour.
  *  @param engType - the type of engineer (rank of engineer in hierarchy).
  */
bool Task::makeProgress(int engType) {
  if (hours[engType] > 0) {
    hours[engType] -= 1;
    totalHours -= 1;
    return true;
  }
  else {
    return false;
  }
}

//------------------------------ addExtraHours -----------------------------------
/** Adds extra hours for the specified type of engineer.
  *  @param engType - the type of engineer (rank of engineer in hierarchy).
  *  @param additionalHours - the number of hours to be added on for the engineer
  */
bool Task::addExtraHours(int engType, int additionalHours) {
  if (engType >= 0 && engType < 5) {
    hours[engType] = (hours[engType] <= 0) ? additionalHours : hours[engType] + additionalHours;
    totalHours += additionalHours;
    return true;
  }
  else {
    return false;
  }
}


/*================================= Getters ===============================*/
int Task::getType() const { return type; }

int Task::getPriority() const { return priority; }

int Task::getTotalHours() const { return totalHours; }

int Task::getTaskId() const { return taskId; }

int Task::getCollabDay() const { return collabStartDay; }


int Task::getCollabHour() const {
  int collabHour = -1;
  for (int hrIndex = 0; hrIndex < 5; hrIndex++) {
    if (hours[hrIndex] != -1) {
      collabHour = hours[hrIndex];
      break;
    }
  }
  return collabHour;
}


int Task::getEngrHours(int engrLvl) const { return hours[engrLvl]; }


/*================================= Setters ===============================*/
void Task::setType(int type_) { type = type_; }

void Task::setPriority(int priority_) { priority = priority_; }

void Task::setTotalHours(int totalHours_) { totalHours = totalHours_; }

void Task::setTaskId(int taskId_) { taskId = taskId_; }

void Task::setCollabDay(int day) { collabStartDay = day; }

bool Task::setHours(int engineerLevel, int numHours) {
  bool foundEngr = false;
  if (engineerLevel >= 0 && engineerLevel < 5) {
    hours[engineerLevel] = numHours;
    foundEngr = true;
  }
  return foundEngr;
}


