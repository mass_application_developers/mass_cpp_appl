#include "Team.h"

extern "C" Place* instantiate(void* argument) {
  return new Team(argument);
}

extern "C" void destroy(Place* teamObj) {
  delete teamObj;
}


//---------------------------------- init ------------------------------------
/** Initializes the Team to default values
  *  @param arrSizes - an integer array of size 3 that gives [0] the number of
  *         production tasks, [1] the number of collaboration tasks, and [2] 
  *         the number of unique team configurations.                   
  */
void* Team::init(void* arrSizes) {
  numProductionTasks = ((int*)arrSizes)[0];
  numCollabTasks = ((int*)arrSizes)[1];
  configNum = getLinearIndex() % ((int*)arrSizes)[2];
  finished = false;
  day = hour = 0;
  productionTasks.resize(numEngTypes);
  activeCollabTask = nullptr;
  srand(index[0] * size[0] + index[1]);
}


//------------------------------ initProduction --------------------------------
/** Initializes the Team's production Tasks.
 *  @param taskArgs - a Task* that ponts to an array of Tasks of size 
 *         numProductionTasks.                          
 */
void* Team::initProduction(void* tasks) {
  Task* tasklist = (Task*)tasks;
  for (int curTask = 0; curTask < numProductionTasks; curTask++) {
    Task* task = new Task(tasklist[curTask]); // Clone task
    if (task->getTotalHours() > 0) {
      bool taskInserted = false;
      for (int trayLvl = 0; trayLvl < numEngTypes && !taskInserted; trayLvl++) {
        if (task->getEngrHours(trayLvl) > 0) /*Insert at highest level with hours*/ {
          productionTasks[trayLvl].push_back(task);
          taskInserted = true;
        }
      }
      if (!taskInserted) {
        delete task;
      }
    }
    else {
      completedTasks.push_back(task);
    }
  }
}


//------------------------------- initCollabs --------------------------------
/** Initializes the Team's collaborative Tasks.
  *  @param tasklist - a Task* that points to an array of Tasks of size
  *         numCollabTasks.                          
  */
void* Team::initCollabs(void* tasks) {
  Task* tasklist = (Task*)tasks;
  for (int curTask = 0; curTask < numCollabTasks; curTask++) {
    Task* task = new Task(tasklist[curTask]);
    collabTasks.push_back(task);
  }
  sort(begin(collabTasks), end(collabTasks), [](Task* a, Task* b) {
    if (a->getCollabDay() > b->getCollabDay()) {
      return true;
    }
    else if (a->getCollabDay() == b->getCollabDay() && a->getCollabHour() > b->getCollabHour()) {
      return true;
    } 
    else {
      return false;
    }
  });
  return nullptr;
}


//------------------------------ initTeamConfig --------------------------------
/** Configures the Team to have the correct number of engineers of each type
  *  @param configOptions - a TeamConfig array providing the different team
  *         configurations. Each team will choose the team configuration based
  *         on its linear index in the Teams Matrix.                        
  */
void* Team::initTeamConfig(void* configOptions) {
  TeamConfig* conf = (TeamConfig*)configOptions;
  config[0] = conf[configNum].numLeads;
  config[1] = conf[configNum].numUx;
  config[2] = conf[configNum].numSr;
  config[3] = conf[configNum].numJr;
  config[4] = conf[configNum].numTesters;
}


//------------------------------- addEngineer ----------------------------------
/** Helps the Engineer Agents determine their type. This method will mutex 
 *  lock and search through the 'config' vector and stop at the first entry
 *  that is less than 5. Then it will increment the entry and return the index
 *  back to the Engineer to indicate the Engineer type they should be.      
 *  @return - returns an integer corresponding to an Engineer type.         
 */
void* Team::addEngineer() {
  int typeToReturn = -1;
  mtx.lock(); 
  for (int engType = 0; engType < numEngTypes; engType++) {
    if (config[engType] > 0) {
      --(config[engType]);
      typeToReturn = engType;
      break;
    }
  }
  mtx.unlock();
  return (void*)&typeToReturn;
}


//------------------------------- assignTask -----------------------------------
/** Gets a Task of the calling engineer's level from any of the active 
 *  projects.
 *  @param engType - the type of the engineer making the call.
 *  @return - returns a Task* for the calling Engineer's level.                               
 */
void* Team::assignTask(void* engType) {
  Task* selectedTask = nullptr;
  mtx.lock();
  if (productionTasks[*(int*)engType].size() > 0) {
    int selectionFactor = rand() % 100;
    // Select Highest Priority Task (50% chance)
    if (selectionFactor >= 0 && selectionFactor < 50) {
      int highestPriority  = -1;
      deque<Task*>::iterator bestTask;
      for (deque<Task*>::iterator it = productionTasks[*(int*)engType].begin();
        it != productionTasks[*(int*)engType].end(); it++) {
        if ((*it)->getPriority() > highestPriority) {
          bestTask = it;
          highestPriority = (*it)->getPriority();
        }
      }
      selectedTask = *bestTask;                        // Select Task
      productionTasks[*(int*)engType].erase(bestTask); // Remove from Tray
    }

    // Select Oldest Task (20% chance)
    else if (selectionFactor >= 50 && selectionFactor < 70) {
      selectedTask = productionTasks[*(int*)engType].front(); // Select Task
      productionTasks[*(int*)engType].pop_front();            // Remove from tray
    }

    // Select Task Randomly (10%)
    else if (selectionFactor >= 70 && selectionFactor < 80) {
      int randIndex = rand() % productionTasks[*(int*)engType].size();
      selectedTask = productionTasks[*(int*)engType].at(randIndex);   // Select Task
      productionTasks[*(int*)engType].erase(productionTasks[*(int*)engType].begin() + randIndex); // Remove from tray
    }

    // Select Most Recent Task
    else if (selectionFactor >= 80 && selectionFactor < 100) {
      selectedTask = productionTasks[*(int*)engType].back();  // Select Task
      productionTasks[*(int*)engType].pop_back();             // Remove from tray
    }
  }
  mtx.unlock();
  return (void*)selectedTask;
}


//-------------------------------- checkCollab ----------------------------------
/** Checks if it is time for a collaboration event to start. If there is, it
 *  sets the activeCollabTask to point to the ongoing collaboration task.                        
 */
void* Team::checkCollab() {
  if (!collabTasks.empty() && day == collabTasks.back()->getCollabDay() 
    && hour == collabTasks.back()->getCollabHour()) {
    activeCollabTask = collabTasks.back();
    collabTasks.pop_back();
  }
}


//--------------------------------- getCollab -----------------------------------
/** Checks if the calling engineer must attend a collaboration task. 
 *  @param engType - the type of the engineer making the call.
 *  @return - returns an integer saying how long the engineer must collaborate
 *            for. Returns 0 when the engineer does not have to attend a collab                             
 */
void* Team::getCollab(void* engType) {
  int *duration = new int(0);
  if (activeCollabTask != nullptr) {
    mtx.lock();
    if (activeCollabTask->getEngrHours(*(int*)engType) > 0) {
      *duration = activeCollabTask->getTotalHours();  // Collaboration Task length
      activeCollabTask->setHours(*(int*)engType, 0); // Assign only one type of each engineer
    }
    mtx.unlock();
  }
  return (void*)duration;
}


//-------------------------------- sendToTray ----------------------------------
/** Sends the received task to the appropriate tray.
 *  @param task - the task to be returned to a tray.
 */
void* Team::sendToTray(void* task) {
  Task* t = (Task*)task;
  if (t->getTotalHours() == 0) /*Task is complete*/ {
    mtx.lock();
    completedTasks.push_back(t);
    mtx.unlock();
  }
  else /*Task is not complete*/ {
    int trayLevel = -1;
    // Find highest engineer in the hierarchy with hours left to complete
    for (int engLevel = 0; engLevel < numEngTypes; engLevel++) {
      if (t->getEngrHours(engLevel) > 0) {
        trayLevel = engLevel;
        break;
      }
    }
    // Return to the correct engineer's tray
    if (trayLevel != -1)
      mtx.lock();
      productionTasks[trayLevel].push_back(t);
      mtx.unlock();
  }
}


//----------------------------------- act --------------------------------------
/** Carries out the necessary tasks for an hour in the simulation.       
 */
void* Team::act() {
  if (!finished) {
    if (completedTasks.size() == numCollabTasks + numProductionTasks) {
      finished = true;
    }
    else {
      incrementTime();  // Advance day and hour
      if (activeCollabTask != nullptr) /*Mark finished collab tasks*/ {
        activeCollabTask->setTotalHours(activeCollabTask->getTotalHours() - 1);
        if (activeCollabTask->getTotalHours() <= 0) {
          completedTasks.push_back(activeCollabTask);
          activeCollabTask = nullptr;
        }
      }
    } 
  }
}


//-------------------------------- isFinished ----------------------------------
/** Checks through the status of all projects to see if all projects are 
 *  complete.
 *  @return - Returns true if all projects are complete. Returns false if any
 *            task still needs work.                                        
 */
bool* Team::isFinished() const {
  return (bool*)&finished;
}


//------------------------------ incrementTime ---------------------------------
/** Advances the current hour by 1 and increments the day as needed.        
 */
void Team::incrementTime() {
  hour++;
  if (hour >= 24) {
    hour = 0;
    day++;
  }
}


//----------------------------- getLinearIndex ---------------------------------
// Returns the linear index in the Teams Matrix
int Team::getLinearIndex() const {
  return index[0] * size[0] + index[1];
}


//------------------------------- reportProgress -------------------------------
/** Reports progress on project to main.
 *  @return - an integer reporting the total number of hours it took to finish
 *            all tasks if it has done so already. Returns -1 if not all tasks
 *            are finished.                       
 */
void* Team::checkProgress() {
  int* hoursTaken = new int;
  if (finished) {
    *hoursTaken = hour + (day * 24);
  }
  else {
    *hoursTaken = -1;
  }
  return hoursTaken;
}