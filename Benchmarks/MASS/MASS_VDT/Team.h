/*==============================================================================
  Team Class
  Author: Ian Dudder
================================================================================
  The Team Class takes the role of a Place in the simulation with 25 Engineer
  Agents residing on it. Each team keeps track of the tasks it was assigned and
  monitors its progress on completing all of them. Additionally, this class
  maintains the current hour and day for the simulation to keep all Engineers
  synchronized.
==============================================================================*/

#ifndef TEAM_H
#define TEAM_H

#include <iostream>
#include "Place.h"
#include "MASS_base.h"
#include "Task.h"
#include <vector>
#include <deque>

class Team : public Place {
public:
  //============================== Helper Structs ==============================
  struct TeamConfig {
    int numLeads;
    int numUx;
    int numSr;
    int numJr;
    int numTesters;
  };

  static const int numEngTypes = 5;      // Number of different types of engineers
  static const int numEngPerTeam = 25;   // Total number of Engineers on a team

  //=============================== Function Ids ===============================
  static const int init_ = 0;           // Initializes default values
  static const int initProduction_ = 1; // Initializes production Tasks
  static const int initCollabs_ = 2;    // Initializes collaborative Tasks
  static const int initTeamConfig_ = 3; // Initializes configuration of team
  static const int addEngineer_ = 4;    // Helps create the Engineer Hierarchy
  static const int checkCollab_ = 5;    // Commences a collaboration event
  static const int getCollab_ = 6;      // Assigns a collaboration task to an engineer
  static const int assignTask_ = 7;     // Assigns a new task to an Engineer
  static const int sendToTray_ = 8;     // Returns a task to a tray
  static const int act_ = 9;            // Acts an hour in the simulation
  static const int isFinished_ = 10;    // Tells if the team has completed all tasks
  static const int checkProgress_ = 11; // Reports if the teams are finished

  //============================== Public Methods ==============================
  Team(void* argument) : Place(argument) {} 

  //------------------------------- Call Method --------------------------------
  virtual void* callMethod(int functionId, void* argument) {
    switch(functionId) {    
      case init_ :
        return init(argument);

      case initProduction_ :
        return initProduction(argument);

      case initCollabs_ :
        return initCollabs(argument);

      case initTeamConfig_ :
        return initTeamConfig(argument);

      case addEngineer_ :
        return addEngineer();

      case checkCollab_ :
        return checkCollab();

      case getCollab_ :
        return getCollab(argument);
        
      case assignTask_ :
        return assignTask(argument);

      case sendToTray_ :
        return sendToTray(argument);

      case act_ :
        return act();

      case isFinished_ :
        return isFinished();

      case checkProgress_ :
        return checkProgress();
    }
    return nullptr;
  }

private:
  //=========================== CallMethod Helpers =============================

  //---------------------------------- init ------------------------------------
  /** Initializes the Team to default values
   *  @param arrSizes - an integer array of size 3 that gives [0] the number of
   *         production tasks, [1] the number of collaboration tasks, and [2] 
   *         the number of unique team configurations.                   
   */
  void* init(void* arrSizes);


  //----------------------------- initProduction -------------------------------
  /** Initializes the Team's production Tasks.
   *  @param taskArgs - a Task* that ponts to an array of Tasks of size 
   *         numProductionTasks.                          
   */
  void* initProduction(void* tasks);

  
  //------------------------------- initCollabs --------------------------------
  /** Initializes the Team's collaborative Tasks.
   *  @param tasklist - a Task* that points to an array of Tasks of size
   *         numCollabTasks.                          
   */
  void* initCollabs(void* tasks);

  
  //----------------------------- initTeamConfig -------------------------------
  /** Configures the Team to have the correct number of engineers of each type
   *  @param configOptions - a TeamConfig array providing the different team
   *         configurations. Each team will choose the team configuration based
   *         on its linear index in the Teams Matrix.                        
   */
  void* initTeamConfig(void* configOptions);


  //------------------------------- addEngineer --------------------------------
  /** Helps the Engineer Agents determine their type. This method will mutex 
   *  lock and search through the 'members' vector and stop at the first entry
   *  that is less than 5. Then it will increment the entry and return the index
   *  back to the Engineer to indicate the Engineer type they should be.      
   *  @return - returns an integer corresponding to an Engineer type.         
   */
  void* addEngineer();

  
  //------------------------------- assignTask ---------------------------------
  /** Gets a Task of the calling engineer's level from any of the active 
   *  projects.
   *  @param engType - the type of the engineer making the call.
   *  @return - returns a Task* for the calling Engineer's level.                               
   */
  void* assignTask(void* engType);


  //------------------------------- checkCollab ---------------------------------
  /** Checks if it is time for a collaboration event to start. If there is, it
   *  sets the activeCollabTask to point to the ongoing collaboration task.                        
   */
  void* checkCollab();


  //-------------------------------- getCollab ----------------------------------
  /** Checks if the calling engineer must attend a collaboration task. 
   *  @param engType - the type of the engineer making the call.
   *  @return - returns an integer saying how long the engineer must collaborate
   *            for. Returns 0 when the engineer does not have to attend a collab                             
   */
  void* getCollab(void* engType);


  //------------------------------- sendToTray ---------------------------------
  /** Sends the received task to the appropriate tray.
   *  @param task - the task to be returned to a tray.
   */
  void* sendToTray(void* task);


  //---------------------------------- act -------------------------------------
  /** Carries out the necessary tasks for a day in the simulation.  */
  void* act();

  
  //------------------------------- isFinished ---------------------------------
  /** Checks through the status of all projects to see if all projects are 
   *  complete.
   *  @return - Returns true if all projects are complete. Returns false if any
   *            task still needs work.                                        
   */
  bool* isFinished() const;


  //------------------------------ reportProgress ------------------------------
  /** Reports progress on project to main.
   *  @return - an integer reporting the total number of hours it took to finish
   *            all tasks if it has done so already. Returns -1 if not all tasks
   *            are finished.                             
   */
  void* checkProgress();


  //============================= Private Methods ==============================

  //----------------------------- incrementTime --------------------------------
  /** Advances the current hour by 1 and increments the day as needed.        
  */
  void incrementTime();
  
  //---------------------------- getLinearIndex --------------------------------
  // Returns the linear index in the Teams Matrix
  int getLinearIndex() const;

  //================================ Member Data ===============================
  int hour;                 // Current hour in the simulation
  int day;                  // Current day of the simulation
  int numProductionTasks;   // Number of production tasks
  int numCollabTasks;       // Number of collaboration tasks
  int configNum;            // Gives the team configuration number
  Task* activeCollabTask;   // Pointer to an in-progress collaboration task.
  bool finished;            // Indicates if all tasks are completed or not
  int config[numEngTypes];  // Helps set up engineers on the team
  vector<deque<Task*>> productionTasks; // Index is engineer type. Value is tray.
  vector<Task*> collabTasks;  // Collaboration Tasks
  vector<Task*> completedTasks; // Tasks that engineers have completed
  ostringstream logText;    // Used to create a MASS log
  mutex mtx;                // Mutex lock to prevent race conditions
};


#endif //TEAM_H