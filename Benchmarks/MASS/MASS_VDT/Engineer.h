/*==============================================================================
  Engineer Class
  Author: Ian Dudder
================================================================================
  The Engineer Class will take the role of Agents in this simulation. There are
  five different types of Engineer Agents:
    Type 0: Project Leads
    Type 1: UX Designers
    Type 2: Senior Software Developers
    Type 3: Junior Software Developers
    Type 4: Test Engineers
  These Engineers will process tasks in a hierarchy structured in the order the
  agents are listed above. Engineer Agents will select a Task to work on and
  work on it until it is complete or they have a collaboration task to tend to.
==============================================================================*/

#ifndef ENGINEER_H
#define ENGINEER_H

#include <iostream>
#include "Agent.h"
#include "MASS_base.h"
#include "Task.h"
#include "Team.h"
#include <unordered_set>


class Engineer : public Agent {
public:
  //=============================== Function Ids ===============================
  static const int init_ = 0;       // Initializes the Engineer. Assigns Types
  static const int work_ = 1;       // Engineer works on a task
  static const int submitTask_ = 2; // Engineer submits a completed task

  //============================== Public Methods ==============================
  Engineer(void* argument) : Agent(argument) {}

  //------------------------------- Call Method --------------------------------
  virtual void* callMethod(int functionId, void* argument) {
    switch(functionId) {
      case init_ :
        return init();

      case work_ :
        return work();
      
      case submitTask_ :
        return submitTask();
    }
    return nullptr;
  }

private:
  //============================= Private Methods ==============================

  //---------------------------------- init ------------------------------------
  // Prompts the Engineer to determine which type of engineer in the hierarchy
  // it should be and initializes the other data members with default values.
  void* init();

  //---------------------------------- work ------------------------------------
  // Has the Engineer Agent work for an hour on a Task.                                  
  void* work();


  //------------------------------- processTask --------------------------------
  // Processes a new task from the tray                 
  void processTask();


  //-------------------------------- submitTask --------------------------------
  /** Returns the task either to the next tray down in the hierarchy or to the
   *  completed task list. */
  void* submitTask();


  //================================ Member Data ===============================
  int type;                 // Type of Engineer Agent (e.g., Lead, Senior, etc.)
  int collabDuration;       // Amount of time the engineer has on a collab task
  Task* currentTask;        // Pointer to the current Task in progress
  bool returnTask;          // Indicates if the task should be returned to tray
  unordered_set<int> processedTasks;  // Tasks that have been selected before 
  ostringstream logText;    // Used to create MASS logs
};

#endif //ENGINEER_H