/*==============================================================================
  Virtual Development Team
  MASS C++ Benchmark Program
  Author: Ian Dudder
================================================================================
  The Virtual Development Team (VDT) benchmark program models teams of software
  engineers completing different tasks for a software project. There are five 
  different types of software engineers ranked in a hierarchy where tasks flow 
  from the highest to lowest rank of engineer before being completed.

  The heirarchy of software engineer types is as follows:
    1. Project Leads
    2. UX Designers
    3. Senior Software Developers
    4. Junior Software Developers
    5. Test Engineers

  Each team is comprised of 25 Engineers total. 

  There are two types of tasks that are completed for a software project: 
  production tasks and collaboration tasks. Production tasks pass through the
  engineer hierarchy from top to bottom with a set number of hours to be 
  completed by each type of engineer. Collaboration tasks on the other hand
  simulate meetings between engineers and require the participatin engineers to
  halt work on their production tasks until the meeting is over.

  The output of this program is a list of completion times for each team in
  the simulation.
==============================================================================*/

#include "MASS.h"
#include "Task.h"
#include "Team.h"       // Place
#include "Engineer.h"   // Agent
#include "Timer.h"	    // Timer
#include <stdlib.h>     // atoi

Timer timer;            // Records runtime 
ostringstream logText;  // Used for MASS logs

//------------------------------- readTasks ------------------------------------
/** Reads in the collaboration and production tasks from a formatted text file.
 *  @param filename - The name of the formatted task file.
 *  @param collabTasks - stores collaboration tasks
 *  @param prodTasks - stores production tasks
 *  @return - returns true if the file could be opened and false if it was not.
 */
bool readTasks(string filename, vector<Task*> *collabTasks, vector<Task*> *prodTasks);


//----------------------------- readTeamConfig ---------------------------------
/** Reads in the different configurations of engineers for teams.
 *  @param filename - the name of the formatted text file providing the team
 *         configurations.
 *  @param configs - a pointer to the variable that will store an array of
 *         TeamConfig objects. Constructed during this function.
 *  @param numConfigs - the number of unique configurations read in.
 */
bool readTeamConfig(string filename, Team::TeamConfig** configs, int *numConfigs);


//--------------------------- allTeamsFinished ---------------------------------
/** Iterates through the array of completion times to check if every team has
 *  finished their tasks.
 *  @param endTimes - the int array of size 'numTeams' that contains the time
 *         in hours that each team completed all tasks. 
 *  @param numTeams - the number of teams in the simulation.
 *  @return - returns true if every team completed all their tasks and false
 *          otherwise.
 */
bool allTeamsFinished(int* endTimes, int numTeams);

//------------------------------------ main ------------------------------------
int main( int argc, char *args[] ) {
	// Check for arguments
	if ( argc != 11 ) {
		cerr << "usage: ./main username password machinefile port nProc nThreads"
      << "sizeX sizeY tasklist.txt teamconfig.txt" << endl;
		return -1;
	}
  
	//---------------------- Get the arguments passed in -------------------------
	char *arguments[4];
	arguments[0] = args[1];               // Username
	arguments[1] = args[2];               // Password
	arguments[2] = args[3];               // machinefile
	arguments[3] = args[4];               // Port Number
	int nProc = atoi( args[5] );          // number of processes
	int nThr = atoi( args[6] );           // number of threads
	const int sizeX = atoi( args[7] );    // Number of columns in Place
	const int sizeY = atoi( args[8] );    // Number of rows in Place
  const string taskFile = args[9];     // Tasklist file name
  const string configFile = args[10];   // Team Configuration file name
  char* dummyArg = " ";                 
  const int teamsHandle = 1;
  const int engineersHandle = 2;

	/* Initialize MASS with the machine information, number of processes, and 
   * number of threads. */
	MASS::init( arguments, nProc, nThr ); 
  timer.start(); // Evaluation begins now

  //----------------------------- Read in Tasks---------------------------------
  vector<Task*> collabs, prods;
  if (!readTasks(taskFile, &collabs, & prods)) {
    return -1;
  }
  int numCollabTasks = collabs.size(), numProdTasks = prods.size();
  // Make Collaboration Tasks Contiguous
  Task* collabTasks = new Task[numCollabTasks];
  for (int ctask = 0; ctask < numCollabTasks; ctask++) {
    collabTasks[ctask] = Task(*(collabs[ctask]));
  }
  // Make Production Tasks Contiguous
  Task* prodTasks = new Task[numProdTasks];
  for (int ptask = 0; ptask < numProdTasks; ptask++) {
    prodTasks[ptask] = Task(*(prods[ptask]));
  }

  //------------------------- Read in Configurations ---------------------------
  Team::TeamConfig* configs = nullptr;
  int numConfigs = 0;
  if (!readTeamConfig(configFile, &configs, &numConfigs)) {
    configs = new Team::TeamConfig;
    configs->numLeads = 5;
    configs->numUx = 5;
    configs->numSr = 5;
    configs->numJr = 5;
    configs->numTesters = 5;
    numConfigs = 1;
  }
  int* arrSizes = new int[3]{numProdTasks, numCollabTasks, numConfigs};

  logText.str("");
  logText << "Team Configuration Options" << endl;
  for (int conf = 0; conf < numConfigs; conf++) {
    logText << "Config #" << conf << endl;
    logText << "    " << "# Leads (type 0): " << configs[conf].numLeads << endl;
    logText << "    " << "# UX (type 1): " << configs[conf].numUx << endl;
    logText << "    " << "# Sr (type 2): " << configs[conf].numSr << endl;
    logText << "    " << "# Jr (type 3): " << configs[conf].numJr << endl;
    logText << "    " << "# Testers (type 4): " << configs[conf].numTesters << endl;
  }
  MASS_base::log(logText.str());

  //---------------------------- Initialize Teams ------------------------------
  Places* teams = new Places(teamsHandle, "Team", 1, dummyArg, 1, 2, sizeX, sizeY);
  teams->callAll(Team::init_, arrSizes, sizeof(int)*3);
  teams->callAll(Team::initProduction_, (void*)prodTasks, sizeof(Task) * numProdTasks);
  teams->callAll(Team::initCollabs_, (void*)collabTasks, sizeof(Task) * numCollabTasks);
  teams->callAll(Team::initTeamConfig_, (void*)configs, sizeof(Team::TeamConfig) * numConfigs);

  //-------------------------- Initialize Engineers ----------------------------
  int numEngineers = sizeX * sizeY * Team::numEngPerTeam;
  Agents* engineers = new Agents(engineersHandle, "Engineer", dummyArg, 1, teams,
   numEngineers);
  engineers->callAll(Engineer::init_, dummyArg, 1);

  //---------------------------- Simulation Loop -------------------------------
  int numTeams = sizeX * sizeY;
  int turn = 0;
  int* completionTimes;
  bool done = false;
	for (; !done; turn++) { 
    teams->callAll(Team::checkCollab_, dummyArg, sizeof(char));
    engineers->callAll(Engineer::work_, dummyArg, sizeof(char));
    engineers->callAll(Engineer::submitTask_, dummyArg, sizeof(char));
    teams->callAll(Team::act_, dummyArg, sizeof(char));
    // Check if done
    completionTimes = (int*)teams->callAll(Team::checkProgress_, nullptr, 0, sizeof(int));
    done = allTeamsFinished(completionTimes, numTeams);
    if (!done) {
      delete [] completionTimes;
    }
	}

  //------------------------------- Final Steps --------------------------------
  logText.str("");
  logText << "Team Completion Times:" << endl;
  for (int team = 0; team < numTeams; team++) {
    int day = completionTimes[team] / 24;
    int hour = completionTimes[team] % 24;
    logText << "    - Team " << team << " with configuration " << team % numConfigs 
      << " completed all tasks on Day " << day << " Hour " << hour << endl;
  }
  MASS_base::log(logText.str());
	
	long elaspedTime_END =  timer.lap();
  logText.str("");
  logText << endl << "End of simulation. Elapsed time using MASS framework with " 
    << nProc << " processes, " << nThr << " threads, and " << turn << " turns"
    << " is: " << elaspedTime_END << endl << endl;
  MASS_base::log(logText.str());

	MASS::finish( );
}


//------------------------------- readTasks ------------------------------------
bool readTasks(string filename, vector<Task*> *collabTasks, vector<Task*> *prodTasks) {
    ifstream taskFile;
    taskFile.open(filename);
    if (!taskFile) {
        cerr << "Failed to open file" << endl;
        return false;
    }
    else {
        string line;
        while(getline(taskFile, line, '\n')) {
            if (line[line.length() - 1] == '\r') {
                line.erase(line.length() - 1);
            }
            if (line.length() == 0 || line == "Collaboration" || line == "Production" || line == " ") {
                continue;
            }
            else {
                vector<int> taskArgs;
                int delimPos = 0;
                string argVal = "";
                while ((delimPos = line.find(':')) != string::npos) {
                    int pos = delimPos + 1;
                    if (taskArgs.size() == 0) {
                        int hrsIn = 0;
                        while (hrsIn < 5) {
                            if (line[pos] == ',') {
                                hrsIn++;
                                taskArgs.push_back(stoi(argVal));
                                argVal = "";
                            }
                            else {
                                argVal += line[pos];
                            }
                            pos++;
                        }
                    }
                    else {
                        while (line[pos] != ',' && pos < line.length()) {
                            argVal += line[pos++];
                        }
                        taskArgs.push_back(stoi(argVal));
                        if (pos < line.length()) pos += 2;
                    }
                    argVal = "";
                    line.erase(0, pos);
                }
                // Create Task
                Task* t = new Task(taskArgs[5], taskArgs[6], taskArgs[7], taskArgs[8]);
                for (int engLvl = 0; engLvl < 5; engLvl++) {
                    t->setHours(engLvl, taskArgs[engLvl]);
                }
                if (taskArgs.size() == 10) {
                    t->setCollabDay(taskArgs[9]);
                    collabTasks->push_back(t);
                }
                if (taskArgs.size() == 9) {
                    prodTasks->push_back(t);
                }
            }
        }
        taskFile.close();
        return true;
    }
}


//----------------------------- readTeamConfig ---------------------------------
bool readTeamConfig(string filename, Team::TeamConfig** configs, int *numConfigs) {
  ifstream configFile;
  configFile.open(filename);
  if (!configFile) {
    cerr << "Failed to open config file" << endl;
    return false;
  }
  else {
    configFile >> (*numConfigs);
    *configs = new Team::TeamConfig[*numConfigs];
    for (int configNum = 0; configNum < *numConfigs; configNum++) {
      char discard;
      configFile >> (*configs)[configNum].numLeads >> discard;
      configFile >> (*configs)[configNum].numUx >> discard;
      configFile >> (*configs)[configNum].numSr >> discard;
      configFile >> (*configs)[configNum].numJr >> discard;
      configFile >> (*configs)[configNum].numTesters;
    }
    return true;
  }
}


//--------------------------- allTeamsFinished ---------------------------------
bool allTeamsFinished(int* endTimes, int numTeams) {
  for (int team = 0; team < numTeams; team++) {
    if (endTimes[team] == -1) {
      return false;
    }
  }
  return true;
}