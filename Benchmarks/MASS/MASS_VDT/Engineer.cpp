#include "Engineer.h"


extern "C" Agent* instantiate(void* argument) {
  return new Engineer(argument);
}


extern "C" void destroy(Agent* engineerObj) {
  delete engineerObj;
}


//----------------------------------- init -------------------------------------
// Prompts the Engineer to determine which type of engineer in the hierarchy
// it should be and initializes the other data members with default values.
void* Engineer::init() {
  currentTask = nullptr;
  type = *(int*)place->callMethod(Team::addEngineer_, nullptr);
  collabDuration = 0;
  returnTask = false;
}


//---------------------------------- work ------------------------------------
/** Has the Engineer Agent work for an hour on a Task.                      */
void* Engineer::work() {
  if (!(*(bool*)place->callMethod(Team::isFinished_, nullptr))) {
    // 1) If not already collaborating, check if there is a collaboration task to attend.
    if (collabDuration == 0) {
      collabDuration = *((int*)place->callMethod(Team::getCollab_, (void*)&type));
    }
    // 2) Work on the collaboration task
    if (collabDuration > 0) {
      collabDuration--;
      return nullptr; // End turn
    }
    // 3) If there are no collaborative tasks, work on a production task.
    if (currentTask == nullptr) {
      currentTask = (Task*)place->callMethod(Team::assignTask_, (void*)&type);
    }
    // 4) Work on the current task (reduce hours by 1).
    if (currentTask != nullptr) {
      // Process if it's the first time on the task.
      if (processedTasks.find(currentTask->getTaskId()) == processedTasks.end()) {
        processTask();
        return nullptr; // End turn
      }
      currentTask->makeProgress(type);
      returnTask = (currentTask->getEngrHours(type) == 0);
    }
  }
}


//-------------------------------- processTask ---------------------------------
/** Processes a new task from the tray.                                       */  
void Engineer::processTask() {
  processedTasks.insert(currentTask->getTaskId());
  int exceptionValue = rand() % 100;
  // Handle Exception and prepare to return task
  if (exceptionValue < 30) {
    if (type > 0) /*Engineer is not a lead*/ {
      int additionalHours = 1 + (exceptionValue % 8);
      currentTask->addExtraHours(type - 1, additionalHours);
      returnTask = true;
    }
    else /*Engineer is a lead*/ {
      currentTask->addExtraHours(type, currentTask->getEngrHours(type) * 2);
    }
  }
}



//--------------------------------- submitTask ---------------------------------
/** Returns the task either to the next tray down in the hierarchy or to the
 *  completed task list.
 */
void* Engineer::submitTask() {
  if (returnTask && currentTask != nullptr) {
    place->callMethod(Team::sendToTray_, (void*)currentTask);
    returnTask = false;
    currentTask = nullptr;
  }
}