# Virtual Development Team (VDT)

The Virtual Development Team (VDT) benchmark program simulates a software development team working on projects by completing different types of tasks. Each software development team has 25 Engineers that can be any of 5 different types: Project leads, User Experience Designer, Senior Software Developer, Junior Software Developer, and Test Engineer.

The simulation feeds each team a list of production tasks and collaboration tasks. Production tasks have a set number of hours to be completed by each type of engineer, and they flow down the hierarchy of engineers from top to bottom. Collaboration tasks have set meeting times that require any participating engineers to halt production until the collaboration task is finished.

The program runs one hour at a time and outputs the amount of time it took each team to finish all the tasks.

## Configuring the Program Files in Your Directory

Running the benchmark programs relies on having the compile.sh and run.sh files point to the mass_cpp_core folder and requires symbolic links in your directory to three library files in mass_cpp_core/ubuntu.

1.	Create symbolic links to the following three files in the mass_cpp_core directory:
    *	/mass_cpp_core/ubuntu/libmass.so
    *	/mass_cpp_core/ubuntu/mprocess
    *	/mass_cpp_core/ubuntu/killMProcess.sh 
2.	Find compile.sh in the benchmark program’s directory. Edit line 2 of this file to point to the mass_cpp_core directory in relation to compile.sh’s location. For example, if the mass_cpp_core folder is located one directory back from compile.sh, the line will read “export MASS_DIR=../mass_cpp_core”.
3.	Find run.sh in the benchmark program’s directory. Edit line 1 of this file to point to the mass_cpp_core/ubuntu folder in relation to run.sh’s location. For example, if the mass_cpp_core folder is located one directory back from run.sh, the line will read “LD_LIBRARY_PATH=../mass_cpp_core/ubuntu”.
4.	Make sure machinefile.txt lists all the machines you want to establish a connection with during execution. The file provided on Bitbucket lists hermes02-hermes12 and cssmpi1h-cssmpi12h and assumes that hermes01 is the master node. The master node should always be excluded from this file.
5.	Compile the benchmark’s source files with  “./compile.sh” to ensure all executable files are up to date.


## Providing Inputs

This benchmark file requires two text files, one to list the tasks for the engineers to complete and another to list the different combinations of each type of engineer for a team.
The task file should be formatted in two sections: the top section for collaboration tasks, and the bottom section for production tasks. Here is an example:
```
Collaboration
toMeet:h1,h2,h3,h4,h5, Type:1, Priority:9999, Length:L, ID:I, Day:d

Production
Hours:h1,h2,h3,h4,h5, Type:0, Priority:18, totalHours:T, ID:I
```

Under the “toMeet” and “Hours” section, h1 refers to the number of hours for project leads, h2 refers to the number for of user experience designers, and so on. Length L should equal the sum of all hours in the “toMeet” section, and totalHours T should equal the sum of all hours in the “Hours” section. Another thing to note is that the IDs of the tasks should be unique, and not repeated for collaboration tasks and production tasks.

The next text file needed to run is the team configuration file, which should be formatted as follows:
#Configurations
#Leads,#UX,#Srs,#Jrs,#Testers
Here is an example of the configuration file that was used to collect the evaluative data in this paper:
```
4
3,4,5,6,7
2,4,6,8,5
5,5,5,5,5
6,5,4,5,5
```
Each line (excluding the first line) must sum to 25.


## Running the Program

Once the two files are in the directory, follow the steps to execute the program:

1.	Execute run.sh with “./run.sh”. 
2.	Enter the following arguments when prompted:
    *	Number of Nodes – the number of computing nodes (machines) you want to use to run your program.
    *	Number of Threads – the number of threads to use.
    *	XSIZE – the number of teams to instantiate along the x-axis (i.e. columns in the places matrix). Note that the places matrix is split across this axis.
    *	YSIZE – the number of teams to instantiate along the y-axis (i.e. rows in the places matrix).
    *	Port Number - the port number you want to use for communicating between remote computing nodes.
    *	Password - your UW password for the hermes machines. It is recommended that you establish an RSA key so that you can authenticate remote computing nodes without having to pass your password between remote computing nodes. 
3.	If the program terminates for any reason before MASS::finish is called, execute ./killMProcess.sh to clean up the processes on the other computing nodes before attempting to execute again.


# VDT File Descriptions

The files in this repository (in alphabetical order) are...

* 1thr - The output files for each of the runs used to collect evaluative data for one thread. 

* compile.sh - Compiles your program into executables to run. Simply use "./compile.sh" to run. Note: Line 2 of this file must have the correct path to the mass_cpp_core library.

* Engineer.h, Engineer.cpp - Source files for the Engineer Agent.

* killMProcess.sh - kills any running processes if the program terminates before MASS::finish() was called to delete running processes on other computing nodes. This file must be a symbolic link to "mass_cpp_core/ubuntu/killMProcess.sh".

* libmass.so - A symbolic link to "mass_cpp_core/ubuntu/libmass.so"

* machinefile.txt - Lists the names of all computers we want to establish a connection with to use as a remote computing node. Note that the master node should be excluded from this file.

* main.cpp - The main driver of the simulation.

* mprocess - A symbolic link to "mass_cpp_core/ubuntu/mprocess".

* run.sh - Runs the benchmark program once all the source files have been compiled. Note that line 1 of this file must have the correct path to the mass_cpp_core library files.

* Task.h, Task.cpp - Source files for the Task class. 

* Tasks.txt - Input file of tasks for each team to recieve.

* Team.h, Teamp.cpp - Source files for the Place-derived Team Class.

* teamconfig.txt - Input file for the number of engineers to have on each team.

* Timer.h, Timer.cpp - Keeps track of the program's runtime. Useful for evaluation.