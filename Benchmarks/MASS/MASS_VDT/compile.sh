#!/bin/sh
export MASS_DIR=/home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_core/

rm Team
rm Engineer 
rm main

g++ -Wall Team.cpp Task.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o Team
g++ -Wall Engineer.cpp Task.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o Engineer
g++ -std=c++11 -Wall main.cpp Task.cpp Timer.cpp -I$MASS_DIR/source -L$MASS_DIR/ubuntu -lmass -I$MASS_DIR/ubuntu/ssh2/include -L$MASS_DIR/ubuntu/ssh2/lib -lssh2 -o main