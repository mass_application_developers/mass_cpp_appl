/*==============================================================================
  Task Class
  Author: Ian Dudder
================================================================================
  A number of Task objects will form a Project in the VDT simulation. There are
  two types of tasks: Production (type 0) and Collaborative (type 1). 

  Production Tasks flow through the task hierarchy and have a predefined number
  of hours required from each Engineer type. When a Production Task is selected
  by an Engineer Agent, there is an E percent chance that the task experiences
  an exception and requires the Engineer to reassign it to their supervisor for
  some number of hours. 

  Collaborative Tasks simulate communication and meetings between team members,
  and are scheduled to occur a set number of times during the day. When an 
  Engineer is assigned to a Collaborative Task, the agent will be unable to
  work on any other tasks.
==============================================================================*/

#ifndef TASK_H
#define TASK_H

#include <iostream>
#include <vector>

using namespace std;

class Task {
public:
  //============================== Public Methods ==============================

  //--------------------------- Default Constructor ----------------------------
  // Initializes the task with default values.
  Task();

  //------------------------------- Constructor --------------------------------
  // Initializes the member data with the specified values.
  Task(int type_, int priority_, int totalHours_, int taskId_);

  //---------------------------- Copy Constructor ------------------------------
  /** Initializes the task with default values.
   *  @param toCopy - a Task object that we wish to create a deep copy of. */
  Task(Task* toCopy);
  
  //----------------------------- makeProgress ---------------------------------
  /** Allows an engineer to make progress on a task by reducing the number of 
   *  hours required from that type of engineer by one hour.
   *  @param engType - the type of engineer (rank of engineer in hierarchy).
   */
  bool makeProgress(int engType);
  
  //----------------------------- addExtraHours --------------------------------
  /** Adds extra hours for the specified type of engineer.
   *  @param engType - the type of engineer (rank of engineer in hierarchy).
   *  @param additionalHours - the number of hours to be added on for the engineer
   */
  bool addExtraHours(int engType, int additionalHours);

  //================================= Getters =================================
  int getType() const;

  int getPriority() const;

  int getTotalHours() const;

  int getTaskId() const;

  int getCollabDay() const;

  int getCollabHour() const;

  int getEngrHours(int engrLvl) const;

  //================================= Setters =================================
  void setType(int type_);

  void setPriority(int priority_);

  void setTotalHours(int hours);

  void setTaskId(int taskId_);

  void setCollabDay(int day);

  bool setHours(int engineerLevel, int numHours);

private:
  //================================ Member Data ===============================
  int type;              // Type of Task (0 for Production, 1 for Collaborative)
  int priority;          // Priority of task
  int totalHours;        // Total number of hours required from all engineers
  int taskId;            // Unique Identifier for the task
  int collabStartDay;    // Start time for collaborative task
  int hours[5];          // Number of hours required from each engineer type
};

#endif //TASK_H