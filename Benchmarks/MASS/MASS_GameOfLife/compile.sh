#!/bin/sh

# old export MASS_DIR=/CSSDIV/research/dslab/mass_cpp/mass_final/dev/mass_cpp/mass_cpp_core
export MASS_DIR=/home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_core/
g++ -Wall Life.cpp -I$MASS_DIR/source -shared -fPIC -std=c++11 -o Life
g++ -std=c++11 -Wall main.cpp Timer.cpp -I$MASS_DIR/source -L$MASS_DIR/ubuntu -lmass -I$MASS_DIR/ubuntu/ssh2/include -L$MASS_DIR/ubuntu/ssh2/lib -lssh2 -o main
