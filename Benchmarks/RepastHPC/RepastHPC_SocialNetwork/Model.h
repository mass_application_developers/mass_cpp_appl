
#ifndef MODEL_H
#define MODEL_H

#include <boost/mpi.hpp>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/AgentRequest.h"
#include "repast_hpc/Graph.h"
#include "repast_hpc/SharedNetwork.h"

#include "Agent.h"
#include "AgentPackage.h"


class ModelProvider{
private:
	repast::SharedContext<Agent>* context;
public:
	ModelProvider(repast::SharedContext<Agent>* agentptr);
	void providePackage(Agent* agent, std::vector<AgentPackage>& out);
	void provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out);
	Agent* getAgent(repast::AgentId id);
};

class ModelReceiver {
private:
	repast::SharedContext<Agent>* context;
public:
	ModelReceiver(repast::SharedContext<Agent>* agentptr);
	Agent * createAgent(AgentPackage package);
	void updateAgent(AgentPackage package);

};
class Model {
public:
	Model(std::string props, int argc, char ** argv, boost::mpi::communicator* world);
	~Model();
	//initialize
	void init();
	//run
	void go();
	//public methods
	bool checkVars();
	int setVar(std::string varName);

	void setup();

	//functions for serialization/ migration
//	void providePackage(Agent* agent, std::vector<AgentPackage>& out);
//	void provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out);
//	Agent * createAgent(AgentPackage package);
//	void updateAgent(AgentPackage package);
	void requestAgents();
	void initSchedule(repast::ScheduleRunner& runner);


private:
	repast::Properties* properties;
	repast::SharedContext<Agent> context;
	int friends;
	int numAgents;
	int divider;
	int degreeToPrint;
	void setData();
	void helpPrint(Agent* agent);
	Agent* findAgentFromContext(int id);
	//repast::SharedNetwork<Agent, repast::RepastEdge<Agent>, repast::RepastEdgeContent<Agent>, repast::RepastEdgeContentManager<Agent> >* agentNetwork;
	//repast::RepastEdgeContentManager<Agent> edgeContentManager;
	ModelProvider* provider;
	ModelReceiver *receiver;
};

#endif
