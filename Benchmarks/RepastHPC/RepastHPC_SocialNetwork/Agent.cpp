#include "Agent.h"

//constructors
Agent::Agent(repast::AgentId id)
{
	id_ = id;
	numFriends = 0;
	//this->friends.resize(numFriends,-1);
}

Agent::Agent(repast::AgentId id, int numFriends)
{
	id_ = id;
	this->numFriends = numFriends;
	//this->friends.resize(numFriends,-1);
}
Agent::Agent(repast::AgentId id, int numFriends, std::set<int> friends)
{
	id_ = id;
	this->numFriends = numFriends;
	this->friends = friends;
}

Agent::~Agent(){ }

std::set<int> Agent::getFriends() const
{
	return friends;
}
int Agent::getNumFriends() const
{
	return numFriends;
}

void Agent::set(int currentRank, int numFriends, std::set<int>friends)
{
	id_.currentRank(currentRank);
	this->numFriends = numFriends;
	this->friends = friends;
}

void Agent::setFriends(int numFriends)
{
	this->numFriends = numFriends;
}

void Agent::addFriend(int j, int numAgents)
{
	this->friends.insert((this->getId().id()+ j + 1)%numAgents);
}

void Agent::addFriend(int j)
{
	this->friends.insert(j);
}
void Agent::findFriends(repast::SharedContext<Agent>* context, int degrees, int numAgents)
{
	//9->8, 8->7

//	int rank = repast::RepastProcess::instance()->rank();
//		int evenNum = degrees / 2;
//		for(int i= 0; i < evenNum; i++)
//		{
//			Agent* toFind = findFriends(context, (this->getId().id()+ i + 1)%numAgents,numAgents);
//			Agent* toFindBehind = findFriends(context, (numAgents - i - 1 + this->getId().id())%numAgents,numAgents);
//			if(toFind != nullptr)
//			{
//			//std::cout<< "Connecting: " << this->getId() << " to " << toFind->getId() << std::endl;
//			agentNetwork->addEdge(this,toFind);
//			this->friends.insert(toFind->getId().id());
//			//toFind->getFriends().insert(this->getId().id());
//			}
//			if(toFindBehind != nullptr)
//			{
//				this->friends.insert(toFindBehind->getId().id());
//			}
//		}
//		if(degrees %2 == 1)
//		{
//			int id = numAgents / 2;
//			Agent* toFind = findFriends(context,(this->getId().id() + id) % numAgents,numAgents);
//			if(toFind != nullptr)
//			{
//			agentNetwork->addEdge(this,toFind);
//			this->friends.insert(toFind->id_.id());
//			toFind->getFriends().insert(this->id_.id());
//			}
//		}


}

Agent* Agent::findFriend(repast::SharedContext<Agent>* context,int toFind, int numAgents)
{
		Agent* toRet = nullptr;
		std::set<Agent*> agents;
		context->selectAgents(agents,true);
		std::set<Agent*>::iterator agent = agents.begin();
		while(agent != agents.end())
		{
			if((*agent)->getId().id() == toFind)
			{
				toRet = *agent;
				return toRet;
			}

			agent++;
		}
		return toRet;
}
