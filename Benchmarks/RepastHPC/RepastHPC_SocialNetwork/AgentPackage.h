#ifndef AGENTPACKAGE_H
#define AGENTPACKAGE_H

#include "repast_hpc/AgentId.h"
#include<set>

struct AgentPackage {
public:
	template<class Archive>
	void serialize (Archive &ar, const unsigned int version)
	{
		ar & id;
		ar & proc;
		ar & type;
		ar & currentProc;

		ar & numFriends;
		ar & friends;

	}
	int id, proc, type, currentProc;

	std::set<int> friends;
	int numFriends;

	repast::AgentId getId() const {
		return repast::AgentId(id,proc,type,currentProc);
	}
	AgentPackage(int _id, int _proc, int _type, int _currentProc, int _numFriends, std::set<int>_friends)
	{

		id = _id;
		proc = _proc;
		type = _type;
		currentProc = _currentProc;
		numFriends = _numFriends;
		friends = _friends;
	}
	AgentPackage(){};
};
#endif /* AGENTPACKAGE_H */
