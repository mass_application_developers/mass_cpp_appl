
#include<stdio.h>
#include<algorithm>
#include<queue>
#include<boost/mpi.hpp>
#include"repast_hpc/Schedule.h"
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"


#include "Model.h"

BOOST_CLASS_EXPORT_GUID(repast::SpecializedProjectionInfoPacket<repast::RepastEdgeContent<Agent> >, "SpecializedProjectionInfoPacket_EDGE");

ModelProvider::ModelProvider(repast::SharedContext<Agent>* agentPtr)
{
	context = agentPtr;
}

ModelReceiver::ModelReceiver(repast::SharedContext<Agent>* agentPtr)
{
	context = agentPtr;
}

Model::Model(std::string props, int argc, char** argv, boost::mpi::communicator* world): context(world)
{
	properties = new repast::Properties(props,argc,argv,world);
	//default in case prop file not set properly
	degreeToPrint = 1;
	this->setData();
	//agentNetwork = new repast::SharedNetwork<Agent, repast::RepastEdge<Agent>, repast::RepastEdgeContent<Agent>, repast::RepastEdgeContentManager<Agent> >("agentNetwork", false, &edgeContentManager);

	//context.addProjection(agentNetwork);
	provider = new ModelProvider(&context);
	receiver = new ModelReceiver(&context);
}

Model::~Model()
{
	delete properties;
	delete provider;
	delete receiver;
}

bool Model::checkVars()
{
	//make sure variables are valid
	if(friends > 0 && numAgents > 0 && divider > 0 && (friends % 2 == 0 || numAgents % 2 == 0))
	{
		return true;
	}
	return false;
}
int Model::setVar(std::string varName)
{
	std::string toInt =(this->properties->getProperty(varName));
	if(toInt.length()!= 0)
	{
		return repast::strToInt(toInt);
	}
	return -1;
}
void Model::setData()
{
	degreeToPrint = std::max(degreeToPrint, setVar("var.x"));
	numAgents = setVar("var.n");
	std::string friendAmt = this->properties->getProperty("var.k");
	if(friendAmt.length() == 0)
	{
		divider = setVar("var.m");
		friends = numAgents/divider;
		return;
	}
	friends = repast::strToInt(friendAmt);
	divider = numAgents/friends;
}
void Model::init()
{
	int rank = repast::RepastProcess::instance()->rank();
	if(rank == 0)
	{
	int worldSize = repast::RepastProcess::instance()->worldSize();
	int currAgent = 0;
	int currRank = 0;
	while(currAgent < numAgents)
	{
		repast::AgentId id(currAgent,rank,0);
		id.currentRank(rank);
		Agent* agent = new Agent(id,friends);
		context.addAgent(agent);
		if(currRank != rank)
		{
			repast::RepastProcess::instance()->moveAgent(id,currRank);

		}
		currAgent++;
		currRank = (currRank+ 1) % worldSize;
	}
	}
	repast::RepastProcess::instance()->synchronizeAgentStatus<Agent, AgentPackage, ModelProvider, ModelReceiver>(context, *provider, *receiver, *receiver);
	//std::cout<<"--------------------------------------------------------------------------"<<endl;
}
void Model::go()
{
	int rank = repast::RepastProcess::instance()->rank();
	if(rank == 0)
	{
	//std::cout<<"Begin Go"<<std::endl;
	//std::cout<<context.size()<<std::endl;
	std::set<Agent*> agents;
	context.selectAgents(numAgents,agents,true);
	std::set<Agent*>::iterator agent = agents.begin();
	while(agent != agents.end())
	{
		std::cout<<"Agent "<< (*agent)->getId().id() << " friends" << std::endl;
		helpPrint(*agent);
		agent++;
	}
	}
	return;
}

Agent* Model::findAgentFromContext(int id)
{
	std::set<Agent*> toFind;
	context.selectAgents(numAgents,toFind,true);
	std::set<Agent*>::iterator agent = toFind.begin();
	while(agent != toFind.end())
	{
		if((*agent)->getId().id()== id)
		{
			return context.getAgent((*agent)->getId());
		}
		agent++;
	}
	//should change this
	int rank = repast::RepastProcess::instance()->rank();
	repast::AgentId fail(-1,rank,0);
	return new Agent(fail);
}
void Model::helpPrint(Agent* agent)
{
	std::set<int> visited;
	std::queue<Agent*> toGo;
	toGo.push(agent);
	int id = (*agent).getId().id();
	int count = 1;
	for(int i = 1; i <= degreeToPrint; i++)
	{
		std::cout<< i << "degree: ";
		int nextIteration = 0;
		while(count != 0 && !toGo.empty())
		{
			Agent* curr = toGo.front();
			toGo.pop();
			for(auto f : curr->getFriends())
			{
				if(!(visited.find(f)!=visited.end()))
				{
					std::cout<<f<<" ";
					visited.insert(f);
					nextIteration++;
					toGo.push(findAgentFromContext(f));
				}
			}
			count--;
		}
		count = nextIteration;
		nextIteration = 0;
		std::cout<<endl;
	}
//	std::set<int> visited;
//	std::queue<Agent*> toGo;
//	toGo.push(agent);
//	int id = (*agent).getId().id();
//	int count = 1;
//	for(int i = 1; i <= degreeToPrint; i++)
//	{
//		std::cout<<i;
//		std::cout<<" degree: ";
//		int nextIteration = 0;
//		while(count != 0 && !toGo.empty())
//		{
//			Agent* curr = toGo.front();
//			toGo.pop();
//			std::cout<<"FRIENDS SIZE " << curr->getFriends().size()<<std::endl;
//			std::vector<Agent*> adj;
//			agentNetwork->adjacent(curr,adj);
//			for(Agent* a: adj)
//			{
//				if(!(visited.find(a->getId().id())!=visited.end()) && a->getId().id() != id)
//				{
//					std::cout<<a->getId().id()<<" ";
//					visited.insert(a->getId().id());
//					nextIteration++;
//					toGo.push(a);
//				}
//			}
//			count--;
//		}
//		count = nextIteration;
//		nextIteration = 0;
//		std::cout<<endl;
//	}
}


void Model::setup()
{
	std::vector<Agent*> agents;
	context.selectAgents(agents);
	for(int i = 0; i < friends/2; i++)
	{
		repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
		repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
		while(iter != iterEnd)
		{
			Agent* curr = &**iter;
			curr->addFriend(i,numAgents);
			iter++;
		}
		repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage,ModelProvider,ModelReceiver>(*provider, *receiver);
		std::vector<Agent*>::iterator it = agents.begin();
		while(it != agents.end())
		{
			std::set<int> theFriends = (*it)->getFriends();
			for(auto& f: theFriends)
			{
				Agent* a = findAgentFromContext(f);
				a->addFriend((*it)->getId().id());
			}
			it++;
		}
		repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage,ModelProvider,ModelReceiver>(*provider, *receiver);
	}
	if(friends %2 == 1)
	{
		repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
		repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
		while(iter != iterEnd)
		{
			Agent* curr = &**iter;
			curr->addFriend(numAgents/2 - 1,numAgents);
			iter++;
		}
		repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage,ModelProvider,ModelReceiver>(*provider, *receiver);

		std::vector<Agent*>::iterator it = agents.begin();
		while(it != agents.end())
		{
			std::set<int> theFriends = (*it)->getFriends();
			for(auto& f: theFriends)
			{
				Agent* a = findAgentFromContext(f);
				a->addFriend((*it)->getId().id());
			}
			it++;
		}
	}
}

Agent* ModelReceiver::createAgent(AgentPackage package)
{
	repast::AgentId id(package.id,package.proc,package.type,package.currentProc);
	return new Agent(id,package.numFriends,package.friends);
}

void ModelProvider::provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out)
{
	const std::vector<repast::AgentId>& ids = req.requestedAgents();
	for(size_t i = 0; i < ids.size(); i++)
	{
		providePackage(this->getAgent(ids[i]),out);
	}
}
Agent* ModelProvider::getAgent(repast::AgentId id)
{
	Agent* a = context->getAgent(id);
	if (a == nullptr)
	{
		a = new Agent(id);
	}
	return a;
}

void ModelProvider::providePackage(Agent* agent, std::vector<AgentPackage>& out)
{

	repast::AgentId id = agent->getId();
	AgentPackage package = {id.id(),id.startingRank(),id.agentType(),id.currentRank(),agent->getNumFriends(),agent->getFriends()};
	out.push_back(package);
}

void ModelReceiver::updateAgent(AgentPackage package)
{
	int rank = repast::RepastProcess::instance()->rank();
	repast::AgentId id(package.id,package.proc,package.type,package.currentProc);
//	std::cout<<rank << " " << id<<endl;
	Agent* agent = context->getAgent(id);
	//std::cout<<rank<<" " << agent->getId()<<std::endl;
	agent->set(package.currentProc,package.numFriends,package.friends);
}

void Model::requestAgents()
{
	int rank = repast::RepastProcess::instance()->rank();
	int worldSize = repast::RepastProcess::instance()->worldSize();
	int currAgent = 0;
	int currRank = 0;
	repast::AgentRequest req(rank);
	while(currAgent < numAgents)
	{
		if(currRank != rank)
		{
			repast::AgentId toGet(currAgent,0,0);
			toGet.currentRank(currRank);
			req.addRequest(toGet);
		}
		currAgent++;
		currRank = (currRank+ 1) % worldSize;
	}
	repast::RepastProcess::instance()->requestAgents<Agent, AgentPackage, ModelProvider, ModelReceiver>(context, req, *provider, *receiver, *receiver,"SET_1");
}

void Model::initSchedule(repast::ScheduleRunner & runner)
{
	runner.scheduleEvent(1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::requestAgents)));
	runner.scheduleEvent(2,repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::setup)));
	//runner.scheduleEvent(2.2, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::requestAgents)));
	runner.scheduleEvent(3,repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::go)));
	runner.scheduleStop(3);
}
