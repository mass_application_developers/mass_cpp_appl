#ifndef AGENT_H
#define AGENT_H

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedNetwork.h"

class Agent {
public:
	//constructors & destructors
	Agent(repast::AgentId id);
	Agent(repast::AgentId id, int numFriends);
	Agent(repast::AgentId id, int numFriends, std::set<int> friends);
	~Agent();

	//Getters
	virtual repast::AgentId& getId(){ return id_;}
	virtual const repast::AgentId& getId() const { return id_;}
	std::set<int> getFriends() const;
	int getNumFriends() const;

	//setters
	void set(int currentRank, int numFriends, std::set<int> friends);
	void setFriends(int numFriends);



	//action
	Agent* findFriend(repast::SharedContext<Agent>* context, int toFind,int numAgents);
	void findFriends(repast::SharedContext<Agent>* context, int degrees, int numAgents);
	void addFriend(int j, int numAgents);
	void addFriend(int j);

private:
	repast::AgentId id_;
	int numFriends;
	std::set<int> friends;

};

#endif
