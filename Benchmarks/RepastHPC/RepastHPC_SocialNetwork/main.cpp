#include<iostream>
#include<boost/mpi.hpp>
#include<chrono>


#include "repast_hpc/RepastProcess.h"
#include "Model.h"


using namespace std;
using namespace std::chrono;
using namespace repast;

//using namespace relogo;

int main(int argc, char **argv)
{
	boost::mpi::environment env(argc,argv);
	string config,properties;
	boost::mpi::communicator world;
	//tentative
	if(argc >= 3)
	{
		config = argv[1];
		properties = argv[2];
	}
	else
	{
		if(world.rank() == 0)
		{
			cout<< "Need x arguments" << endl;
		}
		return -1;
	}
	if(config.size() > 0 && properties.size() >0)
	{
		RepastProcess::init(config, &world);
		Model* model = new Model(properties,argc,argv, &world);
		repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();
		if(model->checkVars())
		{
			auto start = high_resolution_clock::now();
			model->init();
			model->initSchedule(runner);
			runner.run();
			auto stop = high_resolution_clock::now();

			auto duration = duration_cast<milliseconds>(stop - start);

			cout << duration.count() << endl;

		}
		else
		{
			cout<<"Variables are invalid" << endl;
			return -1;
		}
		delete model;
	}
	 else
	{
		if(world.rank() == 0)
		{
			cout<<"Need configuration and/or properties file" << endl;
			return -1;
		}
	}
	RepastProcess:: instance()->done();
	if(world.rank() == 0)
	{
		cout<<"Finished" << endl;
	}

	return 0;
}
