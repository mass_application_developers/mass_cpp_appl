//
// Created by sarah
//

#ifndef MACROPHAGE_H
#define MACROPHAGE_H

#include "relogo/Turtle.h" // derived class of Turtle (mobile agent)
#include "AgentPackage.h"
#include "LungPlace.h"

class Macrophage : public repast::relogo::Turtle {

public:

// Macrophage constants

    typedef enum State {
        RESTING, INFECTED, ACTIVATED, CHRONICALLY_INFECTED, DEAD
    } State;

    // Number of internal bacteria that causes an INFECTED macrophage to become CHRONICALLY_INFECTED
    const int chronicInfection = 75;

    // Number of internal bacteria that causes a CHRONICALLY_INFECTED macrophage to die
    const int bacterialDeath = 100;

// State functions

    void move();

    void collisionFreeJumpBack();

    void react();

    void removeDeadMacrophages();

    void writeState();

// Constructors and destructor

    Macrophage(const repast::AgentId& id, repast::relogo::Observer *obs) :
            repast::relogo::Turtle(id, obs), _state(RESTING), _dayInfected(-1), _intracellularBacteria(0),
            _spawner(false) {}

    // For Macrophage reconstruction after serialization and migration using AgentPackage
    Macrophage(const repast::AgentId id, repast::relogo::Observer *obs, const AgentPackage &package) :
            repast::relogo::Turtle(id, obs), _state(static_cast<State>(package.state)),
            _dayInfected(package.dayInfected),_intracellularBacteria(package.intracellularBacteria),
            _spawner(package.spawner){}

    virtual ~Macrophage() {}

// Getters and setters

    int getDayInfected() const;

    void setDayInfected(int dayInfected);

    int getIntracellularBacteria() const;

    void setIntracellularBacteria(int intracellularBacteria);

    State getState() const;

    void setState(State state);

    bool isSpawner() const;

    void setToSpawner() {
        _spawner = true;
    }

    void setSpawner(bool spawner) {
        _spawner = spawner;
    }

private:

    // whether this is in invisible spawner or not
    bool _spawner;

    State _state;

    // Day (iteration number) the macrophage transitioned from RESTING to INFECTED. If the macrophage isn't
    // infected, this number is -1.
    int _dayInfected;

    // Number of bacteria this macrophage contains inside
    int _intracellularBacteria;

    int previousPlaceCoords[2]{-1, -1}; // [x,y]; to enable jump-back for collision free migration

    // Helper
    void growInfectedIntraCBact(int today);
    void growChronicIntraCBact();

};


#endif //MACROPHAGE_H
