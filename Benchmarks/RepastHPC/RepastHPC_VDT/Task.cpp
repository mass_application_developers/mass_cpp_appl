#include "Task.h"

Task::Task()
{
    for(int i =0; i < 5; i++)
    {
        hours.at(i) = -1;
    }
    type = -1;
    totalHours = -1;
    name = "";
    id = -1;
    prevSupervisor = nullptr;
    priority = -1;
    day = -1;
}
Task::Task(std::vector<int> hours, int type,int priority,int totalHours, std::string name,int id)
{
	this->hours = hours;
    this->type = type;
    this->priority = priority;
    this->totalHours = totalHours;
    this->name = name;
    this->id = id;
    day = 0;
    prevSupervisor = nullptr;

}
Task::~Task()
{

}
std::vector<int> Task::getHours()const
{
    return hours;
}
bool Task::setHours(int index, int value)
{
	if(index >= hours.size())
	{
		return false;
	}
	hours.at(index) = value;
	return true;
}

bool Task::setHours(std::vector<int> hours)
{
	for(int i = 0;i < 5; i++)
	{
		this->hours.at(i) = hours.at(i);
	}
	return true;
}
int Task::getType() const
{
	return type;
}
bool Task::setType(int type)
{
	this->type = type;
	return true;
}
int Task::getTotalHours() const
{
	return totalHours;
}
bool Task::setTotalHours(int hours)
{
	this->totalHours = hours;
	return true;
}

std::string Task::getName() const
{
	return this->name;
}

bool Task::setName(std::string name)
{
	this->name = name;
	return true;
}

int Task::getID() const
{
	return this->id;
}

bool Task::setID(int id)
{
	this->id = id;
	return true;
}

Agent* Task::getSupervisor() const
{
	return prevSupervisor;
}

bool Task::setAgent(Agent* agent)
{
	if(agent == nullptr)
	{
		return false;
	}
	prevSupervisor = agent;
	return true;
}

int Task::getDay() const
{
	return day;
}

bool Task::setDay(int day)
{
	this->day = day;
	return true;
}

int Task::getPriority() const
{
	return priority;
}

bool Task::setPriority(int priority)
{
	this->priority = priority;
	return true;
}

