#ifndef AGENT_H
#define AGENT_H
#include <queue>
#include <deque>
#include <set>
#include "Task.h"
#include "Observer.h"
#include "repast_hpc/AgentId.h"
class Task;
class Observer;
class Agent
{
public:
	Agent(repast::AgentId id, int type, int hour, int day, Observer* observer);
	Agent();
	~Agent();
	virtual repast::AgentId& getId(){ return agentId;}
	virtual const repast::AgentId& getId() const { return agentId;}
	std::deque<Task*> getProduction() const;
	std::queue<Agent*> getUnder() const;
	void addUnder(Agent* agent);
	void addTask(Task* task);
	int getType()const;
	void setType(int type);
	int getDay() const;
	void setDay(int day);
	int getHour() const;
	void setHour(int hour);
	void setObserver(Observer* observer);
	Observer* getObserver();
	void setCollab(int hour, int day);
	bool isCollabTime(int hour, int day);
	void Play();
	void Process();
	void incrementHour();
	void finishTask(Task* task);
	void assignCollab(Task* task);
	void taskComplete();
	void setCurr();
	void removeCurr();
	bool inSet(int num);
	Task* chooseTask();

    //need cstrs
    //getter setters
    //actions
private:
    std::deque<Task*> production;
    std::queue<Agent*> under;
    bool collab[7][24];
    repast::AgentId agentId;
    int type;
    int hour;
    int day;
    Observer *observer;
    Task* curr;
    std::set<int> workedOn;
};
#endif
