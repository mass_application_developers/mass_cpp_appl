#include<iostream>
#include <boost/mpi.hpp>
#include<chrono>
#include "repast_hpc/RepastProcess.h"
#include "Observer.h"

using namespace std;
using namespace std::chrono;
using namespace repast;

int main(int argc,char** argv)
{
	boost::mpi::environment env(argc,argv);
	string config,properties,environment;
	boost::mpi::communicator world;
		//tentative
		if(argc >= 4)
		{
			config = argv[1];
			properties = argv[2];
			environment = argv[3];
		}
		else
		{
			if(world.rank() == 0)
			{
				cout<< "Need 4 arguments" << endl;
			}
			return -1;
		}
	if(config.size() >= 0 && properties.size() >= 0 )
	{
		RepastProcess::init(config,&world);
		auto start = high_resolution_clock::now();

		Observer* obvs = new Observer(properties,argc,argv,&world);
		obvs->init(environment);
		obvs->go();
		auto stop = high_resolution_clock::now();

		auto duration = duration_cast<milliseconds>(stop - start);
		cout << duration.count() << endl;
		delete obvs;




	}
	RepastProcess:: instance()->done();


	return 0;
    
}
