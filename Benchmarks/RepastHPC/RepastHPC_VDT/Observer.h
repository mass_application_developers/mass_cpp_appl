#ifndef OBSERVER_H
#define OBSERVER_H

#include <boost/mpi.hpp>
#include <queue>
#include <deque>
#include <set>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "Agent.h"
class Agent;
class Task;
class Observer
{
public:
	Observer(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	virtual ~Observer();
	void init(std::string environment);
	void addCompleted(int complete);
	void go();
	void incrementHour();
	void createEngineers();
	void createTasks(std::string environment);
	void distributeTasks();
	void createHierarchy();
	void insertInQueue(Agent* agent);

private:
	std::queue<Task*> toGive;
	std::set<Agent*> agent;
	std::deque<Agent*> projectLead;
	std::deque<Agent*> ux;
	std::deque<Agent*> seniorEngineer;
	std::deque<Agent*> juniorEngineer;
	std::deque<Agent*> testEngineer;
	std::set<int> completed;
	void defaultTask();
	void parseLine(std::string line, bool collab);
	void createHierarchy(Agent* agent, int type);
	void assignAgents(Agent* agent,std::deque<Agent*>& under);
	void giveTasks(Task* task,int agentType);
	void parseString(std::string engineer,int &project, int &ux, int &seniorEngineer, int &juniorEngineer, int &testEngineer);
	int hour;
	int day;
	int toComplete;
	repast::Properties* props;

};
#endif
