#ifndef TASK_H
#define TASK_H
#include<string>
#include<vector>
#include "Agent.h"
class Agent;
class Task
{
public:
    Task();
    Task(std::vector<int> hours, int type, int priority, int totalHours, std::string name,int id);
    ~Task();
    std::vector<int> getHours()const;
    bool setHours(int index, int value);
    bool setHours(std::vector<int> hours);
    int  getType() const;
    bool setType(int type);
    int getTotalHours() const;
    bool setTotalHours(int hours);
    std::string getName()const;
    bool setName(std::string name);
    int getID() const;
    bool setID(int id);
    Agent* getSupervisor()const;
    bool setAgent(Agent* supervisor);
    bool setDay(int day);
    int getDay() const;
    int getPriority() const;
    bool setPriority(int priority);

    //need getters and setters
private:   
    //hours represent start time for meeting in collab type, if it is at -1 they are not needed
    //total hours represents how long
    std::vector<int> hours;
    int type;
    int totalHours;
    int priority;
    //day is only used if collab
    int day;
    std::string name;
    int id;
    Agent* prevSupervisor;

};
#endif
