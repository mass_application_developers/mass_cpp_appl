#include "Agent.h"

Agent::Agent(repast::AgentId id, int type, int hour, int day, Observer* observer)
{
	this->agentId = id;
	this->type = type;
	this->hour = hour;
	this->day = day;
	this->observer = observer;
	for(int i = 0; i < 7; i++)
	{
		for(int j = 0; j < 24; j++)
		{
			collab[i][j] = false;
		}
	}
	this->curr = nullptr;
}
Agent::Agent()
{
	this->type = -1;
	this->hour = -1;
	this->day = -1;
	this->observer = nullptr;
	for(int i = 0; i < 7; i++)
	{
		for(int j = 0; j < 24; j++)
		{
			collab[i][j] = false;
		}
	}
	this->curr = nullptr;
}

Agent::~Agent()
{

}

std::deque<Task*> Agent::getProduction()const
{
	return production;
}

std::queue<Agent*> Agent::getUnder() const
{
	return under;
}

void Agent::addUnder(Agent* agent)
{
	under.push(agent);
}

void Agent::addTask(Task* task)
{
	if(task->getType() == 1)
	{
		this->assignCollab(task);
		std::cout<<"Finished assigning collab task for Task: " << task->getID() << std::endl;
	}
	else
	{
		production.push_back(task);
	}
}

int Agent::getType() const
{
	return type;
}

void Agent::setType(int type)
{
	this->type = type;
}

int Agent::getDay() const
{
	return day;
}

void Agent::setDay(int day)
{
	this->day = day;
}

int Agent::getHour() const
{
	return hour;
}

void Agent::setHour(int hour)
{
	this->hour = hour;
}

void Agent::setObserver(Observer* observer)
{
	this->observer = observer;
}

Observer* Agent::getObserver()
{
	return observer;
}

void Agent::setCollab(int hour, int day)
{
	if(hour >= 0 && hour < 24 && day >=0 && day < 7)
	{
		collab[day][hour] = true;
	}
}

bool Agent::isCollabTime(int hour, int day)
{
	if(hour >= 0 && hour < 24 && day >=0 && day < 7)
	{
		return false;
	}
	return collab[day][hour];
}

void Agent::incrementHour()
{
	hour++;
	if(hour >= 24)
	{
		day = (day + 1)% 7;
		hour = 0;
	}
}

void Agent::setCurr()
{
	if(production.size()> 0)
	{
		curr = production.front();
	}
}

void Agent::removeCurr()
{
	curr = nullptr;
}

void Agent::finishTask(Task* task)
{
	if(this->type == task->getHours().size()-1 || task->getType() == 1)
	{
		taskComplete();
	}
	else
	{
		task->setAgent(this);
		Agent* toSend = under.front();
		under.pop();
		toSend->addTask(task);
		under.push(toSend);
	}
	removeCurr();
}

void Agent::assignCollab(Task* task)
{
	if(task == nullptr)
	{
		return;
	}
	int hours = task->getHours().at(this->type);
	if(hours >= 0)
	{
		int day = task->getDay();
		for(int i =0 ;i < task->getTotalHours(); i++)
		{
		  collab[day][hours] = true;
		  hours++;
		  if(hours > 24)
		  {
			  hours = 0;
			  day = (day + 1) % 7;
		  }
		}
	}
	if(task->getHours().size() - 1 == type || under.size() == 0)
	{
		return;
	}
	int i = under.size();
	bool found = false;
	int j = 0;
	while(found || j < i - 1 )
	{
		Agent* toSend = under.front();
		if(!toSend->isCollabTime(task->getHours().at(toSend->getType()),task->getDay()))
		{
			found = true;
			toSend->assignCollab(task);
			return;
		}
		under.pop();
		under.push(toSend);
		j++;
	}

}

void Agent::taskComplete()
{

	observer->addCompleted(curr->getID());
	delete curr;

}

bool Agent::inSet(int num)
{
	for(auto & f: workedOn)
	{
		if(f == num)
		{
			return true;
		}
	}
	return false;
}

void Agent::Process()
{
	int randomNum = rand()%100 + 1;
	if(randomNum <= 30)
	{
		if(curr->getSupervisor() != nullptr)
		{
			//send back to supervisor
			int hours = randomNum % 9;
			curr->getHours().at(curr->getSupervisor()->getType()) = hours;
			curr->getSupervisor()->addTask(curr);
			removeCurr();
		}
		else
		{

			int hours = curr->getHours().at(this->getType());
			hours = hours * 2;
			curr->getHours().at(this->getType()) = hours;
		}

	}

}

void Agent::Play()
{
	if( (production.size() <= 0 && curr == nullptr) || isCollabTime(this->getHour(),this->getDay()))
	{
		return;
	}
	if(curr == nullptr)
	{
		curr = chooseTask();
		if(!inSet(curr->getID()))
		{
			workedOn.insert(curr->getID());
			this->Process();
			incrementHour();
			return;
		}
	}
	int time = curr->getHours().at(this->type);
	time -= 1;
	curr->setHours(this->type,time);
	if(time <= 0)
	{
		finishTask(curr);
	}
	incrementHour();
	return;
}

Task* Agent::chooseTask()
{
	int randNum = rand()%100;
	Task* ret = nullptr;
	//priority
	if(randNum >=0 && randNum <=50)
	{
		int priority = -199;
		int index = 0;
		int prioIndex = 0;
		for(auto & x: production)
		{
			if(x->getPriority()>priority)
			{
				prioIndex = index;
				priority = x->getPriority();
				ret = x;
			}
			index++;
		}
		production.erase(production.begin()+prioIndex);

	}
	//age
	else if(randNum >=51 && randNum <= 70)
	{
		ret = production.front();
		production.pop_front();
	}
	//randomly
	else if(randNum >= 71 && randNum <= 80)
	{
		int random = randNum % production.size();
		ret = production.at(random);
		production.erase(production.begin() + random);
	}
	//most recent
	else
	{
		ret = production.back();
		production.pop_back();
	}
	return ret;

}
