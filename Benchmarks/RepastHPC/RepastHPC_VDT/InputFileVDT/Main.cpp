//Program to create the input file for VDT
//Output: A text file filled with tasks
//Either takes in argument or asks user to fill it out
//input needs to be two ints, amount of production tasks and amount of collab tasks
#include<iostream>
#include<fstream>
#include<time.h>
#include<vector>
using namespace std;

void createWriteCollab(int amount, ofstream& stream);
void createWriteProduction(int amount, ofstream& stream);
int id = 0;


int main(int argc, char** argv)
{
    int production,collab;
    if(argc != 3)
    {
        cout<<"Entering amount of collab tasks" << endl;
        cin>>collab;
        cout<<"Enter amount of production tasks" << endl;
        cin >> production;
    }
    else 
    {
        production = atoi(argv[1]);
        collab = atoi(argv[2]);
    }
    ofstream myFile;
    myFile.open("Tasks.txt",ofstream::out | ofstream::trunc);
    createWriteCollab(collab,myFile);
    myFile<<endl;
    createWriteProduction(production,myFile);
    myFile.close();
}

//Ouptut for collab ex
//toMeet: 6,6,-1,6,6 Type: 1, Priority: 9999, Length: 1, Name: Collab Meeting, ID: 0
//-1 means not needed

void createWriteCollab(int amount, ofstream& stream)
{
    stream <<"Collaboration" << endl;
    srand(time(NULL));
    for(int i = 0; i <amount; i++)
    {
        int toMeet = rand() % 24;
		int toLast = rand() % 2 + 1;
		int day = rand()%7;
        stream << "toMeet:";
        int success = 0;
        std::vector<int> hours;
		for(int j = 0; j < 5; j++)
		{
			int chance = rand()%100;
			if( chance > 35)
			{
                success++;
                hours.push_back(toMeet);
			}
			else
			{
                hours.push_back(-1);
			}
		}
        //if no one has meeting
        if(success == 0)
        {
            int rand1 = rand()%5;
            int rand2 = rand()%5;
            if(rand1 == rand2)
            {
                rand2 = (rand2 + 1) % 5;
            }
            hours.at(rand1) = toMeet;
            hours.at(rand2) = toMeet;
        }
        for(int k = 0; k < hours.size(); k++ )
        {
            stream<<hours.at(k)<<",";
        }
        stream << " Type:1, Priority:9999, Length:" << toLast<< ", ID:" << id << ", Day:" << day; 
        stream << endl;
        id++;
    }
}

void createWriteProduction(int amount, ofstream& stream)
{
    stream <<"Production" << endl;
    srand(time(NULL));
    for(int i =0; i <amount; i++)
    {
        int totalHours = 0;
        stream<< "Hours:";
        for(int j = 0; j < 5; j++)
		{
			int chance = rand()%100;
			if( chance > 35)
			{
				int hour = rand() % 9 + 1;
				stream<<hour<<",";
				totalHours += hour;
			}
			else
			{
				stream<<-1<<",";
			}
		}
        int priority = rand() % 100;
        stream << " Type:0, Priority:" <<priority<<", totalHours:" << totalHours<< ", ID:" << id; 
        stream << endl;
        id++;
    }
}