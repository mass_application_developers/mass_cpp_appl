# ask for number of computing nodes
read -p '# of computing nodes (including this one) = ' NODES

# copy the correct number of hosts to the host file (CSSmpdboot always runs all hosts in mpd.hosts)
cat /home/NETID/dslab/agent_simulation_benchmarking/host_master_list | head -$(($NODES-1)) > ~/mpd.hosts

# boot the mpi cluster
CSSmpdboot -n $NODES -v

# run simulation
mpirun -n $NODES ./main.exe config.props model.props

# exit cluster
mpdallexit
