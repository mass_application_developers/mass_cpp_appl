#include "Observer.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/AgentId.h"
#include <time.h>
#include <bits/stdc++.h>
#include <fstream>
#include <algorithm>
Observer::Observer(std::string propsFile, int argc, char **argv,
		boost::mpi::communicator *comm) {
	props = new repast::Properties(propsFile, argc, argv, comm);
	hour = 0;
	day = 0;

}
Observer::~Observer() {
	delete props;
}
void Observer::init(std::string environment) {
	createEngineers();
	createHierarchy();
	createTasks(environment);
	toComplete = toGive.size();
	distributeTasks();
}

void Observer::parseString(std::string engineer, int &project, int &ux,
		int &seniorEngineer, int &juniorEngineer, int &testEngineer) {
	std::vector<int> tokens;
	std::stringstream check(engineer);
	string amount;
	while (getline(check, amount, ',')) {
		tokens.push_back(stoi(amount));
	}
	if (tokens.size() >= 5) {
		project = tokens.at(0);
		ux = tokens.at(1);
		seniorEngineer = tokens.at(2);
		juniorEngineer = tokens.at(3);
		testEngineer = tokens.at(4);
	}

}
void Observer::createEngineers() {
	int worldSize = repast::RepastProcess::instance()->worldSize();
	int k = 0;
	while(true)
{
	std::string var = "var."
			+ std::to_string(k);
	std::string engineer = (this->props->getProperty(var));
	int lead = 5;
	int ux = 5;
	int senior = 5;
	int junior = 5;
	int testEngineer = 5;
	if (engineer.length() == 0) {
		break;
	}
	if(k % worldSize == repast::RepastProcess::instance()->rank())
	{
	parseString(engineer, lead, ux, senior, junior, testEngineer);


	int Engineerid = 0;
	int theAgents[] = { lead, ux, senior, junior, testEngineer };
	int rank = repast::RepastProcess::instance()->rank();
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < theAgents[i]; j++) {
			repast::AgentId id(Engineerid, rank, 0, rank);
			Agent *toAdd = new Agent(id, i, 0, 0, this);
			Engineerid++;
			this->agent.insert(toAdd);
			insertInQueue(toAdd);
		}
	}
}
	k++;
}
}

void Observer::createHierarchy() {
	for (auto &agents : agent) {
		createHierarchy(agents, agents->getType());
	}
}

void Observer::createHierarchy(Agent *agent, int type) {
	switch (type) {
	case 0:
		assignAgents(agent, ux);
		break;
	case 1:
		assignAgents(agent, seniorEngineer);
		break;
	case 2:
		assignAgents(agent, juniorEngineer);
		break;
	case 3:
		assignAgents(agent, testEngineer);
		break;
	case 4:
		break;
	default:
		break;
	}
}

void Observer::assignAgents(Agent *agent, std::deque<Agent*> &under) {
	std::vector<int> nums;
	for (int i = 0; i < under.size(); i++) {
		nums.push_back(i);
	}
	while (nums.size() > 0) {
		int random = rand() % nums.size();
		agent->addUnder(under.at(random));
		nums.erase(nums.begin() + random);
	}
}
void Observer::createTasks(std::string environment) {
	std::ifstream inFile;
	inFile.open(environment);
	string line;
	bool onCollab = true;
	if (!inFile.is_open()) {
		defaultTask();
	} else {
		while(getline(inFile,line))
		{
			if(line == "Collaboration")
			{
				continue;
			}
			else if (line == "Production")
			{
				onCollab = false;
				continue;
			}
			else if(line == " " || line.length()== 0)
			{
				continue;
			}
			parseLine(line,onCollab);
		}
	}
}

void Observer::parseLine(std::string line, bool collab)
{
	std::vector<int> hours;
	std::string name = "CollabMeeting";
	if(!collab)
	{
		name = "Production";
	}
	int day = 0;
	int values [6] {0,0,0,0,0,0};
	int index = 0;
	for(int i = 0; i < line.length(); i++)
	{
		if(line.at(i) == ':')
		{
			i++;
			string res = "";
			while(i< line.length() && line.at(i) != ' ')
			{
				res = res + line.at(i);
				i++;
			}
			if(index == 0)
			{
				string num = "";
				for(int j = 0; j < res.length(); j++)
				{
					if(res.at(j) == ',')
					{
						hours.push_back(stoi(num));
						num = "";
					}
					else
					{
						num = num + res.at(j);
					}
				}
			}
			else
			{
				int max = (1 > res.size()-1)?1:res.size()-1;
				values[index] = stoi(res.substr(0,max));
			}
			index++;
		}
	}

	Task* task = new Task(hours,values[1],values[2],values[3],name,values[4]);
	if(collab)
	{
		task->setDay(values[5]);
	}
	toGive.push(task);
}
void Observer::defaultTask() {
	std::cout << "Failed to open default tasks applied" << std::endl;
	srand(time(NULL));
	int numTasks = 25;
	int numCollab = 2;
	int numProduction = 23;
	int id = 0;
	//for collab tasks the vector represents if needed and when to meet
	//total hours represent how long
	//day represents what day to meet
	for (int i = 0; i < numCollab; i++) {
		int toMeet = rand() % 24;
		int toLast = rand() % 2 + 1;
		int day = rand() % 7;
		std::vector<int> needed;
		double multiplier = 2.0;
		for (int j = 0; j < 5; j++) {
			int chance = rand() % 100;
			if (chance * multiplier > 50) {
				needed.push_back(toMeet);
				multiplier = max(1.0, multiplier - 0.5);
			} else {
				needed.push_back(-1);
			}
		}
		//collab is type 1, production is type 0
		Task *task = new Task(needed, 1, 9999, toLast, "Meeting", id);
		task->setDay(day);
		toGive.push(task);
		id++;
	}
	for (int i = 0; i < numProduction; i++) {
		int totalHours = 0;
		std::vector<int> hours;
		for (int j = 0; j < 5; j++) {
			int chance = rand() % 100;
			if (chance > 15) {
				int hour = rand() % 9 + 1;
				hours.push_back(hour);
				totalHours += hour;
			} else {
				hours.push_back(-1);
			}
		}
		int priority = rand() % 100;
		Task *task = new Task(hours, 0, priority, totalHours, "Production", id);
		toGive.push(task);
		id++;
	}
}

void Observer::distributeTasks() {
	while (!toGive.empty()) {
		Task *task = toGive.front();
		if(task->getType() == 1)
		{
			toComplete--;
		}
		std::vector<int> hours = task->getHours();
		int i = 0;
		while (i < hours.size()) {
			if (hours.at(i) != -1) {
				break;
			}
			i++;
		}
		giveTasks(task, i);
		toGive.pop();
	}
}
void Observer::giveTasks(Task *task, int agentType) {
	switch (agentType) {
	case 0: {
		Agent *beginAgent = projectLead.front();
		beginAgent->addTask(task);
		projectLead.pop_front();
		projectLead.push_back(beginAgent);
	}
		break;
	case 1: {
		Agent *beginAgent = ux.front();
		beginAgent->addTask(task);
		ux.pop_front();
		ux.push_back(beginAgent);
	}
		break;
	case 2: {
		Agent *beginAgent = seniorEngineer.front();
		beginAgent->addTask(task);
		seniorEngineer.pop_front();
		seniorEngineer.push_back(beginAgent);
	}
		break;
	case 3: {
		Agent *beginAgent = juniorEngineer.front();
		beginAgent->addTask(task);
		juniorEngineer.pop_front();
		juniorEngineer.push_back(beginAgent);
	}
		break;
	case 4: {
		Agent *beginAgent = testEngineer.front();
		beginAgent->addTask(task);
		testEngineer.pop_front();
		testEngineer.push_back(beginAgent);
	}
		break;
	default: {
		this->addCompleted(task->getID());
	}
		break;
	}
}
void Observer::insertInQueue(Agent *agent) {
	int id = agent->getType();
	switch (id) {
	case 0:
		projectLead.push_back(agent);
		break;
	case 1:
		ux.push_back(agent);
		break;
	case 2:
		seniorEngineer.push_back(agent);
		break;
	case 3:
		juniorEngineer.push_back(agent);
		break;
	case 4:
		testEngineer.push_back(agent);
		break;
	default:
		break;
	}
}

void Observer::incrementHour() {
	hour++;
	if (hour > 24) {
		day += 1;
		hour = 0;
	}
}

void Observer::addCompleted(int id) {
	int rank = repast::RepastProcess::instance()->rank();
	std::cout << "Team " << " Finished Task " << id << " At Day: "
			<< day << " At Hour: " << hour << std::endl;
	completed.insert(id);
}

void Observer::go() {
	int deadLine = 20;
	int rank = repast::RepastProcess::instance()->rank();
	while (day <= deadLine) {
		if (toComplete <= completed.size()) {
			std::cout << "Team: " << rank << " Finished all tasks at Day: " << day << " At Hour: " << hour<< std::endl;
			incrementHour();
			break;
		}
		for (auto &a : agent) {
			a->Play();
		}
		incrementHour();
	}

}
