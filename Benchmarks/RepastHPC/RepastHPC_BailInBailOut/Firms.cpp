

#include "Firms.h"
#include <time.h>
Firms::Firms() {
	this->firmid = -1;
	this->owner = nullptr;
	this->ownerId = -1;
	this->productionCost = -1;
	this->liquidity = -1;
	this->dividends = -1;
	this->wages = -1;
	this->profit = -1;
	this->observer = nullptr;
	this->banks = nullptr;
}


Firms::Firms(repast::AgentId id, int firmid, Owner* owner, int ownerId, double productionCost, double liquidity, double dividends, double wages, double profits, Observer* observer)
{
	this->id = id;
	this->firmid = firmid;
	this->owner = owner;
	this->ownerId = ownerId;
	this->productionCost = productionCost;
	this->liquidity = liquidity;
	this->dividends = dividends;
	this->wages = wages;
	this->profit = profits;
	this->observer = observer;
	banks = nullptr;
}


Firms::~Firms() {
	// TODO Auto-generated destructor stub
}

void Firms::setOwner(Owner* owner)
{
	this->owner = owner;
}

void Firms::addWorker(Worker* worker)
{
	workers.push_back(worker);
	wages += worker->getWages();
}

void Firms::setProductionCost(double production)
{
	this->productionCost = production;
}

void Firms::setLiquidty(double liquidity)
{
	this->liquidity = liquidity;
}

void Firms::setDividend(double dividend)
{
	this->dividends = dividend;
}

void Firms::setWages(double wages)
{
	this->wages = wages;
}

void Firms::setProfit(double profit)
{
	this->profit = profit;
}

void Firms::setFirmId(int id)
{
	this->firmid = id;
}

void Firms::setBanks(repast::SharedContext<Bank>* banks)
{
	this->banks = banks;
}

Owner* Firms::getOwner() const
{
	return owner;
}

int Firms::getFirmId() const
{
	return firmid;
}

int Firms::getOwnerID() const
{
	return ownerId;
}

std::vector<Worker*> Firms::getWorkers() const
{
	return workers;
}

double Firms::getProductionCost() const
{
	return productionCost;
}

double Firms::getLiquidity() const
{
	return liquidity;
}

double Firms::getDividends() const
{
	return dividends;
}

double Firms::getWages() const
{
	return wages;
}

double Firms::getProfit() const
{
	return profit;
}

std::map<int,std::tuple<double,double>> Firms::getDebt() const
{
	return debt;
}

double Firms::calculateProductionCost()
{
    srand( (unsigned)time( NULL ) );
	double modifier = (float) rand()/RAND_MAX  + 0.5;
	productionCost *= modifier;
	return productionCost;
}

double Firms::calculateProfit()
{
    srand( (unsigned)time( NULL ) );

	double x = (float) rand()/RAND_MAX + 0.5;
	profit = (productionCost * x) - productionCost;
	return profit;
}

void Firms::setZero()
{
	this->profit = 0;
	this->dividends = 0;
	//this->wages = 0;
}

double Firms::calculateDividends()
{
	if(profit > 0)
	{
	dividends = profit * 0.2;
	profit -= dividends;
	}
	return dividends;
}

double Firms::calculateLiquidity()
{
	liquidity += profit;
	return liquidity;
}

bool Firms::receiveLoan(int k)
{
	if(k > banks->size())
	{
		k = banks->size();
	}
	std::vector<Bank*> toLoan;
	banks->selectAgents(k,toLoan,true);
	double minInterestRate = 2134;
	int minId = -1;
	double amountNeeded = this->liquidity * -1;
	for(auto & f: toLoan)
	{
		if(f->getInterestRate()< minInterestRate)
		{
			minInterestRate = f->getInterestRate();
			minId = f->getBankId();
		}
	}
	//failed to find a loan
	if(minId == -1)
	{
		return false;
	}
	//0 is owner
	//1 is firm
	//2 is bank
	//3 is worker
	//messenger wil always add, to increase number positive to decrease number negative
	observer->createMessenger(2,minId,this->liquidity);
	auto tuple = std::make_tuple(amountNeeded,minInterestRate);
	debt[minId] = tuple;
	//std::cout<<"Got loan from " <<minId << "Amount: "<<amountNeeded<<std::endl;
	this->liquidity += amountNeeded;
	return true;
}

bool Firms::increaseInterest()
{
	for(auto& f: debt)
	{
		double amount = std::get<0>(f.second);
		double interest = std::get<1>(f.second);
		double increase = amount * (1 + interest);
		//std::cout<<"Firm Broke" <<std::endl;
		debt.at(f.first) = std::make_tuple(increase,interest);
	}
	return true;
}

bool Firms::payBank()
{
	for(auto& f: debt)
	{
		double amount = std::get<0>(f.second);
		double interest = std::get<1>(f.second);
		double toPay = amount / 2;
		if(liquidity > toPay)
		{
			amount -= toPay;
			liquidity -= toPay;
			debt.at(f.first) = std::make_tuple(amount,interest);
			observer->createMessenger(2,f.first,toPay);
		}
		else
		{
			liquidity = -1;
		}

	}
	return true;
}

bool Firms::payWages()
{
	profit -= wages;
	if(profit > 0)
	{
		owner->increaseCapital(profit * 0.2);
		profit *= 0.8;
	}
	return true;
}

bool Firms::getOwnerLoan()
{
	if(owner->getCapital() > liquidity * -1 )
	{
		double amount = liquidity * -1;
		owner->setCapital(owner->getCapital()-amount);
		liquidity += amount;
		return true;
	}
	return false;
}

bool Firms::play()
{
	//std::cout<<"Increased Interest" << std::endl;
	increaseInterest();
	//std::cout << "Set everything to zero" << std::endl;
	setZero();
	//std::cout << "Calculating production Cost"<<std::endl;
	calculateProductionCost();
	//std::cout<<"Calculating profits" <<std::endl;
	calculateProfit();
	//std::cout<<"Calculating dividends" << std::endl;
	calculateDividends();
	//std::cout<<"Paying wages" <<std::endl;
	payWages();
	//std::cout<<"Calculating liquidity" << std::endl;
	calculateLiquidity();
	//std::cout<<"Paying bank" << std::endl;
	payBank();
	if(this->liquidity < 0)
	{
		if(getOwnerLoan())
		{
			return true;
		}
		else if(debt.size() <=0)
		{
			receiveLoan(3);
		}
	}
	return this->liquidity >= 0;
}

void Firms::reset(double productionCost, double liquidity, double dividends, double wages, double profits)
{
    srand( (unsigned)time( NULL ) );
	this->productionCost = productionCost;
	this->liquidity = liquidity;
	this->dividends = dividends;
	this->profit = profits;
	this->owner->setCapital(liquidity * ((float) rand()/RAND_MAX  + 0.5));
	this->debt.clear();
	//std::cout<<debt.size()<<std::endl;

}
