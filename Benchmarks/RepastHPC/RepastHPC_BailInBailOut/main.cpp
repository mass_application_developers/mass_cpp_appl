#include<iostream>
#include<boost/mpi.hpp>
#include <chrono>


#include "repast_hpc/RepastProcess.h"
#include "Observer.h"


using namespace std;
using namespace std::chrono;
using namespace repast;

//using namespace relogo;

int main(int argc, char **argv)
{
	boost::mpi::environment env(argc,argv);
	string config,properties;
	boost::mpi::communicator world;
	//tentative
	if(argc >= 3)
	{
		config = argv[1];
		properties = argv[2];
	}
	else
	{
		if(world.rank() == 0)
		{
			cout<< "Need x arguments" << endl;
		}
		return -1;
	}
	if(config.size() > 0 && properties.size() >0)
	{
		auto start = std::chrono::high_resolution_clock::now();
		//std::cout<<"init"<<std::endl;
		RepastProcess::init(config, &world);
		//std::cout<<"Observer construcotr" << std::endl;
		Observer* observer = new Observer(properties,argc,argv, &world);
		//std::cout<<"Schedule Runner" << std::endl;
		repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();
		//std::cout<<"observer init" << std::endl;
		//observer->init();
		//std::cout<<"init schedule runner" << std::endl;
		observer->initSchedule(runner);
		//std::cout<<"Running"<<std::endl;
		runner.run();
		auto stop = std::chrono::high_resolution_clock::now();
		auto duration = duration_cast<milliseconds>(stop - start);
		cout << duration.count() << endl;
		delete observer;
	}
	 else
	{
		if(world.rank() == 0)
		{
			cout<<"Need configuration and/or properties file" << endl;
			return -1;
		}
	}
	RepastProcess:: instance()->done();
	if(world.rank() == 0)
	{
		cout<<"Finished" << endl;
	}

	return 0;
}




