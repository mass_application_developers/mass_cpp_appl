#include "Owner.h"

Owner::Owner() {
	// TODO Auto-generated constructor stub
	capital = -1;
	firm = nullptr;
	firmId = -1;
	bank = nullptr;
	observer = nullptr;
	wages = 0;
	this->ownerId = -1;

}

Owner::~Owner() {
	// TODO Auto-generated destructor stub
}

Owner::Owner(repast::AgentId id, double capital, int firmId, Firms* firm, Bank* bank, Observer* observer)
{
	this->id = id;
	this->capital = capital;
	this->firmId = firmId;
	this->firm = firm;
	this->bank = bank;
	this->observer = observer;
	wages = 0;
	this->ownerId = id.id();
}

void Owner::increaseCapital(double amount)
{
	this->capital += amount;
}

void Owner::giveCapital(double amount)
{
	this->capital -= amount;
}
double Owner::getCapital() const
{
	return capital;
}

Firms* Owner::getFirm() const
{
	return firm;
}

Bank* Owner::getBank() const
{
	return bank;
}

double Owner::getWages() const
{
	return wages;
}

void Owner::setCapital(double capital)
{
	this->capital = capital;
}

void Owner::setFirm(Firms* firm)
{
	this->firm = firm;
}

int Owner::getFirmId() const
{
	return firmId;
}

void Owner::setFirmId(int id)
{
	firmId = id;
}

void Owner::setBank(Bank* bank)
{
	this->bank = bank;
}

void Owner::setWages(double wages)
{
	this->wages = wages;
}

