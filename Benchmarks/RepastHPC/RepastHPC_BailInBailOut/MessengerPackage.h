
#ifndef MESSENGERPACKAGE_H
#define MESSENGERPACKAGE_H

#include "repast_hpc/AgentId.h"

struct MessengerPackage {
public:
	template<class Archive>
	void serialize (Archive &ar, const unsigned int version)
	{
		ar & id;
		ar & proc;
		ar & type;
		ar & currentProc;

		ar & receiveid;
		ar & receivetype;
		ar & amount;

	}
	int id, proc, type, currentProc,receiveid,receivetype;
	double amount;

	repast::AgentId getId() const
	{
		return repast::AgentId(id,proc,type,currentProc);
	}
	MessengerPackage(int _id, int _proc, int _type, int _currentProc, int _receiveid, int _receivetype, double _amount )
	{
		id = _id;
		proc = _proc;
		type = _type;
		currentProc = _currentProc;
		receiveid = _receiveid;
		receivetype = _receivetype;
		amount = _amount;
	}
	MessengerPackage(){};

};



#endif /* MESSENGERPACKAGE_H */
