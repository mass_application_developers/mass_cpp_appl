
#ifndef MESSENGER_H_
#define MESSENGER_H_

#include "repast_hpc/AgentId.h"

class Messenger {
public:
	Messenger();
	Messenger(repast::AgentId id, int receiveId, int receiveType, double amount);
	virtual ~Messenger();
	virtual repast::AgentId& getId(){ return id;}
	virtual const repast::AgentId& getId() const { return id;}
	int getReceiveId() const;
	int getReceiveType() const;
	double getAmount() const;
private:
	repast::AgentId id;
	int receiveId;
	int receiveType;
	double amount;


};

#endif /* MESSENGER_H_ */
