
#ifndef OWNER_H
#define OWNER_H
#include "Bank.h"
#include "Firms.h"
#include "Observer.h"
#include "repast_hpc/AgentId.h"

class Firms;
class Bank;
class Observer;

class Owner {
public:
	Owner();
	Owner(repast::AgentId id,double capital, int firmId, Firms* firm, Bank* bank, Observer* observer);
	virtual ~Owner();
	virtual repast::AgentId& getId(){ return id;}
	virtual const repast::AgentId& getId() const { return id;}
	void increaseCapital(double amount);
	void giveCapital(double amount);
	double getCapital() const;
	Firms* getFirm() const;
	Bank* getBank() const;
	double getWages() const;
	void setCapital(double capital);
	void setFirm(Firms* firm);
	int getFirmId() const;
	void setFirmId(int id);
	void setBank(Bank* bank);
	void setWages(double wages);
private:
	double wages;
	double capital;
	Firms* firm;
	int firmId;
	int ownerId;
	repast::AgentId id;
	Bank* bank;
	Observer* observer;
};

#endif /* OWNER_H_ */
