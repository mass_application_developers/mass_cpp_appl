
#ifndef BANK_H
#define BANK_H

#include<map>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/RepastProcess.h"
#include "Messenger.h"
#include "Observer.h"

class Observer;
class Messenger;

class Bank {
public:
	Bank();
	virtual ~Bank();

	Bank(repast::AgentId id, int bankId, double liquidity, double interestRate, double debt,Observer* observer);
	//getters and setters
	void setBankId(int bankId);
	void setLiquidity(int liquidity);
	void setInterestRate(double interest);
	void setTotalDebt(int debt);
	void setBanks(repast::SharedContext<Bank>*  banks);
	void setObserver(Observer* observer);

	virtual repast::AgentId& getId(){ return id;}
	virtual const repast::AgentId& getId() const { return id;}
	int getBankId() const;
	double getLiquidity() const;
	double getInterestRate() const;
	double getTotalDebt() const;
	std::map<int,std::tuple<double,double>> getDebt() const;

	//actions
	void play();
	bool getLoan(int k);
	void receiveDeposit(int amount);
	void giveLoan(double amount);
	bool isBankrupt();
	void payLoans();
	void increaseInterest();
	void randomizeRate();


private:
	int bankId;
	repast::AgentId id;
	double liquidity;
	double interestRate;
	double totalDebt;
	repast::SharedContext<Bank>* banks;
	std::map<int,std::tuple<double,double>> debt;
	Observer* observer;
};

#endif /* BANK_H_ */
