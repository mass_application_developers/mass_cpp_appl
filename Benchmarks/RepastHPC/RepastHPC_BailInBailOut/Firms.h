

#ifndef FIRMS_H
#define FIRMS_H

#include <vector>
#include <tuple>
#include <map>
#include "Owner.h"
#include "Worker.h"
#include "Observer.h"
#include "Bank.h"
#include "repast_hpc/SharedContext.h"

#include "repast_hpc/AgentId.h"

class Owner;
class Bank;
class Observer;
class Worker;

class Firms {
public:
	Firms();
	virtual ~Firms();
	Firms(repast::AgentId id, int firmid, Owner* owner, int ownerId, double productionCost, double liquidity, double dividends, double wages, double profits, Observer* observer);
	//getters and setters
	void setOwner(Owner* owner);
	void addWorker(Worker* worker);
	void setProductionCost(double production);
	void setLiquidty(double liquidity);
	void setDividend(double dividend);
	void setWages(double wages);
	void setProfit(double profit);
	void setFirmId(int id);
	void setBanks(repast::SharedContext<Bank>*  banks);
	virtual repast::AgentId& getId(){ return id;}
	virtual const repast::AgentId& getId() const { return id;}
	Owner* getOwner() const;
	int getFirmId() const;
	int getOwnerID() const;
	std::vector<Worker*> getWorkers() const;
	double getProductionCost() const;
	double getLiquidity() const;
	double getDividends() const;
	double getWages() const;
	double getProfit() const;
	std::map<int,std::tuple<double,double>> getDebt() const;

	//actions
	double calculateProductionCost();
	double calculateProfit();
	double calculateDividends();
	double calculateLiquidity();
	void setZero();
	bool receiveLoan(int k);
	bool getOwnerLoan();
	bool increaseInterest();
	bool payBank();
	bool payWages();
	bool play();
	void reset(double productionCost, double liquidity, double dividends, double wages, double profits);
private:
	int firmid;
	repast::AgentId id;
	Owner* owner;
	int ownerId;
	std::vector<Worker*> workers;
	double productionCost;
	double liquidity;
	double dividends;
	double wages;
	double profit;
	repast::SharedContext<Bank>* banks;
	std::map<int,std::tuple<double,double>> debt;
	Observer* observer;
};

#endif /* FIRMS_H_ */
