
#include "Bank.h"
#include <time.h>
Bank::Bank() {
	bankId = -1;
	liquidity = -1;
	interestRate = -1;
	totalDebt = -1;
	banks = nullptr;
	observer = nullptr;
}

Bank::~Bank() {
	// TODO Auto-generated destructor stub
}

Bank::Bank(repast::AgentId id, int bankId, double liquidity, double interestRate, double debt, Observer* observer)
{
	this->id = id;
	this->bankId = bankId;
	this->liquidity = liquidity;
	this->interestRate = interestRate;
	this->totalDebt = debt;
	banks = nullptr;
	this->observer = observer;
}

void Bank::setBankId(int bankId)
{
	this->bankId = bankId;
}

void Bank::setLiquidity(int liquidity)
{
	this->liquidity = liquidity;
}

void Bank::setInterestRate(double interest)
{
	this->interestRate = interest;
}

void Bank::setTotalDebt(int debt)
{
	this->totalDebt = debt;
}

void Bank::setObserver(Observer* observer)
{
	this->observer = observer;
}
void Bank::setBanks(repast::SharedContext<Bank>* banks)
{
	this->banks = banks;
}

int Bank::getBankId() const
{
	return bankId;
}

double Bank::getLiquidity() const
{
	return liquidity;
}

double Bank::getInterestRate() const
{
	return interestRate;
}

double Bank::getTotalDebt() const
{
	return totalDebt;
}

std::map<int,std::tuple<double,double>> Bank::getDebt() const
{
	return debt;
}

void Bank::giveLoan(double amount)
{
//	std::cout<<"Giving loan " << amount<< "Liquidity Before: " << this->liquidity<< std::endl;
	this->liquidity += amount;
	//std::cout<<"Giving loan " << amount<< "Liquidity Afor: " << this->liquidity<< std::endl;

}

bool Bank::getLoan(int k)
{
	if(k > banks->size())
	{
		k = banks->size();
	}
	std::vector<Bank*> toLoan;
	toLoan.push_back(this);
	banks->selectAgents(k,toLoan,true);
	double minInterestRate = 2134;
	int minId = -1;
	double amountNeeded = this->liquidity * -1;
	for(auto & f: toLoan)
	{
		if(f->getInterestRate()< minInterestRate && f->getLiquidity() > amountNeeded)
		{
			minInterestRate = f->getInterestRate();
			minId = f->getBankId();
		}
	}
	//failed to find a loan
	if(minId == -1)
	{
		return false;
	}
	//0 is owner
	//1 is firm
	//2 is bank
	//3 is worker
	//messenger wil always add, to increase number positive to decrease number negative
	observer->createMessenger(2,minId,this->liquidity);
	auto tuple = std::make_tuple(amountNeeded,minInterestRate);
	debt[minId] = tuple;
	this->liquidity += amountNeeded;
	totalDebt += amountNeeded;
	return true;
}

void Bank::receiveDeposit(int amount)
{
	this->liquidity += amount;
}

bool Bank::isBankrupt()
{
	return 0 > liquidity + totalDebt;
}

void Bank::payLoans()
{
	double amountPaid = 0;
	for(auto& f: debt)
	{
		double amount = std::get<0>(f.second);
		double interest = std::get<1>(f.second);
		double toPay = amount / 2;
		if(liquidity > toPay)
		{
			amount -= toPay;
			liquidity -= toPay;
			debt.at(f.first) = std::make_tuple(amount,interest);
			amountPaid += toPay;
			observer->createMessenger(2,f.first,toPay);
		}
		else
		{
			liquidity = -1;
		}

	}
	totalDebt -= amountPaid;
}

void Bank::increaseInterest()
{
	double increaseAmount = 0;
	for(auto& f: debt)
	{
		double amount = std::get<0>(f.second);
		double interest = std::get<1>(f.second);
		double increase = amount * (1 + interest);
		increaseAmount += (increase - amount);
		debt.at(f.first) = std::make_tuple(increase,interest);
	}
	totalDebt += increaseAmount;
}

void Bank::randomizeRate()
{
    srand( (unsigned)time( NULL ) );
	this->interestRate = (float)rand()/RAND_MAX;;
}

void Bank::play()
{
	this->payLoans();
	this->increaseInterest();
	if(liquidity < 0)
	{
		getLoan(3);
	}
}
