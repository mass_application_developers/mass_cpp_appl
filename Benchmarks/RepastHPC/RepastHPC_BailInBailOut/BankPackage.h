
#ifndef BANKPACKAGE_H
#define BANKPACKAGE_H

#include "repast_hpc/AgentId.h"
class BankPackage
{
public:
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version)
	{
		ar & id;
		ar & proc;
		ar & type;
		ar & currentProc;

		ar & bankId;
		ar & liquidity;
		ar & interestRate;
		ar & totalDebt;

	}
	int id, proc, type, currentProc;
	int bankId;
	double liquidity, interestRate, totalDebt;

	repast::AgentId getId() const
	{
		return repast::AgentId(id,proc,type,currentProc);
	}

	BankPackage(int _id, int _proc, int _type, int _currentProc, int _bankId, double _liquidty, double _interestRate, double _totalDebt)
	{
		id = _id;
		proc = _proc;
		type = _type;
		currentProc = _currentProc;
		bankId = _bankId;
		liquidity = _liquidty;
		interestRate = _interestRate;
		totalDebt = _totalDebt;
	}
	BankPackage(){};
};




#endif /* BANKPACKAGE_H_ */
