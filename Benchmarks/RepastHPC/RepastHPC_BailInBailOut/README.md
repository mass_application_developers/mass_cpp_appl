# Bail-In/Bail-Out
This set of instructions assumes that you have already set up the mpi on your NETID.
If you have not set it up yet, please refer to the mpi_setup.txt (provided by Dr. Fukuda) file in this folder.

## Configuring the Program Files in Your Directory
1.) Copy the files over from your local directory to the hermes directory (You can use a program called winSCP to help transfer the files)
2.) Connect to the hermes machines at UWB (You can use a program called putty to help connect or a simple ssh should do the trick)
3.) Change your directory to the file directory

Compile the benchmark’s source files with  “./compile.sh” to ensure all executable files are up to date.

## Important Files
makefile:
The make file includes commands that will help you compile and run the program.
e.g make ./compile

model.props: 
The model.props file is used to establish the variables and set up the world. You can configure these values to change the simulation.

## Providing Inputs 


## Running the Program
1a.) If you have not already started the mpd type the command CSSmpdboot 
1b.) If you have not already compiled the program, type the command make ./compile
2.) To run the program simply type the command (replacing # with the amount of nodes to run)
	mpirun -n # ./main.exe config.props model.props

1.	Execute run.sh with “./run.sh”. 
2.	Enter the following arguments when prompted:
    *	Number of Nodes – the number of computing nodes (machines) you want to use to run your program.