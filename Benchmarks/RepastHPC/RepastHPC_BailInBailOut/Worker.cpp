
#include "Worker.h"

Worker::Worker() {
	// TODO Auto-generated constructor stub
	workerId = -1;
	wages = -1;
	consumption = -1;
	deposit = -1;
	capital = -1;
	bank = nullptr;
	observer = nullptr;

}

Worker::Worker(repast::AgentId id, int workerId, double wages, double consumption, double deposit, double capital, Bank* bank, Observer* observer)
{
	this->id = id;
	this->workerId = workerId;
	this->wages = wages;
	this->consumption = consumption;
	this->deposit = deposit;
	this->capital = capital;
	this->bank = bank;
	this->observer = observer;

}
Worker::~Worker() {
	// TODO Auto-generated destructor stub
}

void Worker::setBank(Bank* bank)
{
	this->bank = bank;
}
void Worker::setWorkerId(int id)
{
	this->workerId = id;
}

void Worker::setWages(double wages)
{
	this->wages = wages;
}

void Worker::setConsumption(double consumption)
{
	this->consumption = consumption;
}

void Worker::setDeposit(double deposit)
{
	this->deposit = deposit;
}

void Worker::setCapital(double capital)
{
	this->capital = capital;
}

void Worker::setObserver(Observer* observer)
{
	this->observer = observer;
}

int Worker::getWorkerId() const
{
	return workerId;
}

double Worker::getWages() const
{
	return wages;
}

double Worker::getConsumption() const
{
	return consumption;
}

double Worker::getCapital() const
{
	return capital;
}

Bank* Worker::getBank() const
{
	return bank;
}

bool Worker::depositFunds(int amount)
{
	//0 is owner
	//1 is firm
	//2 is bank
	//3 is worker
	//messenger wil always add, to increase number positive to decrease number negative
	observer->createMessenger(2,bank->getBankId(),amount);
	this->capital -= amount;
	return true;
}

void Worker::play()
{
	receiveWages();
	consume();
	depositFunds(this->wages);
}

void Worker::receiveWages()
{
	this->capital += wages;
}

void Worker::consume()
{
	double consumption = 1.0 - 0.7;
	this->wages *= consumption;
}
