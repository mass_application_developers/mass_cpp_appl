

#include "Observer.h"
#include "repast_hpc/initialize_random.h"
#include <time.h>



Observer::~Observer() {
	// TODO Auto-generated destructor stub
	delete props;
	delete messengerProvider;
	delete messengerReceiver;
	delete bankProvider;
	delete bankReceiver;
}

Observer::Observer(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm) : banks(comm), firms(comm), workers(comm), owners(comm), messenger(comm)
{
	props = new repast::Properties(propsFile, argc, argv, comm);
	initializeRandom(*props, comm);
	messengerId = 0;
	messengerProvider = new MessengerPackageProvider(&messenger);
	messengerReceiver = new MessengerPackageReceiver(&messenger);
	bankProvider = new BankPackageProvider(&banks);
	bankReceiver = new BankPackageReceiver(&banks);
}

void Observer::init()
{
	int banks, firms, workers;
	banks = 20;
	firms = 2000;
	workers = 700000;
	setConstants(workers,firms,banks);
	createBanks(banks);
	createFirms(firms);
	createWorkers(workers);
	chooseBanks();
	distributeWorkers();
	std::cout<<repast::RepastProcess::instance()->rank() << " SIZE: " << this->banks.size()<<std::endl;

}

//messenger is type 4
void Observer::createMessenger(int receiveType, int receiveId, int amount)
{
	int rank = repast::RepastProcess::instance()->rank();
	//std::cout<<"Created Messenger for amount " << amount << "from Rank " << rank << std::endl;
	repast::AgentId id(messengerId,rank,4);
	Messenger* messenge = new Messenger(id,receiveId,receiveType,amount);
	messenger.addAgent(messenge);
	messengerId++;
}

double Observer::getProductionCost() const
{
	return productionCost;
}

double Observer::getInterestRate() const
{
	return interestRate;
}

double Observer::getLiquidity() const
{
	return liquidity;
}

repast::SharedContext<Firms> Observer::getFirms() const
{
	return firms;
}

repast::SharedContext<Worker> Observer::getWorkers() const
{
	return workers;
}

repast::SharedContext<Bank> Observer::getBanks() const
{
	return banks;
}

repast::SharedContext<Owner> Observer::getOwners() const
{
	return owners;
}

repast::SharedContext<Messenger> Observer::getMessenger() const
{
	return messenger;
}

void Observer::setConstants(int & workers, int & firms, int & banks)
{
	srand( (unsigned)time( NULL ) );
	std::string amtWork,amtFirm,amtBank,productionCost, interestRate, liquidity;
	amtWork = props->getProperty("var.workers");
	amtFirm = props->getProperty("var.firms");
	amtBank = props->getProperty("var.banks");
	if(amtWork.length()!= 0 && amtFirm.length()!= 0 && amtBank.length()!= 0)
	{
		workers = std::stoi(amtWork);
		firms = std::stoi(amtFirm);
		banks = std::stoi(amtBank);
	}
	this->productionCost = 50000 * ((float) rand()/RAND_MAX + 1);
	this->interestRate = (float)rand()/RAND_MAX;
	this->liquidity = 150000 * ((float)rand()/RAND_MAX + 1);
	productionCost = props->getProperty("var.production");
	interestRate = props->getProperty("var.interest");
	liquidity = props->getProperty("var.liquidity");
	if(productionCost.length()!= 0 && interestRate.length() != 0 && liquidity.length() != 0)
	{
		this->productionCost = std::stod(productionCost);
		this->interestRate = std::stod(interestRate);
		this->liquidity = std::stod(liquidity);
	}

}


//0 is owner
//1 is firm
//2 is bank
//3 is worker
void Observer::createBanks(int amount)
{
	int worldSize = repast::RepastProcess::instance()->worldSize();
	int type = 2;
	int id = 0;
	repast::AgentRequest req(repast::RepastProcess::instance()->rank());
	for(int i = 0; i < amount; i++)
	{
		double capital = liquidity * ((float)rand()/RAND_MAX + 2);
		repast::AgentId agentId(id,0,type,i % worldSize);
		if(i % worldSize == repast::RepastProcess::instance()->rank())
		{
		Bank* bank = new Bank(agentId,id,capital,interestRate,0,this);
		bank->setBanks(&this->banks);
		banks.addAgent(bank);
		}
		else
		{
			//agentId.currentRank(repast::RepastProcess::instance()->rank());
			req.addRequest(agentId);

		}
		id++;
	}
	repast::RepastProcess::instance()->requestAgents<Bank,BankPackage,BankPackageProvider,BankPackageReceiver>(banks,req,*bankProvider,*bankReceiver,*bankReceiver);
//	std::cout<<repast::RepastProcess::instance()->rank() << " SIZE: " << banks.size()<<std::endl;

	//repast::RepastProcess::instance()->synchronizeAgentStates<BankPackage,BankPackageProvider,BankPackageReceiver>(*bankProvider,*bankReceiver);

}

void Observer::createFirms(int amount)
{
	int worldSize = repast::RepastProcess::instance()->worldSize();
	int firmType = 1;
	int ownerType = 0;
	int firmId = 0;
	int ownerId = 0;
	for(int i = 0; i < amount; i++)
	{
		if(i % worldSize == repast::RepastProcess::instance()->rank())
		{
		double firmCapital = liquidity * ((float)rand()/RAND_MAX + 1);
		double ownerCapital = liquidity * ((float)rand()/RAND_MAX + 0.5);
		repast::AgentId ownerAgent(ownerId,0,ownerType,i % worldSize);
		repast::AgentId firmAgent(firmId,0,firmType,i % worldSize);
		Owner* owner = new Owner(ownerAgent,ownerCapital,firmId,nullptr,nullptr,this);
		Firms* firm = new Firms(firmAgent,firmId,owner,ownerId,productionCost,firmCapital,0,0,0,this);
		firm->setBanks(&this->banks);
		owner->setFirm(firm);
		firms.addAgent(firm);
		owners.addAgent(owner);
		}
		firmId++;
		ownerId++;
	}
}

void Observer::createWorkers(int amount)
{
	int worldSize = repast::RepastProcess::instance()->worldSize();
	int type = 3;
	int id = 0;
	double defaultWages = productionCost * firms.size() * 0.1 * (1-0.2);
	//std::cout<<"Default Wages" << defaultWages <<std::endl;
	for(int i = 0; i < amount; i++)
	{
		if(i % worldSize == repast::RepastProcess::instance()->rank())
		{
		repast::AgentId agentId(id,0,type,i % worldSize);
		Worker* worker = new Worker(agentId,id,0,0,0,0,nullptr,this);
		double wages = defaultWages / (amount * ((float)rand()/RAND_MAX + 0.5));
		//std::cout<<wages<<std::endl;
		worker->setWages(wages);
		workers.addAgent(worker);
		}
		id++;
	}
}

void Observer::chooseBanks()
{
	std::vector<Bank*> theBanks;
	std::set<Worker*> theWorkers;
	std::set<Owner*> theOwners;
	banks.selectAgents(theBanks);
	if(workers.size() > 0)
	{
	workers.selectAgents(theWorkers);
	}
	if(owners.size() > 0)
	{
	owners.selectAgents(theOwners);
	}
	int index = 0;
	for(auto & f: theWorkers)
	{
		f->setBank(theBanks.at(index));
		index = (index + 1) % theBanks.size();
	}

	for(auto & f: theOwners)
	{
		f->setBank(theBanks.at(index));
		index = (index + 1) % theBanks.size();
	}
}

void Observer::distributeWorkers()
{
	std::vector<Worker*>localWorkers;
	std::vector<Firms*>localFirms;
	int index = 0;
	if(firms.size()>0)
	{
	firms.selectAgents(repast::SharedContext<Firms>::LOCAL,firms.size(),localFirms);

	}
	if(workers.size() > 0)
	{
	workers.selectAgents(repast::SharedContext<Worker>::LOCAL,workers.size(),localWorkers);
	}
	for(auto& f: localWorkers)
	{

		localFirms.at(index)->addWorker(f);
		index = (index + 1) % localFirms.size();
	}

}

void Observer::sendMessenger()
{
	int worldSize = repast::RepastProcess::instance()->worldSize();
	if(messenger.size() > 0)
	{
	std::vector<Messenger*> localMessengers;
	messenger.selectAgents(repast::SharedContext<Messenger>::LOCAL,messenger.size(),localMessengers);
	for(auto & f: localMessengers)
	{
		int toSend = f->getReceiveId() % worldSize;
		if(toSend != repast::RepastProcess::instance()->rank())
		{
			repast::RepastProcess::instance()->moveAgent(f->getId(),toSend);
		}
	}
	}
	repast::RepastProcess::instance()->synchronizeAgentStatus<Messenger,MessengerPackage,MessengerPackageProvider,MessengerPackageReceiver>(messenger,*messengerProvider,*messengerReceiver,*messengerReceiver);
	//synchronize call needed
}

void Observer::fulfillMessenger()
{
	std::vector<Messenger*> localMessengers;
	if(messenger.size() > 0)
	{
	messenger.selectAgents(messenger.size(),localMessengers);
	for(auto& f: localMessengers)
	{
		deposit(f->getReceiveType(),f->getAmount(),f->getReceiveId());
		repast::RepastProcess::instance()->agentRemoved(f->getId());
		messenger.removeAgent(f->getId());

	}
	}
	repast::RepastProcess::instance()->synchronizeAgentStatus<Messenger,MessengerPackage,MessengerPackageProvider,MessengerPackageReceiver>(messenger,*messengerProvider,*messengerReceiver,*messengerReceiver);

}

void Observer::deposit(int type, double amount, int id)
{
	switch (type)
	{
		//owner
		case 0:
			depositOwner(amount,id);
			break;
		//firm
		case 1:
			depositFirm(amount,id);
			break;
		//bank
		case 2:
			if(amount != 0)
			{
				//std::cout<<"Bank Deposited" << amount << std::endl;
			}
			//std::cout<<"Depositing" << amount<< std::endl;
			depositBank(amount,id);
			break;
		//worker
		case 3:
			depositWorker(amount,id);
			break;
	}
}

void Observer::depositOwner(double amount,int id)
{
	std::vector<Owner*> localOwners;
	owners.selectAgents(repast::SharedContext<Owner>::LOCAL,owners.size(),localOwners);
	for(auto & f: localOwners)
	{
		if(f->getId().id()== id)
		{
			f->giveCapital(amount);
		}
	}
}

void Observer::depositFirm(double amount, int id)
{
	std::vector<Firms*> localFirms;
	firms.selectAgents(repast::SharedContext<Firms>::LOCAL,firms.size(),localFirms);
	for(auto & f: localFirms)
	{
		if(f->getId().id()== id)
		{
			f->receiveLoan(amount);
		}
	}
}

void Observer::depositBank(double amount, int id)
{
	std::vector<Bank*> localBanks;
	banks.selectAgents(repast::SharedContext<Bank>::LOCAL,banks.size(),localBanks);
	for(auto & f: localBanks)
	{
		if(f->getId().id()== id)
		{
			if(amount < 0)
			{
				f->giveLoan(amount);
			}
			else
			{
				f->receiveDeposit(amount);
			}
		}
	}
}

void Observer::depositWorker(double amount, int id)
{
	std::vector<Worker*> localWorkers;
	if(workers.size() > 0)
	{
	workers.selectAgents(repast::SharedContext<Worker>::LOCAL,workers.size(),localWorkers);
	}
	for(auto & f: localWorkers)
	{
		if(f->getId().id()== id)
		{
			f->setWages(amount);
		}
	}

}


bool Observer::checkBankruptcy()
{
	//repast::RepastProcess::instance()->synchronizeAgentStates<BankPackage,BankPackageProvider,BankPackageReceiver>(*bankProvider,*bankReceiver);
	std::vector<Bank*> allBanks;
	banks.selectAgents(banks.size(),allBanks);
	//std::cout<<" Rank " << repast::RepastProcess::instance()->rank() << "Banks size " << allBanks.size() << std::endl;
	for(auto & f: allBanks)
	{
		std::cout<<f->getId() << " " << f->getLiquidity() <<  std::endl;
		if(f->isBankrupt())
		{
			std::cout<<"Bankrupt" << std::endl;
			return false;
		}
	}
	//std::cout<<endl;
	return true;
}

//banks need to be synchronized because of interestRate
void Observer::synchronizeBanks()
{
	//std::cout<<"Synchronizing" << std::endl;
	repast::RepastProcess::instance()->synchronizeAgentStates<BankPackage,BankPackageProvider,BankPackageReceiver>(*bankProvider,*bankReceiver);
	//std::cout<< "Done synchronizing" << std::endl;
}


void Observer::play()
{
	int round = 0;
	while(checkBankruptcy())
	{
		action();
		sendMessenger();
		fulfillMessenger();
		repast::RepastProcess::instance()->synchronizeAgentStates<BankPackage,BankPackageProvider,BankPackageReceiver>(*bankProvider,*bankReceiver);
		round++;
		//std::cout<<"Round: " << round << std::endl;
	}
	std::cout<<"Over" << std::endl;

}

void Observer::action()
{
	std::vector<Firms*> localFirms;
	if(firms.size() > 0)
	{
	firms.selectAgents(repast::SharedContext<Firms>::LOCAL,firms.size(),localFirms);
	}

	std::vector<Worker*> localWorkers;
	if(workers.size() > 0)
	{
	workers.selectAgents(repast::SharedContext<Worker>::LOCAL,workers.size(),localWorkers);
	}
	std::vector<Bank*> localBanks;
	if(banks.size() > 0)
	{
	banks.selectAgents(repast::SharedContext<Bank>::LOCAL,banks.size(),localBanks);
	}
	for(auto & f: localFirms)
	{
		//outta business
		if(!f->play())
		{
			f->reset(this->productionCost,this->liquidity,0,0,0);
		}
	}
	for(auto & f: localWorkers)
	{
		f->play();
	}
	for(auto & f: localBanks)
	{
		//std::cout<<f->getBankId()<< " balance " << f->getLiquidity()<<std::endl;
		f->play();
	}
}

//Package Provider and receiver Implementation

MessengerPackageProvider::MessengerPackageProvider(repast::SharedContext<Messenger>* messenger)
{
	this->messenger = messenger;
}

MessengerPackageReceiver::MessengerPackageReceiver(repast::SharedContext<Messenger>* messenger)
{
	this->messenger = messenger;
}

BankPackageProvider::BankPackageProvider(repast::SharedContext<Bank>* bank)
{
	this->bank = bank;
}

BankPackageReceiver::BankPackageReceiver(repast::SharedContext<Bank>* bank)
{
	this->bank = bank;
}

void MessengerPackageProvider::providePackage(Messenger* agent, std::vector<MessengerPackage> & out)
{
	repast::AgentId id = agent->getId();
	MessengerPackage package(id.id(),id.startingRank(),id.agentType(),id.currentRank(),agent->getReceiveId(),agent->getReceiveType(),agent->getAmount());
	out.push_back(package);
}

void MessengerPackageProvider::provideContent(repast::AgentRequest req, std::vector<MessengerPackage>& out)
{
	const std::vector<repast::AgentId>& theIds = req.requestedAgents();
	for(size_t i = 0; i < theIds.size(); i++)
	{
		providePackage(messenger->getAgent(theIds[i]),out);
	}
}

Messenger* MessengerPackageReceiver::createAgent(MessengerPackage package)
{
	repast::AgentId id(package.id,package.proc,package.type);
	return new Messenger(id,package.receiveid,package.receivetype,package.amount);
}

void MessengerPackageReceiver::updateAgent(MessengerPackage package)
{
	repast::AgentId id(package.id,package.proc,package.type);
	Messenger* toUpdate = messenger->getAgent(id);
	//actually shouldn't be used... don't need to update messenger as it gets deleted

}

void BankPackageProvider::providePackage(Bank* agent, std::vector<BankPackage> & out)
{
	repast::AgentId id = agent->getId();
	BankPackage package(id.id(), id.startingRank(),id.agentType(),id.currentRank(),agent->getBankId(),agent->getLiquidity(),agent->getInterestRate(),agent->getTotalDebt());
	out.push_back(package);
}

void BankPackageProvider::provideContent(repast::AgentRequest req, std::vector<BankPackage>& out)
{
	const std::vector<repast::AgentId>& id = req.requestedAgents();
	for(size_t i =0; i < id.size(); i++)
	{
		providePackage(bank->getAgent(id[i]),out);
	}
}

Bank* BankPackageReceiver::createAgent(BankPackage package)
{
	repast::AgentId id(package.id,package.proc,package.type,package.currentProc);
	Bank* bank =  new Bank(id,package.bankId,package.liquidity,package.interestRate,package.totalDebt,nullptr);
	//bank->setBanks(this->bank);
	return bank;
}

void BankPackageReceiver::updateAgent(BankPackage package)
{
	int rank = repast::RepastProcess::instance()->rank();
	repast::AgentId id(package.id,package.proc,package.type,package.currentProc);
	std::vector<Bank*> localBanks;
	//bank->selectAgents(bank->size(),localBanks);
	Bank* toUpdate = bank->getAgent(id);

	//std::cout<< "GOT AGENT " << toUpdate->getId() << std::endl;
	toUpdate->setInterestRate(package.interestRate);
	toUpdate->setLiquidity(package.liquidity);
	toUpdate->setTotalDebt(package.totalDebt);
}

void Observer::initSchedule(repast::ScheduleRunner& runner)
{
	runner.scheduleEvent(1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Observer> (this, &Observer::init)));
	runner.scheduleEvent(1.5, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Observer> (this, &Observer::play)));
	runner.scheduleStop(2);
}
