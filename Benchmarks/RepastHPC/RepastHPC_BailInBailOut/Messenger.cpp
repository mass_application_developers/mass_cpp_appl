
#include "Messenger.h"

Messenger::Messenger() {
	// TODO Auto-generated constructor stub
	this->receiveId = -1;
	this->receiveType = -1;
	this->amount = -1;
}

Messenger::~Messenger() {
	// TODO Auto-generated destructor stub
}

Messenger::Messenger(repast::AgentId id, int receiveId, int receiveType, double amount)
{
	this->id = id;
	this->receiveId = receiveId;
	this->receiveType = receiveType;
	this->amount = amount;
}

int Messenger::getReceiveId() const
{
	return receiveId;
}

int Messenger::getReceiveType() const
{
	return receiveType;
}

double Messenger::getAmount() const
{
	return amount;
}
