

#ifndef WORKER_H
#define WORKER_H
#include "Bank.h"
#include "Observer.h"
#include "repast_hpc/AgentId.h"

class Bank;
class Observer;
class Worker {
public:
	Worker();
	Worker(repast::AgentId id, int workerId, double wages, double consumption, double deposit, double capital, Bank* bank, Observer* observer);
	virtual ~Worker();
	//getters and setters
	void setWorkerId(int id);
	void setWages(double wages);
	void setConsumption(double consumption);
	void setDeposit(double deposit);
	void setCapital(double capital);
	void setObserver(Observer* observer);
	void setBank(Bank* bank);
	virtual repast::AgentId& getId(){ return id;}
	virtual const repast::AgentId& getId() const { return id;}
	int getWorkerId() const;
	double getWages() const;
	double getConsumption() const;
	double getDeposit() const;
	double getCapital() const;
	Bank* getBank() const;

	//actions
	bool depositFunds(int amount);
	void play();
	void receiveWages();
	void consume();

private:
	int workerId;
	repast::AgentId id;
	double wages;
	double consumption;
	double deposit;
	double capital;
	Bank* bank;
	Observer* observer;
};

#endif /* WORKER_H_ */
