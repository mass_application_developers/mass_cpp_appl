

#ifndef OBSERVER_H
#define OBSERVER_H

#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/initialize_random.h"
#include "repast_hpc/Schedule.h"
#include <set>
#include <boost/mpi.hpp>
#include "Firms.h"
#include "Worker.h"
#include "Bank.h"
#include "Owner.h"
#include "Messenger.h"
#include "MessengerPackage.h"
#include "BankPackage.h"

class Firms;
class Worker;
class Bank;
class Owner;
class Messenger;
class MessengerPackage;
class BankPackage;

class MessengerPackageProvider {
private:
	repast::SharedContext<Messenger> * messenger;
public:
	MessengerPackageProvider(repast::SharedContext<Messenger> * messenger);
	void providePackage(Messenger* agent, std::vector<MessengerPackage> & out);
	void provideContent(repast::AgentRequest req, std::vector<MessengerPackage>& out);
};

class MessengerPackageReceiver {
private:
	repast::SharedContext<Messenger> * messenger;
public:
	MessengerPackageReceiver(repast::SharedContext<Messenger> * messenger);
	Messenger* createAgent(MessengerPackage package);
	void updateAgent(MessengerPackage package);
};

class BankPackageProvider {
private:
	repast::SharedContext<Bank> * bank;
public:
	BankPackageProvider(repast::SharedContext<Bank> * bank);
	void providePackage(Bank* agent, std::vector<BankPackage> & out);
	void provideContent(repast::AgentRequest req, std::vector<BankPackage>& out);

};

class BankPackageReceiver {
private:
	repast::SharedContext<Bank> * bank;
public:
	BankPackageReceiver(repast::SharedContext<Bank> * bank);
	Bank* createAgent(BankPackage package);
	void updateAgent(BankPackage package);

};

class Observer {
public:
	Observer(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	virtual ~Observer();

	void init();
	void initSchedule(repast::ScheduleRunner& runner);
	void createMessenger(int receiveType,int receiveId, int amount);
	double getProductionCost() const;
	double getInterestRate() const;
	double getLiquidity() const;
	repast::SharedContext<Firms> getFirms() const;
	repast::SharedContext<Worker> getWorkers() const;
	repast::SharedContext<Bank> getBanks() const;
	repast::SharedContext<Owner> getOwners() const;
	repast::SharedContext<Messenger> getMessenger() const;


	//checks if banks are bankrupt
	bool checkBankruptcy();
	void synchronizeBanks();
	void play();
	void action();
	void fulfillMessenger();
	void sendMessenger();
	void deposit(int type, double amount, int id);
	void depositOwner(double amount, int id);
	void depositFirm(double amount, int id);
	void depositWorker(double amount, int id);
	void depositBank(double amount, int id);

private:
	int messengerId;
	double productionCost;
	double interestRate;
	double liquidity;
	void setConstants(int & workers, int & firms, int & banks);
	void createBanks(int amount);
	void createFirms(int amount);
	void createWorkers(int amount);
	void chooseBanks();
	void distributeWorkers();

	repast::SharedContext<Firms> firms;
	repast::SharedContext<Worker>  workers;
	repast::SharedContext<Bank>  banks;
	repast::SharedContext<Owner> owners;
	repast::SharedContext<Messenger> messenger;
	repast::Properties* props;
	MessengerPackageProvider* messengerProvider;
	MessengerPackageReceiver* messengerReceiver;
	BankPackageProvider* bankProvider;
	BankPackageReceiver* bankReceiver;
};

#endif /* OBSERVER_H_ */
