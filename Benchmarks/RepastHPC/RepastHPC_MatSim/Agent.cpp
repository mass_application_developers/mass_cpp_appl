
#include "Agent.h"
#include<set>
#include<queue>

Agent::Agent() {
	// TODO Auto-generated constructor stub
	this->atDestination = false;
	this->distanceTraveled = 0;
	this->speed = -1;
	curr = 0;
	tick = 0;
}

Agent::Agent(repast::AgentId id, int speed)
{
	this->id = id;
	this->atDestination = false;
	this->speed = speed;
	this->distanceTraveled = 0;
	this->curr = 0;
	tick = 0;
}

Agent::Agent(repast::AgentId id, int speed, int distanceTraveled, int pointOrigin, int pointDestination, int curr, std::vector<int>shortestPath, std::vector<int>wayBackHome, int tick)
{
	this->id = id;
	this->speed = speed;
	this->distanceTraveled = distanceTraveled;
	this->pointOrigin = pointOrigin;
	this->pointDestination = pointDestination;
	this->curr = curr;
	this->shortestPath = shortestPath;
	this->atDestination = false;
	this->wayBackHome = wayBackHome;
	this->tick = tick;
}

Agent::~Agent() {
	// TODO Auto-generated destructor stub
}

void Agent::addPath(int road)
{
	shortestPath.push_back(road);
}

void Agent::setTravel(int travel)
{
	distanceTraveled = travel;
}
void Agent::setDestination(bool destination)
{
	atDestination = destination;
}

void Agent::setPointOrigin(int origin)
{
	this->pointOrigin = origin;
}

void Agent::setPointDestination(int destination)
{
	this->pointDestination = destination;
}

void Agent::setCurr(int curr)
{
	this->curr = curr;
}

int Agent::getPathSize() const
{
	return shortestPath.size();
}


int Agent::getPointOrigin() const
{
	return pointOrigin;
}

int Agent::getPointDestination() const
{
	return pointDestination;
}

int Agent::getCurrLink() const
{
	return shortestPath.at(curr);
}

int Agent::getWayHomeLink() const
{
	return wayBackHome.at(curr);
}

int Agent::getSpeed() const
{
	return speed;
}
std::vector<int> Agent::getShortestPath() const
{
	return shortestPath;
}
std::vector<int> Agent::getWayBack() const
{
	return wayBackHome;
}


int Agent::getDistanceTraveled() const
{
	return distanceTraveled;
}

int Agent::getCurr() const
{
	return curr;
}

int Agent::getTick() const
{
	return tick;
}
void Agent::findPath(std::unordered_map<int,Point*>& points, std::unordered_map<int,Road*>& roads)
{
	std::queue<int> q;
	std::vector<int> dist;
	std::vector<bool> prev;
	std::vector<int> parent;
	for(int i = 0; i < points.size(); i++)
	{
		dist.push_back(2147483647);
		prev.push_back(false);
		parent.push_back(-1);

	}
	q.push(pointOrigin);
	dist.at(pointOrigin) = 0;
	while(!q.empty())
	{
		int u = findMinDistance(dist,prev,points.size());
		int vertex = q.front();
		int begin = vertex;
		bool failed = false;
		while(vertex != u)
		{
			q.pop();
			q.push(vertex);
			vertex = q.front();
			if(begin == vertex)
			{
				failed = true;
				break;
			}
		}
		if(failed)
		{
			return;
		}
		q.pop();
		prev.at(u) = true;
		if(u == pointDestination)
		{
			break;
		}
		Point* point = points.at(u);
		std::vector<int> neighbors = point->getNeighbors();
		for(int i: neighbors)
		{
			//if still in q
			if(!prev.at(i))
			{
				int alt = dist.at(u) + roads.at(point->findRoad(i))->getLength();
				if(alt < dist.at(i))
				{
					dist.at(i) = alt;
					parent.at(i) = u;
					q.push(i);
				}
			}
		}
	}
	int previousId = parent.at(pointDestination);
	int destination = pointDestination;
	while(previousId != pointOrigin)
	{
		Point* point = points.at(previousId);
		int roadID = point->findRoad(destination);
		destination = point->getID();
		wayBackHome.push_back(roadID);
		previousId = parent.at(previousId);
	}
	Point* point = points.at(previousId);
	int roadID = point->findRoad(destination);
	destination = point->getID();
	wayBackHome.push_back(roadID);
	for(int i = wayBackHome.size()-1; i >=0; i--)
	{
		shortestPath.push_back(wayBackHome.at(i));
	}
}

int Agent::findMinDistance(std::vector<int>& dist,std::vector<bool>& visited, int vertices)
{
	int min = 2147483647;
	int index = 0;
	for(int i = 0; i < vertices; i++)
	{
		if(visited.at(i) == false && dist.at(i) < min)
		{
			min = dist.at(i);
			index = i;
		}
	}
	return index;
}
int Agent::play(std::unordered_map<int,Point*>& points,std::unordered_map<int,Road*>& roads)
{
	//if already at destination
	if(atDestination || shortestPath.size() <= 0)
	{
		return 2;
	}
	tick++;
	//std::cout<<"Failed to find road here " << shortestPath.size()<< " " << curr << " " <<std::endl;
	//std::cout<<shortestPath.at(curr) << std::endl;
	Road* road= roads.at(shortestPath.at(curr));
	//if on point, and the road ahead is full
	if(distanceTraveled == 0 && road->isFull())
	{
		return 0;
	}
	else if(distanceTraveled == 0 && !road->isFull())
	{
		//std::cout<<"Failed to find point here" << std::endl;
		points.at(road->getOrigin())->decreaseCurrCapacity();
		road->increaseCurr();
	}
	distanceTraveled += 1;
	if(distanceTraveled >= road->getLength())
	{
		if(road->getDestination() == pointDestination || curr + 1 >= shortestPath.size())
		{
			road->decreaseCurr();
			distanceTraveled = 0;
			atDestination = true;
			std::cout << this->getId() << " finished Morning route at tick: " << tick<<std::endl;
			return 2;
		}
		//std::cout<<"FAILED TO FIND POINT HERE" << std::endl;
		if(points.at(road->getDestination())->isFull())
		{
			distanceTraveled = road->getLength();
			return 0;
		}
		curr++;
		road->decreaseCurr();
		distanceTraveled -= road->getLength();
	//	std::cout<<"FAILED TO FIND ROAD" << std::endl;
		road = roads.at(shortestPath.at(curr));
		if(distanceTraveled == 0 || road->isFull())
		{
			distanceTraveled = 0;
		//	std::cout<<"FAILED TO FIND POINT" << std::endl;
			points.at(road->getDestination())->increaseCapacity();
			return 1;
		}
		road->increaseCurr();
		return 1;
	}
	return 0;
}

int Agent::play2(std::unordered_map<int,Point*>& points,std::unordered_map<int,Road*>& roads)
{
	if(atDestination || shortestPath.size() <= 0)
	{
		return 2;
	}
	tick++;
	Road* road= roads.at(wayBackHome.at(curr));
	//if on point, and the road ahead is full
	if(distanceTraveled == 0 && road->isFull())
	{
		return 0;
	}
	else if(distanceTraveled == 0 && !road->isFull())
	{
		points.at(road->getOrigin())->decreaseCurrCapacity();
		road->increaseCurr();
	}
	distanceTraveled += 1;
	if(distanceTraveled >= road->getLength())
	{
		if(road->getDestination() == pointOrigin || curr + 1 >= wayBackHome.size())
		{
			road->decreaseCurr();
			distanceTraveled = 0;
			atDestination = true;
			std::cout << this->getId() << " finished Afternoon route at tick: " << tick<<std::endl;
			return 2;
		}
		if(points.at(road->getDestination())->isFull())
		{
			distanceTraveled = road->getLength();
			return 0;
		}
		curr++;
		road->decreaseCurr();
		distanceTraveled -= road->getLength();
		road = roads.at(wayBackHome.at(curr));
		if(distanceTraveled == 0 || road->isFull())
		{
			distanceTraveled = 0;
			points.at(road->getDestination())->increaseCapacity();
			return 1;
		}
		road->increaseCurr();
		return 1;
	}
	return 0;
}
