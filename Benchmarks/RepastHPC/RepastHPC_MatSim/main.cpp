#include<iostream>
#include<boost/mpi.hpp>
#include<chrono>
#include "repast_hpc/io.h"
#include "rapidxml-1.13/rapidxml.hpp"
#include "repast_hpc/RepastProcess.h"

#include "Observer.h"


using namespace std;
using namespace std::chrono;
using namespace repast;
using namespace rapidxml;
//using namespace relogo;
int main(int argc, char **argv)
{
	boost::mpi::environment env(argc, argv);
	string config,properties,nodeFile,travelFile;
	boost::mpi::communicator world;
	if (argc >= 3)
	{
		config = argv[1];
		properties = argv[2];
		nodeFile = argv[3];
		travelFile = argv[4];
	} else
	{
		if(world.rank() == 0)
		{
			cout<<"need xml files and/or config files?" << endl;
		}
	}
	if(config.size() >= 0 && properties.size() >= 0 )
	{
		//std::cout<<"starting int "<< std::endl;
		auto start = std::chrono::high_resolution_clock::now();
		RepastProcess::init(config,&world);
		Observer* obvs = new Observer(properties,argc,argv,&world);
		repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();
		obvs->init(nodeFile,travelFile);
		obvs->initSchedule(runner);
		runner.run();
		auto stop = high_resolution_clock::now();

		auto duration = duration_cast<milliseconds>(stop - start);
		cout << duration.count() << endl;
		delete obvs;



	}
	RepastProcess:: instance()->done();


	return 0;
}



