

#ifndef AGENTPACKAGE_H
#define AGENTPACKAGE_H
#include "repast_hpc/AgentId.h"
#include <vector>
struct AgentPackage {
public:
	template<class Archive>
	void serialize (Archive &ar, const unsigned int version)
	{
		ar & id;
		ar & proc;
		ar & type;
		ar & currentProc;

		ar & speed;
		ar & distanceTraveled;
		ar & pointOrigin;
		ar & pointDestination;
		ar & curr;
		ar & shortestPath;
		ar & wayBackHome;
		ar & tick;
	}
	int id,proc,type,currentProc, speed, distanceTraveled,pointOrigin,pointDestination,curr,tick;
	std::vector<int> shortestPath;
	std::vector<int> wayBackHome;

	repast::AgentId getId() const {
		return repast::AgentId(id,proc,type,currentProc);
	}
	AgentPackage(int id, int proc, int type, int currentProc, int speed, int distanceTraveled, int pointOrigin, int pointDestination, int curr, std::vector<int> shortestPath,std::vector<int> wayBackHome, int tick)
	{
		this->id = id;
		this->proc = proc;
		this->type = type;
		this->currentProc = currentProc;
		this->speed = speed;
		this->distanceTraveled = distanceTraveled;
		this->pointOrigin = pointOrigin;
		this->pointDestination = pointDestination;
		this->curr = curr;
		this->shortestPath = shortestPath;
		this->wayBackHome = wayBackHome;
		this->tick = tick;
	}
	AgentPackage(){};
};





#endif /* AGENTPACKAGE_H_ */
