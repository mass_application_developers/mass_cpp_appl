
#ifndef AGENT_H
#define AGENT_H

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "Point.h"
#include "Road.h"
#include<vector>

class Agent {
public:
	Agent();
	Agent(repast::AgentId id, int speed);
	Agent(repast::AgentId id, int speed, int distanceTraveled, int pointOrigin, int pointDestination, int curr, std::vector<int>shortestPath,std::vector<int>wayBackHome, int tick);
	void addPath(int roadId);
	void setPointOrigin(int origin);
	void setPointDestination(int destination);
	void setDestination(bool destination);
	void setCurr(int curr);
	void setTravel(int travel);
	void findPath(std::unordered_map<int,Point*>& points, std::unordered_map<int,Road*>& roads);
	//returns 1 if at a new destination, 0 if not, 2 if finished traveling
	int play(std::unordered_map<int,Point*>& points, std::unordered_map<int,Road*>& roads);
	int play2(std::unordered_map<int,Point*>& points, std::unordered_map<int,Road*>& roads);
	virtual repast::AgentId& getId(){ return id;}
	virtual const repast::AgentId& getId() const { return id;}
	int getPathSize() const;
	int getPointOrigin() const;
	int getPointDestination() const;
	int getCurrLink() const;
	int getWayHomeLink() const;
	int getSpeed() const;
	int getDistanceTraveled() const;
	int getCurr() const;
	int getTick() const;
	std::vector<int> getShortestPath() const;
	std::vector<int> getWayBack() const;
	virtual ~Agent();
private:
	int findMinDistance(std::vector<int>& dist,std::vector<bool>& visited, int vertices);
	repast::AgentId id;
	bool atDestination;
	int speed;
	int distanceTraveled;
	int pointOrigin;
	int pointDestination;
	int curr;
	int tick;
	std::vector<int> shortestPath;
	std::vector<int> wayBackHome;
};

#endif /* AGENT_H */
