
#ifndef POINT_H
#define POINT_H

#include<unordered_map>
#include<vector>
#include<set>
#include"Road.h"
class Road;

class Point {
public:
	Point();
	virtual ~Point();
	Point(int id, int carCapacity, double x, double y);
	double getX() const;
	double getY() const;
	int getID() const;
	int getCapacity() const;
	int getCurrCapacity() const;
	void insertRoad(int id, Road* road);
	void increaseCapacity();
	void decreaseCapacity();
	void decreaseCurrCapacity();
	void increaseCurrCapacity();
	bool isFull() const;
	void addPoint(int id);
	bool containsPoint(int point);
	int findRoad(int pointDestination);
	bool deleteRoad(int id);
	bool deletePoint(int id);
	std::unordered_map<int,Road*> getRoads();
	std::set<int> getPoints();
	std::vector<int> getNeighbors();
private:
	double x;
	double y;
	int id;
	int carCapacity;
	int currCapacity;
	std::unordered_map<int,Road*> roads;
	std::set<int> points;

};
#endif /* POINT_H */
