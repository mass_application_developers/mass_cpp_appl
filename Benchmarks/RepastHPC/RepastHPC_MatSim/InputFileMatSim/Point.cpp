
#include "Point.h"

Point::Point() {
	x = -1;
	y = -1;
	// TODO Auto-generated constructor stub

}

Point::Point(int id, int carCapacity,double x, double y) {
	this->id = id;
	this->carCapacity = carCapacity;
	this->currCapacity = 0;
	this->x = x;
	this->y = y;
}
Point::~Point() {
	// for(auto & x: roads)
	// {
	// 	delete x.second;
	// 	roads.erase(x.first);
	// }
}

double Point::getX() const {
	return x;
}
double Point::getY() const {
	return y;
}

int Point::getID() const
{
	return id;
}
void Point::insertRoad(int id, Road* road)
{
	roads.insert({id,road});
}

void Point::increaseCapacity()
{
	this->carCapacity++;
}

int Point::getCapacity() const
{
	return carCapacity;
}

int Point::getCurrCapacity() const
{
	return currCapacity;
}
void Point::decreaseCapacity()
{
	this->carCapacity--;
}
void Point::increaseCurrCapacity()
{
	this->currCapacity++;
}

void Point::decreaseCurrCapacity()
{
	this->currCapacity--;
}

bool Point::isFull() const
{
	return carCapacity <= currCapacity;
}
int Point::findRoad(int pointDestination)
{
	for(auto it: roads)
	{
		if(it.second->getDestination() == pointDestination)
		{
			return it.second->getId();
		}
		if(it.second->getOrigin() == pointDestination)
		{
			return it.second->getId();
		}
	}
	return -1;
}
std::vector<int> Point::getNeighbors()
{
	std::vector<int> res;
	for(auto it: roads)
	{
		if(it.second->getDestination()!= this->getID())
		{
			res.push_back(it.second->getDestination());
		}
		else
		{
			res.push_back(it.second->getOrigin());
		}
	}
	return res;
}
std::set<int> Point::getPoints()
{
	return points;
}

std::unordered_map<int,Road*> Point::getRoads()
{
	return roads;
}
void Point::addPoint(int point)
{
	points.insert(point);
}

bool Point::containsPoint(int point)
{
	for(const auto& i: points)
	{
		if(point == i)
		{
			return true;
		}
	}
	return false;
}

bool Point::deleteRoad(int id)
{
	Road* road = roads.at(id);
	if(roads.erase(id) == 1)
	{
		return true;
	}
	return false;
}
bool Point::deletePoint(int id)
{
	if(points.erase(id) == 1)
	{
		return true;
	}
	return false;
}