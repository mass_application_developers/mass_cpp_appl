//Input should be an int representing x, int representing y and an int representing percent chance that a road gets removed
//Two dice roles for each road, first one represents if a road gets deleted
//second one is which one gets deleted
#include "Point.h"
#include "Road.h"
#include <unordered_map>
#include <stdlib.h>
#include <ctime>
#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
//#include <bits/stdc++.h>
using namespace std;
int roadLinks[][2] = {{1,0},{0,-1},{-1,0},{0,1}}; 
std::unordered_map<int,Point*> points;
std::unordered_map<int,Road*> roads;  
vector<int>startingPoints;
vector<int>endingPoints;
void createPoints(int x, int y);
void createRoads(int x, int y);
void removeRoads(int x, int y, int chance);
void writePointRoadToFile();
void createAgents(int x, int y);
void setUpStartingEnding(int x, int y);
void findRoute(int pointOrigin, int pointDestination,ofstream& myFile);
int findMinDistance(vector<int>& dist,vector<bool>& visited, int vertices);
int main(int argc,char**argv)
{ 
    int x = atoi(argv[1]);
    int y = atoi(argv[2]);
    int chance = atoi(argv[3]);
    createPoints(x,y);
    createRoads(x,y);
    removeRoads(x,y,chance);
    writePointRoadToFile();
    setUpStartingEnding(x,y);
    createAgents(x,y);
    return 1;
}
//makes the points
void createPoints(int x, int y)
{
    int id = 0;
    for(int i = 0; i < y; i++)
	{
		for(int j = 0 ; j < x; j++)
		{
			Point* point = new Point(id,0,j,i);
			points.insert({id,point});
			id++;
		}
	}
}
//makes the roads
void createRoads(int x, int y)
{
    int roadId = 0;
    for(int i = 0; i < x * y ; i++)
    {
        Point* point = points.at(i);
        int pointX = (int)point->getX();
        int pointY = (int)point->getY();
        for (auto direction: roadLinks)
        {
            int myX = pointX + direction[0];
			int myY = pointY + direction[1];
			int destinationId = myY * x + myX;
           	if(x > myX && 0 <= myX  && y > myY && 0 <= myY && !points.at(destinationId)->containsPoint(point->getID()))
			{
                Road* road = new Road(1,roadId,10,point->getID(),destinationId);
                roads.insert({roadId,road});
                point->addPoint(destinationId);
                point->insertRoad(roadId,road);
                points.at(destinationId)->addPoint(point->getID());
                points.at(destinationId)->insertRoad(roadId,road);
				roadId++;
			}
        }
    }
}

void removeRoads(int x, int y, int chance)
{
    srand((unsigned) time(0));
    for(auto & i: points)
    {
        Point* point = i.second;
        int random = rand() % 100 + 1;
        if(random > chance)
        {
            continue;
        }
        set<int> neighbors = point->getPoints();
        vector<int> possible;
        for(auto & j: neighbors)
        {
            if(points.at(j)->getPoints().size() > 2)
            {
                possible.push_back(j);
            }
        }
        if(possible.size()> 0)
        {
            int index = rand() % possible.size();
            int id = possible.at(index);
            int roadId;
            for(auto & k: point->getRoads())
            {
                if(k.second->getDestination() == point->getID() && k.second->getOrigin() == points.at(id)->getID() ||k.second->getOrigin() == point->getID() && k.second->getDestination() == points.at(id)->getID())
                {
                    roadId = k.second->getId();
                    break;
                }
            }
            //delete everything
            point->deletePoint(id);
            point->deleteRoad(roadId);
            points.at(id)->deletePoint(point->getID());
            points.at(id)->deleteRoad(roadId);
            roads.erase(roadId);
        }

    }
}

void writePointRoadToFile()
{
    ofstream myFile;
    myFile.open("Node_Link.txt",std::ofstream::out | std::ofstream::trunc);
    myFile<< "Nodes" << endl;
    for(auto& i : points)
    {
        myFile<<"id: " << i.first<< " x: " << i.second->getX() << " y: " << i.second->getY()<< endl;
    }
    myFile<<"Links" << endl;
    for(auto & j: roads)
    {
        myFile<<"id: "<<j.first<< " PointID1: " << j.second->getOrigin() << " PointID2: " << j.second->getDestination() << endl;
    }
    myFile.close();
}

void setUpStartingEnding(int x, int y)
{
   //horizontally starting points should be (incr 1)
    // 0 -> x-1
    // y * (x-1) -> y * x - 1
    //vertically starting points should be (incr by x)
    // 0 -> y * (x-1)
    // x-1 -> y * x - 1
    // set<int> startingPoint;
    set<int> contains;
     for(int j = 0; j < 2; j++)    
     {
        for(int i = (1 + j) + (j*x); i < (j*x) + x - (j +1); i++)
        {
            if(i < x * y)
            {
                contains.insert(i);
            }
        }
        for(int i = y * (x-(1 + j)) + 1; i < y * (x-(1+j))  + (x - j); i++)
        {
            if(i < x * y)
            {
                contains.insert(i);
            }
        }
        for(int i = 0 + j; i <= y * (x-(1 + j)); i= i + x)
        {
            if(i < x * y)
            {
                contains.insert(i);
            }
        }
        for(int i = x-(1 + j); i <= y * x-(1+j); i = i + x)
        {
            if(i < x * y)
            {
                contains.insert(i);
            }
        }
    }
    for(auto & j: contains)
    {
        startingPoints.push_back(j);
    }
    //ending points
    int middle = (y / 2 ) * x + (x/2);
    int directions[][2] = {{1,0},{0,-1},{-1,0},{0,1},{1,1},{1,-1},{-1,1},{-1,-1}};
    Point* point = points.at(middle);
    endingPoints.push_back(middle);
    for(auto direction: directions)
    {
        int pointX = (int) point->getX();
		int pointY = (int) point->getY();
		if(x > pointX + direction[0] && 0 <= pointX + direction[0] && y > pointY + direction[1] && 0 <= pointY + direction[1])
		{
            int origin = middle;
            origin = origin + direction[0];
            origin = origin + (direction[1] * x);
			endingPoints.push_back(origin);
		}
    }
}
int findMinDistance(vector<int>& dist,vector<bool>& visited, int vertices)
{
    int min = 2147483647;
	int index = 0;
	for(int i = 0; i < vertices; i++)
	{
		if(visited.at(i) == false && dist.at(i) < min)
		{
			min = dist.at(i);
			index = i;
		}
	}
	return index;
}

void findRoute(int pointOrigin, int pointDestination, ofstream & myFile)
{
    std::queue<int> q;
	std::vector<int> dist;
	std::vector<bool> prev;
	std::vector<int> parent;
	for(int i = 0; i < points.size(); i++)
	{
		dist.push_back(2147483647);
		prev.push_back(false);
		parent.push_back(-1);

	}
	q.push(pointOrigin);
	dist.at(pointOrigin) = 0;
	while(!q.empty())
	{
		int u = findMinDistance(dist,prev,points.size());
		int vertex = q.front();
		int begin = vertex;
		bool failed = false;
		while(vertex != u)
		{
			q.pop();
			q.push(vertex);
			vertex = q.front();
			if(begin == vertex)
			{
				failed = true;
				break;
			}
		}
		if(failed)
		{
			return;
		}
		q.pop();
		prev.at(u) = true;
		if(u == pointDestination)
		{
			break;
		}
		Point* point = points.at(u);
		std::vector<int> neighbors = point->getNeighbors();
		for(auto i: neighbors)
		{
			//if still in q
			if(!prev.at(i))
			{
                int roadId = point->findRoad(i);
				int alt = dist.at(u) + roads.at(roadId)->getLength();
				if(alt < dist.at(i))
				{
					dist.at(i) = alt;
					parent.at(i) = u;
					q.push(i);
				}
			}
		}
	}
	int previousId = parent.at(pointDestination);
	int destination = pointDestination;
    while(previousId != -1)
    {

		Point* point = points.at(previousId);
		int roadID = point->findRoad(destination);
		destination = point->getID();
        myFile<< roadID << " ";
        previousId = parent.at(previousId);
    }
}
void createAgents(int x, int y)
{
    //set up starting and ending points
    srand((unsigned) time(0));
    int size = x*y;
    vector<int> capacity(size,0);
    int numAgents = (x * y) / 10;
    ofstream myFile;
    myFile.open("Car_Agents.txt",std::ofstream::out | std::ofstream::trunc);
    myFile<< "Agents" << endl;
    for(int i =0; i < numAgents;i++)
    {
        int num = rand();
        int startingIndex = num% startingPoints.size();
        int endingIndex = num % endingPoints.size();
        while(points.at(startingIndex)->getPoints().size() < capacity.at(startingIndex))
        {
            startingIndex = (startingIndex + 1) % startingIndex;
        }
        int startingPoint = startingPoints.at(startingIndex);
        capacity.at(startingIndex) = capacity.at(startingIndex)++;
        int endingPoint = endingPoints.at(endingIndex);
        myFile<<"Agent ID: " << i << " starting Point: " << startingPoint << " ending Point: " << endingPoint << " Route: ";
        findRoute(startingPoint,endingPoint,myFile);
        myFile<<endl;
    }
}