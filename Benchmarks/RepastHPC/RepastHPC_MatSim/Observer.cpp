

#include "Observer.h"
#include "repast_hpc/Properties.h"
#include"repast_hpc/Schedule.h"
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "rapidxml-1.13/rapidxml.hpp"
#include <stdlib.h>
#include <boost/mpi.hpp>
#include <fstream>
#include <iostream>
#include <vector>
#include <bits/stdc++.h>

ObserverPackageProvider::ObserverPackageProvider(repast::SharedContext<Agent> * agents)
{
	this->agents = agents;
}

void ObserverPackageProvider::providePackage(Agent* agent, std::vector<AgentPackage>& out)
{
	repast::AgentId id = agent->getId();
	AgentPackage package(id.id(),id.startingRank(),id.agentType(),id.currentRank(),agent->getSpeed(),agent->getDistanceTraveled(),agent->getPointOrigin(),agent->getPointDestination(),agent->getCurr(),agent->getShortestPath(),agent->getWayBack(),agent->getTick());
	out.push_back(package);
}

void ObserverPackageProvider::provideContent(repast::AgentRequest req,std::vector<AgentPackage>& out)
{
	const std::vector<repast::AgentId>& ids = req.requestedAgents();
	for(size_t i = 0; i < ids.size(); i++)
	{
		providePackage(agents->getAgent(ids[i]),out);
	}

}


ObserverPackageReceiver::ObserverPackageReceiver(repast::SharedContext<Agent> * agents)
{
	this->agents = agents;
}

Agent* ObserverPackageReceiver::createAgent(AgentPackage package)
{
	repast::AgentId id(package.id,package.proc,package.type,package.currentProc);
	Agent* agent = new Agent(id,package.speed,package.distanceTraveled,package.pointOrigin,package.pointDestination,package.curr,package.shortestPath,package.wayBackHome,package.tick);

	return agent;
}

void ObserverPackageReceiver::updateAgent(AgentPackage package)
{
	repast::AgentId id(package.id,package.proc,package.type,package.currentProc);
	Agent* agent = agents->getAgent(id);
	agent->setCurr(package.curr);
}

Observer::Observer(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm): context(comm) {
	props = new repast::Properties(propsFile,argc,argv,comm);
	provider = new ObserverPackageProvider(&context);
	receiver = new ObserverPackageReceiver(&context);
	finishedCount = 0;
	expectedCount = 0;
	// TODO Auto-generated constructor stub

}

//Need to clean up memory
Observer::~Observer() {
	delete props;
	delete provider;
	delete receiver;
	//this->deletePoints();
	//this->deleteRoads();
	// TODO Auto-generated destructor stub
}
void Observer::deletePoints()
{
    for(auto & x: points)
	{
		delete x.second;
		points.erase(x.first);
	}
}
void Observer::deleteRoads()
{
	for(auto & x: roads)
	{
		delete x.second;
		roads.erase(x.first);
	}

}
void Observer::init(std::string nodeFile, std::string carFile)
{
	std::cout<<"Parsing Node File" << std::endl;
	parseNodeFile(nodeFile);
	std::cout<<"Parsing travel File " << std::endl;
	parseTravelFile(carFile);
	std::cout<<"Context size " << context.size()<<std::endl;
}

void Observer::parseNodeFile(std::string nodeFile)
{
	std::ifstream inFile;
	inFile.open(nodeFile);
	string line;
	bool onNode = true;
	int count = 0;
	if(!inFile.is_open())
	{
	} else
	{
		while(getline(inFile,line))
		{
			if(line.at(0) == 'N')
			{
				continue;
			}
			else if(line.at(0) == 'L')
			{
				onNode = false;
				continue;
			}
			else if (line == " " || line.length() == 0)
			{
				continue;
			}
			parseLine(line,onNode);
		}
	}
}

void Observer::parseTravelFile(std::string carFile)
{
	std::ifstream inFile;
	inFile.open(carFile);
	string line;
	if(!inFile.is_open())
		{
			std::cout<<"Error carFile is not open" << std::endl;
		} else
		{
			while(getline(inFile,line))
			{
				if(line.at(5) == 's')
				{
					continue;
				}
				parseLine(line);
			}
		}
}

void Observer::parseLine(std::string line)
{
	std::vector<int> route;
	int values[3] {-1,-1,-1};
	int index = 0;
	for(int i = 0; i < line.length(); i++)
	{
		if(line.at(i) == ':')
		{
			//not on route yet
			if(index != 3)
			{
				string res = "";
				i++;
				i++;
				while( i < line.length() && line.at(i) != ' ')
				{
					res = res + line.at(i);
					i++;
				}
				values[index] = stoi(res);
				index++;
			}
			else
			{
				i++;
				i++;
				string res = "";
				while ( i < line.length())
				{
					if(line.at(i) == ' ')
					{
						route.push_back(stoi(res));
						res = "";
						i++;
						continue;
					}
					res = res + line.at(i);
					i++;
				}
			}
		}
	}
	std::vector<int> wayBack;
	for(int i = route.size() - 1; i >= 0; i--)
	{
		wayBack.push_back(route.at(i));
	}
	if(values[1] % repast::RepastProcess::instance()->worldSize() == repast::RepastProcess::instance()->rank())
	{
		repast::AgentId id(values[0],repast::RepastProcess::instance()->rank(),0);
		Agent* agent = new Agent(id,1,0,values[1],values[2],0,route,wayBack,0);
		context.addAgent(agent);
	}

}

void Observer::parseLine(std::string line, bool onNode)
{
	int values[3] {-1,-1,-1};
	int index = 0;
	for(int i = 0; i < line.length(); i++)
	{
		if(line.at(i) == ':')
		{
			i++;
			i++;
			string res = "";
			while(i < line.length() && line.at(i) != ' ' )
			{
				res = res + line.at(i);
				i++;
			}
			values[index] = stoi(res);
			index++;
		}
		if(index == 3)
		{
			break;
		}
	}
	if(values[0] == -1)
	{
		return;
	}
	if(onNode)
	{
		Point * point = new Point(values[0],1000,values[1],values[2]);
		points[values[0]] = point;
	}
	else
	{
		Road* road = new Road(1,values[0],10,values[1],values[2]);
		points.at(values[1])->insertRoad(values[0],road);
		points.at(values[2])->insertRoad(values[0],road);
		roads[values[0]] = road;
	}
}

void Observer::moveAgents()
{
	int worldSize = repast::RepastProcess::instance()->worldSize();

	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while(iter != iterEnd)
	{
		Agent* curr = &**iter;
		repast::RepastProcess::instance()->moveAgent(curr->getId(),curr->getPointDestination()%worldSize);
		iter++;
	}
	repast::RepastProcess::instance()->synchronizeAgentStatus<Agent, AgentPackage, ObserverPackageProvider, ObserverPackageReceiver>(context, *provider, *receiver, *receiver);
}

void Observer::moveBack()
{
	int worldSize = repast::RepastProcess::instance()->worldSize();

	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while(iter != iterEnd)
	{
		Agent* curr = &**iter;
		repast::RepastProcess::instance()->moveAgent(curr->getId(),curr->getPointOrigin()%worldSize);
		iter++;
	}
	repast::RepastProcess::instance()->synchronizeAgentStatus<Agent, AgentPackage, ObserverPackageProvider, ObserverPackageReceiver>(context, *provider, *receiver, *receiver);
}
void Observer::setExpectedCount()
{
	this->expectedCount = context.size() - 1;
}

void Observer::setBeginningCount()
{
	this->beginningCount = context.size() - 1;
}

void Observer::doSomething()
{
	int count = 0;
	for(int i = 0; i < 100000; i++)
	//while(finishedCount <= expectedCount)
	//while(finishedCount <= expectedCount && count >= 10000)
	{
		finishedCount = 0;
		repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
		repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
		while(iter != iterEnd)
		{
			Agent* curr = &**iter;
			int result = curr->play(points,roads);
			cleanUp(result,curr,false);
			iter++;
		}
		std::vector<Agent*> currAgents;
		context.selectAgents(currAgents);
		repast::RepastProcess::instance()->synchronizeAgentStatus<Agent, AgentPackage, ObserverPackageProvider, ObserverPackageReceiver>(context, *provider, *receiver, *receiver);
		//std::vector<Agent*> newAgents;
		context.selectAgents(currAgents,true);
		int worldSize = repast::RepastProcess::instance()->worldSize();

		for(int i = 0; i < currAgents.size(); i++)
		{
			if(!points.at(roads.at(currAgents.at(i)->getCurrLink())->getOrigin())->isFull())
			{
				points.at(roads.at(currAgents.at(i)->getCurrLink())->getOrigin())->increaseCurrCapacity();
			}
			else
			{
				currAgents.at(i)->setCurr(currAgents.at(i)->getCurr() - 1);
				currAgents.at(i)->setTravel(roads.at(i)->getLength());
				repast::RepastProcess::instance()->moveAgent(currAgents.at(i)->getId(),roads.at(currAgents.at(i)->getCurrLink())->getDestination()%worldSize);

			}
		}
		repast::RepastProcess::instance()->synchronizeAgentStatus<Agent, AgentPackage, ObserverPackageProvider, ObserverPackageReceiver>(context, *provider, *receiver, *receiver);
	}
}
void Observer::cleanDestination()
{
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while(iter != iterEnd)
	{
		Agent* curr = &**iter;
		curr->setDestination(false);
		curr->setCurr(0);
		iter++;
	}
	finishedCount = 0;
}
void Observer::goHome()
{
	std::cout<<"Starting afternoon route" << std:: endl;
	int count = 0;
	for(int i = 0; i < 100000; i++)
	//while(finishedCount <= beginningCount && count > 10000)
	//while(finishedCount <= beginningCount && count >= 10000)
	{
		finishedCount = 0;
		repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
		repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
		while(iter != iterEnd)
		{
			Agent* curr = &**iter;
			int result = curr->play2(points,roads);
			cleanUp(result,curr,true);
			iter++;
		}
		std::vector<Agent*> currAgents;
		context.selectAgents(currAgents);
		repast::RepastProcess::instance()->synchronizeAgentStatus<Agent, AgentPackage, ObserverPackageProvider, ObserverPackageReceiver>(context, *provider, *receiver, *receiver);
		//std::vector<Agent*> newAgents;
		context.selectAgents(currAgents,true);
		int worldSize = repast::RepastProcess::instance()->worldSize();

		for(int i = 0; i < currAgents.size(); i++)
		{
			if(!points.at(roads.at(currAgents.at(i)->getWayHomeLink())->getOrigin())->isFull())
			{
				points.at(roads.at(currAgents.at(i)->getWayHomeLink())->getOrigin())->increaseCurrCapacity();
			}
			else
			{
				currAgents.at(i)->setCurr(currAgents.at(i)->getCurr() - 1);
				currAgents.at(i)->setTravel(roads.at(i)->getLength());
				repast::RepastProcess::instance()->moveAgent(currAgents.at(i)->getId(),roads.at(currAgents.at(i)->getWayHomeLink())->getDestination()%worldSize);
			}
		}
		repast::RepastProcess::instance()->synchronizeAgentStatus<Agent, AgentPackage, ObserverPackageProvider, ObserverPackageReceiver>(context, *provider, *receiver, *receiver);
	}
}

//check if the move is valid, if it is move the agent to appropriate process for link
//move agent to appropriate proess, appropriate process = pointdestination % process
//process also owns link to the point
//Should check if the agent is at destination
//if it is two or 0 do nothing
void Observer::cleanUp(int result,Agent* agent, bool direction)
{
	int worldSize = repast::RepastProcess::instance()->worldSize();
	std::vector<int> path = agent->getShortestPath();
	if(direction)
	{
		path = agent->getWayBack();
	}
	switch(result)
	{
		case 1:
			//move agent
			repast::RepastProcess::instance()->moveAgent(agent->getId(),roads.at(path.at(agent->getCurr()))->getDestination()%worldSize);
			break;
		case 2:
			finishedCount++;
			//std::cout<< agent->getId() << " Finished AT " << agent->getPointDestination()<<std::endl;
			break;
		default:
			break;
	}

}

void Observer::initSchedule(repast::ScheduleRunner & runner)
{
	runner.scheduleEvent(1.6, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Observer> (this, &Observer::setBeginningCount)));
	runner.scheduleEvent(1.7, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Observer> (this, &Observer::moveAgents)));
	runner.scheduleEvent(1.8, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Observer> (this, &Observer::setExpectedCount)));
	runner.scheduleEvent(1.9, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Observer> (this, &Observer::moveBack)));
	runner.scheduleEvent(2.0, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Observer> (this, &Observer::doSomething)));
	runner.scheduleEvent(2.2, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Observer> (this, &Observer::cleanDestination)));
	runner.scheduleEvent(3.0, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Observer> (this, &Observer::goHome)));
	runner.scheduleStop(3.1);
}

