
#include "Point.h"

Point::Point() {
	x = -1;
	y = -1;
	// TODO Auto-generated constructor stub

}

Point::Point(int id, int carCapacity,double x, double y) {
	this->id = id;
	this->carCapacity = carCapacity;
	this->currCapacity = 0;
	this->x = x;
	this->y = y;
}
Point::~Point() {
	// TODO Auto-generated destructor stub
}

double Point::getX() const {
	return x;
}
double Point::getY() const {
	return y;
}

int Point::getID() const
{
	return id;
}
void Point::insertRoad(int id, Road* road)
{
	roads.insert({id,road});
}

void Point::increaseCapacity()
{
	this->carCapacity++;
}

int Point::getCapacity() const
{
	return carCapacity;
}

int Point::getCurrCapacity() const
{
	return currCapacity;
}
void Point::decreaseCapacity()
{
	this->carCapacity--;
}
void Point::increaseCurrCapacity()
{
	this->currCapacity++;
}

void Point::decreaseCurrCapacity()
{
	this->currCapacity--;
}

bool Point::isFull() const
{
	return carCapacity <= currCapacity;
}
int Point::findRoad(int pointDestination)
{
	for(auto it: roads)
	{
		if(it.second->getDestination() == pointDestination)
		{
			return it.second->getId();
		}
	}
	return -1;
}
std::vector<int> Point::getNeighbors()
{
	std::vector<int> res;
	for(auto it: roads)
	{
		res.push_back(it.second->getDestination());
	}
	return res;
}

