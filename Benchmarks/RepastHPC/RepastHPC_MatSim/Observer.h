

#ifndef OBSERVER_H
#define OBSERVER_H

#include <boost/mpi.hpp>
#include <unordered_map>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/AgentRequest.h"
#include "Point.h"
#include "Road.h"
#include "Agent.h"
#include "AgentPackage.h"

class Point;
class Road;

class ObserverPackageProvider
{
private:
	repast::SharedContext<Agent> * agents;
public:
	ObserverPackageProvider(repast::SharedContext<Agent>* agents);
	void providePackage(Agent* agent, std::vector<AgentPackage> & out);
	void provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out);

};

class ObserverPackageReceiver
{
private:
	repast::SharedContext<Agent> * agents;
public:
	ObserverPackageReceiver(repast::SharedContext<Agent>* agents);
	Agent* createAgent(AgentPackage package);
	void updateAgent(AgentPackage package);

};

class Observer {
public:
	Observer(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	virtual ~Observer();
	//void init(std::string nodeFile, std::string travelFile);
	void init(std::string nodeFile, std::string carFile);
	void doSomething();
	void cleanUp(int result,Agent* agent, bool direction);
	void cleanDestination();
	void goHome();
	void moveAgents();
	void initSchedule(repast::ScheduleRunner& runner);
	void setExpectedCount();
	void setBeginningCount();
	void moveBack();
	void deletePoints();
	void deleteRoads();
	void parseLine(std::string line, bool onNode);
	void parseLine(std::string line);

private:
	void parseNodeFile(std::string nodeFile);
	void parseTravelFile(std::string travelFile);
//	void parseRoutes(std::string routes,Agent* agent);
	repast::Properties* props;
	std::unordered_map<int,Point*> points;
	std::unordered_map<int,Road*> roads;
	repast::SharedContext<Agent> context;
	int finishedCount;
	int expectedCount;
	int beginningCount;
	ObserverPackageProvider * provider;
	ObserverPackageReceiver * receiver;


};

#endif /* OBSERVER_H */
