
#ifndef POINT_H
#define POINT_H

#include<unordered_map>
#include<vector>
#include"Road.h"
class Road;

class Point {
public:
	Point();
	virtual ~Point();
	Point(int id, int carCapacity, double x, double y);
	double getX() const;
	double getY() const;
	int getID() const;
	int getCapacity() const;
	int getCurrCapacity() const;
	void insertRoad(int id, Road* road);
	void increaseCapacity();
	void decreaseCapacity();
	void decreaseCurrCapacity();
	void increaseCurrCapacity();
	bool isFull() const;
	int findRoad(int pointDestination);
	std::vector<int> getNeighbors();
private:
	double x;
	double y;
	int id;
	int carCapacity;
	int currCapacity;
	std::unordered_map<int,Road*> roads;
};

#endif /* POINT_H */

