#ifndef ROAD_H
#define ROAD_H

#include "Point.h"

class Point;

class Road {
public:
	Road();
	Road(int numLanes,int link_id, double length, int destination);
	Road(int numLanes,int link_id, double length,int origin, int destination);
	int getId() const;
	double getLength()  const;
	int getDestination() const;
	int getOrigin() const;
	bool isFull() const;
	void increaseCurr();
	void decreaseCurr();
	virtual ~Road();
private:
	int link_id;
	double  length;
	int capacity;
	int numLanes;
	int destination;
	int origin;
	int currCapacity;
};

#endif
