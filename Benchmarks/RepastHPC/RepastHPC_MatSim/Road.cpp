#include "Road.h"
#include<algorithm>
Road::Road() {
	link_id = -1;
	length = -1;
	numLanes = -1;
}

Road::Road(int numLanes,int link_id, double length, int destination)
{
	this->destination = destination;
	this->link_id = link_id;
	this->length = length;
	this->numLanes = numLanes;
	this->capacity = std::max(((int)(numLanes * length) / 15), 1);
	this->currCapacity = 0;
}
Road::Road(int numLanes,int link_id, double length,int origin, int destination)
{
	this->origin = origin;
	this->destination = destination;
	this->link_id = link_id;
	this->length = length;
	this->numLanes = numLanes;
	this->capacity = std::max(((int)(numLanes * length) / 15), 2);
	this->currCapacity = 0;
}
Road::~Road() {
	// TODO Auto-generated destructor stub
}

double Road::getLength() const
{
	return length;
}
int Road::getId() const
{
	return link_id;
}
int Road::getDestination() const
{
	return destination;
}

int Road::getOrigin() const
{
	return origin;
}
void Road::increaseCurr()
{
	currCapacity++;
}

void Road::decreaseCurr()
{
	currCapacity--;
}

bool Road::isFull() const
{
	return currCapacity >= capacity;
}
