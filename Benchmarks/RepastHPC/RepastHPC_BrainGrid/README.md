# BrainGrid

## Configuring the Program Files in Your Directory

Compile the benchmark’s source files with  “./compile.sh” to ensure all executable files are up to date.

## Providing Inputs 


## Running the Program

BrainGrid does not require any text files as input. Therefore, you can immediately run the following instructions after compiling your program:

1.	Execute run.sh with “./run.sh”. 
2.	Enter the following arguments when prompted:
    *	Number of Nodes – the number of computing nodes (machines) you want to use to run your program.