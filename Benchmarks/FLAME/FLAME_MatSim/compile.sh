# FLAME MATSIM
# new file, may need testing

# Specify grid size
read -p '# of Cars for MatSim: ' NCARS
read -p '# of Places for MatSim: ' NPLACES

# Generate initial state for specified grid size using python script
./init_states.py $NCARS $NPLACES

# Generate parallel C code using xparser

# - navigate to xparser directory
cd /home/NETID/dslab/users/agent_simulation_benchmarking/xparser

# and run xparser on our XML model template
./xparser /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_MatSim/matsim.xml -p

# Set this MPI variable so the FLAME makefile will work
export MPICH_F77=gfortran

# Generate the executable using the FLAME makefile
cd /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_MatSim
make clean
make LIBMBOARD_DIR=/home/NETID/dslab/agent_simulation_benchmarking/libmboard_install/
