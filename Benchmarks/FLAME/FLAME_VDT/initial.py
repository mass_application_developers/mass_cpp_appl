#!/usr/bin/env python
#
# Description:
#    Quick hack to create start states (0.xml) needed by the cellpotts abm
#	input: number of Teams, number of Tasks.

import re
import os
import sys
import random

infile = "tasks.txt"
outfile = "0.xml"

# Open file and write preamble
print "Writing to file %s ... " % outfile

do_stiff_edge=False

fin= open(infile,"r")
fout= open(outfile,"w")


line = fin.next().rstrip()
total,teams=map(int,re.findall(r'\d+',line))

fout.write("<states>\n<itno>0</itno>\n")
fout.write("""
    <environment>
        <NT>%d</NT>
        <NM>%d</NM>
    </environment>""" %(total,teams))



for team_id in range(5):
    for number in range(5):
        for mtype in range(5):
            id = team_id*25+number*5+mtype
            fout.write("""
    <xagent>
        <name>member</name>
        <id>%d</id>
        <ip>0</ip>
        <team_id>%d</team_id>
        <member_type>%d</member_type>
        <hour>0</hour>
        <day>0</day>
        <workhour>0</workhour>
        <coworkhour>0</coworkhour>
        <taskid>-1</taskid>
        <cotaskid>-1</cotaskid>
    </xagent>
    """ %(id,team_id,mtype)) 
id =0;
line = fin.next().rstrip()
line = fin.next().rstrip()

while(line != 'Production'):
   ## print map(int,re.findall(r'\d+',line))
    h1,h2,h3,h4,h5,type,priority,length,id,startday = map(int,re.findall(r'\d+',line))
    print h1,h2,h3,h4,h5,type,priority,length,id,startday
    fout.write("""
    <xagent>
        <name>task</name>
        <task_id>%d</task_id>
        <type>%d</type>
        <times>{%d,%d,%d,%d,%d}</times>.
        <assignteam>-1</assignteam>
        <priority>%d</priority>
        <start_day>%d</start_day>
        <total_hours>0</total_hours>
        <length>%d</length>
        <md>0</md>
    </xagent>"""%(id,type,h1,h2,h3,h4,h5,priority,startday,length))
    line = fin.next().rstrip()

line = fin.next().rstrip()
while line!="End":
   ## print map(int,re.findall(r'\d+',line))
    h1,h2,h3,h4,h5,type,priority,total,id = map(int,re.findall(r'\d+',line))
    fout.write("""
    <xagent>
        <name>task</name>
        <task_id>%d</task_id>
        <type>%d</type>
        <times>{%d,%d,%d,%d,%d}</times>.
        <assignteam>-1</assignteam>
        <priority>%d</priority>
        <start_day>%d</start_day>
        <total_hours>total</total_hours>
        <length>-1</length>
        <md>0</md>
    </xagent>"""%(id,type,h1,h2,h3,h4,h5,priority,total))
    line = fin.next().rstrip()

fout.write("""
    <xagent>    
        <name>timer</name>
        <current_hour>-1</current_hour>
        <current_day>0</current_day>
    </xagent>
    """)

fout.write("\n</states>\n")

fin.close()
fout.close()
