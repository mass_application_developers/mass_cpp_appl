#include "header.h"

#include "timer_agent_header.h"
#include "task_agent_header.h"
#include "member_agent_header.h"

#include <time.h>
#include <stdlib.h>// for random number.

#define collabNumber 5
#define true 1
#define false 0
#define IDLE  0
#define ONPROGRESS  1
#define DONE 2
#define TEAMS 25

#define READY 0
#define RUNNING 1
#define STOP 2

typedef int bool;

/**
 * in array, if cotent ==
 * 0. idle;
 * 1. in progress
 * 2. done
*/


int TIMER_STATUS_H = READY;
int array[1000];
int teamFinishSingal[15];
int teamTask[TEAMS][5];
bool initialized = false;
bool FINISH = false;
const int NUMBER_OF_TASK = 1000;
int ptr = collabNumber+1;

double start,stop,total_time;

void run_timer()
{
    if(TIMER_STATUS_H == READY)
    {
        start = get_time();
    }
    else if(TIMER_STATUS_H == RUNNING)
    {
        stop = get_time();
        total_time = stop - start;
        printf("Execution time - %d:%02d:%03d [mins:secs:msecs]\n",
       (int)(total_time/60), ((int)total_time)%60, (((int)(total_time * 1000.0)) % 1000));
    }
    TIMER_STATUS_H +=1;
}

bool isDone()
{        
    return FINISH;
}
int getId(int team_id)
{
    if(initialized == false)
    {
        for(int i = 0; i<TEAMS;i++)
            {
                for(int j = 0; j<5 ; j++)
                {
                    teamTask[i][j] = -1;

                }   
            }
        initialized = true;    
    }
    int id =-1;
    for(int i = team_id+5;i<NT;i+=NM)
    {
        if(array[i]==0)
        {
            array[i] = ONPROGRESS;
            id = i;
            break;
        }
    }
    //printf("get id is %d\n",id);
    return id;
}

int remain(int team_id)
{
    int id = -1;
    for (int i = team_id; i < NT; i +=NM)
    {
        if (array[i] != DONE)
        {
            id = i;
            return id;
        }
    }
    if(id!=-1)
    {
        return 0;
    }
    return 1;
}

void task_remark(int task_id)
{
    array[task_id] = DONE;
    if(isDone(task_id)==true)
    {
        FINISH = true;
    }
}

void setTeamTaskId(int teamId ,int task_id,int level)
{
    teamId = teamId/5;
    teamTask[teamId][0] = task_id;
    // printf("the Team %d Task is %d\n",teamId,task_id);
}

int getTeamTask(int teamId,int level)
{
    teamId = teamId/5;
    if(level>-1)
    {
        int tempLevel = level;
        while(teamTask[teamId][level]==-1 && tempLevel>0)
        {
            teamTask[teamId][level]=teamTask[teamId][tempLevel-1];
            // printf("the Member %d Task is %d\n",level,teamTask[teamId][level]);
            tempLevel -= 1;
        }
    }
    return teamTask[teamId][level];
}

void resetTeamTask(int teamId ,int task_id,int level)
{
    teamId = teamId/5;
    if(level >0)
    {
        teamTask[teamId][0] = -1;
    }
}
