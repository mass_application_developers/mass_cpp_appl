# Virtual Design Team (VDT)

## Configuring the Program Files in Your Directory

1.	Find compile.sh in the benchmark program’s directory. Edit the xparser line to point to the .xml file corresponding to this benchmark program. At the end of the file, make sure the script navigates to the benchmark program's directory before generating the executable.
2.	Find run.sh in the benchmark program’s directory. Edit the cat line so that it points to the host_master_list file you wish to use.
3.	Compile the benchmark’s source files with  “./compile.sh” to ensure all executable files are up to date. When prompted, specify N, where N x N are the dimensions of the grid.

## Providing Inputs

The required tasks.txt text file is formatted as follows. The first line indicates the number of tasks and the number of teams. The tasks are split into "Collaboration" and "Production" sections. The file ends with "End".

1000 5
Collaboration
toMeet:17,-1,-1,-1,17, Type:1, Priority:9999, Length:2, ID:0, Day:6
toMeet:15,-1,15,-1,15, Type:1, Priority:9999, Length:2, ID:1, Day:0
toMeet:3,3,3,3,3, Type:1, Priority:9999, Length:1, ID:2, Day:1
toMeet:4,-1,4,4,4, Type:1, Priority:9999, Length:2, ID:3, Day:5
toMeet:17,-1,-1,-1,-1, Type:1, Priority:9999, Length:1, ID:4, Day:4
Production
Hours:9,1,-1,-1,-1, Type:0, Priority:67, totalHours:10, ID:5
Hours:6,-1,9,2,4, Type:0, Priority:46, totalHours:21, ID:6
Hours:2,7,4,-1,9, Type:0, Priority:55, totalHours:22, ID:7
Hours:-1,8,6,2,7, Type:0, Priority:35, totalHours:23, ID:8
Hours:-1,-1,7,7,3, Type:0, Priority:89, totalHours:17, ID:9
...
Hours:2,-1,8,-1,1, Type:0, Priority:36, totalHours:11, ID:997
Hours:4,-1,6,3,-1, Type:0, Priority:26, totalHours:13, ID:998
Hours:2,-1,5,9,2, Type:0, Priority:50, totalHours:18, ID:999
End


## Running the Program

The Virtual Design Team (VDT) simulation requires the tasks.txt text file as an input. A premade tasks.txt file is present in the benchmark directory. Therefore, you can immediately run the following instructions after compiling your program:

1.	Execute run.sh with “./run.sh”. 
2.	Enter the following arguments when prompted:
    *	Number of Nodes – the number of computing nodes (machines) you want to use to run your program.
    *	Number of Iterations – the number of Iterations you want the Game of Life to go on for.