#include "function.h"

/***************************
***Timer agent functions****
***************************/
    /**
 * name: update_time(void)
 * descrption: the funcion will update the current time for all memeber agents.
 * @return 0
*/
int update_time()
{
    CURRENT_HOUR++;
    if(TIMER_STATUS_H == READY)
    {
        run_timer();
    }
    if(CURRENT_HOUR>=24)
    {
        CURRENT_HOUR = 0;
        CURRENT_DAY +=1;
    }
    add_time_message(CURRENT_HOUR,CURRENT_DAY);
    //printf("new round\n");
    if(isDone()==true){
        printf("current day is %d, current hour is %d \n", CURRENT_DAY, CURRENT_HOUR);
        run_timer();
        return 1;
    }
    
    return 0;
}


/*****************************
***Members agent functions****
*****************************/
int update()
{
    if(MEMBER_TYPE==0){
        //printf("update\n");
        // while(time_message)
        START_TIME_MESSAGE_LOOP
            HOUR = time_message->hour;
            DAY = time_message->day;
        FINISH_TIME_MESSAGE_LOOP
        //printf("Team %d member %d current day is %d, current hour is %d\n", TEAM_ID, MEMBER_TYPE, DAY, HOUR);
    }
    return 0;
}

int request_task()
{
    if(isDone()==true)
    {
        return 1;
    }
    //if the leader is idle, then the funtion could ask for a new task form timer.
    if(MEMBER_TYPE == 0&&IP==0){
        //printf("Lead is progress");
        if(IP==0)
        {
            //printf (" Lead take a new task\n");
            TASKID= getId(TEAM_ID);
            add_task_bill_message(ID/5,TASKID);
            setTeamTaskId(ID,TASKID,0);
            // printf ("new task ID is %d\n",TASKID);
            if(TASKID == -1)
            {
                // printf("Team %d has done\n",TEAM_ID);
                teamFinishSingal[TEAM_ID] = 1;
                return 0;
            }
        }
    }
    if(MEMBER_TYPE !=0 && IP==0)
    {
        TASKID = getTeamTask(ID,MEMBER_TYPE);
        // printf("Team %d level %d has assign Task %d\n",(ID/5),MEMBER_TYPE,TASKID);
    }
    else
    {
        if(teamFinishSingal[TEAM_ID]==1 && IP == 0)
        {
            // printf("Team %d member %d has done all tasks\n",TEAM_ID,MEMBER_TYPE);
            return 0;
        }
    }
    return 0;
}

int recive_task()
{
    if(isDone()==true)
    {
        return 1;
    }
    if(IP==0){
        // printf("my level is %d \n", MEMBER_TYPE);
        int count = 0;
        int work_hour = -1;
        int task_id = -2;
        int type = -1;
        int level = -1;
        START_TASK_LEVEL_MESSAGE_LOOP
            if(count==0)
            {
                work_hour = task_level_message->period;
                task_id = task_level_message->task_id;
                type = task_level_message->type;
                level = task_level_message->current_level;
                // printf("Level is %d\n",task_id);
                if(level == MEMBER_TYPE && (TASKID==task_id || task_id < 5|| level>0))
                {
                    TASKID = task_id;
                    count ++;
                }
            }
        FINISH_TASK_LEVEL_MESSAGE_LOOP
        if(count !=0)
        {
            // printf("here task ID: %d\n",task_id);
            if(type == 0 && work_hour>0)//if it is a produce work;
            {
                
                WORKHOUR = work_hour;
                // printf("work hour is %d\n",WORKHOUR);
                IP = 3;
                // printf("Receive: Level: %d, Task Id: %d, Engineer ID: %d work hour: %d\n",MEMBER_TYPE,TASKID,ID,WORKHOUR);
                if(MEMBER_TYPE==4 && ID ==4 && TASKID ==5)
                {
                    FINISH = true;
                }
                add_assign_message(MEMBER_TYPE, TASKID, ID);
                // printf("here1\n");
            }
            else if (type == 1 && work_hour > 0) // if it is a collabrative task;
            {
                
                COWORKHOUR = work_hour;
                COTASKID = task_id;
                IP =4;
                add_assign_message(MEMBER_TYPE,COTASKID, ID);
            }
            //printf("here\n");
        }
        else{
            add_assign_message(-1,-1,-1);
        }
    }
    return 0;
}


int move()
{
    if(IP>=3){
        // printf("Team %d member %d Level: %dis working task %d remain %d \n", TEAM_ID,ID,MEMBER_TYPE,TASKID, WORKHOUR);
        if (IP == 3) //IP production task.
        {
            WORKHOUR-=1;
            if (WORKHOUR <= 0)
            {
                IP = 1;
            }
        }
        if (IP == 4) //inporgress collabrative task.
        {
            COWORKHOUR--;
            if (COWORKHOUR <= 0)
            {
                IP = 2;
            }
        }
    }
    return 0;
}

int finish_task()
{
    if(IP==1 || IP ==2){
        //printf("here\n");
        int a;
        srand((unsigned)time(NULL));
        a = (int)rand()%100;
        int task_id = -1;
        int signal = -1;
        if(a<10) //E persent is 10%;
        {
            signal = 2;
        }
        else
        {
            if(MEMBER_TYPE == 4)
            {
                signal = 1;
            }
            else
            {
                signal = 0;
            }
        }
        if(IP == 2)
        {
            task_id = COTASKID;
            if(WORKHOUR > 0)
            {                
                IP = 1;
            } 
            else
            {
                resetTeamTask(ID,TASKID,MEMBER_TYPE);
                IP = 0;
            }
        }
        else if(IP == 1 )
        {
            resetTeamTask(ID,TASKID,MEMBER_TYPE);
            task_id = TASKID;
            IP = 0;
        }
        add_finish_message(MEMBER_TYPE,task_id,ID,signal);
    }
    return 0;
}


/**************************
***Task agent functions****
**************************/

int p_recive()
{   //printf("cool %d\n",TASK_ID);
    if(isDone()==true)
    {
        return 1;
    }
    if(TYPE == 0&&MD ==0){
        START_TASK_BILL_MESSAGE_LOOP
        if(task_bill_message->task_id == TASK_ID){
            //printf("recicive request\n");
            ASSIGNTEAM = task_bill_message->team_id;
            MD = 1;
        }
        FINISH_TASK_BILL_MESSAGE_LOOP
        if(MD==0){
            //printf("Task %d is not preparing to send MD = %d\n",TASK_ID, MD);
        }
    }   
    return 0;
}

int c_recive()
{
    if(isDone()==true)
    {
        return -1;
    }
    if(TYPE == 1&&MD ==0){
        START_TIME_MESSAGE_LOOP
        if(time_message->day == START_DAY)
        {
            ASSIGNTEAM = TASK_ID%5;
            MD = 1;
            break;
        }
        FINISH_TASK_LEVEL_MESSAGE_LOOP
    }
    return 0;
}

int send_task()
{
    //printf("here %d\n",TASK_ID);
    if(MD == 1){
        // printf("preparing send a task: %d\n",TASK_ID);

        int level = 0;
        int time = -1;
        for(int i = 0;i < 5;i++)
        {
            // printf("Time is %d\n",TIMES[i]);
            if(TIMES[i]!=-1)
            {
                level = i;
                time = TIMES[i];
                // printf("Task id: %d level %d=>The period is %d\n",TASK_ID,i,time);
                add_task_level_message(level,TASK_ID,ASSIGNTEAM,time,TYPE);
                return 0;
            }
        }
        // printf("Task id: %d has done\n",TASK_ID);
        return 1;
    }
    return 0;
}

int signal()
{
    
    if(MD == 1){
    //printf("Singal here\n");
    START_ASSIGN_MESSAGE_LOOP
        if(assign_message->task_id == TASK_ID && MD == 1){
            // printf("Task: %d assigned MD= %d\n",TASK_ID,MD);
            MD = 2;
            // printf("Task: %d assigned MD= %d\n",TASK_ID,MD);
        }
    FINISH_ASSIGN_MESSAGE_LOOP
    }
    return 0;
}

int is_finish()
{
    if(MD != 2)
    {
        return 0;
    }
    int signal = 0; 
    int level = 0;
    int taskId = -1;
    
    START_FINISH_MESSAGE_LOOP
        signal = finish_message->signal;
        level = finish_message->level;
        taskId = finish_message->task_id;
    FINISH_FINISH_MESSAGE_LOOP
    if(taskId==-1)
    {
        return 0;
    }
    // printf("task ID: %d ,level: %d ,signal: %d \n",taskId,level,signal);
    if(signal == 1)
    {
        MD = 3;
        TIMES[level] = -1;
        // printf("Task %d has finish\n",TASK_ID);
        task_remark(TASK_ID);
        return 1;
    }
    if(signal == 0)
    {
        // printf("Task %d READY FOR NEXT LEVEL\n",TASK_ID);
        TIMES[level] = -1;
        MD = 1;
    }
    if(signal ==2)
    {
        if(level == 0)
        {
            TIMES[0] *=2;
        }
        else
        {
            int b = (int)rand()%8+1;
            TIMES[level] += b;
        }
        MD = 1;
    }
    
    return 0;
}