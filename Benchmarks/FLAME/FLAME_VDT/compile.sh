# FLAME VDT

rm -rf node*.xml
-p 'input file tasks.txt'

# Generate initial state for specified states using python script
./initial.py

# Generate parallel C code using xparser

# - navigate to xparser directory
cd /home/NETID/dslab/agent_simulation_benchmarking/xparser/

# and run xparser on our XML model template
./xparser /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_VDT/model.xml -p

# Set this MPI variable so the FLAME makefile will work
export MPICH_F77=gfortran

# Generate the executable using the FLAME makefile
cd /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_VDT
make clean
make LIBMBOARD_DIR=/home/NETID/dslab/agent_simulation_benchmarking/libmboard_install/