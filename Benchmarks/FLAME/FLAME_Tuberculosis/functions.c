#include "header.h"
#include "Place_agent_header.h"
#include "Macrophage_agent_header.h"
#include "TCell_agent_header.h"
#include "AgentFactory_agent_header.h"
#include "DayTracker_agent_header.h"

#include "functions.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
/**
 * author : Sarah Panther
 * since  : August 15th, 2019
 *
 * CSS 499 - MASS C++ Benchmarking and Comparison
 *
 * Tuberculosis simulation function files
 */


//--------------------------- Place functions -----------------------------------

/**
 * update message board with current place states using placeStart messages
 * @return 0 - all place agents remain in the simulation
 */
int write_place() {
    add_placeStart_message(P_ID, BACTERIA, CHEMOKINE, MACROPHAGE, T_CELL);
//    printf("Place %d: bacteria - %d, chemokine - %d, macrophage - %d, t-cell - %d \n", P_ID, BACTERIA, CHEMOKINE,
//           MACROPHAGE, T_CELL);
    return 0;
}

/**
 * Advance time for current place:
 *
 * - decay current place's chemokine level by decrementing it if it isn't 0
 *
 * - grow bacteria if it is the correct day by setting this place's bacteria flag to 1 if any neighboring places have
 *   bacteria
 *
 * @return 0 - all place agents remain in the simulation
 */
int decay_chemokine_and_grow_bacteria() {

    // decay chemokine level
    if (CHEMOKINE > 0) {
        --CHEMOKINE;
    }

    // check the day to see if bacteria should grow
    day_message = get_first_day_message();

    int today = day_message->day_number;

    // if this place doesn't already have a bacteria and today is a bacterial growth day, check neighbors to see if
    // bacteria will spread here
    if (BACTERIA == FALSE && (today % bacterialGrowth) == 0) {

        // look through neighbor's messages to see if they are infected
        placeStart_message = get_first_placeStart_message();

        while (placeStart_message) {
            // if neighbor has bacteria, this current place becomes infected too
            if (placeStart_message->bacteria == TRUE) {
                BACTERIA = TRUE;
                break;
            }

            // if neighbor isn't infected, check the next message
            placeStart_message = get_next_placeStart_message(placeStart_message);
        }
    }

    add_placeDecayAndGrow_message(P_ID, CHEMOKINE);
    return 0;
}

/**
 * Immune cells enter simulation space via blood vessels.
 *
 * - When today < tCellEntrance, only macrophages come through the blood vessels.
 *
 * - When today >= tCellEntrance, macrophages and T-cells each have a 50% chance of appearing through the blood
 *   vessels if no immune cells are on top of the blood vessels
 *
 * - If there are immune cells present on the blood vessel Place, the type of immune cell that is not currently
 *   located on the Place is able to come through the blood vessel and enter the simulation
 *
 * - Macrophage and T-cells agents are added to the simulation by the Agent Factory
 *
 * return 0 - all place agents remain in the simulation
 */

int cell_recruitment() {

    // if not a blood vessel, do nothing
    if (BLOOD_VESSEL == FALSE) {
        return 0;
    }

    day_message = get_first_day_message();
    int today = day_message->day_number;

    Spawn spawnType = spawnImmuneCell(today);

    switch (spawnType) {
        case macroSpawn:
            add_requestMacrophage_message(P_ID, X, Y, BACTERIA, PLACE_NEIGHBORS);
            MACROPHAGE = TRUE;
            break;

        case tCellSpawn:
            add_requestTCell_message(P_ID, X, Y, PLACE_NEIGHBORS);
            T_CELL = TRUE;
            break;

        case noneSpawn:
            break;

        default:
            printf("Invalid spawn type for Place %d: %d ,%d", P_ID, X, Y);
            break;
    }
    return 0;
}

/**
 * Helps cell_recruitment() function determine whether to request a T-cell or macrophage from the Agent Factory or if
 * no immune cell should be spawned
 *
 * @param today
 * @return 0 - all Places remain in the simulation
 */
Spawn spawnImmuneCell(int today) {

    // if there are max number of immune cells on this Place, don't recruit any new cells
    if (MACROPHAGE == TRUE && T_CELL == TRUE) {
        return noneSpawn;
    } else if (T_CELL == FALSE && MACROPHAGE == TRUE) {
        if (today >= tCellEntrance) {
            return tCellSpawn;
        } else {
            return noneSpawn;
        }
    } else if (MACROPHAGE == FALSE && T_CELL == TRUE) {
        return macroSpawn;
    }
        // no immune cells at this current Place
    else {
        return ((today >= tCellEntrance) && (rand() & 1) == 1) ? tCellSpawn : macroSpawn;
    }
}

/**
 * If there isn't already a macrophage on this space, read through the macrophage messages and grant only one
 * macrophage access to the current place.
 *
 * Note that movement is conservative. Macrophages can only move to Places that are currently unoccupied by other
 * macrophages.
 *
 * @return 0 - all Places remain in the simulation
 */
int approve_macrophage_movement() {
    if (MACROPHAGE == FALSE) {
        bool granted = false;

        // look through requests to move to this current Place and grant the move only to the first macroMoveReq message
        macroMoveReq_message = get_first_macroMoveReq_message();

        while (macroMoveReq_message) {
            int macroID = macroMoveReq_message->m_id;

            if (!granted) {
                // add grantMacroMove message granting the first macrophage permission
                add_grantMacroMove_message(macroID, TRUE, X, Y, BACTERIA);
//                printf("\nPlace %d granting macrophage %d access to %d, %d\n", P_ID, macroID, X, Y);
                granted = true;
            } else {
                // deny all other macrophages permission with the grantMacroMove message
                add_grantMacroMove_message(macroID, FALSE, X, Y, BACTERIA);
//                printf("\nPlace %d DENYING macrophage %d access to %d, %d\n", P_ID, macroID, X, Y);
            }
            macroMoveReq_message = get_next_macroMoveReq_message(macroMoveReq_message);
        }
    }
    return 0;
}

/**
 * If there isn't already a T-cell on this space, read through the T-cell messages and grant only T-cell access
 * to the current place.
 *
 * Note that movement is conservative.  T-cells can only move to Places that are currently unoccupied by other T-cells.
 *
 * @return 0 - all Places remain in the simulation
 */

int approve_t_cell_movement() {
    if (T_CELL == FALSE) {
        bool granted = false;

        // look through requests to move to this current Place and grant the move only to the first tCellMoveReq message
        tCellMoveReq_message = get_first_tCellMoveReq_message();

        while (tCellMoveReq_message) {
            int tCellID = tCellMoveReq_message->t_id;

            if (!granted) {
                // add tCellState message granting the first T-cell permission to move here
                add_tCellState_message(tCellID, X, Y);
//                printf("\nPlace %d granting T-cell %d access to %d, %d\n", P_ID, tCellID, X, Y);
                granted = true;
            } else {
                // deny all other T-cells permission with the tCellState message
                add_tCellState_message(tCellID, tCellMoveReq_message->old_p_x, tCellMoveReq_message->old_p_y);
            }
            tCellMoveReq_message = get_next_tCellMoveReq_message(tCellMoveReq_message);
        }
    }
    return 0;
}


/**
 * Applies rules to current Place:
 *
 * - updates Place's state to reflect if there is a macrophage and/or t-cell on it
 *
 * - adjusts chemokine level to max (max = 2) if any INFECTED, ACTIVATED, or CHRONICALLY INFECTED macrophages are on
 *   the current Place or neighboring this Place
 *
 * - if a CHRONICALLY INFECTED macrophage has burst and died (currently in DEAD state) on this Place or in any
 *   neighboring spaces (within chronically infected macrophage's Moore's area), bacteria will be spawned here
 *
 * - if an ACTIVATED macrophage is currently on this space or a RESTING macrophage that hasn't been infected yet, it
 *   will kill any bacteria located here. This rule has precedence over bacterial death spread
 *
 * - Note that it is the RESTING macrophage agent's responsibility to update itself to INFECTED once it is located on
 *   a bacteria and eats it (in the macrophage_react() function)
 *
 * @return 0 - all Places remain in the simulation
 */
int react_to_macro_and_tcell() {

    bool maxedChemokine = false;

    // because an ACTIVATED or a RESTING macrophage kills or eats, respectively, any bacteria it's
    // located on and becomes infected, this tracks if any are located on the current Place
    bool activatedOrResting = false;
    bool macroOnPlace = false;

    // until we know that there is a macrophage on this Place (macroOnPlace == true), we assume there is no
    // macrophage until proven otherwise by a macroState message
    MACROPHAGE = FALSE;

    macroState_message = get_first_macroState_message();
    // process messages from macrophages in neighboring Places or on the current Place
    while (macroState_message) {

        State state = macroState_message->state;
        macroOnPlace = (macroState_message->x == X) && (macroState_message->y == Y);

        // we assume there is at most one macroState message from a macrophage on this Place
        if (macroOnPlace && state != DEAD) {
                MACROPHAGE = TRUE;
        }

        if (!maxedChemokine && (state == INFECTED || state == ACTIVATED || state == CHRONICALLY_INFECTED)) {
            CHEMOKINE = maxChemokine;
            maxedChemokine = true;
        }

        // If an activated/resting macrophage is on this Place, any bacteria here is killed
        if ((state == ACTIVATED || state == RESTING) && macroOnPlace) {
            activatedOrResting = true;
            BACTERIA = FALSE;
        }

        // Macrophage has died due to a bacterial or T-cell death (either on this Place or from its neighbors) and
        // there is no activated or resting macrophage on this Place to kill the bacteria - spread bacteria here
        if (BACTERIA == FALSE && !activatedOrResting && state == DEAD) {
            BACTERIA = TRUE;
        }
        macroState_message = get_next_macroState_message(macroState_message);
    }

    // update T-cell state
    tCellState_message = get_first_tCellState_message();
    // we are expecting at most one T-cell message located at this Place
    if (tCellState_message) {
        T_CELL = TRUE;
    } else {
        T_CELL = FALSE;
    }
    return 0;
}


//--------------------------- Macrophage functions -----------------------------------

/**
 * The macrophage chooses a spot to move to in its Moore's area based on the highest neighboring chemokine value and
 * requests this Place by posting a move request message
 *
 * @return 0 - All macrophages remain in the simulation
 */
int macrophage_request_move() {
    // Note: deterministic random movement - can be altered later

    int reqPlace = -1;

    // neighbors with chemokine levels of 1
    int mediocreNeighbors[agentNeighborSize] = {0};
    int neighborIndex = 0;

    // find the neighboring Place with the highest chemokine level
    placeDecayAndGrow_message = get_first_placeDecayAndGrow_message();

    while (placeDecayAndGrow_message) {
        int chemokine = placeDecayAndGrow_message->chemokine;

        if (chemokine == maxChemokine) {
            reqPlace = placeDecayAndGrow_message->id;
            break;
        } else if (chemokine == 1) {
            mediocreNeighbors[neighborIndex++] = placeDecayAndGrow_message->id;
        }

        placeDecayAndGrow_message = get_next_placeDecayAndGrow_message(placeDecayAndGrow_message);
    }

    // the neighboring Places don't have any chemokine present
    if (reqPlace == -1 && neighborIndex == 0) {
        int randIndex = rand() % agentNeighborSize;
        reqPlace = M_NEIGHBORS[randIndex];
    }

        // if there isn't a max chemokine level Place but some chemokine = 1 level Places
    else if (reqPlace == -1) {
        reqPlace = mediocreNeighbors[rand() % neighborIndex];
    }

    add_macroMoveReq_message(reqPlace, M_ID);
    return 0;
}

/**
 * The macrophage reads through grantMacroMove message to see if the requested Place granted the macrophage's move to
 * it. If the move is approved, the macrophage updates its location to the requested Place; if not, the macrophage
 * doesn't move.
 *
 * Note - messages to this function are filtered to only include messages with this current macrophage's ID (will
 * only receive messages addressed to it)
 *
 * @return 0 - All macrophages remain in the simulation
 */
int macrophage_move() {
    grantMacroMove_message = get_first_grantMacroMove_message();

    if (grantMacroMove_message) {

        if (grantMacroMove_message->m_permission_granted == TRUE) {
            // move macrophage to requested Place
            M_X = grantMacroMove_message->place_x;
            M_Y = grantMacroMove_message->place_y;
            LOCATED_ON_BACTERIA = grantMacroMove_message->on_bacteria;

            // recalculate neighbors' Place IDs
            int *neighbors = calculateNeighbors(M_X, M_Y);
            if (neighbors) {
                memcpy(M_NEIGHBORS, neighbors, sizeof(M_NEIGHBORS));
                free(neighbors);
            }
        }
    }
    return 0;
}


/**
 * Macrophage's state at the beginning of the function can be one of four states:
 * - RESTING
 * - INFECTED
 * - ACTIVATED
 * - CHRONICALLY_INFECTED
 *
 * Macrophage's state after the function may be any one of those four states in addition to DEAD.
 *
 * See the Tuberculosis simulation specification's section about Macrophage agents in the mass_cpp_appl repository on
 * bitbucket.org for the details
 *
 * @return 0 if macrophage is alive after reacting to its current circumstances
 *         1 if the macrophage dies - macrophage agent is removed from the simulation
 */
int macrophage_react() {

    bool onTCell = false;

    // check the day to see if there are any T-cells in the simulation
    day_message = get_first_day_message();
    int today = day_message->day_number;

    // check to see if macrophage is on the same Place as a T-cell
    if (today >= tCellEntrance) {
        tCellState_message = get_first_tCellState_message();

        if (tCellState_message) {
            onTCell = true;
        }
    }

    switch (STATE) {
        case RESTING:
            if (LOCATED_ON_BACTERIA == TRUE) {
                STATE = INFECTED;
                DAY_INFECTED = today;
                growInfectedIntraCBact(DAY_INFECTED);
            }
            break;

        case INFECTED:
            // check if INFECTED macrophage will be activated by T-cell
            if (onTCell) {
                STATE = ACTIVATED;
                //printf("Macrophage %d became ACTIVATED", M_ID);
                INTRACELLULAR_BACTERIA = 0;
                break;
            }
            // grow intra-cellular bacteria at the infected macrophage rate
            growInfectedIntraCBact(today);

            // if not activated, check to see if internal bacterial level will cause this macrophage to become
            // CHRONICALLY_INFECTED
            if (INTRACELLULAR_BACTERIA >= chronicInfection) {
                STATE = CHRONICALLY_INFECTED;
                //printf("Macrophage %d became CHRONICALLY INFECTED", M_ID);
            }
            //printf("\nInfected macrophage %d has %d bacteria", M_ID, INTRACELLULAR_BACTERIA);
            break;

        case ACTIVATED:
            // do nothing - Place will react to this macrophage's presence accordingly
            break;

        case CHRONICALLY_INFECTED:
            // grow intra-cellular bacteria at the chronically infected macrophage rate
            growChronicIntraCBact();
            if (INTRACELLULAR_BACTERIA >= bacterialDeath || onTCell) {
                STATE = DEAD;
//                printf("\nMacrophage %d died with %d bacteria\n", M_ID, INTRACELLULAR_BACTERIA);
            }
            break;

        default:
            printf("Invalid state in macrophage_react() for Macrophage %d at ( %d, %d )", M_ID, M_X, M_Y);
            break;
    }
    add_macroState_message(M_NEIGHBORS, M_X, M_Y, STATE);

    // remove dead macrophages from the simulation
    if (STATE == DEAD) {
        return 1;
    }
    return 0;
}

/**
 * Grows bacteria at the rate 2*t + 1, where t = today - day_infected
 *
 * @param today - current day (iteration number)
 */
void growInfectedIntraCBact(int today) {
    int t = today - DAY_INFECTED;
    INTRACELLULAR_BACTERIA = (2 * t) + 1;
    // demo with 10*1
//    INTRACELLULAR_BACTERIA = (10 * t) + 1;
}

/**
 * Grows bacteria inside the macrophage at a constant rate of an additional 2 bacteria per time step: a much slower
 * rate than when it was in the INFECTED state
 */
void growChronicIntraCBact() {
    INTRACELLULAR_BACTERIA += 2;
}

//------------------------------------ Macrophage & T-cell helper functions --------------------------------------------

/**
 * - Macrophage and T-cell's x and y variables have to be correctly updated to new position before this function is
 *   run to calculate their new neighboring spaces
 * - assumes agentNeighborSize = 8 ; 8 neighbors for macrophage and T-cell helper movement
 *
 * @param agentX - agent's X position
 * @param agentY - agent's Y position
 */
int *calculateNeighbors(int agentX, int agentY) {
    int neighborIndex = 0;
    int *neighbors = (int *) malloc(agentNeighborSize * sizeof(int));

    if (!neighbors) {
        return NULL;
    }

    // x and y match up to the standard x-axis and y-axis of the simulation space
    int y = -1 * movementRadius;
    int x = -1 * movementRadius;
    for (; y <= movementRadius; y++) {
        for (; x <= movementRadius; x++) {

            // don't add current position to NEIGHBORS array
            if (y == 0 && x == 0) {
                continue;
            }
            // add the Place ID's in the Moore's area of the macrophage's current location to its NEIGHBORS array
            neighbors[neighborIndex++] = toPlaceID(torus(x + agentX), torus(y + agentY));
        }
    }
    return neighbors;
}

/**
 * Translates a Place's x and y coordinates into the Place's ID number
 *
 * @param x - index along the x axis
 * @param y - index along the y axis
 * @return - 1D location (Place 0 is located at left bottom corner of grid simulation space and the last Place (grid
 *           side length * grid side length - 1) is located at the right top corner)
 */
int toPlaceID(int x, int y) {
    return x + (y * GRID_SIZE);
}

/**
 * Grid of Places is a torus (left overflows to right, top overflows to bottom, etc.)
 *
 * for helping calculate neighboring indices of Places
 *
 * @param index
 * @return toroidal index
 */
int torus(int index) {

    if (index < 0) {
        index += GRID_SIZE;
    } else if (index >= GRID_SIZE) {
        index -= GRID_SIZE;
    }
    return index;
}

//------------------------------------------- T-cell functions ---------------------------------------------------------

/**
 * the T-cell requests to move to one of the Places neighboring it by posting a tCellMoveReq message
 *
 * Note that this is the same function as macrophage_request_move() because T-cells have the same movement behavior
 * as macrophages; they both seek to move to the space with the highest chemokine level.  If there is no chemokine
 * present, their movement is random (in their current position's Moore's area).
 *
 * @return 0 - all T-cells remain in the simulation
 */
int t_cell_request_move() {
    // Note: deterministic random movement - can be altered later

    int reqPlace = -1;

    // neighbors with chemokine levels of 1
    int mediocreNeighbors[agentNeighborSize] = {0};
    int neighborIndex = 0;

    // find the neighboring Place with the highest chemokine level
    placeDecayAndGrow_message = get_first_placeDecayAndGrow_message();

    while (placeDecayAndGrow_message) {

        int chemokine = placeDecayAndGrow_message->chemokine;

        if (chemokine == maxChemokine) {
            reqPlace = placeDecayAndGrow_message->id;
            break;
        } else if (chemokine == 1) {
            mediocreNeighbors[neighborIndex++] = placeDecayAndGrow_message->id;
        }

        placeDecayAndGrow_message = get_next_placeDecayAndGrow_message(placeDecayAndGrow_message);
    }

    // the neighboring Places don't have any chemokine present
    if (reqPlace == -1 && neighborIndex == 0) {
        reqPlace = T_NEIGHBORS[rand() % agentNeighborSize];
    }

        // if there isn't a max chemokine level Place but some chemokine = 1 level Places
    else if (reqPlace == -1) {
        reqPlace = mediocreNeighbors[rand() % neighborIndex];
    }

    add_tCellMoveReq_message(reqPlace, T_X, T_Y, T_ID);
    return 0;
}

/**
 * the T-cell moves to the requested Place if the request is granted in the grantTCellMove message; if not, the
 * T-cell doesn't move
 *
 * Note that messages to this function are filtered for this current T-cells ID, so there should only be one message
 *
 * @return 0 - all T-cells remain in the simulation
 */
int t_cell_move() {
    tCellState_message = get_first_tCellState_message();

    if (tCellState_message) {
        // move T-cell to requested Place if permission is granted
        int newX = tCellState_message->x;
        int newY = tCellState_message->y;
        bool moved = (newX != T_X) && (newY != T_Y);

        if (moved) {
            T_X = newX;
            T_Y = newY;
            // recalculate neighbors' Place IDs
            int *neighbors = calculateNeighbors(T_X, T_Y);
            if (neighbors) {
                memcpy(T_NEIGHBORS, neighbors, sizeof(T_NEIGHBORS));
                free(neighbors);
            }
        }
        // if the T-cell hasn't moved, don't do anything
    }
    return 0;
}


//------------------------------------------- Agent Factory functions --------------------------------------------------

/**
 * Creates new macrophage and T-cell agents for requesting Places that are blood vessels
 *
 * - assumes requesting Place's are valid blood vessels (entry points)
 *
 * @return 0 - Agent Factory remains in the simulation
 */
int create_new_macrophages_and_t_cells() {

    // go through all the requestMacrophage messages and create new macrophage agents as requested
    requestMacrophage_message = get_first_requestMacrophage_message();
    while (requestMacrophage_message) {

        // add macrophage to simulation
        int placeX = requestMacrophage_message->x;
        int placeY = requestMacrophage_message->y;
        int onBacteria = requestMacrophage_message->has_bacteria;

        add_Macrophage_agent(AVAILABLE_MACROPHAGE_ID++, placeX, placeY, RESTING, onBacteria, -1, 0,
                             requestMacrophage_message->place_neighbors);

        requestMacrophage_message = get_next_requestMacrophage_message(requestMacrophage_message);
    }

    // go through all the requestTCell messages and create new T-cell agents as requested
    requestTCell_message = get_first_requestTCell_message();
    while (requestTCell_message) {

        // add T-cell to simulation
        int t_placeX = requestTCell_message->x;
        int t_placeY = requestTCell_message->y;

        add_TCell_agent(AVAILABLE_T_CELL_ID++, t_placeX, t_placeY, requestTCell_message->place_neighbors);

        requestTCell_message = get_next_requestTCell_message(requestTCell_message);
    }
    return 0;
}

//-------------------------------------------- Day Tracker functions ---------------------------------------------------

/**
 * Posts the current day number (iteration number)
 *
 * @return 0 - day tracker remains in the simulation
 */
int post_day() {
    // post the current day and increment it for the next day
    add_day_message(DAY++);
    return 0;
}
