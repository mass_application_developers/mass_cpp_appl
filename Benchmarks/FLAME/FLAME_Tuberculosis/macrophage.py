class Macrophage(object):

    def __init__(self, id, x, y, neighbors):
        self.id = id
        self.x = x
        self.y = y
        self.neighbors = neighbors



    def __repr__(self):
        return """
        <xagent>
            <name>Macrophage</name>
            <m_id>{id}</m_id>
            <m_x>{x}</m_x>
            <m_y>{y}</m_y>
            <state>{state}</state>
            <located_on_bacteria>{located_on_bacteria}</located_on_bacteria>
            <day_infected>{day_infected}</day_infected>
            <intracellular_bacteria>{intracellular_bacteria}</intracellular_bacteria>
            <m_neighbors>{{{neighbors}}}</m_neighbors>
        </xagent>""".format(id=self.id, x=self.x, y=self.y, state=0, located_on_bacteria=0, 
                            day_infected=-1, intracellular_bacteria=0, neighbors=", ".join(map(str, 
                            self.neighbors)))