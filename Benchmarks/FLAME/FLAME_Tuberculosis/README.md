# Tuberculosis

## Configuring the Program Files in Your Directory

1.	Find compile.sh in the benchmark program’s directory. Edit the xparser line to point to the .xml file corresponding to this benchmark program. At the end of the file, make sure the script navigates to the benchmark program's directory before generating the executable.
2.	Find run.sh in the benchmark program’s directory. Edit the cat line so that it points to the host_master_list file you wish to use.
3.	Compile the benchmark’s source files with  “./compile.sh” to ensure all executable files are up to date. When prompted, specify N, where N x N are the dimensions of the grid.

## Providing Inputs 


## Running the Program

The Tuberculosis simulation does not require any text files as input. Therefore, you can immediately run the following instructions after compiling your program:

1.	Execute run.sh with “./run.sh”. 
2.	Enter the following arguments when prompted:
    *	Number of Nodes – the number of computing nodes (machines) you want to use to run your program.
    *	Number of Iterations – the number of Iterations you want the Game of Life to go on for.