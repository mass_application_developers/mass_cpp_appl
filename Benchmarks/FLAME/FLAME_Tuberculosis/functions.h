//
// Created by sarah on 7/25/19.
//

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdbool.h>

// Global constants ----------------------------------------------------------------------------------------------------
#define FALSE 0
#define TRUE 1

// Environmental constants----------------------------------------------------------------------------------------------

// Number of days between points of time when extra-cellular bacteria grows radially out by one unit
#define bacterialGrowth 10

// Macrophage constants-------------------------------------------------------------------------------------------------

typedef enum State{RESTING, INFECTED, ACTIVATED, CHRONICALLY_INFECTED, DEAD} State;

// Max chemokine level dispersed by an INFECTED and CHRONICALLY_INFECTED agent - also how many days the signal lasts at
// the current Place
#define maxChemokine 2

// Number of internal bacteria that causes an INFECTED macrophage to become CHRONICALLY_INFECTED
#define chronicInfection 75

// Number of internal bacteria that causes a CHRONICALLY_INFECTED macrophage to die
#define bacterialDeath 100

// T-cell constants-----------------------------------------------------------------------------------------------------
#define tCellEntrance 10

// Macrophage & T-cell constants----------------------------------------------------------------------------------------

// size of array of neighbors for agent's movement, bacterial death spread area, and chemokine dispersal
#define agentNeighborSize 8

// radial area of movement for Macrophages & T-cells
#define movementRadius 1

typedef enum Spawn{macroSpawn, tCellSpawn, noneSpawn} Spawn;

// Place helper functions
Spawn spawnImmuneCell(int today);

// Macrophage helper functions
void growInfectedIntraCBact(int today);
void growChronicIntraCBact();

// Macrophage and T-cell helper functions
int * calculateNeighbors(int agentX, int agentY);
int toPlaceID(int x, int y);
int torus(int index) ;

#endif //FUNCTIONS_H
