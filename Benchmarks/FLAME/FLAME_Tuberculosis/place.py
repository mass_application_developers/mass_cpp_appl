class Place(object):

    def __init__(self, id, x, y, bacteria, blood_vessel, place_neighbors):
        self.id = id
        self.x = x
        self.y = y
        self.bacteria = bacteria
        self.blood_vessel = blood_vessel
        self.macrophage = 0
        self.place_neighbors = place_neighbors

    def place_macrophage(self):
        self.macrophage = 1

    def spawn_bacteria(self):
        self.bacteria = 1


    def __repr__(self):
        return """
        <xagent>
            <name>Place</name>
            <p_id>{id}</p_id>
            <x>{x}</x>
            <y>{y}</y>
            <bacteria>{bacteria}</bacteria>
            <chemokine>{chemokine}</chemokine>
            <blood_vessel>{blood_vessel}</blood_vessel>
            <macrophage>{macrophage}</macrophage>
            <t_cell>{t_cell}</t_cell>
            <place_neighbors>{{{place_neighbors}}}</place_neighbors>
        </xagent>""".format(id=self.id, x=self.x, y=self.y, 
                            bacteria=self.bacteria, chemokine=0, 
                            blood_vessel=self.blood_vessel, macrophage=self.macrophage, 
                            t_cell=0, place_neighbors=", ".join(map(str, self.place_neighbors)))