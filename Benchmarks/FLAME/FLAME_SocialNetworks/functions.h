/**
 * author : Sarah Panther
 * since  : September 23rd, 2019
 *
 * CSS 499 - MASS C++ Benchmarking and Comparison
 *
 * Social Networks simulation function files
 */

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#define FRIEND_MESSAGE_SIZE 10

#include <stdbool.h>

bool alreadyFriends(int id);    // Checks to see if this Person agent is already friends with the input Person
void printOutput();             // Prints this Person's friends to the command line when the needed degree is reached

#endif //FUNCTIONS_H
