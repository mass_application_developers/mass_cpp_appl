# FLAME SOCIAL NETWORKS

# Specify social network to create
read -p 'Number of people in the social network (must be an even number) = ' NUM_PEOPLE
read -p 'Degree of friendship to find = ' DEGREE_FRIEND
read -p 'Number of 1st degree friends each person has = ' K_FIRST

# Generate initial state for specified grid size using java program
javac init_state_SocialNet.java
java init_state_SocialNet $NUM_PEOPLE $DEGREE_FRIEND k $K_FIRST

# Generate parallel C code using xparser
cd /home/NETID/dslab/agent_simulation_benchmarking/xparser/
./xparser /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_SocialNetworks/Social_Networks.xml -p

# Set this MPI variable so the FLAME makefile will work
export MPICH_F77=gfortran

# Generate the executable using the FLAME makefile
cd /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_SocialNetworks/
make clean
make LIBMBOARD_DIR=/home/NETID/dslab/agent_simulation_benchmarking/libmboard_install/
