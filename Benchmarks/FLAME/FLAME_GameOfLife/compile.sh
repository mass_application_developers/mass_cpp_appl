# FLAME GAME OF LIFE

# Specify grid size
read -p 'Size N for an N x N grid: ' NSIZE

# Generate initial state for specified grid size using python script
./init_state.py $NSIZE $NSIZE

# Generate parallel C code using xparser

# - navigate to xparser directory
cd /home/NETID/dslab/users/agent_simulation_benchmarking/xparser

# and run xparser on our XML model template
./xparser /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_GameOfLife/Game_of_life.xml -p

# Set this MPI variable so the FLAME makefile will work
export MPICH_F77=gfortran

# Generate the executable using the FLAME makefile
cd /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_GameOfLife
make clean
make LIBMBOARD_DIR=/home/NETID/dslab/agent_simulation_benchmarking/libmboard_install/
