# FLAME GAME OF LIFE

# ask for number of computing nodes
read -p '# of computing nodes (including this one) = ' NODES

# copy the correct number of hosts to the host file (CSSmpdboot always runs all hosts in mpd.hosts)
cat /home/NETID/dslab/agent_simulation_benchmarking/host_master_list | head -$(($NODES-1)) > ~/mpd.hosts

# boot the mpi cluster
CSSmpdboot -n $NODES -v

# ask for the number of iterations
read -p '# iterations = ' ITERATIONS

# run the parallel model using round-robin partitioning
mpirun -np $NODES main $ITERATIONS 0.xml -r

# mpi exit out of computing nodes used
mpdallexit
