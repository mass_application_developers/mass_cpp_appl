import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.JFrame;

/**
 * @author Sarah Panther - adapted from Wout.java by Dr. Fukuda
 * @since 11-19-19
 */

class BGOut {
    static final int defaultCellWidth = 10;

    static final Color bgColor = new Color(224, 224, 224);
    static final Color somaColor = new Color(0, 0, 102);

    private int N = 0;              // the simulation space size
    private JFrame gWin;            // a graphics window
    private int cellWidth;          // each Place's cell's width in the window
    private Insets theInsets;       // the insets of the window

    // Neuron type colors
    private static Color nTypeColors[];

    // Neuron signal colors
    private static Color signalColors[];

    public BGOut(int size) {
        N = size;
        startGraphics(N);
    }

    private void startGraphics(int N) {
        // set to cells to this size so it's easy to see what's going on
        cellWidth = defaultCellWidth;

        // initialize window and graphics:
        gWin = new JFrame("Self-Organizing Neural Network");
        gWin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gWin.setLocation(50, 50);  // screen coordinates of top left corner
        gWin.setResizable(false); // false because cells don't resize themselves
        gWin.setVisible(true);    // show it!
        theInsets = gWin.getInsets();
        gWin.setSize(N * cellWidth + theInsets.left + theInsets.right,
                N * cellWidth + theInsets.top + theInsets.bottom);

        // wait for frame to get initialized
        long resumeTime = System.currentTimeMillis() + 1000;
        do {
        } while (System.currentTimeMillis() < resumeTime);

        // pop out the graphics
        Graphics g = gWin.getGraphics();
        g.setColor(bgColor);
        g.fillRect(theInsets.left,
                theInsets.top,
                N * cellWidth,
                N * cellWidth);

        // set signal colors - 7 to represent default threshold value 0.4 to
        // >=1.0 in .1 increments
        signalColors = new Color[10];
        signalColors[0] = new Color(30, 60, 0);
        signalColors[1] = new Color(50, 100, 0);
        signalColors[2] = new Color(76, 153, 0);
        signalColors[3] = new Color(102, 204, 0);
        signalColors[4] = new Color(128, 255, 0);
        signalColors[5] = new Color(153, 255, 51);
        signalColors[6] = new Color(178, 255, 102);
        signalColors[7] = new Color(204, 255, 153);
        signalColors[8] = new Color(229, 255, 204);
        signalColors[9] = new Color(255, 255, 255);

        nTypeColors = new Color[3];
        // Excitatory
        nTypeColors[0] = new Color(127, 0, 255);
        // Inhibitory
        nTypeColors[1] = new Color(192,192, 192);
        // Neutral
        nTypeColors[2] = new Color(153, 204, 255);
    }

    public void writeToGraphics(Place[] places) {
        Graphics g = gWin.getGraphics();

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                displayPlace(g, places[BGXmlParser.toPlaceID(i, j, N)], i, j);
            }
        }
    }

    public void setTitle(String iterationTitle) {
        gWin.setTitle(iterationTitle);
    }

    private void displayPlace(Graphics g, Place p, int x, int y) {
        // clear Place square
        g.setColor(bgColor);

        // if Place has Neuron and no signal
        if (p.hasSoma()) {
            g.setColor(somaColor);
        } else if (p.getOccupyingNID() != -1) {
            if (p.getSignalLevel() < .4) {
                switch (p.getType()) {
                    case EXCITATORY:
                        g.setColor(
                                nTypeColors[Neuron_Type.EXCITATORY.ordinal()]);
                        break;

                    case INHIBITORY:
                        g.setColor(
                                nTypeColors[Neuron_Type.INHIBITORY.ordinal()]);
                        break;

                    case NEUTRAL:
                        g.setColor(
                                nTypeColors[Neuron_Type.NEUTRAL.ordinal()]);
                        break;

                    default:
                        // something went wrong
                        g.setColor(Color.BLACK);
                        System.out.println("ERROR in display color");
                        break;
                }
            } else {
                // truncate signals to values below 100
                double truncate = (p.getSignalLevel() > 100) ?
                                  (100) : (p.getSignalLevel());
                // scale to max value of 9 and min of 0
                double scaled = (9.0 / 100) * (truncate - 100) + 9.0;
                int sigColorI = (int) (scaled);
                g.setColor(signalColors[sigColorI]);
            }
        }
        g.fillRect(theInsets.left + x * cellWidth,
                theInsets.top + y * cellWidth, cellWidth, cellWidth);
    }
}
