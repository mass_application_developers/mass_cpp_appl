import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * @author Sarah Panther
 * @since 11-19-19
 */

public class BGOutDriver {
    public static void main(String args[]) throws FileNotFoundException {
        // validate the arguments
        if (args.length < 4) {
            System.err.println("usage: java TBDisplayDriver size iterations " +
                    "outputFrequency staticOutput(y/n)");
            System.exit(-1);
        }
        int size = Integer.parseInt(args[0]);

        // number of total xml state files
        int iterations = Integer.parseInt(args[1]);

        // number of display windows (or updates if static output is not
        // selected) = iterations / output frequency
        int outputFrequency = Integer.parseInt(args[2]);
        int windows = iterations / outputFrequency;

        // staticOutput :
        // y = different windows per output state
        // n = update the same window per output state
        boolean staticOut = args[3].toUpperCase().equals("Y");

        Place[] places;

        BGOut box = new BGOut(size);

        for (int i = 1; i < iterations; i += outputFrequency) {
            BGXmlParser xmlParse = new BGXmlParser(size, new File("../" +i +
                    ".xml"));
            places = xmlParse.getPlaces();

            if (staticOut) {
                box = new BGOut(size);  // create a graphics
            }

            else {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            box.setTitle("Brain Grid : Iteration " + i);
            box.writeToGraphics(places);
        }
        System.out.println("Done...");
    }
}



