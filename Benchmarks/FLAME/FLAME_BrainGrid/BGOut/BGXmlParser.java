import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;

/**
 * @author Sarah Panther
 * @since 11-19-19
 */

enum AgentType {PLACE, NEURON, ITERATIONTRACKER }

enum Neuron_Type { EXCITATORY, INHIBITORY, NEUTRAL }

public class BGXmlParser {

    private Place[] places;

    public Place[] getPlaces() {
        return places;
    }

    /**
     * Assumes Place agents come before Neuron agents in output XML files
     * @param sSize
     * @param file
     */
    BGXmlParser(int sSize, File file) {
        try {
            NodeList nodes = fileToNodes(file);
            places = new Place[sSize * sSize];

            // instantiate all the Places and get the Neuron's signals
            for (int i = 0; i < nodes.getLength(); i++) {
                Element e = (Element) nodes.item(i);

                NodeList name = e.getElementsByTagName("name");
                Element line = (Element) name.item(0);
                String agentType = getCharDataFromElement(line);

                AgentType agent = AgentType.valueOf(agentType.toUpperCase());

                switch (agent) {
                    case PLACE:
                        int p_id = Integer.parseInt(elementToString(e, "p_id"));

                        int occupyingNID = Integer.parseInt(elementToString(
                                e, "occupying_n_id"));

                        places[p_id] = new Place(p_id, occupyingNID);
                        break;

                    case NEURON:
                        int nID = Integer
                                .parseInt(elementToString(e, "n_id"));

                        int somaPlace = Integer.parseInt(elementToString(e,
                                "soma_place"));

                        double signal = Double.parseDouble(elementToString(e,
                                                   "sum_of_recvd_signals"));
                        Neuron_Type type =
                                Neuron_Type.values()[Integer.parseInt(
                                        elementToString(e,"neuron_type"))];
                        for (Place place : places) {
                            if (place.getOccupyingNID() == nID) {
                                place.setSignalLevel(signal);
                                place.setType(type);
                            }
                            if (somaPlace == place.getId()) {
                                place.setSoma();
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        } catch (ParserConfigurationException e) {
            System.out.println("Parser Configuration Exception in BGXmlParser");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("IO Exception in BGXmlParser");
            System.out.println(e.getMessage());
        } catch (SAXException e) {
            System.out.println("SAX Exception in BGXmlParser");
            System.out.println(e.getMessage());
        }
    }

    private NodeList fileToNodes(File f)
            throws ParserConfigurationException, IOException, SAXException {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new InputStreamReader(new FileInputStream(f)));

        Document doc = db.parse(is);
        return doc.getElementsByTagName("xagent");
    }

    private String getCharDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            return ((CharacterData) child).getData();
        } else {
            return "\nCannot get character data";
        }
    }

    private String elementToString(Element e, String tag) {
        NodeList n = e.getElementsByTagName(tag);
        return getCharDataFromElement((Element) n.item(0));
    }

    public static int toPlaceID(int x, int y, int sSize) {
        return x + (y * sSize);
    }

}
