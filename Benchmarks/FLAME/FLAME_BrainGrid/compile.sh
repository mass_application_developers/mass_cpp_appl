# FLAME BRAINGRID

# Specify # of iterations
read -p '# of iterations to run Brain Grid: ' ITERATIONS

# Generate initial state for specified grid size using python script
javac *.java
java initStateSONN $ITERATIONS y

# Generate parallel C code using xparser

# - navigate to xparser directory
cd /home/NETID/dslab/agent_simulation_benchmarking/xparser/

# and run xparser on our XML model template
./xparser /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_BrainGrid/Self_Organizing_Neural_Net.xml -p

# Set this MPI variable so the FLAME makefile will work
export MPICH_F77=gfortran

# Generate the executable using the FLAME makefile
cd /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_BrainGrid/
make clean
make LIBMBOARD_DIR=/home/NETID/dslab/agent_simulation_benchmarking/libmboard_install/
