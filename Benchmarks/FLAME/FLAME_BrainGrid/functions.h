//
// Modified by sarah on 11/4/19
//

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdlib.h>
#include <stdbool.h>

// Global constants ----------------------------------------------------------------------------------------------------
#define FALSE 0
#define TRUE 1
#define DOES_NOT_EXIST -1
#define NUM_DIRECTIONS 8
#define LOC_MESSAGE_SIZE 10

typedef enum Neuron_Type {
    EXCITATORY, INHIBITORY, NEUTRAL
} Neuron_Type;

typedef enum Part_Type {
    AXON, SYNAP, DENDRITE
} Part_Type;

typedef enum Axon_State {
    AXON_NOT_CREATED, CONTINUOUS_GROWTH, SYNAPSE_GEN_AND_GROWTH, STOP_AXON_AND_SYNAPSE
} Axon_State;

// Axon helper functions
void request_axon_synapse_growth(int iteration);

void create_axon();

void axon_continuous_growth();

void synapse_growth();

void synapse_grow_request(bool branch, int origin, int oldDir);

// Dendrite helper functions

void request_dendrite_growth(int iteration);

void create_dendrite(int dendriteNum);

void dendrite_growth(int dendriteNum);

void dendrite_grow_request(bool branch, int dendriteNum, int origin, int dir);

typedef enum Dendrite_State {
    DENDRITE_NOT_CREATED, GROW, STOP_DENDRITE
} Dendrite_State;

// grow() helper functions
bool process_axonal_approval(int p_id, int parent, int approval);
void approve_axon_growth(int requestIndex);
void remove_parent_end(int parent_p_id);
void change_axon_and_synap_state();

bool process_dendrite_approval(int p_id, int parent, int approval);
void approve_dendrite_growth(int requestIndex);
void change_dendrites_state();

void reset_grow_requests();
void compute_new_neighbors_all_g_ends();

// process_connections() helper function
void add_to_new_neighbors(int placeId, Part_Type partType);

// receive_new_connections() helper function
bool in_array(int array[], int size, int num);

// General helper functions

int new_growth_direction(int dir);

int new_branch_direction(int dir);

int fix_dir(int newDir);

int directionToNeighbor(int origin, int dir);

int torus(int rcIndex);

typedef enum Direction {
    NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST, WEST, NORTH_WEST
} Direction;
#endif //FUNCTIONS_H
