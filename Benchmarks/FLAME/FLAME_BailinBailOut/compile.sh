# FLAME BAIL IN BAIL OUT SQR

# Specify grid size
read -p '#of Banks, #of workers #of firms' BANK WORKER FIRM

# Generate initial state for specified states using python script
./initial_state.py $BANK $WORKER $FIRM

# Generate parallel C code using xparser

# - navigate to xparser directory
cd /home/NETID/dslab/agent_simulation_benchmarking/xparser/

# and run xparser on our XML model template
# OLD ./xparser ../mass_cpp_appl/Benchmarks/FLAME/Bailin-out/model.xml -p
./xparser /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_BailinBailOut/model.xml -p

# Set this MPI variable so the FLAME makefile will work
export MPICH_F77=gfortran

# Generate the executable using the FLAME makefile
# OLD cd ../mass_cpp_appl/Benchmarks/FLAME/Bail_In_Bail_Out_SQR
cd /home/NETID/dslab/agent_simulation_benchmarking/mass_cpp_appl/Benchmarks/FLAME/FLAME_BailinBailOut
make clean
# OLD make LIBMBOARD_DIR=/home/NETID/dslab/users/agent_simulation_benchmarking/libmboard_install/
make LIBMBOARD_DIR=/home/NETID/dslab/agent_simulation_benchmarking/libmboard_install