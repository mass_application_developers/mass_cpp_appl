#include "Bail_in_out_function.h"


#include <time.h>
#include <stdlib.h>// for random number.

// #define m 0.8
// #define o 0.2
// #define x 0.1
// #define II 0.8
// #define NUMBER_OF_BANK 5
// #define NUMBER_OF_WORKERS 20000
// #define NUMBER_OF_FIRMS 2000


/***************************
***Frims agent functions****
***************************/

// int production_cost()
// {
//     srand((unsigned)time(NULL));
//     double modifier = (float)rand()/RAND_MAX  + 0.5;
//     COST *= modifier;
//     return 0;
// }

// int calculate_profit()
// {
//     srand((unsigned)time(NULL));
//     double y = (float)rand()/RAND_MAX  + 0.5;
//     PROFIT = (COST*y)-COST; 
//     return 0;    
// }

// /**
//  * require profit > 0;
// */
// int calculate_dividends()
// {
//     PROFIT*=m;
//     return 0;
// }

// int calculate_liqudity()
// {
//     FLIQ += PROFIT;
//     return 0;
// }

/**
 * require liq<0
 * 
*/
int firm_increase_interest()
{
    if(TIMER_STATUS_H == READY)
    {
        run_timer();
    }
    if(STATE_SIGNAL<=0)
    {
        STATE_SIGNAL--;
        if(TIMER_STATUS_H == RUNNING && TIMER_STATUS_H != STOP)
        {
            run_timer();
        }
        return 1;
    }
    // printf("firm id is %d is runing \n",F_ID);
    double increase_amount = 0;
    if(FIRM_DEBT_LIST.size<=0){
        int find = NUMBER_OF_BANK;
        for(int i = 0;i<find;i++)
        {
            add_debt(&FIRM_DEBT_LIST,i,0,0);
        }
    }
    for (int i = 0; i < FIRM_DEBT_LIST.size; i++)
    {
        double amount = FIRM_DEBT_LIST.array[i].amount;
        double interest = FIRM_DEBT_LIST.array[i].interest;
        double increase = amount*(1+interest);
        increase_amount += (increase-amount);
        FIRM_DEBT_LIST.array[i].amount = increase;
        FIRM_DEBT_LIST.array[i].interest = interest;
        //printf("debt[%d] amount is %f \ndebt[%d] interest is %f\n", i , FIRM_DEBT_LIST.array[i].amount, i , FIRM_DEBT_LIST.array[i].interest);
    }
    COST = production_cost(COST);
    PROFIT = calculate_profit(COST);
    calculate_dividends(&PROFIT);
    return 0;
}

int pay_wages()
{
    FLIQ=calculate_liqudity(FLIQ,PROFIT);
    add_settlement_message(F_ID,COST,PROFIT);
    double wages = (NUMBER_OF_WORKERS/NUMBER_OF_FIRMS)*COST*NUMBER_OF_FIRMS*x*(1-o)/ NUMBER_OF_WORKERS*0.5;
    PROFIT -= wages;
    return 0;
}

int pay_captial()
{

    double amount = PROFIT*0.2;
    add_capitals_message(F_ID,amount);
    PROFIT*=0.8;
    return 0;
}

int pay_bank()
{
    double amount_paid = 0;
    for(int i = 0; i<FIRM_DEBT_LIST.size;i++){
        int loan_id = FIRM_DEBT_LIST.array[i].id;
        double amount = FIRM_DEBT_LIST.array[i].amount;
        double interest = FIRM_DEBT_LIST.array[i].interest;
        double toPay = amount/2;
        if(FLIQ > toPay&& loan_id!=-1 )
        {
            amount -= toPay;
            FLIQ -=toPay;
            amount_paid +=toPay;
            FIRM_DEBT_LIST.array[i].amount = amount;
            FIRM_DEBT_LIST.array[i].interest = interest;
            add_topay_message(loan_id, amount, interest);
        }
        else
        {
            FLIQ=-1;
        }
    }
    return 0;
}


int get_onwer_loan()
{
    if(FLIQ<0){
        double amount;
        START_RECIVECAPITAL_MESSAGE_LOOP
            amount = recivecapital_message->captial;
            if(F_ID == recivecapital_message->firm_id){
                if(amount > FLIQ*-1)
                {
                    double updates = FLIQ*-1;
                    FLIQ += updates;
                    add_capitals_message(F_ID,(amount-updates));
                }
            }
        FINISH_RECIVECAPITAL_MESSAGE_LOOP
        // printf("firm %d get owner's loan: %f \n", F_ID, amount);
    }
    return 0;
}

int receive_loan()
{
    //printf("receive loan\n");
    if(FLIQ<0){
        double lowest_interest_rate = 2134;
        int min_id = -1;
        double amount_needed = FLIQ*-1;
        START_LIST_MESSAGE_LOOP
            if (lowest_interest_rate > list_message->i && list_message->liq > amount_needed)
            {
                lowest_interest_rate = list_message->i;
                min_id = list_message->bank_id;
            }
        FINISH_LIST_MESSAGE_LOOP
        if(min_id == -1)
        {
            return 0;
        }
        for (int i = 0; i < FIRM_DEBT_LIST.size; i++)
            {
                if (FIRM_DEBT_LIST.array[i].id == min_id ||  i == (FIRM_DEBT_LIST.size -1))
                {
                    FIRM_DEBT_LIST.array[i].id = min_id;
                    FIRM_DEBT_LIST.array[i].amount += amount_needed;
                    FIRM_DEBT_LIST.array[i].interest += lowest_interest_rate;
                }
            }
        FLIQ += amount_needed;
        add_loan_com_message(min_id,F_ID,amount_needed,lowest_interest_rate);
        //printf("Bank %d, send to Firm %d, $ %f, about interest rate: %f \n",min_id,F_ID,amount_needed,lowest_interest_rate);
    }
    return 0;
}

int firm_bankrupt()
{
    //printf("firm %d has %F \n",F_ID,FLIQ);
    if(FLIQ<0)
    {
        STATE_SIGNAL--;
        printf("firm %d bankrupt!\n",F_ID);
        return 1;
    }
    return 0;

}



/***************************
*House hold agent functions*
***************************/
/**
 *revice wage or dv.
 *input the settlemnt message
 *@return 0
 */
int receive_wage()
{
    START_SETTLEMENT_MESSAGE_LOOP
    if (H_TYPE == 1)
    {
        double cost = settlement_message->cost;
        srand(time(NULL));
        // double r = (double)(rand() *10 + 5) / 10;
        WAGE = cost*NUMBER_OF_FIRMS*x*(1-o)/ NUMBER_OF_WORKERS*0.5;
    }
    else if (H_TYPE == 0) 
    {
        double amout = settlement_message->profit;
        WAGE = amout * o;
    }
    FINISH_SETTLEMENT_MESSAGE_LOOP
    CAPTIAL += WAGE;
    consume(&WAGE);
    return 0;
}

// /**
//  *calculate consume, add change the remain wages/dv
//  *nothing need to ouput.
//  *@return 0
//  */
// int consume()
// {
//     WAGE *= (1-m);
//     return 0;
// }

/**
 *calculate consume, add change the remain wages/dv
 *nothing need to ouput.
 *@return 0
 */
int deposit()
{
    double aoumount = WAGE * (1-0.7);
    add_topay_message(DP_ID,aoumount,-1);
    return 0;
}

int send_captial()
{
    //printf("send_captial!\n");
    if(H_TYPE == 0 && CAPTIAL >=0)
    {
        add_recivecapital_message(F_H_ID,CAPTIAL);
    }
    return 0;
}

int getCapital()
{
    double amount = 0;
    START_CAPITALS_MESSAGE_LOOP
    amount= capitals_message->amount;
    FINISH_CAPITALS_MESSAGE_LOOP
    CAPTIAL+=amount;
    return 0;
}


/***************************
****Banks agent functions***
***************************/

// /**
//  *update the Banks interest rate
//  *@return 0
//  */

// int update_liq()
// {
//     srand(time(NULL));
//     int r = rand()*40;
//     I = (0.8 + (double)(r / 100)) * II;
//     return 0; 
// }

/***
 * increase interest 
 * @return 0
 */
 
int bank_increase_interest()
{
    if(STATE_SIGNAL<=0)
    {
        STATE_SIGNAL--;
        if(TIMER_STATUS_H == RUNNING && TIMER_STATUS_H != STOP)
        {
            run_timer();
        }
        return 1;
    }
    update_liq(&I);
    double increase_amount = 0;
    if(BANK_DEBT_LIST.size<=0){
        int find = NUMBER_OF_BANK;
        for(int i = 0;i<find;i++)
        {
            add_debt(&BANK_DEBT_LIST,i,0,0);
        }
    }
    for (int i = 0; i < BANK_DEBT_LIST.size; i++)
    {
        double amount = BANK_DEBT_LIST.array[i].amount;
        double interest = BANK_DEBT_LIST.array[i].interest;
        double increase = amount*(1+interest);
        increase_amount += (increase-amount);
        BANK_DEBT_LIST.array[i].amount = increase;
        BANK_DEBT_LIST.array[i].interest = interest;
    }
    BANK_CR += increase_amount;
    return 0;
}

/**
 *loan from other bank if the liq <= 0.
 *@input meassage: LIST message
 *@output message: loan_com
 *@return 0
 */
int bank_loan()
 
{
    if(LIQ<=0){
        double amount_needed = LIQ * -1;
        {
            int loan_id = -1;
            double lowest_interest_rate = 2134;
            START_LIST_MESSAGE_LOOP
            if (lowest_interest_rate > list_message->i && list_message->liq > amount_needed)
            {
                lowest_interest_rate = list_message->i;
                loan_id = list_message->bank_id;
            }
            FINISH_LIST_MESSAGE_LOOP
            if (loan_id == -1)
            {
                return 0; // means can not find a bank.
            }

            for (int i = 0; i < BANK_DEBT_LIST.size; i++)
            {
                if (BANK_DEBT_LIST.array[i].id == loan_id ||  i == (BANK_DEBT_LIST.size -1) )
                {
                    BANK_DEBT_LIST.array[i].id = loan_id;
                    BANK_DEBT_LIST.array[i].amount += amount_needed;
                    BANK_DEBT_LIST.array[i].interest += lowest_interest_rate;
                }
            }
            // //LOANER is a dynamic list here. 
            // LOANER.array[LOANERNUMBER_OF_BANK].id = id; // add loaner id into list.
            // LOANER.array[LOANERNUMBER_OF_BANK].loan = amount_needed; 
            // LOANER.array[LOANERNUMBER_OF_BANK].ir = lowest_interest_rate;
            LIQ += amount_needed;
            BANK_CR += amount_needed;
            add_loan_com_message(BANK_ID,-1,amount_needed,lowest_interest_rate);
        }
    }
    return 0;
}

/**
 *Bank pay to the loaner.
 *@output measseage：TOPAY Message. 
 *@return 0
 */

int bank_pay()
{
    double amount_paid = 0;
    for(int i = 0; i<BANK_DEBT_LIST.size;i++){
        int loan_id = BANK_DEBT_LIST.array[i].id;
        double amount = BANK_DEBT_LIST.array[i].amount;
        double interest = BANK_DEBT_LIST.array[i].interest;
        double toPay = amount/2;
        if(LIQ > toPay&& loan_id!=-1 )
        {
            amount -= toPay;
            LIQ -=toPay;
            amount_paid +=toPay;
            BANK_DEBT_LIST.array[i].amount = amount;
            BANK_DEBT_LIST.array[i].interest = interest;
            add_topay_message(loan_id, amount, interest);
        }
        else
        {
            LIQ=-1;
        }
    }
    BANK_CR -= amount_paid;
        if(LIQ>0)
    {
        add_list_message(BANK_ID,I,LIQ);
    }
    return 0;
}

/**
 *recive any payment to bank 
 *@input measseage：TOPAY Message. 
 *@return 0
 */
int get_pay()
{
    START_TOPAY_MESSAGE_LOOP
        if(topay_message->bank_id == BANK_ID){
            LIQ+= topay_message->amount;
        }
    FINISH_TOPAY_MESSAGE_LOOP

    // doesn't recive any pay.
    return 0;
}

/**
*@input loan_com message
*@output list message
*bank given loan
*/
int comfir_pay()
{  
    double amount = 0;
    START_LOAN_COM_MESSAGE_LOOP
    amount += loan_com_message->amount;
    FINISH_LOAN_COM_MESSAGE_LOOP
    LIQ += amount;
    return 0;
}



int bank_bankrupt()
{
    //printf("BANK %d has LIQ: %f Credit: %f|\n",BANK_ID,LIQ,BANK_CR);
    if((LIQ+BANK_CR)<0)
    {
        STATE_SIGNAL--;
        printf("BANK %d Bankrupt!\n",BANK_ID);
    }
    return 0;
}