#!/usr/bin/env python
#
# Description:
#    Quick hack to create start states (0.xml) needed by the cellpotts abm
#	input: number of banks, number of wokers, number of firms

import re
import os
import sys
import random

outfile = "0.xml"

# expect 3 input arguments
if len(sys.argv) <= 3:
    print >> sys.stderr, "\nUsage: %s <banks><wokers><firms>" % sys.argv[0]
    print >> sys.stderr, """ pleas add 2 integer numbers as arguments.
    More info:
    input: X number of banks, Y number of wokers, Z number of firms
    """
    sys.exit(1)

number_of_banks,number_of_wokers, number_of_firms = map(int,sys.argv[1:])
print "input: ", number_of_banks,number_of_wokers, number_of_firms

# Open file and write preamble
print "Writing to file %s ... " % outfile

do_stiff_edge=False


f = open(outfile,"w")
f.write("<states>\n<itno>0</itno>\n")

id = 0
cost = 35000
interest = 0.8
liq = 20000
print random.randint(1,3)

f.write("""
    <environment>
        <NUMBER_OF_BANK>%d</NUMBER_OF_BANK>
        <NUMBER_OF_WORKERS>%d</NUMBER_OF_WORKERS>
        <NUMBER_OF_FIRMS>%d</NUMBER_OF_FIRMS>
    </environment>""" % (number_of_banks, number_of_wokers, number_of_firms))

for x in range(number_of_firms):
    ini_cost = cost*(random.random()+0.5)
    f.write("""
        <xagent>
            <name>firms</name>
            <f_id>%d</f_id>
            <cost>%f</cost>
            <profit>%f</profit>
            <fliq>%f</fliq>
        </xagent>
    """%(x,ini_cost,ini_cost*0.1+liq,liq))

for x in range(number_of_wokers/number_of_firms):
    for y in range(number_of_firms):
        bid = random.randint(0,number_of_banks) 
        f.write("""
        <xagent>
            <name>households</name>
            <f_h_id>%d</f_h_id>
            <dp_id>%d</dp_id>
            <h_type>1</h_type>
            <wage>100.0<wage>
            <captial>0</captial>
        </xagent>
        """%(x,bid))

for x in range(number_of_firms):
    bid = random.randint(0,number_of_banks) 
    f.write("""
        <xagent>
            <name>households</name>
            <f_h_id>%d</f_h_id>
            <dp_id>%d</dp_id>
            <h_type>0</h_type>
            <wage>100.0<wage>
            <captial>0.0</captial>  
        </xagent>
    """%(x,bid))

for x in range(number_of_banks):
    f.write("""
        <xagent>
            <bank_cr>0</bank_cr> 
            <name>banks</name>
            <i>%f</i>
            <bank_id>%d</bank_id>
            <liq>%d</liq>
        </xagent>
    """%(interest*(random.random()*0.4+0.8),liq*(random.random()+1),x))

# End XML file and close
f.write("\n</states>\n")
f.close();