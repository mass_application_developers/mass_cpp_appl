# FLAME BAIL IN BAIL OUT SQR

rm *-[1-9].xml
# ask for number of computing nodes
read -p '# of computing nodes (including this one) = ' NODES

# copy the correct number of hosts to the host file (CSSmpdboot always runs all hosts in mpd.hosts)
cat /home/NETID/dslab/agent_simulation_benchmarking/host_master_list | head -$(($NODES-1)) > ~/mpd.hosts

# boot the mpi cluster
CSSmpdboot -n $NODES -v

# Specify # of iterations
read -p '# of iterations to run Bail in and out (must be the same as specified in the compile script): ' ITERATIONS



# run the parallel model using round-robin partitioning
mpirun -np $NODES ./main $ITERATIONS 0.xml -p 
# mpi exit out of computing nodes used
mpdallexit