#include "Graph.h"
#include <iomanip>
Graph::Graph(int graphSize) {
  // Error Handling
  if (graphSize < 5) {
    std::cerr << "Want GRAPH_SIZE > 10\n";
    std::exit(-1);
  }

  // Initialize adjacency matrix
  size = graphSize;
  buildGraph();

  // Initialize Vertex Weights
  links = new Link[size];

  // Initializes Dijkstra Handle
  T = new Table*[size];
  for (int j = 0; j < size; j++) {
    T[j] = new Table[size];
  }

  // Run Dijkstra
  findShortestPath();
}

Graph::~Graph() {
  // Delete Adjacency Matrix
  for (int i = 0; i < size; i++) {
    delete adjGraph[i];
    adjGraph[i] = nullptr;
  }
  delete adjGraph;
  adjGraph = nullptr;

  // Delete Table
  for (int m = 0; m < size; m++) {
    delete[] T[m];
    T[m] = nullptr;
  }
  delete[] T;
  T = nullptr;

  // Delete Links
  delete[] links;
}

void Graph::buildGraph() {
  // Initializing rows
  adjGraph = new int*[size];
  // Initialize columns of rows
  for (int i = 0; i < size; i++) {
    adjGraph[i] = new int[size];
  }

  // Randomly Generate 
  srand(time(nullptr));
  for (int m = 0; m < size; m++) {
    int spots = rand()%(size);
    for (int n = 0; n < spots; n++) {
      int spot = rand()%size;
      // Ignoring Symmetrically Handled Elements
      if (adjGraph[spot][m] == 1) continue;
      adjGraph[m][spot] = 1;
    }
  }
}

// Dijkstra's Algorithm
void Graph::findShortestPath() {
  // Iterate through all Vertcies (as Source)
  for (int src = 0; src < size; src++) {
    // Update Source
    T[src][src].visited = true;
    T[src][src].dist = 0;

    // Check Source's Neighbors
    bool loneVertexFlag = true;
    for (int srcNeighbor = 0; srcNeighbor < size; srcNeighbor++) {
      if (src != srcNeighbor && adjGraph[src][srcNeighbor] == 1) {
        loneVertexFlag = false;
        T[src][srcNeighbor].dist = links[srcNeighbor].weight;
        T[src][srcNeighbor].path = src;
      }
    }
    // Skip Iteration if Link is not directed to any other link
    if (loneVertexFlag) continue;

    // Iterate size times
    for (int iteration = 0; iteration < size; iteration++) {
      int v = -1; // v will hold the index for the next best path
      int mp = -1; // mp is the metric to determine new v

      // Determines v via mp
      for (int checkV = 0; checkV < size; checkV++) {
        if (T[src][checkV].dist != -1 
           && !T[src][checkV].visited
           && (mp == -1 || mp > T[src][checkV].dist)) {
          mp = T[src][checkV].dist;
          v = checkV;
        }
      }

      // if v was updated
      if (v != -1) {
        T[src][v].visited = true;
        // Updates dist for all adjacent to v
        for (int adjV = 0; adjV < size; adjV++) {
          if ( v != adjV
             && adjGraph[v][adjV] == 1
             && (T[src][adjV].dist > T[src][v].dist + links[adjV].weight
             || T[src][adjV].dist == -1) ) {
            T[src][adjV].dist = T[src][v].dist + links[adjV].weight;
            T[src][adjV].path = v;
          }
        }
      }
    }
  }
}

// Returns a vector dest, . . ., src
std::vector<int> Graph::findPath(int src, int dest) {
  std::vector<int> retVal;
  if (T[src][dest].path != -1) {
    int current = dest;
    while (current != src) {
      retVal.push_back(current);
      current = T[src][current].path;
    }
    retVal.push_back(src);
  }
  return retVal;
}

void Graph::printAdjGraph() {
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j ++) {
      std::cout << adjGraph[i][j] << " ";
    }
    std::cout << "\n";
  }
}

void Graph::printGraph() {
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      // Obtain path
      std::vector<int> path = findPath(i,j);

      // Skip non-existent paths
      if (path.empty()) continue;

      // Print Path
      std::cout << i << "->" << j << std::setw(5);
      for (int k = path.size() - 1; k >= 0; k--) {
        std::cout << path[k] << " ";
      }
      std::cout << "\n";
    }
  }
}

//----------------------Agent-Methods----------------------------------------//
void Graph::initAgents(int maxAgents) {
  srand(time(nullptr));
  int i = 0;
  // Initialize Agents
  while (i < maxAgents) {
    int src = rand()%size;
    int dest = rand()%size;

    // Trivial Path
    if (src == dest) {
      std::cout << "Bad Path\n";
      continue;
    }

    // Initialize "Agent's" path
    std::vector<int> tempPath = findPath(src, dest);

    // Safe Link
    if ( (links[src].agentIds).size() != Link::MAX_CAPACITY_AGENTS && !tempPath.empty() ) {
      // Print Agent "Identifier" for display
      std::cout << "v" << i << " ";

      // Print Path
      for (int k = tempPath.size() - 1; k >= 0; k--) {
        std::cout << tempPath[k] << " ";
      }
      std::cout << "\n";

      // Inserting Path and Updating Link
      agents.push_back(tempPath);
      (links[src].agentIds).push(i++);
    }
  }
}

// Run the simulation
void Graph::runAgents(int TIME_MAX, int maxAgents) {
  // Error Handling
  if (TIME_MAX < 3 || maxAgents < 10) {
    std::cerr << "Want TIME_MAX > 3 NUMBER_OF_AGENTS > 10 \n";
    std::exit(-1);
  }

  // Initializies Agents before simulation
  std::cout << "Initializing Agents...\n";
  initAgents(maxAgents);
  int numAgents = maxAgents;
  std::cout << "\n";

  // Symbol Explanation
  std::cout << "Symbol Legend\n";
  std::cout << "* = Agent has moved\n";
  std::cout << "- = Agent has finished\n";
  std::cout << "$ = Agent has been stopped\n";
  std::cout << "X = Agent has been killed\n\n";

  // Printing Output Header
  std::cout << "Running Simulation...\n";
  std::cout << "t" << std::setw(5); 
  for (int printHeader = 0; printHeader < numAgents; printHeader++) {
    std::cout << "v" << printHeader << std::setw(5);
  }
  std::cout << "\n";

  // Fixes Bug for too fast/skipping Agents
  std::vector<int> unSafe;

  // Runs Simulation
  for (int time = 2; time < TIME_MAX && numAgents > 0; time+=2) {
    // Print Output Body
    std::cout << time << std::setw(5);
    for (int printOut = 0; printOut < maxAgents; printOut++) {
      std::cout << agents[printOut].back() << std::setw(6);
    }
    std::cout << "\n";

    // Go through each link
    for (int i = 0; i < size; i++) {
      // Accounts for too fast/skipping Agents
      bool safe = true;

      if (!unSafe.empty()) {
        if (unSafe[0] >= i) {
          for (int xSafe = 0; xSafe < unSafe.size(); i++) {
            // Checks if Safe/NotSafe
            if (unSafe[xSafe] > i) break;
            else if (unSafe[xSafe] == i) {
              safe = false;
              break;
            }
          }
        }
      }

      // Skipping unSafe Link
      if (safe == false) continue;

      // For convince of iterating through Link's agents
      int curSize = (links[i].agentIds).size();

      // Go through each link's agentId
      for (int j = 0; j < curSize; j++) {
        std::vector<int> tempAgent = agents[(links[i].agentIds).front()];
        std::cout << (links[i].agentIds).front();
        // Finished Traversal
        if (tempAgent.size() == 1) {
          (links[i].agentIds).pop();
          std::cout << "-";
          numAgents--;
          continue;
        }

        // Checks the next link (Move/StopsLink)
        int nextLink = tempAgent.back();
        if ( (links[nextLink].agentIds).size() != Link::MAX_CAPACITY_AGENTS ) {
          // Preparing unSafe Checks
          if ( (links[nextLink].agentIds).empty() ) unSafe.push_back(nextLink);

          std::cout << "*";

          // Move Agent's id to the next Link
          (links[nextLink].agentIds).push((links[i].agentIds).front());
          // Update Agent's path
          agents[(links[i].agentIds).front()].pop_back();
          // Remove that Agent's id from the current queue
          (links[i].agentIds).pop();
        }
        else {
          std::cout << "$";

          // Killing an agent
          if (links[nextLink].stallTime == 3) {
            std::cout << links[nextLink].agentIds.front() << "X";

            // Killing bottle-neck Agent
            (links[nextLink].agentIds).pop();
            numAgents--;
            links[nextLink].stallTime = 0;
          }
          links[nextLink].stallTime++;
          break;
        }
      }
    }
    // Preparation for next iteration
    unSafe.clear();
    std::cout << "\n";
  }
}
