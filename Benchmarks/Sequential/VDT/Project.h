#ifndef PROJECT_H
#define PROJECT_H
#include "ProjectManager.h"
#include "Graph.h"
#include <iostream>
#include <string>

class Project {
  public:
    Project(std::string pName, std::string fileName);
    // Traverse over the graph nodes (via breadth)
    void simulate();
    void displayTeamsOnProcesses() const;

  private:
    std::string projectName;
    ProjectManager* manager;
    Graph* ProcessGraph;
};
#endif