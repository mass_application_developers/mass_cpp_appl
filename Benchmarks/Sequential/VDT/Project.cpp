#include "Project.h"
#include <iomanip>   // setw

Project::Project(std::string pName, std::string fileName) {
  // Initialize Project Name
  projectName = pName;

  // Initialize ProjectManager
  manager = new ProjectManager(1000000);

  // Initialize Graph
  ProcessGraph = new Graph(fileName);

  // Initialize Team
  // void ProjectManager::addTeam(int id, int totalPeople, int avgWage, int avgIntel, Graph::Process* current)
  (*manager).addTeam(0,50,20,30,ProcessGraph->start);

  // Ensure that start is not evaluated
  ProcessGraph->start->isCompleted = true;
}

void Project::simulate() {
  int count = 0;

  // Displaying Header
  for (int i = 0; i < ProcessGraph->processesSize; i++) {
    std::cout << i << std::setw(5);
  }

  // Run till completed or iterated 100000 times
  while(true) {
    // Check for completion
    if(ProcessGraph->end->completedParents == ProcessGraph->end->prev.size()
      || count++ >= 10  ) {
      break;
    }

    displayTeamsOnProcesses();
    (*manager).runTeams();
  }
  displayTeamsOnProcesses();
}

// Assumes Header already printed
void Project::displayTeamsOnProcesses() const {
  for (int i = 0; i < ProcessGraph->processesSize; i++) {
    if(ProcessGraph->processes[i]->currentTeam != -1) {
      std::cout << ProcessGraph->processes[i]->currentTeam << std::setw(5);
    }
    else {
      std::cout << "x" << std::setw(5);
    }
  }
  std::cout << "\n";
}
