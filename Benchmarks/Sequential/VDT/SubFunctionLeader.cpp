#include "SubFunctionLeader.h"

Person::Message* SubFunctionLeader::runTeam() {
  // Error Checking
  if (currentProcess == nullptr) {
    return nullptr;
  }

  // Check if currentProcess has not been operated upon
  if (currentProcess->isCompleted || currentProcess->currentTeam != -1) {
    return nullptr;
  }

  // Check Parental Process Requirements
  if (currentProcess->completedParents != currentProcess->prev.size()) {
    return nullptr;
  }

  // Determine Unsatisfied Requirements
  int reqRsc, reqWork, avgWage, avgIntel;
  std::tie(reqRsc, reqWork, avgWage, avgIntel) = processReqCond();

  // Create and send Message if needed
  if (reqRsc != 0 || reqWork != 0) {
    Person::Message* retMsg = new Person::Message(teamId, reqWork, reqRsc, avgWage, avgIntel);
    return retMsg;
  }

  // Perform Process
  if (currentProcess->minTime != 0) {
    currentProcess->minTime--;
  }
  else {
    // Indicates to other teams to skip this completed process
    currentProcess->isCompleted = true;
  } // End of processing and post-processing
  return nullptr;
}

// Assuming that ever team has more than 1 member
std::tuple<int, int, int, int> SubFunctionLeader::processReqCond() {
  int reqRsc = 0;
  int reqWork = 0;

  // Check Resources Requirements
  if (currentProcess->resourcesReq > totalTeamResources) {
    reqRsc = currentProcess->resourcesReq;
  }
  
  // Check Workers and Knowledge Requirements
  if (currentProcess->numWorkerReq > team->members.size()) {
    reqWork = currentProcess->numWorkerReq - team->members.size();

    // Ensuring knowledgeReq is also satisfied
    if (currentProcess->knowledgeReq > team->totalIntel + reqWork*team->members[0]->knowledgeNum) {
      reqWork = (currentProcess->knowledgeReq - team->totalIntel) / team->members[0]->knowledgeNum + 1;
    }
  }

  // Check Knowledge
  if (currentProcess->knowledgeReq > team->totalIntel && reqWork == 0) {
    reqWork = (currentProcess->knowledgeReq - team->totalIntel) / team->members[0]->knowledgeNum + 1;
  }
  
  return std::make_tuple(reqRsc, reqWork, team->members[0]->wage, team->members[0]->knowledgeNum);
}

//-------------------------------Team----------------------------------------//
SubFunctionLeader::Team::Team(int totalPeople, int avgIntel, int avgWage) {
  // Init members
  for (int i = 1; i < totalPeople; i++) {
    // SubFunctionMember(int wageNum, int avgIntel)
    SubFunctionMember* temp = new SubFunctionMember(avgWage, avgIntel);
    members.push_back(temp);
    // Updating totalIntel, and totalWages
    totalIntel += temp->knowledgeNum;
    totalWages += temp->wage;
  }
}

SubFunctionLeader::Team::~Team() {
  for (int i = 0; i < members.size(); i++) {
    delete members[i];
    members[i] = nullptr;
  }
}