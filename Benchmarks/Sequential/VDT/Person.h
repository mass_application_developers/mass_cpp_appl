#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <string>

class Person {
  public:
    Person(int wageNum) {
      wage = wageNum;
    }
    int wage;

    // Used for process exceptions
    struct Message {
      int teamNum;
      int workerNumRequest;
      int resourcesNumRequest;
      int avgWage;
      int avgIntel;

      Message(int id, int worker, int rsc, int wage, int intel) {
        teamNum = id;
        workerNumRequest = worker;
        resourcesNumRequest = rsc;
        avgWage = wage;
        avgIntel = intel;
      }
    };
};

#endif