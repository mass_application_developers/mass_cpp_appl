#ifndef BRAINGRID_H
#define BRAINGRID_H
#include "Neuron.h"
#include <stdlib.h>
#include <iostream>

class BrainGrid {
  public:
    // Formated return values for the SafeGrowth method
    enum GrowthFlag {STOP = -1, SAFE = 0, CONNECT = 1};

    // Constructor & Desconstructor
    BrainGrid(const int Size);
    ~BrainGrid();

    void Simulate(const int iter);

    // For Debugging
    void DisplayStage() const;
    void DisplayType() const;

   private:
    int size;
    Neuron** Grid;

    // Used when navigating, that corrects "cell" value when exceeding any bounds
    int TorusSafe(int cell);

    // Growth pattern to initiate during the first iteration
    /*
      ACTIVE to ACTIVE <GROW>
      ACTIVE to INACTIVE <STOP>
      ACTIVE to CONNECTED <STOP>
      ACTIVE to STOPPED <???>
      ACTIVE to INDEF <SAFE>
      ACTIVE to VISIT1/VISIT2/ENACTED <CHECK>
    */
    void GrowActive(int i, int j);

    // Growth pattern to perpetuate after first iteration
    /*
      NEUTRAL is VISIT1/VISIT2

      NEUTRAL to INACTIVE <STOP>
      NEUTRAL to CONNECTED <STOP>
      NEUTRAL to STOPPED <???>
      NEUTRAL to INDEF <SAFE>
      NETURAL to VISIT1/VISIT2/ENACTED <CHECK>
    */
    void GrowNeutral(int i, int j);

    // Growth pattern helper that finalizes the synapse construction
    BrainGrid::GrowthFlag SafeGrowth(Neuron& activeSrc, Neuron& dest);
};

#endif
