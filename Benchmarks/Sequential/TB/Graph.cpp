#include "Graph.h"

//-----------------Overarching Methods--------------------------//
void Graph::simulate(int iterations) {
  // Initialize Simulation Environment
  initEnvironment(1);

  for(int iter=0; iter<iterations; iter++) {
    // Assists in program's clarity
    curTurn = iter%2;
    nextTurn = (iter+1)%2;

    // Display Bacteria
    std::cout << "State: " << iter << " bacterial state\n";
    display(Graph::BACTERIA);

    // Display Alert
    std::cout << "State: " << iter << " alert state\n";
    display(Graph::ALERT);

    // Display Agents
    std::cout << "State: " << iter << " agents\n";
    display(Graph::AGENTS);

    // Iterate through environment
    for(int row=0; row<graphSize; row++) {
      for(int col=0; col<graphSize; col++) {
        // Upate Environment (Chemicals/Bacteria)
        updatePlace(row,col);

        // Update/Move Agents
        updateAgent(row,col);
      }
    } // Finished an Iteration
  } // Finished Simulation

  curTurn = iterations%2;
  nextTurn = (iterations+1)%2;

  // Display Bacteria
  std::cout << "Final: bacterial state\n";
  display(Graph::BACTERIA);

  // Display Alert
  std::cout << "Final: alert state\n";
  display(Graph::ALERT);

  // Display Agents
  std::cout << "Final: agents\n";
  display(Graph::AGENTS);
}

// Assume graphSize >> numRowEntryPoint
// This method also takes on the responsibility of reseting Graph
void Graph::initEnvironment(int EPperRow) {
  // Pre-Initailized for Initailziation of Bacteria, as well as Agents
  curTurn = 1;
  nextTurn = 0;

  // Reset Current and Next Space
  for(int i=0; i < graphSize; i++) {
    for(int j=0; j < graphSize; j++) { 
      T[i][j][0].defaultPlace();
      T[i][j][1].defaultPlace();
    }
  }

  // Initialize EntryPoints (Evenly Spaced) and Agents
  int unitDist = graphSize/(EPperRow + 1);
  for(int i=unitDist; i<graphSize; i+= unitDist) {
    for(int j=unitDist; j<graphSize; j+= unitDist) {
      T[i][j][0].entryPointFlag = true;
      T[i][j][1].entryPointFlag = true;

      // Initialize an Agent
      // Check for pre-existing Agent on Place
      if(T[i][j][0].agentSize == 0 )
        { makeAgent(i,j,false); } // Finished creating Agent
    }
  }

  // Initialize Bacteria
  Disperse(graphSize/2-1,graphSize/2-1,2,Graph::BACTERIA);

  // Returning curTurn and nextTurn to logical state
  curTurn = 0;
  nextTurn = 1;
}

// Note: To display Agents on an environmental element agents will
// be weighted and sum'd together uniquely
void Graph::display(Graph::Mode mode) {
  for(int row=0; row<graphSize; row++) {
    for(int col=0; col<graphSize; col++) {
      switch(mode) {
        // Bacteria_Flag
        case Graph::BACTERIA:
          std::cout << T[row][col][curTurn].bacteriaFlag << " ";
          break;
        // Alert_Flag
        case Graph::ALERT:
          std::cout << T[row][col][curTurn].alertFlag << " ";
          break;
        // Agent_Flag
        case Graph::AGENTS:
          if(T[row][col][curTurn].agentSize != 0) {
            // Check for Infection
            if(T[row][col][curTurn].agentIds[1] != -1)
              { std::cout << "I"; }
       //     int agentSum = (T[row][col][curTurn].agentIds[1] == -1) ? 0 : 3;
            // Check for Non-Infected
            if(T[row][col][curTurn].agentIds[0] != -1)
              { std::cout << "A"; }
       //     agentSum += (T[row][col][curTurn].agentIds[0] == -1) ? 0 : 1;
       //     std::cout << agentSum << " ";
            std::cout << " ";
          }
          else
            { std::cout << "0 "; }
          break;
      }
    } // Finished Printing a Row
    std::cout << "\n";
  } // Finsihed Printing
}

//-----------------Update Helpers-------------------------------//
/*
 Note: When updating the Place Table remember that there is
 a next space. Therefore before updating the neighbors in
 a T[i][j]'s responsible zone, update self into the next
 space before hand. This way every time we move from one to
 the other space a solid copy is made.

 Note: Collision will only be allowed if One Agent is
 INFECTED and the Other Agent is not-INFECTED. So one can easily
 indicate that we have Agents via:
   Place.agentSize != 0
*/

void Graph::updatePlace(int row, int col) {
  //------Update Bacteria-----------------------------------//
  if(T[row][col][curTurn].bacteriaFlag) {
    Disperse(row,col,bacteriaSpeed,Graph::BACTERIA);

    // Update Entry Point
    // Collision at Entry Point is handled by makeAgent()
    if(T[row][col][curTurn].entryPointFlag)
      { makeAgent(row,col,true); } 
  }

  //------Update Alert--------------------------------------//
  if(T[row][col][curTurn].alertFlag != 0)
    { Decay(row,col,Graph::ALERT); }
    
}

/*
  Note: Agent creation depends on the freeIds vector. If
  freeIds vector is empty create a new Agent and push onto
  agents vector.

  Note: Agent deletion update the deathFlag to true, and
  release Agent's id to freeIds.
*/
void Graph::updateAgent(int row, int col) {
  // Check for No-Agents
  if(T[row][col][curTurn].agentSize == 0) {return;}

  // Reference for ease-of-access
  int id1 = T[row][col][curTurn].agentIds[0];
  int id2 = T[row][col][curTurn].agentIds[1];

  // Non-Infected Agents
  if(id1 != -1) {
    // Time Span Overage
    if(globalAgents[id1].agentLifeSpan-- == 0) { killAgent(id1); }

    // Active Agent Actions
    if(globalAgents[id1].activeFlag) {
      Disperse(row,col,alertSpeed,Graph::ALERT);
      ClearBacteria(row,col);
    }
    // Non-Active Agent Actions
    else {
      // Check for Infection (checks for collisions)
      if(T[row][col][curTurn].bacteriaFlag &&
        !globalAgents[id1].activeFlag &&
        T[row][col][ nextTurn ].agentIds[1] == -1) {
//  std::cout << "Checking for an Infection\n";
        // 50-50 Chance of Agent Infection/Alert
        srand(time(NULL));
        int chance = rand()%5;
        // Before Infection Ensure Clear in Current and Next Space
        if(chance < 2 && id2 == -1 && T[row][col][nextTurn].agentIds[1] == -1) {
          globalAgents[id1].infectedFlag = true;
          T[row][col][nextTurn].agentIds[1] = id1;
          T[row][col][curTurn].agentIds[0] = -1;
  std::cout << "Infected: " << id1 << "\n to:" << row << " " << col << "\n";
          return;
        }
        else { 
          globalAgents[id1].activeFlag = true;  
  std::cout << "Alerted: " << id1 << "\n to: " << row << " " << col << "\n";
          return;
        }
      }
      // Alert the Agent
      else if(T[row][col][curTurn].alertFlag != 0) {
        globalAgents[id1].activeFlag = true;
  std::cout << "Alerted: " << id1 << "\n to:" << row << " " << col << "\n";
        return;
      }
    }
    // Move
    moveAgent(id1);
  }

  // Infected Agent
  if(id2 != -1) {
  std::cout << "Infected working\n";
    // Time Span Overage or Killed (via ALERT)
    if(globalAgents[id2].agentLifeSpan == 0 || T[row][col][curTurn].alertFlag != 0) {
      Disperse(row,col,bacterialDeathSpeed,Graph::BACTERIA);
      killAgent(id2);
    }
    // Disperse and Move
    else {
      Disperse(row,col,bacteriaSpeed,Graph::BACTERIA);
      moveAgent(id2);
    }
  }
}

//-----------------Radial Helpers-------------------------------//
// Assumed that it is called on the correct location for mode
void Graph::Disperse(int row, int col, int speed, Graph::Mode mode) {
  //std::cout << "singles\n";
  updateSingle(row, col, mode);
  //std::cout << "neighbors\n";
  updateNeighbors(row, col, speed, mode);
}

// Expecting mode == Graph::ALERT || mode == Graph::KILL
void Graph::Decay(int row, int col, Graph::Mode mode) {
  updateSingle(row, col, mode);
}

void Graph::ClearBacteria(int row, int col) {
  updateSingle(row, col, Graph::CLEAR);
  updateNeighbors(row, col, clearSpeed, Graph::CLEAR);
}

void Graph::updateSingle(int row, int col, Graph::Mode mode) {
  switch(mode) {
    // Bacteria Flag
    case Graph::BACTERIA:
      T[row][col][ nextTurn ].bacteriaFlag = true;
      totalBacteria++;
      break;
    // Clear Flag
    case Graph::CLEAR:
      T[row][col][ nextTurn ].bacteriaFlag = false;
      totalBacteria--;
      break;
    // Alert Flag
    case Graph::ALERT:
      if(T[row][col][curTurn].alertFlag == 0)
        { T[row][col][ nextTurn ].alertFlag = alertTimeSpan; }
      else
        { T[row][col][ nextTurn ].alertFlag =
          T[row][col][curTurn].alertFlag - 1; }
      if(T[row][col][ nextTurn ].alertFlag==0) {return;}
      break;
  }
}

// Segmentation Fault here
void Graph::updateNeighbors(int row, int col, int speed, Graph::Mode mode) {
  // Update affected neighborhoods
  for(int trow = -speed/2; trow<=speed/2; trow++) {
    int i = Torus(row+trow);
    for(int tcol = -speed/2; tcol<=speed/2; tcol++) {
      if(trow==0 && tcol==0) {continue;}
      int j = Torus(col+tcol);
      // Used to monitor the progress of the update
   //   std::cout << i << " " << j << "\n";

      // Update "unvisited" places
      switch(mode) {
        // Bacteria Flag
        case Graph::BACTERIA:
          if(T[i][j][curTurn].bacteriaFlag) {break;} // Indicates visited
          T[i][j][ nextTurn].bacteriaFlag = true;
          totalBacteria++;
          break;
        // Clear Flag
        case Graph::CLEAR:
          if( !T[i][j][curTurn].bacteriaFlag ) {break;} // Indicates unvisited
          T[i][j][ nextTurn].bacteriaFlag = false;
          totalBacteria--;
          break;
        // Alert Flag
        case Graph::ALERT:
          if(T[i][j][curTurn].alertFlag != 0) {break;} // Indicates visited
          T[i][j][ nextTurn ].alertFlag = T[row][col][ nextTurn ].alertFlag;
          break;
      } // Updated Single Place
    }
  } // Completed Growth/Diffusion
}

//-----------------Agent Helpers--------------------------------//
/*
   Note: Handling Collisions
   Determined by next space values(current values are assumed valid)
   if an agent is created it gets priority over colliding agent(s)
   if there is a conventional collision and the type pair is
     INACTIVE/ACTIVE to INFECTED, accept the collision
   if there is a conventional collision and the type pairs are
     INACTIVE/ACTIVE to INACTIVE/ACTIVE, or INFECTED to INFECTED
     simply choose the first arrived
*/
void Graph::moveAgent(int agentId) {
  // Choose random direction
  srand(time(NULL));
  int direction = rand()%8;
  
  // For ease of access
  int currentRow = globalAgents[agentId].location[curTurn][0];
  int currentCol = globalAgents[agentId].location[curTurn][1];

  // Will not move if not logically existent in the current space
  if(currentRow == -1 || currentCol == -1) { return; }

  // Determine the new location based off random direction
  int nextRow = currentRow;
  int nextCol = currentCol;
  switch(direction) {
    // NW
    case 0:
      nextRow = Torus(currentRow-1); nextCol = Torus(currentCol-1);
      break;
    // N
    case 1:
      nextRow = Torus(currentRow-1);
      break;
    // NE
    case 2:
      nextRow = Torus(currentRow-1); nextCol = Torus(currentCol+1);
      break;
    // W
    case 3:
      nextCol = Torus(currentCol-1);
      break;
    // E
    case 4:
      nextCol = Torus(currentCol+1);
      break;
    // SW
    case 5:
      nextRow = Torus(currentRow+1); nextCol = Torus(currentCol-1);
      break;
    // S
    case 6:
      nextRow = Torus(currentRow+1);
      break;
    // SE
    case 7:
      nextRow = Torus(currentRow+1); nextCol = Torus(currentCol+1);
      break;
  }
  // Type stored for brevity
  int type = globalAgents[agentId].infectedFlag;//) ? 1 : 0;

  // Ensures that when agents move no collision
  if(T[nextRow][nextCol][nextTurn].agentIds[ type ] != -1) {
    nextRow = currentRow;
    nextCol = currentCol;
 std::cout << "Collision\n";
  }

  // Update the Places for the next id
  T[currentRow][currentCol][curTurn].agentIds[ type ] = -1;
  T[nextRow][nextCol][nextTurn].agentIds[ type ] = agentId;
  T[nextRow][nextCol][nextTurn].agentSize++;
  T[currentRow][currentCol][curTurn].agentSize--;

  // Finalized the agent's move at the end for clarity
  globalAgents[agentId].location[nextTurn][0] = nextRow;
  globalAgents[agentId].location[nextTurn][1] = nextCol;

 std::cout << "MOVING: " << agentId << "\n to: " 
  << nextRow << " " << nextCol << " from: "
  << currentRow << " " << currentCol << " type: "
  << type << "\n";
}

// Handles: EntryP Agent creaton w/ collision in next space
void Graph::makeAgent(int row, int col, bool active) {
  // Checking for EntryP collision with not-INFECTED
  if(T[row][col][nextTurn].agentIds[active] != -1
    && T[row][col][curTurn].agentIds[active] != -1) {return;}

  // Initialize the Id
  int id = ( !freeIds.empty() ) ? freeIds.back() : globalAgents.size();

  // Re-use/Create Agent
  if( !freeIds.empty() ) {
    // Remove obtained id from freeIds vector
    freeIds.pop_back();
    
    // Update from default
    globalAgents[id].deadFlag = false;
    globalAgents[id].activeFlag = active;
  }
  else
    { globalAgents.push_back( Agent(id,row,col,nextTurn,active) ); }

  // Update Agent's Location
  globalAgents[id].location[nextTurn][0] = row;
  globalAgents[id].location[nextTurn][1] = col;

  // Add agent to Place's agents[0], recall INFECTED live on agents[1]
  T[row][col][nextTurn].agentSize = T[row][col][curTurn].agentSize + 1;
  T[row][col][nextTurn].agentIds[active] = id;
  std::cout << "MAKING: " << id << "\n to: " << row << " " << col << "\n";
}

// Assumes: id is valid (id < globalAgents.size())
void Graph::killAgent(int id) {
  // Obtain current position of indicated agent
  int row = globalAgents[id].location[curTurn][0];
  int col = globalAgents[id].location[curTurn][1];
int type = globalAgents[id].infectedFlag;
  // Error-handling
  if(row == -1 || col == -1) { return; }

  std::cout << "KILLING: " << id << " type: " << type
    <<"\n to: " << row << " " << col << "\n";

  // Nullify at Place's next state
  if(globalAgents[id].infectedFlag)
    { T[row][col][ nextTurn ].agentIds[1] = -1; }
  else
    { T[row][col][ nextTurn ].agentIds[0] = -1; }
  T[row][col][ nextTurn ].agentSize = T[row][col][curTurn].agentSize - 1;

  // Free Id (i.e. put id into freeIds vector)
  freeIds.push_back(id);

  // RESET AGENT PARAMETERS
  globalAgents[id].resetAgent();
  globalAgents[id].deadFlag = true;
}
