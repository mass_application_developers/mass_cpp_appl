#ifndef AGENT_H
#define AGENT_H

#include <iostream>
#include <vector>

class Agent {
  private:
    // Used to Count Msgs by their Degrees
    std::vector<int> countPerDegree;
  public:
    int sendMsgTo(int agent, int degree);
    void recieveMsgFrom(int agent, int degree, int msg);
};
#endif
