#ifndef GRAPH_H
#define GRAPH_H
#include "Agent.h"
#include <iostream>
#include <vector>
#include <queue>
#include <cstdlib>    // srand/rand
#include <ctime>      // time

// Stored in Adjacency Matrix
class Graph {
  private:
    int** adjGraph;
    Agent* agents;
    int graphSize;
    void buildGraph();
    void degree_BFS();
  public:
    Graph(int size);

    void displayDegrees() const;

    void setVal(int src, int dest,const int val);
    int getVal(int src, int dest) const;
    bool isValid(int src, int dest) const;

    // Runs a single round of communication
    void runAgents();
};

#endif
