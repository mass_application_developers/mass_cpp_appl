#include "Firm.h"

//-------------------------Firm Implementation-------------------------------//
// Constructor/Destructor
Firm::Firm() : Agent() {
  clusterId = 0;
  recvFlag = false;
  srand(time(NULL));
  totalCapital = rand() % (int)1e6 + 1e4;
}

Firm::Firm(std::vector<int> raw) {
  clusterId = raw[0];
  agentId = raw[1];
  totalCapital = raw[2];
}

std::string Firm::initMsg(const int phase) {
  std::stringstream ss;
  switch (phase) {
    // Loan / Labor Define
    case 0:
      ss << "0-0-0_C1_C0A" << agentId << "_5000";
      recvFlag = true;
      break;
    default:
      std::cout << "Firm does not initiate this transaction" << std::endl;
  }
  return ss.str();
}

std::string Firm::runAgent(const std::vector<std::string>& msg) {
  // String Stream to hold values for a new message
  std::stringstream ss;

  // Process Initialized message
  if (msg[0] == "0-1-1")   {
    ss << "0-2-1_" << msg[2] << "_" << msg[1] << "_" << msg[3];

    // Deny Claim
    if (recvFlag == false) {
      ss << "-N";
      return ss.str();
    }
    ss << "-Y";

    // Update Loan
    std::size_t bankIdOffset = msg[2].find("A");
    int bankId = atoi( (msg[2].substr(bankIdOffset+1)).c_str() );
    ActiveLoans.resize(bankId+1);
   std::cout << "Reserving BankId: " << bankId << "\n";

    // Processing Loan
    int loan = atoi( msg[3].c_str() );
    ActiveLoans[bankId] = loan;
    totalCapital += loan;
    recvFlag = false;

    // Transaction Completion Output
    std::cout << agentId << " Accepted Loan: " << loan << "\n";
    std::cout << agentId << " Total Capital: " << totalCapital << "\n";
  }
  return ss.str();
}

std::vector<int> Firm::stripCopy() const {
  std::vector<int> retVal;
  retVal.push_back(clusterId);
  retVal.push_back(agentId);
  retVal.push_back(totalCapital);
  return retVal;
}
