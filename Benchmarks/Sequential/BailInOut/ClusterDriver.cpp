#include <iostream>
#include <vector>
#include "Cluster.h"

int main(int argc, char* argv[]) {
  // Initialization
  Cluster cluster1(0,10); // firms
  Cluster cluster2(1,4); // banks
  std::vector<Cluster> clusters = {cluster1, cluster2}; // Permissible in c++11

  // Running sSimulation
  for (int iter = 0; iter < 1; iter++) {
    // Cycle through Phases
    for (int i = 0; i < 1; i++) {
      // Initiate Phase
      clusters[0].initMsg(i); // Hardcoded
      clusters[1].initMsg(i);

      // Phase
      while (!(clusters[0].outBuffer).empty() || !(clusters[1].outBuffer).empty()) {
        // Send Messages
        std::cout << "\n Begin Sending \n";
        for (int j = 0; j < clusters.size(); j++) {
          while (!(clusters[j].outBuffer).empty()) {
            std::string msg0 = clusters[j].outBuffer.back();
            clusters[j].outBuffer.pop_back();
            clusters[(j+1)%2].inBuffer.push_back(msg0);
          }
        }
        std::cout << "\n Finished Sending \n";

        // Process Messages
        clusters[0].runCluster();
        clusters[1].runCluster();
      }
    }
  }
  std::cout << "End Simultion \n";
};
