
#include <ctime> // clock_t
#include <iostream>
#include <math.h>     // floor
#include <sstream>     // ostringstream
#include <vector>
#include <stdlib.h>   //malloc, rand

#include "Timer.h"

static const int maxVisible = 3;
static const int maxMetabolism = 4;
static const int maxInitAgentSugar = 10;
static const bool show = false; //print progress from gpu

using namespace std;
int initSugarAmount(int idx, int size, int mtPeakX, int mtPeakY, int maxMtSug);

// Sets initial amount of sugar for the cell
void setSugar(int *curSugar, int *maxSugar, int *nextAgentIdx, int *destinationIdx,
        double *pollution, double *avePollution, int size, int maxMtSug)
{
    for (int idx =0; idx < size*size; idx ++) {
        pollution[idx] = 0.0;    // the current pollution
        avePollution[idx] = 0.0; // averaging four neighbors' pollution
        destinationIdx[idx] = -1; // the next place to migrate to
        nextAgentIdx[idx] = -1;      // the next agent to come here
        int mtCoord[2];
        mtCoord[0] = size/3;
        mtCoord[1] = size - size/3 - 1;
        
        int mt1 = initSugarAmount(idx, size, mtCoord[0], mtCoord[1], maxMtSug);
        int mt2 = initSugarAmount(idx, size, mtCoord[1], mtCoord[0], maxMtSug);
        
        curSugar[idx] = mt1 > mt2 ? mt1 : mt2;
        maxSugar[idx] = mt1 > mt2 ? mt1 : mt2;
    }
}

int initSugarAmount(int idx, int size, int mtPeakX, int mtPeakY, int maxMtSug) {
    int x_coord = idx % size;
    int y_coord = idx / size;
    
    double distance = sqrt((float)(( mtPeakX - x_coord ) * ( mtPeakX - x_coord ) + (mtPeakY - y_coord) * (mtPeakY - y_coord)));

    // radius is assumed to be simSize/2.
    int r = size/2;
    if ( distance < r )
    {
        // '+ 0.5' for rounding a value.
        return ( int )( maxMtSug + 0.5 - maxMtSug / ( double )r * distance );
    }
    else
        return 0;
}

void initAgents(int *nAgentsInPlace, int nAgents, int size, int *agentSugar, int *agentMetabolism)
{
    srand (239); //seed
    int ratio = 100 * nAgents / (size*size);  //percentage of non-empty cells

    for (int idx =0; idx < size*size; idx ++) {
        unsigned int randNumber = rand() % 100; //random number from 0 to 100
        if (randNumber <= ratio) {
            nAgentsInPlace[idx] = 1;
            agentSugar[idx] = rand() % maxInitAgentSugar +1;
            agentMetabolism [idx] = rand() % maxMetabolism +1;
        }
        else {
            nAgentsInPlace[idx] = 0;
            agentSugar[idx] = -1;
            agentMetabolism [idx] = -1;
        }
    }
}

void incSugarAndPollution(int *curSugar, int *maxSugar, double *pollution, int size) {
    for (int idx =0; idx < size*size; idx ++) {

        if ( curSugar[idx] < maxSugar[idx] )
        {
            curSugar[idx]++;
        }
        pollution[idx] += 1.0;
    }
}

// Calculates average pollution between 4 neighbors
void avePollutions(double *pollution, double *avePollution, int size) {
    for (int idx =0; idx < size*size; idx ++) {
        double top, bottom, right, left;
        idx + size < size*size ? top = pollution[idx + size] : 0.0;
        idx - size >= 0 ? bottom = pollution[idx-size] : 0.0;
        (idx +1) % size != 0 ? right = pollution[idx + 1] : 0.0;
        (idx - 1) % size != size-2 ? left = pollution[idx -1] : 0.0;
       
        avePollution[idx] = ( top + bottom + left + right ) / 4.0;
    }
}

void updatePollutionWithAverage(double *pollution, double *avePollution, int size) {
    for (int idx =0; idx < size*size; idx ++) {
        pollution[idx] = avePollution[idx];
        avePollution[idx] = 0.0;
    }
}

void findPlaceForMigration(int *nAgentsInPlace, int *curSugar, double *pollution, 
    int maxVisible, int size, int *destinationIdx)
{
    for (int idx =0; idx < size*size; idx ++) {
        if (nAgentsInPlace[idx] > 0) {
            // consider visibility along x axis:
            for (int i = 1; i<=maxVisible; i++) {
                int destIdx = idx+i;
                if ((destIdx < size*size) && nAgentsInPlace[destIdx] == 0 &&
                    (curSugar[destIdx] / ( 1.0 + pollution[destIdx]) > 0.0)) {

                    destinationIdx[idx] = destIdx;
                    return;
                }
            }

            // consider visibility along y axis:
            for (int i = 1; i<=maxVisible; i++) {
                int destIdx = idx+i*size;
                if ((destIdx < size*size) && nAgentsInPlace[destIdx] == 0 &&
                    (curSugar[destIdx] / ( 1.0 + pollution[destIdx]) > 0.0)) {

                    destinationIdx[idx] = destIdx;
                    return;
                }
            }
        }
    }
}

void selectAgentToAccept(int *destinationIdx, int *nextAgentIdx, int size) {
    for (int idx =0; idx < size*size; idx ++) {
        int destIdx = destinationIdx[idx];

        if (nextAgentIdx[destIdx] == -1) { //if place empty - move there
            nextAgentIdx[destIdx] = idx;
        }
    }
}

void migrate(int *nAgentsInPlace, int *destinationIdx, int *nextAgentIdx, int *agentSugar, int *agentMetabolism, int size) {
    for (int idx =0; idx < size*size; idx ++) {
        if (nextAgentIdx[destinationIdx[idx]] == idx) {
            nAgentsInPlace[destinationIdx[idx]] = 1;
            nAgentsInPlace[idx] = 0;
            
            // move whatever data agents have:
            agentSugar[destinationIdx[idx]] = agentSugar[idx];
            agentSugar[idx] = -1;

            agentMetabolism[destinationIdx[idx]] = agentMetabolism[idx];
            agentMetabolism[idx] = -1;
        }
    }
}

void resetMigrationData (int *destinationIdx, int *nextAgentIdx, int size) {
    for (int idx =0; idx < size*size; idx ++) {
        destinationIdx[idx] = -1; // the next place to migrate to
        nextAgentIdx[idx] = -1;      // the next agent to come here
    }
}

void metabolize(int *nAgentsInPlace, int *agentSugar, int *agentMetabolism, int *curSugar, double *pollution, int size) {
    for (int idx =0; idx < size*size; idx ++) {
        if (nAgentsInPlace[idx] > 0) {
            agentSugar[idx] += curSugar[idx];
            agentSugar[idx] -= agentMetabolism[idx];

            curSugar[idx] = 0;
            pollution[idx] += agentMetabolism[idx];

            if( agentSugar[idx] < 0 )
            {
                // Kill agent:
                nAgentsInPlace[idx] = 0;
                agentSugar[idx] = -1;
                agentMetabolism[idx] = -1;
            }
        }
    }
}

void displayResults(int *results_matrix, int size) {
    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            cout << results_matrix[i*size+j] << "\t";
        }
        cout << endl;
    }
    cout << endl;
}

void runHostSim(int size, int max_time, int nAgents) {

    int maxMtSugar = 4; //max level of sugar in mountain peak

    // Create a space:
    int *curSugar, *maxSugar, *nAgentsInPlace, *nextAgentIdx, *destinationIdx, *agentSugar, *agentMetabolism;
    double *pollution, *avePollution;

    int nBytesInt = sizeof(int) * size * size;
    int nBytesDbl = sizeof(double) * size * size;

    // Allocate memory:
    curSugar = (int*)malloc(nBytesInt);
    maxSugar = (int*)malloc(nBytesInt);
    nAgentsInPlace = (int*)malloc(nBytesInt);
    nextAgentIdx = (int*)malloc(nBytesInt);
    destinationIdx = (int*)malloc(nBytesInt);
    agentSugar = (int*)malloc(nBytesInt);
    agentMetabolism = (int*)malloc(nBytesInt);

    pollution = (double*)malloc(nBytesDbl);
    avePollution = (double*)malloc(nBytesDbl);
    
    setSugar(curSugar, maxSugar, nextAgentIdx, destinationIdx, pollution, avePollution, size, maxMtSugar);
    initAgents(nAgentsInPlace, nAgents, size, agentSugar, agentMetabolism);

    // Start a timer
    Timer time;
    time.start();

    if (show) {
        cout << "INITIAL SUGAR AND ANT DISTRIBUTION" << endl;
        cout << "SUGAR LEVELS:" << endl;
        displayResults(curSugar, size);
        cout << "ANT DISTRIBUTION:" << endl;
        displayResults(nAgentsInPlace, size);
        cout << "AGENT STORED SUGAR:" << endl;
        displayResults(agentSugar, size);
        cout << "AGENT METABOLISM:" << endl;
        displayResults(agentMetabolism, size);
    }

    // Simulate ant behavior:
    for (int t=0; t < max_time; t++) {
        incSugarAndPollution(curSugar, maxSugar, pollution, size);

        // No need to syncronize here, because the next kernel execution is put in a queue after the previous one
        avePollutions(pollution, avePollution, size);
        updatePollutionWithAverage(pollution, avePollution, size);

        findPlaceForMigration(nAgentsInPlace, curSugar, pollution, maxVisible, size, destinationIdx);
        selectAgentToAccept(destinationIdx, nextAgentIdx, size);
        migrate(nAgentsInPlace, destinationIdx, nextAgentIdx, agentSugar, agentMetabolism, size);
        resetMigrationData(destinationIdx, nextAgentIdx, size);
        metabolize(nAgentsInPlace, agentSugar, agentMetabolism, curSugar, pollution, size);

        if (show) {
            cout << "Time: t=" << t << endl;
            cout << "End of step sugar and ant distribution: " << endl;
            cout << "SUGAR LEVELS:" << endl;
            displayResults(curSugar, size);
            cout << "ANT DISTRIBUTION:" << endl;
            displayResults(nAgentsInPlace, size);
            cout << "AGENT STORED SUGAR:" << endl;
            displayResults(agentSugar, size);
            cout << "AGENT METABOLISM:" << endl;
            displayResults(agentMetabolism, size);
        }
    }
    cout << "Elapsed time on CPU = " << time.lap() << endl;
}

int main(int argc, char const *argv[])
{
    cout << "Starting the CPU simulation" << endl;
    int size[] = { 5, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };
    int max_time = 100;
    for (int i=0; i< (sizeof(size)/sizeof(size[0])) ; i++) {
        int nAgents = size[i]*size[i] / 5;
        runHostSim(size[i], max_time, nAgents);
    }

    cout << "End of the CPU simulation" << endl;
    return 0;
}
