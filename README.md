![logo.png](https://bitbucket.org/repo/AEx9kp/images/1314816353-logo.png)

# Agent-Based Modeling System Documentation

- [MASS C++](https://bitbucket.org/mass_library_developers/mass_cpp_core/src/master/)
- [RepastHPC](https://repast.github.io/docs.html)
    - [Github](https://github.com/repast)
- [FLAME](http://flame.ac.uk/docs/)
    - [Github](https://github.com/FLAME-HPC/)

# Benchmark Specifications

- [Bail-in, Bail-out](https://docs.google.com/document/d/1SuFFwxvz_VcbiUJ8LaEryV9FAIxMJOZt/edit?usp=sharing&ouid=116353300264645071321&rtpof=true&sd=true)
- [MATSim](https://docs.google.com/document/d/1Ili-bK1ZECfDVfc_bTdGLDvD9uFvUOXIdTIAHhHmZNU/edit?usp=sharing)
- [Brain Grid, Self-Organizing Neural Network](https://docs.google.com/document/d/0B-DdRv6zRAzAS1k1SkJ0aEJ1SDFyTEJWMGlnVDFWVlA3eHhz/edit?usp=sharing&ouid=116353300264645071321&resourcekey=0-FHn5PVBz35pRIM6u3NFbew&rtpof=true&sd=true)
- [Social Networks Simulation](https://docs.google.com/document/d/1cgQVoXzEVYP8-j14XGmj-VfmzwV7PYAf6BIdp4sdVRc/edit?usp=sharing)
- [Tuberculosis Simulation](https://docs.google.com/document/d/1YHIMwWgh0NJvnT6wz-AUywbDLd15RsCE7dRXTaBQY6k/edit?usp=sharing)
- [Virtual Design Team Simulation](https://docs.google.com/document/d/1Ui5LuTLX2wAuGuDM0AMGgmanBnyLpKFRwiq7DxEklGU/edit?usp=sharing)

# Benchmark Papers

- [Sarah Panther, Autumn 2019](http://depts.washington.edu/dslab/MASS/reports/SarahPanther_au19.pdf)
- [Sarah Panther, Winter 2020](http://depts.washington.edu/dslab/MASS/reports/SarahPanther_wi20.pdf)
- [Sarah Panther, Spring 2020](http://depts.washington.edu/dslab/MASS/reports/SarahPanther_sp20.pdf)
- [Nathan Wong, Winter 2021](http://depts.washington.edu/dslab/MASS/reports/NathanWong_wi21.pdf)
- [Nathan Wong, Spring 2021](http://depts.washington.edu/dslab/MASS/reports/NathanWong_sp21.pdf)
- [Ian Dudder, Summer 2021](http://depts.washington.edu/dslab/MASS/reports/IanDudder_su21.pdf)