# Wiki of containerization support for MASS C++ applications
---
## General Requirements
docker version >= 17.09

docker-compose version >= 3.4

---
## Best practice to add containerization support for a new MASS C++ application
To add containerization support, 7 files are necessary

1. Dockerfile
2. docker-compose.yml
3. resources/key
4. resources/key.pub
5. resources/machinefile.txt
6. compile.sh
7. run.sh
8. .dockerignore (optional)

---
### 1. Dockerfile
This file collects application's source code, tries to compile the application, and build a docker image.
This file usually place at the root directory of the application intends to add containerization support.
The content of thie file varies according to need. Usually only 2 areas requires speical attention.


#### 1.1
The first line usually uses **FROM** statement, for example

    FROM dslabcss/mass_cpp_core:1.0.0

This stands for the docker image build upon docker image "dslabcss/mass_cpp_core:1.0.0", you may need to change 1.0.0 to another verison of mass cpp docker image that is reliable for testing.
(To look up available versions of mass cpp docker image, checkout https://hub.docker.com/repository/docker/dslabcss/mass_cpp_core)

#### 1.2
The second area is where **COPY** statements are used, for example

    COPY *.h ./

This statement will copy all files end with ".h" into the docker image's working directory.
You may need to append to the list of **COPY** statements if there are files required but not covered; Or remove some **COPY** statements if excessive files are added.

---
### 2. docker-compose.yml
This file defines a number of parameters being used while building an image and turning an image into container(s).

#### 2.1
The image parameter, for exmaple

    image: dslabcss/tuberculosis

This line tells docker-compose to build the image into name "dslabcss/tuberculosis".
"dslabcss" inside "dslabcss/tuberculosis" stands for an shared Dockerhub account, so just leave it as is.
You may change "tuberculosis" to the name of your application.

#### 2.2
The build parameter, for exmaple

    build:
      context: .
      dockerfile: ./Dockerfile

These lines tell docker-compose to use the Dockerfile from "./Dockerfile"
Usually keep it as it unless you maintain your Dockerfile in another location.

#### 2.3
The deploy parameter, for exmaple

    deploy:
      replicas: 2

This line tells docker-compose to spawn 2 containers while turning images into containers
You may change the replicas number if you want to test execution with another number of nodes.

#### 2.4
The volumes parameter, for exmaple

    volumes:
      - ./resources:/app/resources
      - ./output:/app/output

These lines tell docker-compose directories that will mount to containers.
These volumes are shared among containers and host machine just like NFS.

---
### 3. resources/key
This file is a private key stored under directory resources/, and used to authorize access among containers.
Just copy it from MASS_Tuberculosis.

---
### 4. resources/key.pub
This file is a public key stored under directory resources/, and used to authorize access among containers.
Just copy it from MASS_Tuberculosis.

---
### 5. resources/machinefile.txt
This file is a file required by MASS C++ library to indicate name of nodes involved in computation.
You can change this file anytime, even after containers are launched since it's inside a shared volume.
Leave it as blank if only local node involves; otherwise add name of nodes according to MASS C++ library
To find out names of nodes, you may run **docker network inspect <application_network_name>** after containers are launched

---
### 6. compile.sh
This file is a bash script that contains commands to build the MASS application

---
### 7. run.sh
This file is a bash script that execute the MASS application
(It's being named "debugRun" in MASS_Tuberculosis)
**cp resources/machinefile.txt ./machinefile.txt** is a line of required code to have latest machinefile.txt before each execution

---
### 8. .dockerignore (optional)
This file is an additional file to help **COPY** statement inside Dockerfile
All the files listed inside this file will be ignored while using **COPY** inside Dockerfile

